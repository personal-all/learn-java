package com.moonsky.base.aspectj.demo01;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

/**
 * @author benshaoye
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:aspectj.demo01.application.xml")
public class AspectDemo01EmployeeServiceTestTest {

    @Autowired
    private EmployeeService employeeService;

    @Test
    public void testName() {
        employeeService.save();
        System.out.println();
        employeeService.find();
        System.out.println();
        employeeService.update();
        System.out.println();
        employeeService.delete();
        System.out.println();
        employeeService.hasReturningValue();
        System.out.println();
        employeeService.withAround();
        System.out.println();
        try {
            employeeService.withThrowing();
        } catch (Throwable t) {
            System.out.println(" 0000================>> " + t.getMessage());
        }
    }
}