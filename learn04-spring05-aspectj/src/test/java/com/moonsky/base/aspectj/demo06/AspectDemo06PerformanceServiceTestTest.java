package com.moonsky.base.aspectj.demo06;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

/**
 * @author benshaoye
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:aspectj.demo06.application.xml")
public class AspectDemo06PerformanceServiceTestTest {

    @Autowired
    private PerformanceService service01;
    private PerformanceService service02 = new PerformanceService();
    private PerformanceService service03 = new ExtendedPerformanceService();

    static class ExtendedPerformanceService extends PerformanceService {
        @Override
        public void running() {
            super.running();
        }
    }

    static void run(Runnable runner, int count) {
        long no1 = System.currentTimeMillis();
        for (int i = 0; i < count; i++) {
            runner.run();
        }
        long no2 = System.currentTimeMillis();
        System.out.println(no2 - no1);

    }

    static void run(Runnable runner){
        runner.run();
    }

    Runnable runner = () -> {
        final int count = 1000;
        run(() -> service01.running(), count);
        run(() -> service02.running(), count);
        run(() -> service03.running(), count);
    };

    @Test
    public void testRunning() {
        run(runner);
        run(runner);
        run(runner);
        run(runner);
        run(runner);
    }

    @Test
    public void testMaxValue() {
    }
}