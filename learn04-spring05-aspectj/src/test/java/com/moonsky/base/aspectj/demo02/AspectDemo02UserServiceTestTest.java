package com.moonsky.base.aspectj.demo02;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

/**
 * @author benshaoye
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:aspectj.demo02.application.xml")
public class AspectDemo02UserServiceTestTest {

    @Autowired
    private UserService userService;

    @Test
    public void testAllMethodWithoutThrowing() {
        System.out.println(userService.getClass());
        System.out.println();
        userService.allMethodWithoutThrowing();
        System.out.println();
        try {
            userService.allMethodWithThrowing();
        } catch (Throwable t) {
            System.out.println(t.getMessage());
        }
    }
}