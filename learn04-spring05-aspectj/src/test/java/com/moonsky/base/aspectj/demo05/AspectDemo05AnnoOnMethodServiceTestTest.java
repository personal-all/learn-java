package com.moonsky.base.aspectj.demo05;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

/**
 * @author benshaoye
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:aspectj.demo05.application.xml")
public class AspectDemo05AnnoOnMethodServiceTestTest {

    @Resource(name = "annoOnMethodService")
    private AnnoOnMethodService annoOnMethodService;

    private void running(Runnable runner) {
        runner.run();
        System.out.println();
    }

    @Test
    public void testName() {
        running(() -> annoOnMethodService.annotation01());
        running(() -> annoOnMethodService.annotation02());
    }
}