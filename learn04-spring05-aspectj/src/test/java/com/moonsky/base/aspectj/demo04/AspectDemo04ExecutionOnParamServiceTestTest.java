package com.moonsky.base.aspectj.demo04;

import com.moonsky.base.aspectj.CurrentType01;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Collections;
import java.util.HashMap;

import static org.junit.Assert.*;

/**
 * @author benshaoye
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:aspectj.demo04.application.xml")
public class AspectDemo04ExecutionOnParamServiceTestTest {

    @Autowired
    private ExecutionOnParamService executionOnParamService;

    private void running(Runnable runner) {
        runner.run();
        System.out.println();
    }

    @Test
    public void testOnParam() {
        running(() -> executionOnParamService.doAfter01(CurrentType01.VALUE));
        running(() -> executionOnParamService.doAfter02(CurrentType01.VALUE, "name"));
        running(() -> executionOnParamService.doAfter03(20, "name"));
        running(() -> executionOnParamService.doAfter04(20, "name", "name"));
        running(() -> executionOnParamService.doAfter05(Collections.emptySet(), "name", "name"));
        running(() -> executionOnParamService.doAfter06(Collections.emptySet(), "name", "name"));
        running(() -> executionOnParamService.doAfter07(Collections.emptyMap(), "name", "name"));
        running(() -> executionOnParamService.doAfter08(new HashMap<>(), "name", "name"));
    }
}