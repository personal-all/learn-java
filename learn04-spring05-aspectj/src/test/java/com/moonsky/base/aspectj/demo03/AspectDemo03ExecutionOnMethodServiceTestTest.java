package com.moonsky.base.aspectj.demo03;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author benshaoye
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:aspectj.demo03.application.xml")
public class AspectDemo03ExecutionOnMethodServiceTestTest {

    @Autowired
    private ExecutionOnMethodService onMethodService;
    @Autowired
    private ExecutionOnReturningService onReturningService;

    private void running(Runnable runner) {
        runner.run();
        System.out.println();
    }

    @Test
    public void testBefore() {
        running(() -> onReturningService.before01And$Or());
        running(() -> onReturningService.before02And$Or());
        running(() -> onReturningService.before03And$Or());
        running(() -> onReturningService.before04And$Or());
        running(() -> onReturningService.before05And$Or());
        running(() -> onReturningService.before06And$Or());
        running(() -> onReturningService.before07And$Or());
    }

    @Test
    public void testAfterReturning() {
        running(() -> onMethodService.afterReturning01And$Or());
        running(() -> onMethodService.afterReturning02And$Or());
        running(() -> onMethodService.afterReturning03And$Or());
        running(() -> onMethodService.afterReturning04And$Or());
        running(() -> onMethodService.afterReturning05And$Or());
        running(() -> onMethodService.afterReturning06And$Or());
        running(() -> onMethodService.afterReturning07And$Or());
    }
}