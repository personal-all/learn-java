package com.moonsky.base.aspectj.demo01;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

/**
 * @author benshaoye
 */
@Aspect
public class NowBeforeAllAdvisor {

    @Before("execution(public * *(..))")
    public void before() {
        System.out.println(" 前置通知所有方法 =============================");
    }
}
