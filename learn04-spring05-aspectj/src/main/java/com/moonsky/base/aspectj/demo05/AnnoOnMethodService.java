package com.moonsky.base.aspectj.demo05;

import com.moonsky.base.aspectj.Base01;
import com.moonsky.base.aspectj.Base02;

/**
 * @author benshaoye
 */
public class AnnoOnMethodService {

    @Base01
    public void annotation01(){
        System.out.println(" 业务方法 annotation01 ........... Base01");
    }

    @Base02
    public void annotation02(){
        System.out.println(" 业务方法 annotation02 ........... Base02");
    }
}
