package com.moonsky.base.aspectj.demo02;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Repository;

/**
 * @author benshaoye
 */
@Aspect
@Repository
public class UserAspect {

    /*
     * --------------------------------------------------------------------------------------------------
     * 基本
     * --------------------------------------------------------------------------------------------------
     */

    /**
     * 所有类所有方法
     */
    @Pointcut("execution(* *(..))")
    public void markAll() { }

    /**
     * 所有类所有 public 方法
     * protected 、private 方法类似
     */
    @Pointcut("execution(public * *(..))")
    public void markAllPublic() { }

    /**
     * 所有返回值类型为 String 的方法
     */
    @Pointcut("execution(public String *(..))")
    public void markReturnningType() { }

    /**
     * 所有返回值类型为 String 或 int 或 Long 的方法
     */
    @Pointcut("execution(public (String || int || Long) *(..))")
    public void markMultiReturnningType() { }

    /**
     * 所有类所有返回值类型不是 String 的方法 public 方法
     * protected 、private 方法类似
     * <p>
     * 注意返回值类型位置的符号“!”
     */
    @Pointcut("execution(public !String *(..))")
    public void markNotReturningType() {}

    /**
     * 所有类所有返回值类型不是 String 或 int 的方法 public 方法
     */
    @Pointcut("execution(public !(String || int) *(..))")
    public void markNotMultiReturningType() {}

    /**
     * 所有不是类{@link UserService}的所有 public 方法
     * protected 、private 方法类似
     * <p>
     * 注意类位置的符号“!”
     */
    @Pointcut("execution(public * !com.moonsky.base.aspectj.demo02.UserService(..))")
    public void markNotAssignClass() {}

    /**
     * 任何同时实现接口{@link Cloneable}和{@link java.io.Serializable}类的所有方法
     */
    @Pointcut("execution(public * !(" +
        "Cloneable+ && java.io.Serializable+" +
        ").*(..))")
    public void markNotMultiAssignClass() {}

    /*
     * --------------------------------------------------------------------------------------------------
     * 方法包含注解
     * --------------------------------------------------------------------------------------------------
     */

    /**
     * 包含指定注解（java.lang.Deprecated）的 public 方法
     * protected 、private 方法类似
     */
    @Pointcut("execution(@java.lang.Deprecated public * *(..))")
    public void markHasAnnotation() { }

    /**
     * 同时包含多个指定注解（
     * {@link org.springframework.stereotype.Repository}
     * {@link Deprecated}
     * ）的 public 方法
     * protected 、private 方法类似
     * <p>
     * 同时，是 and 的意思
     *
     * @see #markMultiAnnotationWith()
     * @see #markMultiAnnotationWithOr()
     */
    @Pointcut("execution(" +
        // 写多行字符串是为了方便阅读，建议在行首都添加一个空格，
        // 这种习惯能避免很多错误，在 java 代码中写 sql 语句也应养成这样的习惯
        " @java.lang.Deprecated" +
        " @org.springframework.stereotype.Repository" +
        " public * *(..))")
    public void markMultiAnnotationWithAnd() { }

    /**
     * 包含多个注解其中一个注解的 public 方法，多个注解之间是“或”（or）的关系
     *
     * @see #markMultiAnnotationWithAnd()
     * @see #markMultiAnnotationWith()
     */
    @Pointcut("execution(" +
        " @(java.lang.Deprecated" +
        " ||org.springframework.stereotype.Repository" +
        " ||org.springframework.stereotype.Service" +
        " ) public * *(..))")
    public void markMultiAnnotationWithOr() { }

    /**
     * 多个注解的“and”和“or”同时使用
     *
     * @see #markMultiAnnotationWithAnd()
     * @see #markMultiAnnotationWithOr()
     */
    @Pointcut("execution(" +
        " @(java.lang.Deprecated" +
        " ||org.springframework.stereotype.Repository)" +
        "" +
        " @org.springframework.stereotype.Service" +
        " public * *(..))")
    public void markMultiAnnotationWith() { }

    /*
     * --------------------------------------------------------------------------------------------------
     * 方法返回值包含注解
     * --------------------------------------------------------------------------------------------------
     */

    /**
     * 任何返回值类型包含指定注解{@link Deprecated}的方法
     *
     * @see #markReturningMultiAnnotationWithAnd()
     * @see #markReturningMultiAnnotationWithOr()
     */
    @Pointcut("execution(public (@java.lang.Deprecated *) *(..))")
    public void markReturningHasAnnotation() {}

    /**
     * 任何返回值类型包含指定注解{@link Deprecated}和{@link Repository}的方法
     * <p>
     * 注解间是“和”的关系——and
     *
     * @see #markReturningHasAnnotation()
     * @see #markReturningMultiAnnotationWithAnd()
     */
    @Pointcut("execution(public (" +
        " @java.lang.Deprecated" +
        " @org.springframework.stereotype.Service" +
        " *) *(..))")
    public void markReturningMultiAnnotationWithAnd() {}

    /**
     * 任何返回值类型包含指定注解{@link Deprecated}或{@link Repository}的方法
     * <p>
     * 注解间是“或”的关系——or
     *
     * @see #markReturningHasAnnotation()
     * @see #markReturningMultiAnnotationWithAnd()
     */
    @Pointcut("execution(public (" +
        " @(java.lang.Deprecated" +
        " ||org.springframework.stereotype.Service)" +
        " *) *(..))")
    public void markReturningMultiAnnotationWithOr() {}

    public void before(JoinPoint point) {
        System.out.println(" ====================>>>> before");
    }

    public Object around(ProceedingJoinPoint point) throws Throwable {
        System.out.println(" ====================>>>> before around");
        Object ret = point.proceed();
        System.out.println(" ====================>>>> after around");
        return ret;
    }

    public void afterReturning() {
        System.out.println(" ====================>>>> afterReturning");
    }

    public void afterThrowing(Throwable t) {
        System.out.println(" ====================>>>> afterThrowing");
    }

    public void after() {
        System.out.println(" ====================>>>> after");
    }
}
