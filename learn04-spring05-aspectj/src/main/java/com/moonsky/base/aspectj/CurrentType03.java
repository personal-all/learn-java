package com.moonsky.base.aspectj;

/**
 * @author benshaoye
 */
@Anno02
@Base03
public class CurrentType03 implements CurrentInterface01to03 {

    public final static CurrentType03 VALUE = new CurrentType03();
}
