package com.moonsky.base.aspectj.demo03;

import com.moonsky.base.aspectj.*;

/**
 * @author benshaoye
 */
public class ExecutionOnReturningService {

    /*
     * --------------------------------------------------------------------------------------------
     */

    public CurrentType01 before01And$Or() {
        System.out.println(" 业务方法【before01】..................... Base01 & Base02 & Anno01");
        return CurrentType01.VALUE;
    }


    public CurrentType02 before02And$Or() {
        System.out.println(" 业务方法【before02】..................... Base01 & Base03");
        return CurrentType02.VALUE;
    }

    public CurrentType03 before03And$Or() {
        System.out.println(" 业务方法【before03】..................... Base03 & Anno02");
        return CurrentType03.VALUE;
    }

    public CurrentType04 before04And$Or() {
        System.out.println(" 业务方法【before04】..................... Base02");
        return CurrentType04.VALUE;
    }

    public CurrentType05 before05And$Or() {
        System.out.println(" 业务方法【before05】..................... 没有");
        return CurrentType05.VALUE;
    }

    public CurrentType06 before06And$Or() {
        System.out.println(" 业务方法【before06】..................... Base01");
        return CurrentType06.VALUE;
    }

    public CurrentType07 before07And$Or() {
        System.out.println(" 业务方法【before07】..................... Anno01 & Anno02");
        return CurrentType07.VALUE;
    }
}
