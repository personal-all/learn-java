package com.moonsky.base.aspectj.demo04;

import com.moonsky.base.aspectj.Base01;
import com.moonsky.base.aspectj.CurrentType01;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author benshaoye
 */
public class ExecutionOnParamService {

    public void doAfter01(CurrentType01 name) {
        System.out.println(" 业务方法（doAfter01）(CurrentType01)");
    }

    public void doAfter02(CurrentType01 name, String current) {
        System.out.println(" 业务方法（doAfter02）(CurrentType01, String)");
    }

    public void doAfter03(int age, String current) {
        System.out.println(" 业务方法（doAfter03）(int, String)");
    }

    public void doAfter04(int age, String current, String name) {
        System.out.println(" 业务方法（doAfter04）(int, String, String)");
    }

    public void doAfter05(Set<? extends HashMap> set, String current, String name) {
        System.out.println(" 业务方法（doAfter05）(Set<? extends HashMap>, String, String)");
    }

    public void doAfter06(Set<HashMap> set, String current, String name) {
        System.out.println(" 业务方法（doAfter06）(Map<String, String>, String, String)");
    }

    public void doAfter07(Map<String, String> set, String current, String name) {
        System.out.println(" 业务方法（doAfter07）(Map<String, String>, String, String)");
    }

    public void doAfter08(HashMap<String, String> set, String current, String name) {
        System.out.println(" 业务方法（doAfter08）(HashMap<String, String>, String, String)");
    }
}
