package com.moonsky.base.aspectj.demo06;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

/**
 * @author benshaoye
 */
@Aspect
public class PerformanceAspect {

    @Before("execution(public void running())")
    public void before() { }
}
