package com.moonsky.base.aspectj.demo04;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;

/**
 * @author benshaoye
 */
@Aspect
public class ExecutionOnParamAspect {

    @After("execution(public * doAfter*(@com.moonsky.base.aspectj.Base01 *,..))")
    public void afterAllTypeOnBase01() {
        System.out.println(" 后置通知【after】afterAllTypeOnBase01 ------- Base01 on first 01");
    }

    @After("execution(public * doAfter*(@com.moonsky.base.aspectj.Base01 *,..))")
    public void afterAllTypeOnBase02() {
        System.out.println(" 后置通知【after】afterAllTypeOnBase02 ------- Base01 on first 02");
    }

    @After("execution( public * doAfter*(" +
        " @com.moonsky.base.aspectj.Base01 " +
        " com.moonsky.base.aspectj.CurrentInterface01to03+,..))")
    public void after01() {
        System.out.println(" 后置通知【after】after01 ------- Base01 on first and implement CurrentInterface01to03");
    }

    @After("execution( public * doAfter*(" + " @com.moonsky.base.aspectj.Base01 " +
        " com.moonsky.base.aspectj.CurrentType01,..))")
    public void after02() {
        System.out.println(" 后置通知【after】after02 ------- Base01 on first and type is CurrentType01");
    }

    @After("execution( public * doAfter*(int,String))")
    public void after03() {
        System.out.println(" 后置通知【after】after03 ------- (int, String)");
    }

    @After("execution( public * doAfter*(int,String,..))")
    public void after04() {
        System.out.println(" 后置通知【after】after04 ------- (int, String, ..)");
    }

    @After("execution( public * doAfter*(java.util.Set<? extends java.util.Map+>, String, ..))")
    public void after05() {
        System.out.println(" 后置通知【after】after05 ------- (java.util.Set<? extends java.util.Map+>, String, ..)");
    }

    @After("execution( public * doAfter*(java.util.Set<java.util.Map+>, String, ..))")
    public void after06() {
        System.out.println(" 后置通知【after】after06 ------- (java.util.Set<java.util.Map+>, String, ..)");
    }

    @After("execution( public * doAfter*(java.util.Map<CharSequence+, Object+>, String, ..))")
    public void after07() {
        System.out.println(" 后置通知【after】after07 ------- (java.util.Map<CharSequence+, Object+>, String, ..)");
    }

    @After("execution( public * doAfter*(java.util.Map+, String, ..))")
    public void after08() {
        System.out.println(" 后置通知【after】after08 ------- (java.util.Map+, String, ..)");
    }
}
