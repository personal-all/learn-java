package com.moonsky.base.aspectj.demo05;

import com.moonsky.base.aspectj.Base01;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

/**
 * @author benshaoye
 */
@Aspect
public class AnnoOnMethodAspect {

    /**
     * - @annotation 内只能包含一个注解类名
     */
    @Around("@annotation(com.moonsky.base.aspectj.Base01)")
    public Object annotation01(ProceedingJoinPoint point) throws Throwable {
        System.out.println(" Around before annotation01 ........... Base01");
        return point.proceed();
    }
    @Around("@annotation(com.moonsky.base.aspectj.Base02)")
    public Object annotation02(ProceedingJoinPoint point) throws Throwable {
        System.out.println(" Around before annotation02 ........... Base02");
        return point.proceed();
    }
}
