package com.moonsky.base.aspectj;

/**
 * @author benshaoye
 */
@Anno01
@Base01
@Base02
public class CurrentType01 implements CurrentInterface01to03 {

    public final static CurrentType01 VALUE = new CurrentType01();
}
