package com.moonsky.base.aspectj.demo03;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

/**
 * 此类主要用于演示 execution 方式配置切入点示例
 * <p>
 * 当注解在返回值类型上时，要求的是返回值类型声明处注解，而不是使用时
 *
 * @author benshaoye
 */
@Aspect
public class ExecutionWithAnnotatedOnReturningAspect {

    /**
     * AfterReturning 拦截的是一定要有返回值的方法，即返回值类型不能是 void
     * <p>
     * 拦截被 Base01 注解的方法
     */
    @Before(value = "execution(" +
        " public @com.moonsky.base.aspectj.Base01 * before*(..))")
    public void before01() {
        System.out.println(" 【before01】 with: 【Base01】.");
    }

    /**
     * 拦截被 Base01 和 Base02 注解的方法
     */
    @Before(value = "execution(public " +
        " @com.moonsky.base.aspectj.Base01" +
        " @com.moonsky.base.aspectj.Base02" +
        " * before*(..))")
    public void before02() {
        System.out.println(" 【before02】 with: 【Base01 & Base02】.");
    }

    /**
     * 拦截被 Base01 和 Base02 注解的方法
     */
    @Before(value = "execution(public " +
        " @(com.moonsky.base.aspectj.Anno01" +
        " ||com.moonsky.base.aspectj.Anno02)" +
        " * before*(..))")
    public void before03() {
        System.out.println(" 【before03】 with: 【Anno01 | Anno02】.");
    }

    /**
     * 拦截被 Base01 注解，同时至少还被 Base02 和 Base03 其中之一注解的方法
     */
    @Before(value = "execution(public " +
        " @com.moonsky.base.aspectj.Base01" +
        " @(com.moonsky.base.aspectj.Base02" +
        " ||com.moonsky.base.aspectj.Base03)" +
        " * before*(..))")
    public void before04() {
        System.out.println(" 【before04】 with: 【Base01 & (Base02 | Base03)】.");
    }

    @Before("execution(public" +
        " @com.moonsky.base.aspectj.Base01" +
        " com.moonsky.base.aspectj.CurrentInterface01to03+" +
        " before*(..))")
    public void before05(){
        System.out.println(" 【before05】 with: 【Base01】, implement CurrentInterface01to03");
    }
}
