package com.moonsky.base.aspectj.demo01;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;

/**
 * @author benshaoye
 */
@Aspect
public class NowBeforeOnlySaveAdvisor {

    /*

    before 和 around 的执行顺序与配置顺序正相关：
      before 不一定在 around 前执行，
      如果 around 配置在 before 前，around 就会在 before 前执行
      但是不论怎么配置，before 和 around before 都会在 业务方法 前执行
      也一定会在 after* 方法前执行

      同样 after 也不一定在 around 后执行，也与配置顺序相关
      配置多个 after、before 等，会按配置顺序执行

     */

    private void doBefore(JoinPoint joinPoint) {
        System.out.println(joinPoint.getKind());
        System.out.println(joinPoint.getTarget().getClass());
        System.out.println(joinPoint.getThis().getClass());
        System.out.println(joinPoint.getSignature());
        System.out.println(joinPoint.getSourceLocation());
        System.out.println(joinPoint.getStaticPart());
    }

    @Before("execution(public * save*(..))")
    public void before() {
        System.out.println(" 前置通知 save ============================= ");
    }

    @Around("execution(* com.moonsky.base.aspectj.demo01.*.with*(..))")
    public Object around(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        System.out.println(" 环绕前通知 ----------------------");

        // 必须添加这一句才执行目标方法
        Object ret = proceedingJoinPoint.proceed();
        System.out.println(" ---------------------- 环绕后通知");

        return ret;
    }

    @AfterReturning("execution(public * com.moonsky.base.aspectj.demo01.*.delete*(..))")
    public void afterReturning() {
        System.out.println(" --------------------------- 后置通知 delete");
    }

    /**
     * 后置通知除了定义 value 拦截规则外，还可以通过{@link AfterReturning#returning()}指定一个值
     * <p>
     * 这个值可通过相同参数名在{@link #afterElse(String)}中得到目标方法的返回值
     *
     * @param value
     */
    @AfterReturning(value = "execution(public * com.moonsky.base.aspectj.demo01..has*(..))", returning = "value")
    public void afterElse(String value) {
        System.out.println(" --------------------------- 后置通知: " + value);
    }

    @AfterThrowing(value = "execution(public * com.moonsky.base.aspectj.demo01..*(..))", throwing = "t")
    public void afterThrowing(Throwable t) {
        System.out.println(" 异常通知： 捕获到了异常才通知 —— " + t.getMessage());
    }

    /**
     * 相当于 finally 语法块儿
     */
    @After("execution(* *(..))")
    public void after() {
        System.out.println("相当于 finally 语法块儿");
    }
}
