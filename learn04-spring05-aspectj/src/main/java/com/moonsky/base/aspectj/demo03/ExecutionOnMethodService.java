package com.moonsky.base.aspectj.demo03;

import com.moonsky.base.aspectj.*;

/**
 * @author benshaoye
 */
public class ExecutionOnMethodService {

    /*
     * --------------------------------------------------------------------------------------------
     */

    @Anno01
    @Base01
    @Base02
    public String afterReturning01And$Or() {
        System.out.println(" 业务方法【afterReturning01】..................... Base01 & Base02 & Anno01");
        return "afterReturning";
    }

    @Base01
    @Base03
    public String afterReturning02And$Or() {
        System.out.println(" 业务方法【afterReturning02】..................... Base01 & Base03");
        return "afterReturning";
    }

    @Anno02
    @Base03
    public String afterReturning03And$Or() {
        System.out.println(" 业务方法【afterReturning03】..................... Base03 & Anno02");
        return "afterReturning";
    }

    @Base02
    public String afterReturning04And$Or() {
        System.out.println(" 业务方法【afterReturning04】..................... Base02");
        return "afterReturning";
    }

    public String afterReturning05And$Or() {
        System.out.println(" 业务方法【afterReturning05】..................... 没有");
        return "afterReturning";
    }

    @Base01
    public String afterReturning06And$Or() {
        System.out.println(" 业务方法【afterReturning04】..................... Base01");
        return "afterReturning";
    }

    @Anno01
    @Anno02
    public String afterReturning07And$Or() {
        System.out.println(" 业务方法【afterReturning04】..................... Anno01 & Anno02");
        return "afterReturning";
    }
}
