package com.moonsky.base.aspectj.demo02;

/**
 * @author benshaoye
 */
public class UserService {

    public String allMethodWithoutThrowing() {
        System.out.println("执行业务方法..................................... Without Throwing");
        return "UserService";
    }

    public String allMethodWithThrowing() {
        System.out.println("执行业务方法..................................... With Throwing");
        throw new IllegalArgumentException("spring");
    }
}
