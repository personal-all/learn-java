package com.moonsky.base.aspectj.demo03;

import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;

/**
 * 此类主要用于演示 execution 方式配置切入点示例
 *
 * @author benshaoye
 */
@Aspect
public class ExecutionWithAnnotatedOnMethodAspect {

    /**
     * AfterReturning 拦截的是一定要有返回值的方法，即返回值类型不能是 void
     * <p>
     * 拦截被 Base01 注解的方法
     *
     * @param ret
     */
    @AfterReturning(value = "execution(" +
        " @com.moonsky.base.aspectj.Base01" +
        " public * afterReturning*(..))", returning = "ret")
    public void afterReturningWithOneAnnotation(String ret) {
        System.out.println(" 【AfterReturning】 with: 【Base01】. return value of: " +
            ret);
    }

    /**
     * 拦截被 Base01 和 Base02 注解的方法
     *
     * @param ret
     */
    @AfterReturning(value = "execution(" +
        " @com.moonsky.base.aspectj.Base01" +
        " @com.moonsky.base.aspectj.Base02" +
        " public * afterReturning*(..))", returning = "ret")
    public void afterReturningWithMultiAnnotations(String ret) {
        System.out.println(" 【AfterReturning】 with: 【Base01 & Base02】. return value of: " +
            ret);
    }

    /**
     * 拦截被 Base01 和 Base02 注解的方法
     *
     * @param ret
     */
    @AfterReturning(value = "execution(" +
        " @(com.moonsky.base.aspectj.Anno01" +
        " ||com.moonsky.base.aspectj.Anno02)" +
        " public * afterReturning*(..))", returning = "ret")
    public void afterReturningWithMultiAnnotations00(String ret) {
        System.out.println(" 【AfterReturning】 with: 【Anno01 | Anno02】. return value of: " +
            ret);
    }

    /**
     * 拦截被 Base01 注解，同时至少还被 Base02 和 Base03 其中之一注解的方法
     *
     * @param ret
     */
    @AfterReturning(value = "execution(" +
        " @com.moonsky.base.aspectj.Base01" +
        " @(com.moonsky.base.aspectj.Base02" +
        " ||com.moonsky.base.aspectj.Base03)" +
        " public * afterReturning*(..))", returning = "ret")
    public void afterReturningWithMultiAnnotations01(String ret) {
        System.out.println(" 【AfterReturning】 with: 【Base01 & (Base02 | Base03)】. return value of: " +
            ret);
    }
}
