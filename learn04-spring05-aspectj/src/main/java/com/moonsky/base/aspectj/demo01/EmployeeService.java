package com.moonsky.base.aspectj.demo01;

/**
 * @author benshaoye
 */
public class EmployeeService {

    public void save() {
        System.out.println("【save】 .............................");
    }

    public void find() {
        System.out.println("【find】 .............................");
    }

    public void update() {
        System.out.println("【update】 .............................");
    }

    public void delete() {
        System.out.println("【delete】 .............................");
    }

    public String hasReturningValue() {
        System.out.println("【hasReturningValue】 ..................");
        return "Hello.";
    }

    public void withAround() {
        System.out.println("【withAround】 测试环绕通知");
    }

    public void withThrowing() {
        System.out.println("【withThrowing】 抛出一个异常");
        throw new NullPointerException("withThrowing");
    }
}
