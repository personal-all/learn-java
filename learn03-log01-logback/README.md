### 简介
中文文档：[http://www.logback.cn/](http://www.logback.cn/)

logback 主要由三个模块组成：
1. logback-access：主要用于与 servlet 容器集成，提供通过 http 访问日志的功能；
2. logback-classic：Log4j 的改良版本，它完整的实现了 slf4j API，可以很方便的更换成其他日志系统，如：Log4j；
> logback 和 Log4j 是同一个作者，它在 log4j 上改良并优化了性能；

3. logback-core：为 logback 提供基础服务；

### 主要标签
1. logger
2. appender
3. layout

### logback 配置文件查找顺序，按优先查找
1. 根据系统参数 logback.configurationFile=xxxxx.xml
> 如：java -Dlogback.configutationFile=logback.xml

2. 查找 classpath 中的：logback.groovy
3. 查找 classpath 中的：logback-test.xml
4. 查找 classpath 中的：logback.xml
5. 查找 com.qos.logback.classic.spi.Configurator 实现类，然后调用其 configure 方法；
6. 使用默认配置：ch.qos.logback.classic.BasicConfigurator

## Java 日志系统：
#### 统一日志门面：slf4j