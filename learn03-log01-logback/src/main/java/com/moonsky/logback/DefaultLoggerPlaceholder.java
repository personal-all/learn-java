package com.moonsky.logback;

import java.util.ServiceLoader;

/**
 * @author benshaoye
 */
public class DefaultLoggerPlaceholder {

    public void serviceLoader(){
        ServiceLoader.load(getClass());
    }
}
