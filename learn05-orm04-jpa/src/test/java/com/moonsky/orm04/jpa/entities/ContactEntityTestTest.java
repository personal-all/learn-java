package com.moonsky.orm04.jpa.entities;

import com.moon.core.util.Console;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Date;
import java.util.concurrent.atomic.AtomicLong;

import static java.lang.System.currentTimeMillis;

/**
 * @author benshaoye
 */
class ContactEntityTestTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Nested
    static class GenerateIdentity {

        private final static long COUNT = 1000000;
        private final static int threadCount = 50;

        private AtomicLong longer;

        @BeforeEach
        void setUp() {
            longer = new AtomicLong();
        }

        @ParameterizedTest
        @ValueSource(longs = COUNT)
        void testSysTimeMillis(long count) throws Exception {
            System.out.println(currentTimeMillis());
            for (int i = 0; i < threadCount; i++) {
                new Thread(getSysRunner(count)).start();
            }
            System.out.println(String.format("End: %s", currentTimeMillis()));
        }

        Runnable getSysRunner(long count) {
            return () -> {
                AtomicLong longer = this.longer;
                for (; longer.getAndIncrement() < count; ) {
                    currentTimeMillis();
                }
                System.out.println(currentTimeMillis());
            };
        }

        @ParameterizedTest
        @ValueSource(longs = COUNT)
        void testDateGetTime(long count) throws Exception {
            System.out.println(String.format("Begin date#getTime(): %s", currentTimeMillis()));
            for (int i = 0; i < threadCount; i++) {
                new Thread(getDate(count)).start();
            }
            System.out.println(String.format("End date#getTime(): %s", currentTimeMillis()));
        }

        Runnable getDate(long count) {
            return () -> {
                AtomicLong longer = this.longer;
                for (; longer.getAndIncrement() < count; ) {
                    new Date().getTime();
                }
                System.out.println(String.format("With date#getTime(): %s", currentTimeMillis()));
            };
        }
    }
}