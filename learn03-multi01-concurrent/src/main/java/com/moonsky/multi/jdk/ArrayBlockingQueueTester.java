package com.moonsky.multi.jdk;

import com.moon.core.models.KeyValue;
import com.moon.core.util.DateUtil;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * @author benshaoye
 */
public class ArrayBlockingQueueTester {

    public static void main(String[] args) {
        final int targetCount = 100_000_000;

        // ArrayBlockingQueue arrQueue = new ArrayBlockingQueue(targetCount);
        BlockingQueue<KeyValue> queue = new LinkedBlockingQueue<>();

        final long startTime = System.currentTimeMillis();
        new Thread(() -> {
            int idx = 0;
            while (idx < targetCount) {
                try {
                    queue.put(KeyValue.of(Integer.toString(idx), "Value: " + idx));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                idx++;
            }
        }).start();

        new Thread(() -> {
            int idx = 0;
            while (idx < targetCount) {
                try {
                    queue.take();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                idx++;
            }
            long lastTime = System.currentTimeMillis();
            System.out.println("The end time is: " + lastTime + ", duration: " + (lastTime - startTime));
        }).start();
        System.out.println("Begin with: " + startTime + ", " + DateUtil.format());
    }
}
