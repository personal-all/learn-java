## Spring XML 配置相关详解

#### 01. 配置文件头
```xml
<beans xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns="http://www.springframework.org/schema/beans"
       xmlns:mvc="http://www.springframework.org/schema/mvc"
       xmlns:jdbc="http://www.springframework.org/schema/jdbc"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="
       http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/spring-beans.xsd
       http://www.springframework.org/schema/mvc
       http://www.springframework.org/schema/mvc/spring-mvc.xsd
       http://www.springframework.org/schema/jdbc
       http://www.springframework.org/schema/jdbc/spring-jdbc.xsd
       http://www.springframework.org/schema/context
       http://www.springframework.org/schema/jdbc/spring-context.xsd">
  <!-- configurations to do ... -->
  
  <!-- 加载 properties 配置信息 + 忽略不存在 properties -->
  <context:property-placeholder
   location="xxxx.properties"
   ignore-unresolvable="true" 
   ignore-resource-not-found="true"/>

  <!-- 自动扫描并配置注解信息：@Controller、@Service、@Component、@Repository -->
  <context:component-scan base-package="com.aaa.bbb.ccc"/>
  
  <!-- mvc 注解驱动 + 配置类型转换器 -->
  <mvc:annotation-driven conversion-service="conversionService"/>
  
  <!-- bean 配置方式 -->
  <bean class="aaa.bbb.ccc.BeanType" id="beanType">
    <constructor-arg index="0" value="1"/>
    <constructor-arg name="name" value="lilei"/>
    
    <property name="password" value="123456"/>
    
    <property name="list">
      <list>
        <value>value0</value>     
      </list>
    </property>
  </bean>
</beans>
```

#### 02. 加载配置文件的方式：
1. ClassPathApplicationContext；
2. FileSystemApplicationContext；

#### 03. 实例化 bean 的方式：bean.demo02.applicationContext.xml
1. 参考：[bean.demo02.applicationContext.xml](https://gitee.com/744531854/learn-java/blob/master/learn04-spring03-beans/src/main/resources/bean.demo02.applicationContext.xml)

#### 04. bean 的作用范围（scope）：bean.demo03.applicationContext.xml
2. 参考：[bean.demo03.applicationContext.xml](https://gitee.com/744531854/learn-java/blob/master/learn04-spring03-beans/src/main/resources/bean.demo03.applicationContext.xml)

#### 05. bean 完整声明周期：bean.demo05.applicationContext.xml
1. 初识：[bean.demo04.applicationContext.xml](https://gitee.com/744531854/learn-java/blob/master/learn04-spring03-beans/src/main/resources/bean.demo04.applicationContext.xml)
2. 完整：[bean.demo05.applicationContext.xml](https://gitee.com/744531854/learn-java/blob/master/learn04-spring03-beans/src/main/resources/bean.demo05.applicationContext.xml)
3. 拓展：[bean.demo06.applicationContext.xml](https://gitee.com/744531854/learn-java/blob/master/learn04-spring03-beans/src/main/resources/bean.demo06.applicationContext.xml)

#### 06. bean 的属性设置（property）：bean.demo07.applicationContext.xml
1. 基本配置 + `p`命名空间 + `el`表达式注入：[bean.demo07.applicationContext.xml](https://gitee.com/744531854/learn-java/blob/master/learn04-spring03-beans/src/main/resources/bean.demo07.applicationContext.xml)
2. 复杂数据类型配置：[bean.demo08.applicationContext.xml](https://gitee.com/744531854/learn-java/blob/master/learn04-spring03-beans/src/main/resources/bean.demo08.applicationContext.xml)
3. properties 文件注入：[bean.demo09.applicationContext.xml](https://gitee.com/744531854/learn-java/blob/master/learn04-spring03-beans/src/main/resources/bean.demo09.applicationContext.xml)

#### 07. 注解驱动
1. 完全注解扫描：`<context:component-scan base-package="com.xxx.yyy">`；
2. xml 文件注册 bean，注解驱动属性配置：`<context:annotation-config/>`；

#### 08. properties 文件注入：bean.demo11.applicationContext.xml
3. 基本注入 + 可忽略：[bean.demo11.applicationContext.xml](https://gitee.com/744531854/learn-java/blob/master/learn04-spring03-beans/src/main/resources/bean.demo11.applicationContext.xml)
