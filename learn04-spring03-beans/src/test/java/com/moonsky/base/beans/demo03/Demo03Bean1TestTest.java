package com.moonsky.base.beans.demo03;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import static org.junit.Assert.*;

/**
 * @author benshaoye
 */
public class Demo03Bean1TestTest {

    @Test
    public void testGetBean() {
        String path = "bean.demo03.applicationContext.xml";
        ApplicationContext context = new ClassPathXmlApplicationContext(path);

        Bean1 bean1 = (Bean1) context.getBean("createBean1");
        Bean1 bean11 = (Bean1) context.getBean("createBean-1");

        Bean2 singletonBean20 = (Bean2) context.getBean("singletonBean2");
        Bean2 singletonBean21 = (Bean2) context.getBean("singletonBean2");
        Bean2 singletonBean22 = (Bean2) context.getBean("singletonBean2");

        assertTrue(singletonBean20 == singletonBean21);
        assertTrue(singletonBean22 == singletonBean21);

        Bean1 prototypeBean20 = (Bean1) context.getBean("prototypeBean2");
        Bean1 prototypeBean21 = (Bean1) context.getBean("prototypeBean2");
        Bean1 prototypeBean22 = (Bean1) context.getBean("prototypeBean2");

        assertTrue(prototypeBean20 != prototypeBean21);
        assertTrue(prototypeBean22 != prototypeBean21);
    }
}