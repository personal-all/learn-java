package com.moonsky.base.beans.demo06;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author benshaoye
 */
public class Demo06UserDaoImplTestTest {

    @Test
    public void testName() {
        String path = "bean.demo06.applicationContext.xml";
        ApplicationContext context = new ClassPathXmlApplicationContext(path);
        UserDao userDao = (UserDao) context.getBean("userDao");

        userDao.find();
        userDao.save();
        userDao.find();
        userDao.update();
        userDao.delete();

        System.out.println(context.getBean(JdkProxyBeanPostProcessor.class));
    }
}