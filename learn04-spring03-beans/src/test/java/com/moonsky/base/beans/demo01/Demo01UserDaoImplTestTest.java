package com.moonsky.base.beans.demo01;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import java.io.*;
import java.util.function.Consumer;

import static java.lang.System.getProperty;

/**
 * @author benshaoye
 */
public class Demo01UserDaoImplTestTest {

    @Test
    public void testTraditional4User() {
        UserDao userDao = new UserDaoImpl();

        userDao.sayHello();
    }

    @Test
    public void testSpring4UserWithClasspath() {
        String path = "bean.demo01.applicationContext.xml";
        ApplicationContext context = new ClassPathXmlApplicationContext(path);

        UserDao userDao = (UserDao) context.getBean("userDao");

        userDao.sayHello();
    }

    @Test
    public void testSpring4UserWithFileSystem() throws IOException {
        setUp("bean.demo01.applicationContext.xml", path -> {
            System.out.println(path);
            ApplicationContext context = new FileSystemXmlApplicationContext(path);

            UserDao userDao = (UserDao) context.getBean("userDao");

            userDao.sayHello();
        }, "D:/", "/usr");
    }

    /**
     * 为了测试{@link #testSpring4UserWithFileSystem()}方法，从 classpath 复制一份配置文件
     * 至指定文件系统目录，然后将文件系统绝对路径交给 consumer 使用，最后释放并删除资源
     *
     * @param path
     * @param consumer
     * @param winRoot
     * @param unixRoot
     *
     * @throws IOException
     * @see #testSpring4UserWithFileSystem()
     */
    void setUp(String path, Consumer<String> consumer, String winRoot, String unixRoot) throws IOException {
        // 根据操作系统，设置不同根路径
        File root = new File(getProperty("os.name").toLowerCase().contains("windows") ? winRoot : unixRoot);
        File targetFile = new File(root, path);

        // 确保源文件只有一个，先删除，再创建一个新的空文件
        targetFile.delete();
        File tempFile = targetFile.getParentFile();
        if (!tempFile.exists()) {
            tempFile.mkdirs();
        }
        targetFile.createNewFile();

        InputStream stream = null;
        OutputStream out = null;
        try {
            // 复制
            stream = getClass().getClassLoader().getResourceAsStream(path);
            out = new FileOutputStream(targetFile);
            byte[] bytes = new byte[10240];
            int limit;
            do {
                limit = stream.read(bytes, 0, bytes.length);
                if (limit >= 0) {
                    out.write(bytes, 0, limit);
                }
            } while (limit >= 0);
            // 关闭后使用
            close(out);
            close(stream);

            // 使用
            consumer.accept(targetFile.getAbsolutePath());

            // 删除，确保源目录干净整洁
            // 这里不删除根目录，为的是防止意外删除其他文件，造成不可挽回的错误
            targetFile.delete();
        } finally {
            // 确保在错误的情况下也能正确释放资源
            close(out);
            close(stream);
        }
    }

    static void close(Closeable closeable) {
        try {
            closeable.close();
        } catch (Exception e) {
            // ignore
        }
    }
}