package com.moonsky.base.beans.demo11;

import com.moonsky.base.beans.demo10.UserDao;
import com.moonsky.base.beans.demo10.UserService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author benshaoye
 */
public class Demo11UserDaoTestTest {

    @Test
    public void testName() {
        String path = "bean.demo11.applicationContext.xml";
        ApplicationContext context = new ClassPathXmlApplicationContext(path);

        OverrideProperties properties = context.getBean(OverrideProperties.class);
        assertEquals("1", properties.getPrimaryValue());
    }
}