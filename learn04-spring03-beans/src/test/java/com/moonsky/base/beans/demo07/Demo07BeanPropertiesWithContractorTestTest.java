package com.moonsky.base.beans.demo07;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author benshaoye
 */
public class Demo07BeanPropertiesWithContractorTestTest {

    @Test
    public void testName() {
        String path = "bean.demo07.applicationContext.xml";
        ApplicationContext context = new ClassPathXmlApplicationContext(path);

        BeanPropertiesWithContractor contractor = (BeanPropertiesWithContractor) context.getBean("contractor");
        System.out.println(contractor);
        BeanPropertiesWithContractor contractor0 = (BeanPropertiesWithContractor) context.getBean("contractor0");
        System.out.println(contractor0);
        BeanPropertiesWithContractor contractor1 = (BeanPropertiesWithContractor) context.getBean("contractor1");
        System.out.println(contractor1);
        BeanPropertiesWithContractor contractor2 = (BeanPropertiesWithContractor) context.getBean("contractor2");
        System.out.println(contractor2);

        BeanPropertiesWithSetter setter = (BeanPropertiesWithSetter) context.getBean("setter");
        System.out.println(setter);

        BeanPropertiesWithSetter setterP = (BeanPropertiesWithSetter) context.getBean("setterP");
        System.out.println(setterP);

        BeanPropertiesWithSetter setterSpEL0 = (BeanPropertiesWithSetter) context.getBean("setterSpEL0");
        System.out.println(setterSpEL0);

        BeanPropertiesWithSetter setterSpEL1 = (BeanPropertiesWithSetter) context.getBean("setterSpEL1");
        System.out.println(setterSpEL1);

        BeanPropertiesWithSetter setterSpEL2 = (BeanPropertiesWithSetter) context.getBean("setterSpEL2");
        System.out.println(setterSpEL2);

        BeanPropertiesWithSetter setterSpEL3 = (BeanPropertiesWithSetter) context.getBean("setterSpEL3");
        System.out.println(setterSpEL3);

        BeanPropertiesWithSetter setterSpEL4 = (BeanPropertiesWithSetter) context.getBean("setterSpEL4");
        System.out.println(setterSpEL4);
    }
}