package com.moonsky.base.beans.demo05;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author benshaoye
 */
public class Demo05LifeCycle1TestTest {

    @Test
    public void testName() {
        String path = "bean.demo05.applicationContext.xml";
        ApplicationContext context = new ClassPathXmlApplicationContext(path);

        ((ClassPathXmlApplicationContext) context).close();
    }
}