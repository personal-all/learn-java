package com.moonsky.base.beans.demo09;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author benshaoye
 */
public class Demo09UserComponentTestTest {

    @Test
    public void testName() {
        String path = "bean.demo09.applicationContext.xml";
        ApplicationContext context = new ClassPathXmlApplicationContext(path);

        UserComponent component = context.getBean(UserComponent.class);
        StudentComponent studentComponent = context.getBean(StudentComponent.class);
        User user = context.getBean(User.class);
        Student student = context.getBean(Student.class);

        System.out.println(user);
        System.out.println(student);

        ((ClassPathXmlApplicationContext) context).close();
    }
}