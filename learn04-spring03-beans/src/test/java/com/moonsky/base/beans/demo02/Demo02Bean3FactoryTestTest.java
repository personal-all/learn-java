package com.moonsky.base.beans.demo02;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author benshaoye
 */
public class Demo02Bean3FactoryTestTest {

    @Test
    public void testName() {
        String path = "bean.demo02.applicationContext.xml";
        ApplicationContext context = new ClassPathXmlApplicationContext(path);
    }
}