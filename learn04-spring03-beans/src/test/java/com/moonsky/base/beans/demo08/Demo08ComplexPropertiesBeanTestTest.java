package com.moonsky.base.beans.demo08;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author benshaoye
 */
public class Demo08ComplexPropertiesBeanTestTest {

    @Test
    public void testName() {
        String path = "bean.demo08.applicationContext.xml";
        ApplicationContext context = new ClassPathXmlApplicationContext(path);

        ComplexPropertiesBean complex = (ComplexPropertiesBean) context.getBean("complexPropertiesBean");
        System.out.println("\n\n================= 【ComplexPropertiesBean】=================");
        System.out.println(complex);

        System.out.println("\n\n================= 【Map】 object object map =================");
        complex.getObjectObjectMap().forEach(this::outEntry);

        System.out.println("\n\n================= 【Properties】 int value type properties =================");
        complex.getIntValueTypeProperties().forEach(this::outEntry);

        System.out.println("\n\n================= 【Properties】 string value type properties =================");
        complex.getStringStringProperties().forEach(this::outEntry);

        System.out.println("\n\n================= 【List】 string list =================");
        complex.getStringList().forEach(this::outItem);

        System.out.println("\n\n================= 【Set】 string set =================");
        complex.getStringSet().forEach(this::outItem);
    }

    private void outEntry(Object key, Object value) {
        if (key == null) {
            System.out.println(null + "\t" + value);
        } else {
            System.out.println(key.getClass() + "\t" + key + " : " + value.getClass() + "\t" + value);
        }
    }

    private void outItem(Object value) {
        if (value == null) {
            System.out.println("<Null value>");
        } else {
            System.out.println(value.getClass() + "\t" + value);
        }
    }
}