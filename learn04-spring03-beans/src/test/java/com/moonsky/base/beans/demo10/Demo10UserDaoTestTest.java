package com.moonsky.base.beans.demo10;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import static org.junit.Assert.assertTrue;

/**
 * @author benshaoye
 */
public class Demo10UserDaoTestTest {

    @Test
    public void testName() {
        String path = "bean.demo10.applicationContext.xml";
        ApplicationContext context = new ClassPathXmlApplicationContext(path);

        UserDao userDao = (UserDao) context.getBean("userDao");
        UserService userService = (UserService) context.getBean("userService");

        assertTrue(userDao.getUserService() == userService);
        assertTrue(userService.getUserDao() == userDao);
    }
}