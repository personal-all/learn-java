package com.moonsky.base.beans.demo04;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author benshaoye
 */
public class Demo04BeanLifeCycleTestTest {

    @Test
    public void testInstance() {
        String path = "bean.demo04.applicationContext.xml";
        ApplicationContext context = new ClassPathXmlApplicationContext(path);
        System.out.println("==========================================");
        System.out.println("执行业务内容");
        System.out.println("==========================================");
        ((ClassPathXmlApplicationContext) context).close();
    }
}