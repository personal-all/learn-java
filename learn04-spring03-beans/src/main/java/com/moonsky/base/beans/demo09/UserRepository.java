package com.moonsky.base.beans.demo09;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

/**
 * @author benshaoye
 */
@Repository("userRepository")
public class UserRepository {
    @Autowired
    @Qualifier("userComponent")
    public UserComponent userComponent;
}
