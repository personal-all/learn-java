package com.moonsky.base.beans.demo07;

/**
 * @author benshaoye
 */
public class BeanPropertiesWithContractor {

    private final String name;
    private final int age;

    public BeanPropertiesWithContractor(String name) {
        this(name, 0);
    }

    public BeanPropertiesWithContractor(String name, int age) {
        System.out.println("初始化：" + getClass());
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public int getRandomAge() {
        return (int) (Math.random() * 100);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BeanPropertiesWithContractor{");
        sb.append("name='").append(name).append('\'');
        sb.append(", age=").append(age);
        sb.append('}');
        return sb.toString();
    }
}
