package com.moonsky.base.beans.demo09;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author benshaoye
 */
@Component("userComponent")
public class UserComponent {

    @Resource(name = "userService")
    private UserService userService;

    @Value("张三")
    private String value;
}
