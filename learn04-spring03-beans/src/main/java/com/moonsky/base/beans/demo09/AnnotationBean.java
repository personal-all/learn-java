package com.moonsky.base.beans.demo09;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * @author benshaoye
 */
@Component
public class AnnotationBean
    implements BeanNameAware, BeanFactoryAware, ApplicationContextAware, InitializingBean, DisposableBean {

    private int index = 1;

    private int index() {
        return index++;
    }

    public AnnotationBean() {
        System.out.println(index() + "\tcreate an instance");
        System.out.println(index() + "\tinit field values");
    }

    @PostConstruct
    public void initMethod() {
        System.out.println(index() + "\tinitMethod");
    }

    @Override
    public void setBeanName(String name) {
        System.out.println(index() + "\tBeanNameAware#setBeanName");
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        System.out.println(index() + "\tBeanFactoryAware#setBeanFactory");
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        System.out.println(index() + "\tApplicationContextAware#setApplicationContext");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println(index() + "\tInitializingBean#afterPropertiesSet");
    }

    @Override
    public void destroy() throws Exception {
        System.out.println(index() + "\tDisposableBean#destroy");
    }

    @PreDestroy
    public void destroyMethod() throws Exception {
        System.out.println(index() + "\tdestroyMethod");
    }
}
