package com.moonsky.base.beans.demo02;

/**
 * @author benshaoye
 */
public class Bean2Factory {

    public static CreateBean2 getInstance() {
        System.out.println("正在通过工厂方法实例化 CreateBean2 ");
        return new CreateBean2();
    }
}
