package com.moonsky.base.beans.demo05;

import com.moonsky.base.beans.demo03.Bean1;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * @author benshaoye
 */
public class LifeCycle1 implements BeanNameAware,
                                   BeanFactoryAware,
                                   ApplicationContextAware,
                                   InitializingBean,
                                   DisposableBean {

    private double num = Math.random();

    private String currentName;

    public LifeCycle1() {
        System.out.println("第一步 ，实例化 bean >>>> " + num);
    }

    @Override
    public void setBeanName(String name) {
        System.out.println("第三步 ，" + num + "\tsetBeanName: " + name);
        this.currentName = name;
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        System.out.println("第四步 ，setBeanFactory");
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        System.out.println("第四步 ，setApplicationContext");
    }

    public String getCurrentName() {
        return currentName;
    }

    public void setCurrentName(String currentName) {
        System.out.println("第二步 ，封装属性");
        this.currentName = currentName;
    }

    @Override
    public String toString() {
        return num + "\t" + currentName + " :\t" + System.identityHashCode(this);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("第六步 ，InitializingBean#afterPropertiesSet()");
    }

    public void initMethod() {
        System.out.println("第七步 ，initMethod()");
    }

    public void run1() {
        System.out.println("9 - run1");
    }

    public void run2(String name) {
        System.out.println("9 - run2");
    }

    public void run3(int age) {
        System.out.println("9 - run3");
    }

    public void run4(Bean1 bean1) {
        System.out.println("9 - run4");
    }

    @Override
    public void destroy() throws Exception {
        System.out.println("第十步 ，DisposableBean#destroy()");
    }

    public void destroyMethod() throws Exception {
        System.out.println("第十一步 ，destroyMethod()");
    }
}
