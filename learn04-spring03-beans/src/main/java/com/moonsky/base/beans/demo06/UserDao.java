package com.moonsky.base.beans.demo06;

/**
 * @author benshaoye
 */
public interface UserDao {

    void save();

    void find();

    void update();

    void delete();
}
