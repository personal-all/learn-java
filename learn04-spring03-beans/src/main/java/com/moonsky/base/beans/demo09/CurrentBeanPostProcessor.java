package com.moonsky.base.beans.demo09;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

/**
 * @author benshaoye
 */
public class CurrentBeanPostProcessor implements BeanPostProcessor {

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        String name = "life cycle #postProcessBeforeInitialization \twith beanName:\t";
        System.out.println(name + beanName + " for: " + bean.getClass());
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        String name = "life cycle #postProcessAfterInitialization \t\twith beanName\t";
        System.out.println(name + beanName + " for: " + bean.getClass());
        return bean;
    }
}
