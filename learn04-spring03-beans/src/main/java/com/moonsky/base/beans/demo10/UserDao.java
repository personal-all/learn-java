package com.moonsky.base.beans.demo10;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author benshaoye
 */
public class UserDao {

    @Autowired
    private UserService userService;

    public UserService getUserService() {
        return userService;
    }
}
