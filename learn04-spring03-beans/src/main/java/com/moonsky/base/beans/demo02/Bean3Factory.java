package com.moonsky.base.beans.demo02;

/**
 * @author benshaoye
 */
public class Bean3Factory {

    public Bean3Factory() {
        System.out.println("Bean3Factory 实例工厂正在被实例化");
    }

    public CreateBean3 getInstance() {
        System.out.println("正在通过实例工厂方法实例化 CreateBean3");
        return new CreateBean3();
    }
}
