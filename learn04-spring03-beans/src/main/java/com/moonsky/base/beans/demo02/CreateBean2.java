package com.moonsky.base.beans.demo02;

/**
 * @author benshaoye
 */
public class CreateBean2 {

    public CreateBean2() {
        System.out.println("CreateBean2 无参构造器被实例化");
    }

    public static CreateBean2 create() {
        System.out.println("CreateBean2 正在执行静态实例方法");
        return new CreateBean2();
    }
}
