package com.moonsky.base.beans.demo09;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author benshaoye
 */
@Component
public class StudentService {
    @Autowired
    private StudentDao studentDao;
}
