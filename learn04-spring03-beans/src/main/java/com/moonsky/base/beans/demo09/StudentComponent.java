package com.moonsky.base.beans.demo09;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author benshaoye
 */
@Service
public class StudentComponent {
    @Autowired
    private StudentService studentService;
}
