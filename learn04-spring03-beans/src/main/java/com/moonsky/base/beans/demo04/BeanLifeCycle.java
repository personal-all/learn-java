package com.moonsky.base.beans.demo04;

/**
 * @author benshaoye
 */
public class BeanLifeCycle {

    public BeanLifeCycle() {
        System.out.println("BeanLifeCycle 无参构造器被实例化");
    }

    public void initMethod() {
        System.out.println("BeanLifeCycle 正在执行初始化方法: initMethod");
    }

    public void destroyMethod() {
        System.out.println("BeanLifeCycle 正在执行销毁方法: destroyMethod");
    }
}
