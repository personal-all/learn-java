package com.moonsky.base.beans.demo06;

/**
 * @author benshaoye
 */
public class UserDaoImpl implements UserDao {

    @Override
    public void save() {
        System.out.println("save");
    }

    @Override
    public void find() {
        System.out.println("find");
    }

    @Override
    public void update() {
        System.out.println("update");
    }

    @Override
    public void delete() {
        System.out.println("delete");
    }
}
