package com.moonsky.base.beans.demo07;

/**
 * @author benshaoye
 */
public class BeanPropertiesWithSetter {

    private String currentName;
    private String currentAge;

    private BeanPropertiesWithContractor contractor;

    public BeanPropertiesWithSetter() {
        System.out.println("初始化：" + getClass());
    }

    public String getCurrentName() {
        return currentName;
    }

    public void setCurrentName(String currentName) {
        this.currentName = currentName;
    }

    public String getCurrentAge() {
        return currentAge;
    }

    public void setCurrentAge(String currentAge) {
        this.currentAge = currentAge;
    }

    public BeanPropertiesWithContractor getContractor() {
        return contractor;
    }

    public void setContractor(BeanPropertiesWithContractor contractor) {
        this.contractor = contractor;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BeanPropertiesWithSetter{");
        sb.append("currentName='").append(currentName).append('\'');
        sb.append(", currentAge='").append(currentAge).append('\'');
        sb.append(", contractor=").append(contractor);
        sb.append('}');
        return sb.toString();
    }
}
