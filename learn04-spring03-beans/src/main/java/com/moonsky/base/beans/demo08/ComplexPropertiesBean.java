package com.moonsky.base.beans.demo08;

import java.util.*;

/**
 * @author benshaoye
 */
public class ComplexPropertiesBean {

    private List<String> stringList;
    private Set<String> stringSet;
    private Map<String, Object> stringObjectMap;
    private Map<Object, Object> objectObjectMap;
    private Properties stringStringProperties;
    private Properties intValueTypeProperties;

    private int[] valuesArray;

    private List<String> listFormConstructor;

    public ComplexPropertiesBean() {
        listFormConstructor = new ArrayList<>();
        listFormConstructor.add("123");
        listFormConstructor.add("456");
        listFormConstructor.add("789");
    }

    public int[] getValuesArray() {
        return valuesArray;
    }

    public void setValuesArray(int[] valuesArray) {
        this.valuesArray = valuesArray;
    }

    public List<String> getStringList() {
        return stringList;
    }

    public void setStringList(List<String> stringList) {
        this.stringList = stringList;
    }

    public Set<String> getStringSet() {
        return stringSet;
    }

    public void setStringSet(Set<String> stringSet) {
        this.stringSet = stringSet;
    }

    public Map<String, Object> getStringObjectMap() {
        return stringObjectMap;
    }

    public void setStringObjectMap(Map<String, Object> stringObjectMap) {
        this.stringObjectMap = stringObjectMap;
    }

    public Map<Object, Object> getObjectObjectMap() {
        return objectObjectMap;
    }

    public void setObjectObjectMap(Map<Object, Object> objectObjectMap) {
        this.objectObjectMap = objectObjectMap;
    }

    public Properties getStringStringProperties() {
        return stringStringProperties;
    }

    public void setStringStringProperties(Properties stringStringProperties) {
        this.stringStringProperties = stringStringProperties;
    }

    public List<String> getListFormConstructor() {
        return listFormConstructor;
    }

    public void setListFormConstructor(List<String> listFormConstructor) {
        this.listFormConstructor = listFormConstructor;
    }

    public Properties getIntValueTypeProperties() {
        return intValueTypeProperties;
    }

    public void setIntValueTypeProperties(Properties intValueTypeProperties) {
        this.intValueTypeProperties = intValueTypeProperties;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ComplexPropertiesBean{");
        sb.append("\n\tstringList=").append(stringList);
        sb.append(", \n\tstringSet=").append(stringSet);
        sb.append(", \n\tstringObjectMap=").append(stringObjectMap);
        sb.append(", \n\tobjectObjectMap=").append(objectObjectMap);
        sb.append(", \n\tstringStringProperties=").append(stringStringProperties);
        sb.append(", \n\tcustomValueTypeProperties=").append(intValueTypeProperties);
        sb.append(", \n\tvalues=").append(Arrays.toString(valuesArray));
        sb.append('}');
        return sb.toString();
    }
}
