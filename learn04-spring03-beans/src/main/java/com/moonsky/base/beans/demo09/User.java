package com.moonsky.base.beans.demo09;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author benshaoye
 */
@Component
@Scope("prototype")
public class User {
    @Value("${current.user.name}")
    private String name;
    @Value("${current.user.age}")
    private String age;
    @Value("#{student.name}")
    private String studentName;

    public String getName() {
        return name;
    }

    public String getAge() {
        return age;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("User{");
        sb.append("name='").append(name).append('\'');
        sb.append(", age='").append(age).append('\'');
        sb.append(", studentName='").append(studentName).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
