package com.moonsky.base.beans.demo10;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author benshaoye
 */
public class UserService {

    @Autowired
    private UserDao userDao;

    public UserDao getUserDao() {
        return userDao;
    }
}
