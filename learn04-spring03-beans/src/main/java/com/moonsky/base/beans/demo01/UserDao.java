package com.moonsky.base.beans.demo01;

/**
 * @author benshaoye
 */
public interface UserDao {
    void sayHello();
}
