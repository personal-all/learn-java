package com.moonsky.base.beans.demo06;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

import java.lang.reflect.Proxy;

/**
 * @author benshaoye
 */
public class JdkProxyBeanPostProcessor implements BeanPostProcessor {

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        Class type = bean.getClass();
        ClassLoader cl = type.getClassLoader();
        Object proxyBean = Proxy.newProxyInstance(cl, type.getInterfaces(), (proxy, method, args) -> {
            if ("save".equals(method.getName())){
                System.out.println("[before execute save method]");
            }
            return method.invoke(bean, args);
        });
        return proxyBean;
    }
}
