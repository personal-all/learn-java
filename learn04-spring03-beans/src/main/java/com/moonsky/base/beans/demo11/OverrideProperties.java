package com.moonsky.base.beans.demo11;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author benshaoye
 */
@Component
public class OverrideProperties {

    @Value("${default.primary}")
    private String primaryValue;

    public String getPrimaryValue() {
        return primaryValue;
    }

    public void setPrimaryValue(String primaryValue) {
        this.primaryValue = primaryValue;
    }
}
