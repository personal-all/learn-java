package com.moonsky.base.beans.demo05;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

/**
 * @author benshaoye
 */
public class LifeCycleBeanPostProcessorOther implements BeanPostProcessor {

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("第五步 ，LifeCycleBeanPostProcessorOther#postProcessBeforeInitialization");
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("第八步 ，LifeCycleBeanPostProcessorOther#postProcessBeforeInitialization");
        return bean;
    }
}
