package com.moonsky.base.aop.demo07;

import org.springframework.aop.MethodBeforeAdvice;

import java.lang.reflect.Method;

/**
 * @author benshaoye
 */
public class CurrBeforeAdvice implements MethodBeforeAdvice {

    @Override
    public void before(Method method, Object[] args, Object target) throws Throwable {
        System.out.println(" 前置通知 CurrBeforeAdvice  .......................................");
    }
}
