package com.moonsky.base.aop.demo07;

import org.springframework.aop.AfterReturningAdvice;

import java.lang.reflect.Method;

/**
 * @author benshaoye
 */
public class CurrAfterAdvice implements AfterReturningAdvice {

    @Override
    public void afterReturning(Object returnValue, Method method, Object[] args, Object target) throws Throwable {
        System.out.println(" ....................................... 后置通知 CurrAfterAdvice");
    }
}
