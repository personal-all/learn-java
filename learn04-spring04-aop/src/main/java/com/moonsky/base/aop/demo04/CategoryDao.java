package com.moonsky.base.aop.demo04;

/**
 * @author benshaoye
 */
public class CategoryDao {

    public void save() {
        System.out.println("save");
    }

    public void find() {
        System.out.println("find");
    }

    public void update() {
        System.out.println("update");
    }

    public void delete() {
        System.out.println("delete");
    }
}
