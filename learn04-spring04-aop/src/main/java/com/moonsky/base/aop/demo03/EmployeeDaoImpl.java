package com.moonsky.base.aop.demo03;

/**
 * @author benshaoye
 */
public class EmployeeDaoImpl implements EmployeeDao {

    @Override
    public void save() {
        System.out.println("save");
    }

    @Override
    public void find() {
        System.out.println("find");
    }
}
