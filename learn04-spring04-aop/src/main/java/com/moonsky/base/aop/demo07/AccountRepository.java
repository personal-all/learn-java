package com.moonsky.base.aop.demo07;

/**
 * @author benshaoye
 */
public interface AccountRepository {

    void save();
    void find();
    void update();
    void delete();
}
