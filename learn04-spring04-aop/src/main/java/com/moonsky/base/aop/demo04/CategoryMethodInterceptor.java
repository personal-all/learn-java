package com.moonsky.base.aop.demo04;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @author benshaoye
 */
public class CategoryMethodInterceptor implements MethodInterceptor {

    private final Object target;

    public CategoryMethodInterceptor(Object target) {this.target = target;}

    public Object intercept(
        Object o, Method method, Object[] objects, MethodProxy methodProxy
    ) throws Throwable {
        System.out.println("前置通知。。。。。");
        Object ret = methodProxy.invoke(target, objects);
        System.out.println("。。。。。后置通知");
        return ret;
    }

    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        System.out.println("前置通知。。。。。");
        Object ret = invocation.proceed();
        System.out.println("。。。。。后置通知");
        return ret;
    }
}
