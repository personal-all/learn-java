package com.moonsky.base.aop.demo01;

/**
 * @author benshaoye
 */
public interface StudentDao {
    void save();
    void find();
    void update();
    void delete();
}
