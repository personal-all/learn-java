package com.moonsky.base.aop.demo03;

/**
 * @author benshaoye
 */
public interface EmployeeDao {

    void save();

    void find();
}
