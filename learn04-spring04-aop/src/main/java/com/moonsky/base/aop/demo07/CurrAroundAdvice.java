package com.moonsky.base.aop.demo07;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

/**
 * @author benshaoye
 */
public class CurrAroundAdvice implements MethodInterceptor {

    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        System.out.println(" 环绕通知 【前】 CurrAroundAdvice .......................................");
        Object ret = invocation.proceed();
        System.out.println(" ....................................... 环绕通知 【后】 CurrAroundAdvice");
        return ret;
    }
}
