package com.moonsky.base.aop.demo06;

/**
 * @author benshaoye
 */
public interface EmployeeService {
    void save();
    void find();
    void update();
    void delete();
}
