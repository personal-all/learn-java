package com.moonsky.base.aop.demo02;

import org.springframework.aop.MethodBeforeAdvice;

import java.lang.reflect.Method;

/**
 * @author benshaoye
 */
public class MyBeforeAdvice2 implements MethodBeforeAdvice {

    @Override
    public void before(Method method, Object[] args, Object target) {
        System.out.println("前置验证。。。。。。。。MyBeforeAdvice2");
    }
}
