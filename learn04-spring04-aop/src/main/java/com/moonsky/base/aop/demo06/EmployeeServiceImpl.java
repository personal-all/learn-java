package com.moonsky.base.aop.demo06;

/**
 * @author benshaoye
 */
public class EmployeeServiceImpl implements EmployeeService {

    @Override
    public void save() {
        System.out.println("SAVE Employee ...................");
    }

    @Override
    public void find() {
        System.out.println("FIND Employee ...................");
    }

    @Override
    public void update() {
        System.out.println("UPDATE Employee ...................");
    }

    @Override
    public void delete() {
        System.out.println("DELETE Employee ...................");
    }
}
