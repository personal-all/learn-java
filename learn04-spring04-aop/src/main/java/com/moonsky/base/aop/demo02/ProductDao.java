package com.moonsky.base.aop.demo02;

/**
 * @author benshaoye
 */
public interface ProductDao {
    void save();
    void find();
}
