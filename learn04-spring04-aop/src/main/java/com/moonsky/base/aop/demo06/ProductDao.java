package com.moonsky.base.aop.demo06;

/**
 * @author benshaoye
 */
public class ProductDao {

    public void save() {
        System.out.println("save product ......................");
    }

    public void find() {
        System.out.println("find product ......................");
    }

    public void update() {
        System.out.println("update product ......................");
    }

    public void delete() {
        System.out.println("delete product ......................");
    }
}
