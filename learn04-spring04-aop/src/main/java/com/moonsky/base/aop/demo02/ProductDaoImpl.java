package com.moonsky.base.aop.demo02;

/**
 * @author benshaoye
 */
public class ProductDaoImpl implements ProductDao {

    @Override
    public void save() {
        System.out.println("save");
    }

    @Override
    public void find() {
        System.out.println("find");
    }
}
