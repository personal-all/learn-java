package com.moonsky.base.aop.demo07;

/**
 * @author benshaoye
 */
public class EnterpriseService {

    public void save() {
        System.out.println("{{save}} enterprise -------------------------------------");
    }

    public void find() {
        System.out.println("{{find}} enterprise -------------------------------------");
    }

    public void update() {
        System.out.println("{{update}} enterprise -------------------------------------");
    }

    public void delete() {
        System.out.println("{{delete}} enterprise -------------------------------------");
    }
}
