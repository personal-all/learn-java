package com.moonsky.base.aop.demo06;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

/**
 * @author benshaoye
 */
public class NowMethodInterceptor implements MethodInterceptor {

    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        System.out.println(" 环绕通知 ==> 前置 NowMethodInterceptor ...............");
        Object ret = invocation.proceed();
        System.out.println(" ............... 环绕通知 ==> 后置 NowMethodInterceptor ");
        return ret;
    }
}
