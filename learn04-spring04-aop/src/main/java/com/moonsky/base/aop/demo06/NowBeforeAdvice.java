package com.moonsky.base.aop.demo06;

import org.springframework.aop.MethodBeforeAdvice;

import java.lang.reflect.Method;

/**
 * @author benshaoye
 */
public class NowBeforeAdvice implements MethodBeforeAdvice {

    @Override
    public void before(Method method, Object[] args, Object target) {
        System.out.println(" 前置通知 ==> NowBeforeAdvice ...............");
    }
}
