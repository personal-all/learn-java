package com.moonsky.base.aop.demo07;

/**
 * @author benshaoye
 */
public class AccountRepositoryImpl implements AccountRepository {

    @Override
    public void save() {
        System.out.println("【save】 account .....................................");
    }

    @Override
    public void find() {
        System.out.println("【find】 account .....................................");
    }

    @Override
    public void update() {
        System.out.println("【update】 account .....................................");
    }

    @Override
    public void delete() {
        System.out.println("【delete】 account .....................................");
    }
}
