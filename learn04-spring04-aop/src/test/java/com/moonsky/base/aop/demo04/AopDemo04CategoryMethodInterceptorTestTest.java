package com.moonsky.base.aop.demo04;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

/**
 * @author benshaoye
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:aop.demo04.application.xml")
public class AopDemo04CategoryMethodInterceptorTestTest {

    /**
     * 源
     */
    @Autowired
    private CategoryDao categoryDao;

    /**
     * 一般切面代理
     */
    @Autowired
    @Qualifier("categoryDaoProxy")
    private CategoryDao categoryDaoProxy;
    /**
     * 一般切面代理 的另一种注入方式
     */
    @Resource(name = "categoryDaoProxy")
    private CategoryDao categoryDaoProxy0;

    /**
     * 自定义切入点的代理（全代理）
     */
    @Resource(name = "categoryDaoRegexpProxy0")
    private CategoryDao categoryDaoRegexpProxy0;

    /**
     * 自定义切入点（只拦截 save 方法）
     */
    @Resource(name = "categoryDaoRegexpProxy1")
    private CategoryDao categoryDaoRegexpProxy1;

    /**
     * 自定义切入点（拦截多个自定义方法）
     */
    @Resource(name = "categoryDaoRegexpProxy2")
    private CategoryDao categoryDaoRegexpProxy2;

    private void setSeparator(Object target) {
        String str = "########################################################################################";
        System.out.println("\033[031m");
        System.out.println(str + str);
        System.out.print("\033[038m");
        System.out.println("#");
        System.out.println("# " + target.getClass());
        System.out.println("#\n");
    }

    private void executeDemo(CategoryDao dao) {
        setSeparator(dao);
        dao.save();
        dao.find();
        dao.update();
        dao.delete();
    }

    @Test
    public void testName() {
        Assert.assertTrue(categoryDaoProxy0 == categoryDaoProxy);

        executeDemo(categoryDao);

        executeDemo(categoryDaoProxy);

        executeDemo(categoryDaoProxy0);

        executeDemo(categoryDaoRegexpProxy0);

        executeDemo(categoryDaoRegexpProxy1);

        executeDemo(categoryDaoRegexpProxy2);
    }
}