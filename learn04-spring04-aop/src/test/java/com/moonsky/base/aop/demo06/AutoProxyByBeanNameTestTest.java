package com.moonsky.base.aop.demo06;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

/**
 * @author benshaoye
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:aop.demo06.application.xml")
public class AutoProxyByBeanNameTestTest {

    private final static String STR = "===============================================================================";

    @Autowired
    private ProductDao productDao;
    @Autowired
    private EmployeeService employeeService;

    @Test
    public void testName() {
        productDao.save();
        System.out.println();
        productDao.find();
        System.out.println();
        productDao.update();
        System.out.println();
        productDao.delete();
        System.out.println();

        String str = STR + STR;
        System.out.println();
        System.out.println(str);
        System.out.println(str);
        System.out.println();

        employeeService.save();
        System.out.println();
        employeeService.find();
        System.out.println();
        employeeService.update();
        System.out.println();
        employeeService.delete();
        System.out.println();
    }
}