package com.moonsky.base.aop.demo03;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

/**
 * @author benshaoye
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:aop.demo03.application.xml")
public class AopDemo03EmployeeBeforeAdviceTestTest {

    @Resource(name = "employeeDao")
    private EmployeeDao employeeDao;
    @Resource(name = "employeeDaoJdkProxy")
    private EmployeeDao employeeDaoJdkProxy;
    @Resource(name = "employeeDaoCglibProxy1")
    private EmployeeDao employeeDaoCglibProxy1;
    @Resource(name = "employeeDaoCglibProxy2")
    private EmployeeDao employeeDaoCglibProxy2;

    @Test
    public void testName() {
        System.out.println(employeeDao.getClass());
        employeeDao.save();
        employeeDao.find();
        System.out.println("+++++++++++++++++++++++++++++++++++++++++++++");
        System.out.println(employeeDaoJdkProxy.getClass());
        employeeDaoJdkProxy.save();
        employeeDaoJdkProxy.find();
        System.out.println("+++++++++++++++++++++++++++++++++++++++++++++");
        System.out.println(employeeDaoCglibProxy1.getClass());
        employeeDaoCglibProxy1.save();
        employeeDaoCglibProxy1.find();
        System.out.println("+++++++++++++++++++++++++++++++++++++++++++++");
        System.out.println(employeeDaoCglibProxy2.getClass());
        employeeDaoCglibProxy2.save();
        employeeDaoCglibProxy2.find();
    }
}