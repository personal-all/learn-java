package com.moonsky.base.aop.demo05;

import org.junit.Test;
import org.springframework.aop.aspectj.annotation.AnnotationAwareAspectJAutoProxyCreator;
import org.springframework.aop.framework.autoproxy.BeanNameAutoProxyCreator;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;

/**
 * 在 demo 04 中，创建代理的方式
 * 每种规则都需要单独创建规则器，
 * 更重要的是每个需要代理的类都需要单独写一个 bean ，当大量 bean 需要配置的时候，将是一项繁重的工作
 * <p>
 * 本节主要就演示自动创建代理：
 * -  1、{@link BeanNameAutoProxyCreator}：基于 bean 名称的自动代理创建器
 * -  2、{@link DefaultAdvisorAutoProxyCreator}：根据 Advisor 本身信息创建代理的创建器
 * -  3、{@link AnnotationAwareAspectJAutoProxyCreator}：基于 bean 中的 AspectJ 注解创建代理
 *
 * @author benshaoye
 */
public class AopDemo05ProxyDemoTestTest {

    @Test
    public void testName() {
        ProxyDemo demo = new ProxyDemo() {};
    }
}