package com.moonsky.base.aop.demo01;

import org.junit.Test;
import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;

import java.lang.reflect.Proxy;

/**
 * @author benshaoye
 */
public class AopDemo01StudentDaoImplTestTest {

    @Test
    public void testTraditional() {
        StudentDao dao = new StudentDaoImpl();

        dao.save();
        dao.find();
        dao.update();
        dao.delete();
    }

    @Test
    public void testJdkProxy() {
        StudentDao dao = new StudentDaoImpl();

        Object target = dao;
        Class type = target.getClass();
        ClassLoader loader = type.getClassLoader();
        Class[] interfaces = type.getInterfaces();

        StudentDao studentDao = (StudentDao) Proxy.newProxyInstance(loader, interfaces, ((proxy, method, args) -> {
            if ("save".equals(method.getName())){
                System.out.println("权限校验");
            }
            return method.invoke(target, args);
        }));

        dao.save();
        dao.delete();

        System.out.println("=================================");
        studentDao.save();
        studentDao.delete();
    }

    @Test
    public void testCglibProxy() {
        StudentDao dao = new StudentDaoImpl();


        Object target = dao;
        Class type = target.getClass();

        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(type);
        enhancer.setCallback((MethodInterceptor) (o, method, objects, methodProxy) -> {
            if ("save".equals(method.getName())){
                System.out.println("Cglib ");
            }
            return methodProxy.invoke(target, objects);
        });
        StudentDao studentDao = (StudentDao) enhancer.create();

        dao.save();
        dao.delete();

        System.out.println("=================================");
        studentDao.save();
        studentDao.delete();
    }
}