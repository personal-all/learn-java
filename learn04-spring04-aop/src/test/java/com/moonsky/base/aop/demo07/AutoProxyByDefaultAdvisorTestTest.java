package com.moonsky.base.aop.demo07;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

/**
 * @author benshaoye
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:aop.demo07.application.xml")
public class AutoProxyByDefaultAdvisorTestTest {
    private final static String STR = "===============================================================================";
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private EnterpriseService enterpriseService;

    @Test
    public void testName() {
        accountRepository.save();
        System.out.println();
        accountRepository.find();
        System.out.println();
        accountRepository.update();
        System.out.println();
        accountRepository.delete();
        System.out.println();

        String str = STR + STR;
        System.out.println();
        System.out.println(str);
        System.out.println(str);
        System.out.println();

        enterpriseService.save();
        System.out.println();
        enterpriseService.find();
        System.out.println();
        enterpriseService.update();
        System.out.println();
        enterpriseService.delete();
        System.out.println();
    }
}