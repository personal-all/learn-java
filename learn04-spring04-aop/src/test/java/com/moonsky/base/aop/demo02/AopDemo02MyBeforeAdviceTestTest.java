package com.moonsky.base.aop.demo02;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

/**
 * @author benshaoye
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:aop.demo02.application.xml")
public class AopDemo02MyBeforeAdviceTestTest {

    @Resource(name = "productDao")
    private ProductDao productDao;
    @Resource(name = "productDaoProxy")
    private ProductDao productDaoProxy;

    @Test
    public void testName() {
        productDao.save();
        productDao.find();
        System.out.println("== 分隔符 ===================================");
        productDaoProxy.save();
        productDaoProxy.find();
    }
}