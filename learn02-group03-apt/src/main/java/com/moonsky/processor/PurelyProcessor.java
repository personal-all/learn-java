package com.moonsky.processor;

import com.google.auto.service.AutoService;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Filer;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import java.util.Set;

import static com.google.common.collect.Sets.newHashSet;
import static java.util.stream.Collectors.toSet;
import static javax.lang.model.SourceVersion.latestSupported;

/**
 * @author benshaoye
 */
@AutoService(AbstractProcessor.class)
public class PurelyProcessor extends AbstractProcessor {

    protected final static Class[] SUPPORTED_TYPES = {
        AutoService.class
    };

    public PurelyProcessor() { }

    protected ProcessingEnvironment getProcessing() { return processingEnv; }

    protected Elements getElements() { return getProcessing().getElementUtils(); }

    protected Filer getFiler() { return getProcessing().getFiler(); }

    protected Types getTypes() { return getProcessing().getTypeUtils(); }

    @Override
    public boolean process(
        Set<? extends TypeElement> annotations, RoundEnvironment env
    ) {
        System.out.println("====================");
        return false;
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        return newHashSet(SUPPORTED_TYPES).stream().map(Class::getName).collect(toSet());
    }

    @Override
    public SourceVersion getSupportedSourceVersion() { return latestSupported(); }
}
