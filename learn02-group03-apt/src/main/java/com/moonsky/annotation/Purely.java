package com.moonsky.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author benshaoye
 */
@Retention(RetentionPolicy.SOURCE)
public @interface Purely {}
