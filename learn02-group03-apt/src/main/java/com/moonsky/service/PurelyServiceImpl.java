package com.moonsky.service;

import com.moonsky.annotation.Purely;

/**
 * @author benshaoye
 */
public class PurelyServiceImpl implements PurelyService{

    @Override
    public void sayHello() {
        System.out.println("Hello World.");
    }
}
