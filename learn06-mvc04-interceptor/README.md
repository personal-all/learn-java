### Spring MVC 拦截器 interceptor 使用说明
拦截器要实现接口：`org.springframework.web.servlet.HandlerInterceptor`，
由于 Java 1.7 之前接口不能有实现，所以在通常也是通过继承适配器：`org.springframework.web.servlet.handler.HandlerInterceptorAdapter`

### 异步拦截器
实现异步拦截器要实现接口：`org.springframework.web.servlet.AsyncHandlerInterceptor`

### spring-mvc.xml 中拦截器配置方式
```xml
<beans xmlns="http://www.springframework.org/schema/beans"
  xmlns:mvc="http://www.springframework.org/schema/mvc"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="
    http://www.springframework.org/schema/beans
    http://www.springframework.org/schema/beans/spring-beans.xsd
    http://www.springframework.org/schema/mvc
    http://www.springframework.org/schema/mvc/spring-mvc.xsd">
  
  <mvc:interceptors>
    <mvc:interceptor>
      <!-- 需要拦截的路径 -->
      <mvc:mapping path="/api/**"/>
      <mvc:mapping path="/admin/**"/>
      <!-- 不拦截的路径 -->
      <mvc:exclude-mapping path="/api/this-excludes/**"/>
      <mvc:exclude-mapping path="/api/that-excludes/**"/>
      <!-- 处理器，一个拦截器只能配置一个处理器 -->
      <bean class="xxx.xxxx.xxx.CustomInterceptor"/>
    </mvc:interceptor>
  </mvc:interceptors>
  
</beans>
```
##### 拦截器路径的通配符
|符号|说明|
|---|----|
| ?  | 匹配单个字符 |
| *  | 匹配 0 到多个字符，但不包括“/”符号 |
/ ** | 匹配多级目录 |