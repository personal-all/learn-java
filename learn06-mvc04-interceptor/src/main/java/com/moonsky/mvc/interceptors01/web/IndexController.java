package com.moonsky.mvc.interceptors01.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

/**
 * @author benshaoye
 */
@Controller
public class IndexController {

    @RequestMapping("index")
    public String index(Map map) {
        System.out.println(String.format("【Controller】 on %s#%s", getClass(), "index"));
        return "home";
    }
}
