package com.moonsky.mvc.interceptors01.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

/**
 * @author benshaoye
 */
@Controller
@RequestMapping("view")
public class ViewController {

    @RequestMapping("first")
    public String first(Map map) {
        System.out.println(String.format("【Controller】 on %s#%s", getClass(), "first"));
        return "home";
    }

    @RequestMapping("second")
    public String second(Map map) {
        System.out.println(String.format("【Controller】 on %s#%s", getClass(), "first"));
        return "home";
    }
}
