package com.moonsky.mvc.interceptors01.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author benshaoye
 */
@Controller
@RequestMapping("api")
public class InterceptorController {

    @RequestMapping("first")
    public String first(ModelMap model) {
        System.out.println(String.format("【Controller】 on %s#%s", getClass(), "first"));
        return "home";
    }

    @RequestMapping("second")
    public String second(ModelMap model) {
        System.out.println(String.format("【Controller】 on %s#%s", getClass(), "second"));
        return "home";
    }
}
