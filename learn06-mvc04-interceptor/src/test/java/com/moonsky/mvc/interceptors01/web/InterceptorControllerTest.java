package com.moonsky.mvc.interceptors01.web;

import com.moonsky.mvc.interceptors01.interceptor.AuthorizationInterceptor;
import com.moonsky.mvc.interceptors01.interceptor.DefaultInterceptor;
import com.moonsky.mvc.interceptors01.interceptor.FirstInterceptor;
import com.moonsky.mvc.interceptors01.interceptor.SecondInterceptor;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.handler.ConversionServiceExposingInterceptor;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * @author benshaoye
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration("classpath:spring-mvc-interceptor01.xml")
@WebAppConfiguration
public class InterceptorControllerTest {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    @Test
    void testFirst() throws Exception {
        MockHttpServletRequestBuilder request = get("/api/first");

        ResultActions actions = mockMvc.perform(request);

        actions.andExpect(status().isOk());

        actions.andExpect(view().name("home"));

        actions.andExpect(handler().handlerType(InterceptorController.class));
        actions.andExpect(handler().methodName("first"));

        actions.andDo(result -> {
            HandlerInterceptor[] interceptors = result.getInterceptors();

            assertNotNull(interceptors);
            assertEquals(3, interceptors.length);
            assertEquals(ConversionServiceExposingInterceptor.class, interceptors[0].getClass());
            assertEquals(AuthorizationInterceptor.class, interceptors[1].getClass());
            assertEquals(FirstInterceptor.class, interceptors[2].getClass());

            ConversionServiceExposingInterceptor dft = (ConversionServiceExposingInterceptor) interceptors[0];

            MatcherAssert.assertThat(dft, CoreMatchers.isA(ConversionServiceExposingInterceptor.class));
        });
    }

    @Test
    void testSecond() throws Exception {
        MockHttpServletRequestBuilder request = get("/api/second");

        ResultActions actions = mockMvc.perform(request);

        actions.andExpect(status().isOk());

        actions.andExpect(view().name("home"));

        actions.andExpect(handler().handlerType(InterceptorController.class));
        actions.andExpect(handler().methodName("second"));

        actions.andDo(result -> {
            HandlerInterceptor[] interceptors = result.getInterceptors();
            assertNotNull(interceptors);
            assertEquals(4, interceptors.length);
            assertEquals(ConversionServiceExposingInterceptor.class, interceptors[0].getClass());
            assertEquals(DefaultInterceptor.class, interceptors[1].getClass());
            assertEquals(AuthorizationInterceptor.class, interceptors[2].getClass());
            assertEquals(SecondInterceptor.class, interceptors[3].getClass());
        });
    }
}
