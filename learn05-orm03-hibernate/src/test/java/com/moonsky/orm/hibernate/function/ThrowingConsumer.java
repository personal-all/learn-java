package com.moonsky.orm.hibernate.function;

/**
 * @author benshaoye
 */
public interface ThrowingConsumer<T> {
    void accept(T t) throws Throwable;
}
