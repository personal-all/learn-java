package com.moonsky.orm.hibernate.annotation.embeddedid;

import com.moonsky.orm.hibernate.HibernateBaseTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * @author benshaoye
 */
class UnionStudentEntityTestTest extends HibernateBaseTest {

    @BeforeEach
    void setUp() {
        initializeFactory(UnionStudentEntity.class);
    }

    @AfterEach
    void tearDown() {
        destroy();
    }

    @Test
    void testTableStructure() {
        String tableName = getTableName(UnionStudentEntity.class);
        assertExistsTable(tableName);
        assertTableColumnsCount(tableName, 3);
        assertTableColumnTypeLike(tableName, "cert_no", "varchar(255)");
        assertTableColumnTypeLike(tableName, "serial_value", "varchar(255)");
        assertTableColumnTypeLike(tableName, "student_name", "varchar(255)");
    }
}