package com.moonsky.orm.hibernate.annotation.relation.m2o;

import com.moonsky.orm.hibernate.HibernateBaseTest;
import org.junit.jupiter.api.*;

import static com.moon.core.util.RandomStringUtil.nextUpper;

/**
 * @author benshaoye
 */
class M2oEnterpriseEntityTestTest extends HibernateBaseTest {

    @BeforeEach
    void setUp() {
        initializeFactory(M2oEnterpriseEntity.class,M2oEmployeeEntity.class);
    }

    @AfterEach
    void tearDown() {
        destroy();
    }

    M2oEnterpriseEntity randomEnterprise() {
        M2oEnterpriseEntity en = new M2oEnterpriseEntity();
        en.setEnterpriseName(nextUpper(14));
        return en;
    }

    M2oEmployeeEntity randomEmployee() {
        M2oEmployeeEntity emp = new M2oEmployeeEntity();
        emp.setEmployeeName(nextUpper(8));
        return emp;
    }

    @Test
    @DisplayName("使用完整“一方”对象")
    void testInsert0() throws Exception {
        M2oEnterpriseEntity enterprise = randomEnterprise();

        autoInsert(enterprise);

        M2oEmployeeEntity employee = randomEmployee();
        employee.setEnterprise(enterprise);
        autoInsert(employee);

        autoSession(session -> {
            System.out.println(">> 测试开始  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            M2oEmployeeEntity emp = session.get(M2oEmployeeEntity.class, employee.getId());
            System.out.println("===== 使用 ------------------------- enterprise ------->>>");
            Assertions.assertEquals(enterprise, emp.getEnterprise());
        });
    }

    @Test
    @DisplayName("使用只包含主键的“一方”对象")
    void testInsert1() throws Exception {
        M2oEnterpriseEntity enterprise = randomEnterprise();
        autoInsert(enterprise);


        M2oEnterpriseEntity enter = new M2oEnterpriseEntity();
        enter.setId(enterprise.getId());

        M2oEmployeeEntity employee = randomEmployee();
        employee.setEnterprise(enter);
        autoInsert(employee);

        autoSession(session -> {
            System.out.println(">> 测试开始  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            M2oEmployeeEntity emp = session.get(M2oEmployeeEntity.class, employee.getId());
            System.out.println("===== 使用 ------------------------- enterprise ------->>>");
            Assertions.assertEquals(enterprise, emp.getEnterprise());
        });
    }
}