package com.moonsky.orm.hibernate.annotation.strategy.auto.guid;

import com.moon.core.util.IteratorUtil;
import com.moon.core.util.RandomStringUtil;
import com.moonsky.orm.hibernate.HibernateBaseTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author benshaoye
 */
class AutoGuidEntityTestTest extends HibernateBaseTest {

    @BeforeEach
    void setUp() {
        initializeFactory(AutoGuidEntity.class);
    }

    @AfterEach
    void tearDown() {
        destroy();
    }

    AutoGuidEntity randomEntity() {
        AutoGuidEntity entity = new AutoGuidEntity();
        entity.setGuidName(RandomStringUtil.nextUpper(4, 8));
        return entity;
    }

    @Test
    void testInsertAutoGuid() throws Exception {
        List<AutoGuidEntity> list = new ArrayList<>();
        IteratorUtil.forEach(25, idx -> list.add(randomEntity()));

        autoInsert(list);
    }
}