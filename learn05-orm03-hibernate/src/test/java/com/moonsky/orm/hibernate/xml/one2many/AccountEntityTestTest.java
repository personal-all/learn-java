package com.moonsky.orm.hibernate.xml.one2many;

import com.moon.core.io.IOUtil;
import com.moon.core.lang.ref.FinalAccessor;
import com.moon.core.util.ListUtil;
import com.moon.core.util.SetUtil;
import com.moonsky.orm.hibernate.function.ThrowingConsumer;
import com.moonsky.orm.hibernate.function.ThrowingRunner;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;
import org.junit.jupiter.api.*;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Consumer;

import static com.moon.core.util.IteratorUtil.forEach;
import static com.moon.core.util.RandomStringUtil.*;
import static com.moon.core.util.RandomUtil.nextInt;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @author benshaoye
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class AccountEntityTestTest {

    private final static String propKey = "com.moonsky.orm.hibernate.entity.SecondaryUserEntityTest";
    private final static String cancelMatch = "^[\\d]{1}$";

    private Configuration config;
    private SessionFactory factory;

    @BeforeEach
    void setUp() {
        Configuration configuration = new Configuration();
        configuration.configure();
        configuration.addClass(AddressEntity.class);
        configuration.addClass(AccountEntity.class);
        SessionFactory sessionFactory = configuration.buildSessionFactory();

        this.config = configuration;
        this.factory = sessionFactory;
    }

    @AfterEach
    void tearDown() {
        IOUtil.close(factory);
        factory = null;
        config = null;
    }

    void autoSession(ThrowingConsumer<? super Session>... consumers) {
        for (ThrowingConsumer<? super Session> consumer : consumers) {
            try (Session session = factory.openSession()) {
                consumer.accept(session);
            } catch (Throwable t) {
                throw new IllegalStateException(t.getMessage(), t);
            }
        }
    }

    void autoTransaction(Session session, ThrowingRunner runner) {
        Transaction tx = session.beginTransaction();
        try {
            runner.run();
            tx.commit();
        } catch (Throwable t) {
            tx.rollback();
            throw new IllegalStateException(t.getMessage(), t);
        }
    }

    AddressEntity randomAddress() {
        AddressEntity address = new AddressEntity();
        address.setLocation(nextLetter(18, 48));
        address.setContactMobile("186" + nextDigit(8));
        address.setContactName(nextUpper(3, 8));
        return address;
    }

    AccountEntity randomAccount() {
        AccountEntity account = new AccountEntity();
        account.setUsername(nextUpper(4, 10));
        account.setPassword(nextLower(6, 18));
        account.setEmail(nextLower(2, 5) + nextDigit(5, 8) + "@163.com");
        account.setMobile("135" + nextDigit(8));

        int size = nextInt(2, 8);
        for (int i = 0; i < size; i++) {
            account.ensureAddressSet().add(randomAddress());
        }
        return account;
    }

    /**
     * 包含信息：“object references an unsaved transient instance”的异常{@link TransientObjectException}
     * 原因：
     * 1. 一对多关系中，应该先保存依赖方和被依赖方保存后，会将被依赖主键更新到对应依赖对象对应字段上去
     * > 如本例中当保存完所有 AddressEntity 和 AccountEntity 后，
     * 之后 hibernate 会将 AccountEntity 的主键 id 值用 update 语句更新到“tb_address”的“account_id"字段；
     * <br>hibernate 执行 update 的时机是关闭 session 或手动 flush 时；
     * <br>AddressEntity 和 AccountEntity 保存的先后顺序无要求；
     * <p>
     * 可根据打印语句判断 hibernate 执行时机
     *
     * @throws Exception
     */
    @Test
    @Order(0)
    @DisplayName("同一事务中同步保存【Account】和【Address】")
    void testInsertRows$InSameTx() throws Exception {
        FinalAccessor<AccountEntity> accessor = FinalAccessor.of();
        autoSession(session -> {
            List<AccountEntity> accountList = ListUtil.ofArrayList();
            forEach(nextInt(2, 6), index -> accountList.add(randomAccount()));
            autoTransaction(session, () -> {
                System.out.println("Starting            &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
                forEach(accountList, account -> {
                    // 保存 account 和 address 先后顺序无要求
                    forEach(account.ensureAddressSet(), address -> {
                        session.save(address);
                    });
                    System.out.println("====================================================");
                    session.save(account);
                    System.out.println("----------------------------------------------------");
                });
                System.out.println(">>>>>>> flush begin");
                session.flush();
                System.out.println(">>>>>>> flush complete");
            });
            accessor.set(accountList.get(0));
        }, session -> {
            System.out.println(" get AccountEntity 对象 ++++++++++++++++++++++++++++++++++++++");
            AccountEntity account = accessor.get();
            AccountEntity queried = session.get(AccountEntity.class, account.getId());
            /**
             * {@link AccountEntity#addressSet}默认是懒加载的，在需要太的时候才执行 select 查询语句；
             * 可在配置文件中 set 标签上设置 lazy="false" 即可立即加载
             */
            Set<AddressEntity> accSet = new TreeSet<>(Comparator.comparing(AddressEntity::getId));
            accSet.addAll(account.getAddressSet());
            account.setAddressSet(accSet);
            Set<AddressEntity> qurSet = new TreeSet<>(Comparator.comparing(AddressEntity::getId));
            qurSet.addAll(queried.getAddressSet());
            queried.setAddressSet(qurSet);
            assertEquals(account, queried);
        }, session -> {
            System.out.println(" get AccountEntity 手动关闭 session ++++++++++++++++++++++++++++++++++++++");
            AccountEntity account = accessor.get();
            AccountEntity queried = session.get(AccountEntity.class, account.getId());

            /**
             * 这里手动关闭 session 便将不会加载 addressSet
             *
             * 这时如果需要获取 addressSet 便会抛出异常：{@link LazyInitializationException}
             */
            IOUtil.close(session);

            assertThrows(Exception.class, () -> {
                try {
                    assertEquals(account, queried);
                } catch (Exception e) {
                    System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                    e.printStackTrace(System.out);
                    System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
                    throw new IllegalArgumentException(e);
                }
            });
        });
    }

    /**
     * 本例演示了“依赖方”和“被依赖方”可以在不同事务中保存，
     * 但此时要先保存“依赖方”，后保存“被依赖方”，
     * 被依赖方保存之后，hibernate 会记录需要更新的“依赖方”数据
     * 所以在保存 account 之前，要将 addressList set 上去；
     * <br>【只要在 flush 或关闭 session 之前 set 上去即可】
     *
     * @throws Exception
     */
    @Test
    @Order(1)
    @DisplayName("不同事务中保存【Account】和【Address】1")
    void testInsertRows$InDiffTx1() throws Exception {
        List<AddressEntity> addressList = ListUtil.ofArrayList();
        forEach(3, idx -> addressList.add(randomAddress()));
        autoSession(session -> autoTransaction(session,
            () -> forEach(addressList, (Consumer<? super AddressEntity>) session::save)));

        // 测试不同事务中保存依赖关系对象
        AccountEntity account = randomAccount();
        account.ensureAddressSet().clear();
        autoSession(session -> autoTransaction(session, () -> {
            session.save(account);
            /**
             *  flush 或关闭 session 之前将关联的 addressSet set 上去即可
             */
            account.setAddressSet(SetUtil.ofHashSet(addressList));
        }));
    }

    /**
     * 先保存被依赖方，后保存依赖方，此时不会自动建立依赖关系
     *
     * @throws Exception
     */
    @Test
    @Order(2)
    @DisplayName("不同事务中保存【Account】和【Address】2")
    void testInsertRows$InDiffTx2() throws Exception {
        AccountEntity account = randomAccount();
        account.ensureAddressSet().clear();
        autoSession(session -> autoTransaction(session, () -> {
            session.save(account);
        }));

        /**
         * 如果先保存 account（被依赖方）后保存 address（依赖方）
         *
         * 此时就只是普通保存，不会自动建立依赖关系
         */
        List<AddressEntity> addressList = ListUtil.ofArrayList();
        forEach(3, idx -> addressList.add(randomAddress()));
        autoSession(session -> autoTransaction(session,
            () -> forEach(addressList, (Consumer<? super AddressEntity>) session::save)));
        account.setAddressSet(SetUtil.ofHashSet(addressList));
    }

    @Test
    @Order(3)
    @DisplayName("不同事务中保存【Account】和【Address】3")
    void testInsertAndUpdateRows$InDiffTx3() throws Exception {
        AccountEntity account = randomAccount();
        account.ensureAddressSet().clear();

        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++");
        autoSession(session -> autoTransaction(session, () -> {
            session.save(account);
        }));

        /**
         * 如果先保存 account（被依赖方）后保存 address（依赖方）
         *
         * 此时就只是普通保存，不会自动建立依赖关系
         */
        List<AddressEntity> addressList = ListUtil.ofArrayList();
        forEach(3, idx -> addressList.add(randomAddress()));

        System.out.println("------------------------------------------------");
        autoSession(session -> autoTransaction(session,
            () -> forEach(addressList, (Consumer<? super AddressEntity>) session::save)));
        account.setAddressSet(SetUtil.ofHashSet(addressList));

        System.out.println("================================================");
        autoSession(session -> {
            // 如果这里不使用事务，将不会建立关联关系
            autoTransaction(session, () -> {
                /**
                 * 执行 update 语句时会先将“不关联的数据” account_id 设置为 null，然后在添加关联关系
                 */
                session.update(account);
            });
        });
    }

    @Test
    @Order(4)
    @DisplayName("不同事务中保存【Account】和【Address】4")
    void testInsertAndInsertRows$InDiffTx4() throws Exception {
        AccountEntity account = randomAccount();
        account.ensureAddressSet().clear();

        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++");
        autoSession(session -> autoTransaction(session, () -> {
            session.save(account);
        }));

        // 不会自动建立依赖关系
        List<AddressEntity> addressList = ListUtil.ofArrayList();
        forEach(3, idx -> addressList.add(randomAddress()));

        System.out.println("------------------------------------------------");
        autoSession(session -> autoTransaction(session,
            () -> forEach(addressList, (Consumer<? super AddressEntity>) session::save)));

        System.out.println("================================================");
        account.setAddressSet(SetUtil.ofHashSet(addressList));
        autoSession(session -> {
            autoTransaction(session, () -> {
                /**
                 * 与{@link AccountEntity#testInsertAndUpdateRows$InDiffTx3()}不同
                 *
                 * save 会新建一条 account 数据，然后在关联
                 */
                session.save(account);
            });
        });
    }
}