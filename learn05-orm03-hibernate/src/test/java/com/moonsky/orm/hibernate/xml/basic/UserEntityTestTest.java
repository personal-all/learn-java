package com.moonsky.orm.hibernate.xml.basic;

import com.moon.core.io.IOUtil;
import com.moon.core.lang.ref.FinalAccessor;
import com.moon.core.util.ListUtil;
import com.moon.core.util.MapperUtil;
import com.moon.core.util.runner.RunnerUtil;
import com.moonsky.orm.hibernate.function.ThrowingBiConsumer;
import com.moonsky.orm.hibernate.function.ThrowingConsumer;
import com.moonsky.orm.hibernate.function.ThrowingMultiConsumer;
import com.moonsky.orm.hibernate.function.ThrowingRunner;
import org.hibernate.IdentifierLoadAccess;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.boot.spi.XmlMappingBinderAccess;
import org.hibernate.cfg.Configuration;
import org.hibernate.dialect.*;
import org.hibernate.service.ServiceRegistry;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author benshaoye
 */
@DisplayName("测试：insert|load|get|find|byId")
class UserEntityTestTest {

    final static Function<String, UserEntity> converter = str -> {
        Map<String, String> data = (Map<String, String>) RunnerUtil.run(str);
        UserEntity user = new UserEntity();
        MapperUtil.override(data, user);
        return user;
    };

    final static Supplier<UserEntity> defaultSupplier = () -> {
        String primary
            = "{address:'北京天坛',email:'111@162.com',username:'username',password:'password',mobile:'13212321232'}";
        return converter.apply(primary);
    };

    private ApplicationContext context;
    private Configuration configuration;
    private SessionFactory factory;

    /**
     * 初始化数据库
     */
    void initializeDatabase() {
        context = new ClassPathXmlApplicationContext("classpath:spring-orm.xml");
    }

    @BeforeEach
    void setUp() {
    }

    void autoConfigured(ThrowingRunner... runners) {
        SessionFactory factory = null;
        try {
            Configuration config = configuration = new Configuration();
            config.configure().addClass(UserEntity.class);
            factory = this.factory = config.buildSessionFactory();
            for (ThrowingRunner runner : runners) {
                runner.run();
            }
        } catch (Throwable t) {
            throw new IllegalStateException(t.getMessage(), t);
        } finally {
            IOUtil.close(factory);
        }
    }

    void autoSessionAfterConfigured(ThrowingConsumer<Session>... consumers) {
        autoConfigured(() -> {
            for (ThrowingConsumer<Session> consumer : consumers) {
                try (Session session = factory.openSession()) {
                    consumer.accept(session);
                }
            }
        });
    }

    /**
     * Hibernate 初始化：
     * <p>
     * 文档：https://www.w3cschool.cn/hibernate/
     * <p>
     * 1. 创建{@link Configuration}对象；
     * 2. 调用{@link Configuration#configure(String)}传入配置文件（xxx.cfg.xml）路径，加载并完成配置
     * > 也可不传入路径，此时默认路径为：classpath:hibernate.cfg.xml
     * <p>
     * 3. 调用{@link Configuration#buildSessionFactory(ServiceRegistry)}方法，创建{@link SessionFactory}
     * > 这里的{@link ServiceRegistry}服务注册对象需要手动配置，也可不配置，
     * 此时会自动使用{@link StandardServiceRegistryBuilder}和{@link Configuration#getProperties()}
     * 创建一个 SessionFactory
     */
    void defaultHibernateSetUp() {
        // hibernate 配置对象
        Configuration config = new Configuration();

        /**
         * 默认读取 hibernate.cfg.xml 配置文件
         * {@link Configuration#configure()}
         * {@link StandardServiceRegistryBuilder#DEFAULT_CFG_RESOURCE_NAME}
         *
         * 也可手动指定配置文件：{@link Configuration#configure(String)}
         */
        config.configure();

        // 创建服务注册对象
        StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder();
        builder.applySettings(config.getProperties());
        ServiceRegistry registry = builder.build();

        /**
         * 执行这个{@link Configuration#addClass(Class)}同目录同文件名的 xxxx.hbm.xml 映射文件
         *
         * @see MetadataSources#addClass(Class)
         * @see MetadataSources#addResource(String)
         * @see XmlMappingBinderAccess#bind(String)
         */
        config.addClass(UserEntity.class);

        /**
         * 另外还有个：{@link Configuration#buildSessionFactory()}
         * 里面默认也是使用{@link StandardServiceRegistryBuilder}创建 ServiceRegistry
         * 进一步创建{@link org.hibernate.SessionFactory}
         */
        SessionFactory sessionFactory = config.buildSessionFactory(registry);
        this.factory = sessionFactory;
    }

    /**
     * 最简单的使用默认配置文件构建{@link Configuration}和{@link SessionFactory}
     * <p>
     * 结果和{@link #defaultHibernateSetUp()}得到的一样
     */
    void simpleConfiguration() {
        Configuration config = new Configuration();
        SessionFactory factory = config.configure().buildSessionFactory();
    }

    @AfterEach
    void tearDown() {
        if (factory != null && factory.isOpen()) {
            IOUtil.close(factory);
        }
    }

    /**
     * Hibernate 不同数据库方言:
     * <p>
     * 有些有多种版本的方言怎么区分呢？~~~ 我的办法是首先看名字和版本，不行然后就一个一个试
     * -------------------------------------------------------------------------------------------------------------
     * <p>
     * | 数据库	                    | 方言名                                       | 类指向                          |
     * |----------------------------|---------------------------------------------|--------------------------------|
     * | DB2	                    | org.hibernate.dialect.DB2Dialect            | {@link DB2Dialect}             |
     * | HSQLDB	                    | org.hibernate.dialect.HSQLDialect           | {@link HSQLDialect}            |
     * | HypersonicSQL              | org.hibernate.dialect.HSQLDialect           | {@link HSQLDialect}            |
     * | Informix                   | org.hibernate.dialect.InformixDialect       | {@link InformixDialect}        |
     * |                            | org.hibernate.dialect.Informix10Dialect     | {@link Informix10Dialect}      |
     * | Ingres                     | org.hibernate.dialect.IngresDialect         | {@link IngresDialect}          |
     * |                            | org.hibernate.dialect.Ingres9Dialect        | {@link Ingres9Dialect}         |
     * |                            | org.hibernate.dialect.Ingres10Dialect       | {@link Ingres10Dialect}        |
     * | Interbase                  | org.hibernate.dialect.InterbaseDialect      | {@link InterbaseDialect}       |
     * | Microsoft SQL Server 2000  | org.hibernate.dialect.SQLServerDialect      | {@link SQLServerDialect}       |
     * | Microsoft SQL Server 2005  | org.hibernate.dialect.SQLServer2005Dialect  | {@link SQLServer2005Dialect}   |
     * | Microsoft SQL Server 2008  | org.hibernate.dialect.SQLServer2008Dialect  | {@link SQLServer2008Dialect}   |
     * | MySQL                      | org.hibernate.dialect.MySQLDialect          | {@link MySQLDialect}           |
     * |                            | org.hibernate.dialect.MySQL5Dialect         | {@link MySQL5Dialect}          |
     * |                            | org.hibernate.dialect.MySQL5InnoDBDialect   | {@link MySQL5InnoDBDialect}    |
     * |                            | org.hibernate.dialect.MySQL55Dialect        | {@link MySQL55Dialect}         |
     * |                            | org.hibernate.dialect.MySQL57Dialect        | {@link MySQL57Dialect}         |
     * |                            | org.hibernate.dialect.MySQL57InnoDBDialect  | {@link MySQL57InnoDBDialect}   |
     * |                            | org.hibernate.dialect.MySQL8Dialect         | {@link MySQL8Dialect}          |
     * | Oracle (any version)       | org.hibernate.dialect.OracleDialect         | {@link OracleDialect}          |
     * | Oracle 8i                  | org.hibernate.dialect.Oracle8iDialect       | {@link Oracle8iDialect}        |
     * | Oracle 9i                  | org.hibernate.dialect.Oracle9iDialect       | {@link Oracle9iDialect}        |
     * | Oracle 10g                 | org.hibernate.dialect.Oracle10gDialect      | {@link Oracle10gDialect}       |
     * | Oracle 11g                 | org.hibernate.dialect.Oracle10gDialect      | {@link Oracle10gDialect}       |
     * | Oracle 12c                 | org.hibernate.dialect.Oracle12cDialect      | {@link Oracle12cDialect}       |
     * | PostgreSQL                 | org.hibernate.dialect.PostgreSQL81Dialect   | {@link PostgreSQL81Dialect}    |
     * |                            | org.hibernate.dialect.PostgreSQL82Dialect   | {@link PostgreSQL82Dialect}    |
     * |                            | org.hibernate.dialect.PostgreSQL9Dialect    | {@link PostgreSQL9Dialect}     |
     * |                            | org.hibernate.dialect.PostgreSQL91Dialect   | {@link PostgreSQL91Dialect}    |
     * |                            | org.hibernate.dialect.PostgreSQL92Dialect   | {@link PostgreSQL92Dialect}    |
     * |                            | org.hibernate.dialect.PostgreSQL93Dialect   | {@link PostgreSQL93Dialect}    |
     * |                            | org.hibernate.dialect.PostgreSQL94Dialect   | {@link PostgreSQL94Dialect}    |
     * |                            | org.hibernate.dialect.PostgreSQL95Dialect   | {@link PostgreSQL95Dialect}    |
     * |                            | org.hibernate.dialect.PostgreSQL10Dialect   | {@link PostgreSQL10Dialect}    |
     * | Progress                   | org.hibernate.dialect.ProgressDialect       | {@link ProgressDialect}        |
     * | SAP DB                     | org.hibernate.dialect.SAPDBDialect          | {@link SAPDBDialect}           |
     * | Sybase                     | org.hibernate.dialect.SybaseDialect         | {@link SybaseDialect}          |
     * |                            | org.hibernate.dialect.Sybase11Dialect       | {@link Sybase11Dialect}        |
     * | Sybase Anywhere            | org.hibernate.dialect.SybaseAnywhereDialect | {@link SybaseAnywhereDialect}  |
     * <p>
     * -------------------------------------------------------------------------------------------------------------
     * <p>
     * - 排坑01：Unable to create requested service [org.hibernate.engine.jdbc.env.spi.JdbcEnvironment]
     * > 数据库方言设置错了；[参考: https://blog.csdn.net/xiaozhegaa/article/details/69230127](https://blog.csdn.net/xiaozhegaa/article/details/69230127)
     * <p>
     * - Unknown entity: com.moonsky.orm.hibernate.entity.UserEntity
     * > 需要调用{@link Configuration#addClass(Class)}注册目标类，如：
     * <br>config.addClass(UserEntity.class);
     * <p>
     * - Hibernate 实体映射文件必须是同目录下的同名 xxxx.hbm.xml 文件，
     * 否则在执行{@link Configuration#addClass(Class)}的时候会报错：Unknown Mapping
     * <p>
     * - Hibernate 自动创建表时，如果实体类型是 String ，生成的自增主键是 varchar 类型的；
     * 这样的语句数据库不能执行成功，自增主要求是 int 类型的
     * <p>
     * - 【新增和修改】数据的时候必须包裹在一个 hibernate 事务中执行，否则失败
     * > 但不同数据库方言（或版本）下表现并不一致，我在 MySQL5.6 中插入成功，但在 MySQL8 插入失败，所以还是按 Hibernate 官方建议使用事务
     * <p>
     * - 关于数据库版本和引擎：
     * ```
     * SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL
     * server version
     * ```
     * > MySQL 5 及以下数据库使用默认引擎是 MyISAM，如需使用 InnoDB 引擎需指定带 InnoDB 方言的引擎；
     * <br>MySQL 8 默认 InnoDB 引擎，此时不需要指定
     * ```
     * -- 这是使用{@link MySQLDialect}未指定引擎生成的建表语句
     * create table tb_user (
     * id integer not null auto_increment,
     * username varchar(255),
     * password varchar(255),
     * mobile varchar(255),
     * email varchar(255),
     * address varchar(255),
     * primary key (id)
     * ) type=MyISAM
     * <p>
     * -- 这是使用{@link MySQL5Dialect}默认 MyISAM 引擎生成的建表语句
     * create table tb_user (
     * id integer not null auto_increment,
     * username varchar(255),
     * password varchar(255),
     * mobile varchar(255),
     * email varchar(255),
     * address varchar(255),
     * primary key (id)
     * ) engine=MyISAM
     * <p>
     * -- 这是使用{@link MySQL5InnoDBDialect}指定 InnoDB 引擎生成的建表语句
     * create table tb_user (
     * id integer not null auto_increment,
     * username varchar(255),
     * password varchar(255),
     * mobile varchar(255),
     * email varchar(255),
     * address varchar(255),
     * primary key (id)
     * ) engine=InnoDB
     * ```
     *
     * @throws Exception
     */
    @Test
    @DisplayName("插入数据")
    void testInsertAnUserAccount() throws Exception {
        FinalAccessor<Integer> userId = FinalAccessor.of();
        autoSessionAfterConfigured(session -> {
            UserEntity user = defaultSupplier.get();

            // 在事务中执行插入数据
            Transaction transaction = session.beginTransaction();
            // 返回主键
            session.save(user);
            transaction.commit();

            assertNotNull(user.getId());
            userId.set(user.getId());
        }, session -> {
            UserEntity emptyUser = defaultSupplier.get();

            // 查询不用在事务中执行
            UserEntity dbUser = session.load(UserEntity.class, userId.get());

            assertNotNull(dbUser);
            assertEquals(userId.get(), dbUser.getId());
            assertEquals(dbUser.getAddress(), emptyUser.getAddress());
            assertEquals(dbUser.getEmail(), emptyUser.getEmail());
            assertEquals(dbUser.getMobile(), emptyUser.getMobile());
            assertEquals(dbUser.getPassword(), emptyUser.getPassword());
            assertEquals(dbUser.getUsername(), emptyUser.getUsername());
        });
    }

    void autoSessionAfterInsert(final int count, ThrowingBiConsumer<Session, UserEntity>... consumers) {
        FinalAccessor<Map<Integer, UserEntity>> accessors = FinalAccessor.of(new HashMap<>());
        List<ThrowingConsumer<Session>> consumerList = Stream.of(consumers)
            .map(consumer -> (ThrowingConsumer<Session>) session -> {
                Map<Integer, UserEntity> userMap = accessors.get();
                List<Integer> idList = ListUtil.ofArrayList(userMap.keySet());

                Integer uniqueId = idList.get(0);
                UserEntity current = userMap.get(uniqueId);
                consumer.accept(session, current);
            }).collect(Collectors.toList());
        consumerList.add(0, session -> {
            Transaction tx = session.beginTransaction();
            for (int i = 0; i < count; i++) {
                UserEntity user = defaultSupplier.get();
                // 返回主键
                session.save(user);
                accessors.get().put(user.getId(), user);
            }
            tx.commit();
        });
        autoSessionAfterConfigured(consumerList.toArray(new ThrowingConsumer[consumerList.size()]));
    }

    void autoSessionAfterInsert(
        final int count, ThrowingMultiConsumer<Session, List<UserEntity>, UserEntity>... consumers
    ) {
        FinalAccessor<Map<Integer, UserEntity>> accessors = FinalAccessor.of(new HashMap<>());
        List<ThrowingConsumer<Session>> consumerList = Stream.of(consumers)
            .map(consumer -> (ThrowingConsumer<Session>) session -> {
                Map<Integer, UserEntity> userMap = accessors.get();
                List<Integer> idList = ListUtil.ofArrayList(userMap.keySet());
                List<UserEntity> list = ListUtil.mapAsList(idList, id -> userMap.get(id));
                consumer.accept(session, list, list.get(0));
            }).collect(Collectors.toList());
        consumerList.add(0, session -> {
            Transaction tx = session.beginTransaction();
            for (int i = 0; i < count; i++) {
                UserEntity user = defaultSupplier.get();
                // 返回主键
                session.save(user);
                accessors.get().put(user.getId(), user);
            }
            tx.commit();
        });
        autoSessionAfterConfigured(consumerList.toArray(new ThrowingConsumer[consumerList.size()]));
    }

    /**
     * 同一个 session 会话，load 方法会使用缓存
     *
     * @param count
     *
     * @throws Exception
     * @see Session#load(Object, Serializable)
     */
    @DisplayName("测试 Session#load(..) 方法")
    @ParameterizedTest
    @ValueSource(ints = {5})
    void testSessionLoad(final int count) throws Exception {
        assertTrue(count > 0);
        autoSessionAfterInsert(1, (session, user) -> {
            // 执行 load 方法并不会执行 select 查询语句
            session.load(UserEntity.class, 2);
        });
        autoSessionAfterInsert(1, (session, user) -> {
            UserEntity selected = session.load(UserEntity.class, 2);
            /**
             * 由于 load 方法返回的是代理对象，只有在真正需要值的时候才会去加载值
             * 所以，load 方法查询不存在值的时候仍然返回一个代理对象
             */
            assertNotNull(selected);
            System.out.println(selected.getClass());
            /**
             * 使用 load 语句查询出来的对象的时候
             * 才会真正执行查询语句，并且要求一定有结果
             */
            assertThrows(Exception.class, () -> {
                System.out.println(selected);
            });
        });
        autoSessionAfterInsert(count, (session, first) -> {
            Integer uniqueId = first.getId();

            // 只执行一次查询语句（select...），返回 UserEntity 代理对象
            UserEntity user0 = session.load(UserEntity.class, uniqueId);
            UserEntity user1 = session.load(UserEntity.class, uniqueId);
            assertEquals(user0, first);
            assertSame(user0, user1);

            assertSame(first.getClass(), UserEntity.class);
            assertNotSame(user0.getClass(), UserEntity.class);
        });
    }

    /**
     * 同一个 session 会话，get 方法也会使用缓存
     * <p>
     * 不同的是：
     * - load 方法返回对象的代理对象实例
     * - get 方法返回对象的真实实例
     *
     * @param count
     *
     * @throws Exception
     * @see Session#get(Class, Serializable)
     */
    @DisplayName("测试 Session#get(..) 方法")
    @ParameterizedTest
    @ValueSource(ints = {5})
    void testSessionGet(final int count) throws Exception {
        assertTrue(count > 0);
        autoSessionAfterInsert(1, (session, user) -> {
            // 执行 get 方法就会立即执行 select 查询语句
            session.get(UserEntity.class, 2);
        });
        autoSessionAfterInsert(1, (session, user) -> {
            /**
             * get 一个不存在的对象返回 null
             * load 一个不存在的对象抛出异常
             */
            UserEntity selected = session.get(UserEntity.class, 2);
            assertNull(selected);
        });
        autoSessionAfterInsert(count, (session, first) -> {
            Integer uniqueId = first.getId();

            // 只执行一次查询语句（select...），返回 UserEntity 对象
            UserEntity user0 = session.get(UserEntity.class, uniqueId);
            UserEntity user1 = session.get(UserEntity.class, uniqueId);
            assertEquals(user0, first);
            assertSame(user0, user1);

            assertSame(first.getClass(), UserEntity.class);
            assertSame(user0.getClass(), UserEntity.class);
        });
    }

    /**
     * find 方法和 get 方法呈现结果基本一样
     *
     * @param count
     *
     * @throws Exception
     * @see Session#find(Class, Object)
     * @see #testSessionGet(int)
     */
    @DisplayName("测试 Session#find(..) 方法")
    @ParameterizedTest
    @ValueSource(ints = {5})
    void testSessionFind(final int count) throws Exception {
        assertTrue(count > 0);
        autoSessionAfterInsert(1, (session, user) -> {
            // 执行 find 方法会立即执行 select 查询语句
            session.find(UserEntity.class, 2);
        });
        autoSessionAfterInsert(1, (session, user) -> {
            /**
             * find 一个不存在的对象返回 null
             * get 一个不存在的对象返回 null
             * load 一个不存在的对象抛出异常
             */
            UserEntity selected = session.find(UserEntity.class, 2);
            assertNull(selected);
        });
        autoSessionAfterInsert(count, (session, first) -> {
            Integer uniqueId = first.getId();

            // 只执行一次查询语句（select...），返回 UserEntity 对象
            UserEntity user0 = session.find(UserEntity.class, uniqueId);
            UserEntity user1 = session.find(UserEntity.class, uniqueId);
            assertEquals(user0, first);
            assertSame(user0, user1);

            assertSame(first.getClass(), UserEntity.class);
            assertSame(user0.getClass(), UserEntity.class);
        });
    }

    /**
     * @param count
     *
     * @throws Exception
     */
    @DisplayName("IdentifierLoadAccess0")
    @ParameterizedTest
    @ValueSource(ints = 5)
    void testIdentifierLoadAccess0(final int count) throws Exception {
        assertTrue(count > 0);
        autoSessionAfterInsert(count, (session, first) -> {
            Integer uniqueId = first.getId();

            IdentifierLoadAccess<UserEntity> userAccess = session.byId(UserEntity.class);
            // getReference 不会立即执行 select 语句
            userAccess.getReference(uniqueId);
            // load 方法始终会立即执行查询
            userAccess.load(uniqueId);
        }, (session, first) -> {
            System.out.println("======================================================");
            IdentifierLoadAccess<UserEntity> userAccess = session.byId(UserEntity.class);
            /**
             * getReference 查询“数据池”中不存在的数据时会立即执行 select 语句
             * 和{@link Session#load(Object, Serializable)}方法一样，并不会返回 null
             * 但是会在第一次使用这个对象的时候验证数据是否存在（load 的查询时机更靠后）
             */
            UserEntity user = userAccess.getReference(count + 5);
            assertFalse(user == null);
            assertThrows(Exception.class, () -> System.out.println(user));
        }, (session, first) -> {
            System.out.println("======================================================");
            IdentifierLoadAccess<UserEntity> userAccess = session.byId(UserEntity.class);
            /**
             * {@link IdentifierLoadAccess#load(Serializable)}方法始终会立即执行查询
             * 和{@link Session#get(String, Serializable)} 方法的过程类似
             */
            assertNull(userAccess.load(count + 5));

            Optional<UserEntity> hotOptional = userAccess.loadOptional(count + 5);
            assertFalse(hotOptional.isPresent());
        });
    }

    /**
     * {@link IdentifierLoadAccess#getReference(Serializable)}不会立即执行 select 查询语句
     * 而是立即返回一个代理对象，真实数据在使用时执行查询语句，并拿到真实值
     *
     * @param count
     *
     * @throws Exception
     */
    @DisplayName("IdentifierLoadAccess#getReference")
    @ParameterizedTest
    @ValueSource(ints = 5)
    void testIdentifierLoadAccess1(final int count) throws Exception {
        assertTrue(count > 0);
        autoSessionAfterInsert(count, (session, first) -> {
            IdentifierLoadAccess<UserEntity> userAccess = session.byId(UserEntity.class);
            // getReference 不会立即执行 select 语句
            UserEntity user = userAccess.getReference(first.getId());
            assertFalse(user == null);
            System.out.println("++++++++++++++++++++> " + user.getClass());
            assertEquals(first, user);
            System.out.println(user);
        });
    }

    @DisplayName("IdentifierLoadAccess#load")
    @ParameterizedTest
    @ValueSource(ints = 5)
    void testIdentifierLoadAccess2(final int count) throws Exception {
        assertTrue(count > 0);
        autoSessionAfterInsert(count, (session, first) -> {
            IdentifierLoadAccess<UserEntity> userAccess = session.byId(UserEntity.class);
            // load 方法始终会立即执行查询
            UserEntity user = userAccess.load(first.getId());
            assertFalse(user == null);
            System.out.println("++++++++++++++++++++> " + user.getClass());
            System.out.println(user);
            assertEquals(first, user);
        });
    }

    @Test
    @DisplayName("基本数据验证")
    void testOverrideEntityByMap() {
        String primary = "{address:'北京天坛',email:'111@162.com',}";
        String secondary = "{username:'username',password:'password',mobile:'13212321232'}";
        Map<String, String> map0 = (Map<String, String>) RunnerUtil.run(primary);
        assertEquals(2, map0.size());
        assertEquals("北京天坛", map0.get("address"));
        assertEquals("111@162.com", map0.get("email"));

        Map<String, String> map1 = (Map<String, String>) RunnerUtil.run(secondary);
        assertEquals(3, map1.size());
        assertEquals("username", map1.get("username"));
        assertEquals("password", map1.get("password"));
        assertEquals("13212321232", map1.get("mobile"));

        Map<String, String> data = new HashMap<>(map0);
        data.putAll(map1);

        assertEquals(5, data.size());
        assertEquals("北京天坛", data.get("address"));
        assertEquals("111@162.com", data.get("email"));
        assertEquals("username", data.get("username"));
        assertEquals("password", data.get("password"));
        assertEquals("13212321232", data.get("mobile"));

        UserEntity user = new UserEntity();
        MapperUtil.override(data, user);

        assertNull(user.getId());
        assertEquals("北京天坛", user.getAddress());
        assertEquals("111@162.com", user.getEmail());
        assertEquals("username", user.getUsername());
        assertEquals("password", user.getPassword());
        assertEquals("13212321232", user.getMobile());
    }
}