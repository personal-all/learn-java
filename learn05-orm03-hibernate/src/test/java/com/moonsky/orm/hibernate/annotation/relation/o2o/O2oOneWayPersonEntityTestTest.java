package com.moonsky.orm.hibernate.annotation.relation.o2o;

import com.moon.core.lang.annotation.AnnotationUtil;
import com.moonsky.orm.hibernate.HibernateBaseTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.Entity;

/**
 * @author benshaoye
 */
class O2oOneWayPersonEntityTestTest extends HibernateBaseTest {

    @BeforeEach
    void setUp() {
        initializeFactory(O2oOneWayPersonEntity.class, O2oOneWayOriginLocationEntity.class);
    }

    @AfterEach
    void tearDown() {
        destroy();
    }

    @Test
    void testCreateTable() throws Exception {
        String personTableName = AnnotationUtil.get(O2oOneWayPersonEntity.class, Entity.class).name();
        assertExistsTable(personTableName);
        assertTableColumnsCount(personTableName, 3);
        assertTableColumnTypeLike(personTableName, "id", "int");
        assertTableColumnTypeLike(personTableName, "personName", "varchar");
        assertTableColumnTypeLike(personTableName, "location_id", "int");

        personTableName = AnnotationUtil.get(O2oOneWayOriginLocationEntity.class, Entity.class).name();
        assertExistsTable(personTableName);
        assertTableColumnsCount(personTableName, 2);
        assertTableColumnTypeLike(personTableName, "id", "int");
        assertTableColumnTypeLike(personTableName, "location", "varchar");
    }
}