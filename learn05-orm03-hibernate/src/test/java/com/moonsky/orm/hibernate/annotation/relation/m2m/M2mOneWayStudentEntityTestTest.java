package com.moonsky.orm.hibernate.annotation.relation.m2m;

import com.moon.core.lang.annotation.AnnotationUtil;
import com.moon.core.lang.reflect.FieldUtil;
import com.moon.core.util.IteratorUtil;
import com.moon.core.util.ListUtil;
import com.moon.core.util.RandomStringUtil;
import com.moon.core.util.SetUtil;
import com.moonsky.orm.hibernate.HibernateBaseTest;
import com.moonsky.orm.hibernate.annotation.BaseUuidEntity;
import org.hibernate.MultiIdentifierLoadAccess;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.Entity;
import javax.persistence.JoinTable;

import java.lang.reflect.Field;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.moon.core.lang.reflect.FieldUtil.getDeclaredField;
import static com.moon.core.util.IteratorUtil.forEach;
import static com.moon.core.util.ListUtil.ofArrayList;
import static com.moon.core.util.RandomStringUtil.nextLower;
import static com.moon.core.util.RandomStringUtil.nextUpper;
import static com.moon.core.util.SetUtil.ofHashSet;
import static com.moon.core.util.SetUtil.ofLinkedHashSet;
import static java.util.Collections.sort;
import static java.util.Comparator.comparing;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author benshaoye
 */
class M2mOneWayStudentEntityTestTest extends HibernateBaseTest {

    @BeforeEach
    void setUp() {
        initializeFactory(M2mOneWayStudentEntity.class, M2mOneWayTeacherEntity.class);
    }

    @AfterEach
    void tearDown() {
        destroy();
    }

    @Test
    void testAssertTableStructure() {
        String tableName = AnnotationUtil.get(M2mOneWayTeacherEntity.class, Entity.class).name();

        assertExistsTable(tableName);
        assertTableColumnsCount(tableName, 2);
        assertTableColumnTypeLike(tableName, "id", "varchar");
        assertTableColumnTypeLike(tableName, "teacher_name", "varchar");

        tableName = AnnotationUtil.get(M2mOneWayStudentEntity.class, Entity.class).name();

        assertExistsTable(tableName);
        assertTableColumnsCount(tableName, 2);
        assertTableColumnTypeLike(tableName, "id", "varchar");
        assertTableColumnTypeLike(tableName, "student_name", "varchar");

        Field field = getDeclaredField(M2mOneWayStudentEntity.class, "teacherSet");
        String relTableName = AnnotationUtil.get(field, JoinTable.class).name();

        assertExistsTable(relTableName);
        assertTableColumnsCount(relTableName, 2);
        assertTableColumnTypeLike(relTableName, "student_id", "varchar");
        assertTableColumnTypeLike(relTableName, "teacher_id", "varchar");
    }

    M2mOneWayStudentEntity randomStudent() {
        M2mOneWayStudentEntity student = new M2mOneWayStudentEntity();
        student.setStudentName(nextUpper(8));
        return student;
    }

    M2mOneWayTeacherEntity randomTeacher() {
        M2mOneWayTeacherEntity teacher = new M2mOneWayTeacherEntity();
        teacher.setTeacherName(nextLower(7));
        return teacher;
    }

    @Test
    void testInsertRows() {
        List<M2mOneWayStudentEntity> students = createList(4, () -> randomStudent());

        List<M2mOneWayTeacherEntity> teachers = createList(8, () -> randomTeacher());

        students.forEach(student -> student.setTeacherSet(ofLinkedHashSet(teachers)));
        teachers.forEach(teacher -> teacher.setStudentSet(ofLinkedHashSet(students)));

        autoInsert(teachers);

        autoInsert(students);

        autoSession(session -> {
            List<String> ids = teachers.stream().map(BaseUuidEntity::getId).collect(Collectors.toList());
            MultiIdentifierLoadAccess<M2mOneWayTeacherEntity> access = session
                .byMultipleIds(M2mOneWayTeacherEntity.class);
            List<M2mOneWayTeacherEntity> teacherList = access.multiLoad(ids);
            List<M2mOneWayTeacherEntity> srcTeachers = ofArrayList(teachers);
            sort(teacherList, comparing(BaseUuidEntity::getId));
            sort(srcTeachers, comparing(BaseUuidEntity::getId));
            forEach(teacherList, (teacher, index) -> {
                M2mOneWayTeacherEntity srcTeacher = srcTeachers.get(index);
                assertEquals(teacher.getTeacherName(), srcTeacher.getTeacherName());
                assertEquals(teacher.getId(), srcTeacher.getId());

                List<M2mOneWayStudentEntity> studentsSet = ofArrayList(teacher.getStudentSet());
                List<M2mOneWayStudentEntity> srcStudents = ofArrayList(srcTeacher.getStudentSet());

                studentsSet.sort(Comparator.comparing(BaseUuidEntity::getId));
                srcStudents.sort(Comparator.comparing(BaseUuidEntity::getId));
                forEach(studentsSet, (student, idx) -> {
                    M2mOneWayStudentEntity srcStudent = srcStudents.get(idx);
                    assertEquals(student.getStudentName(), srcStudent.getStudentName());
                    assertEquals(student.getId(), srcStudent.getId());
                });
            });
        });

        autoSession(session -> {
            List<String> ids = students.stream().map(BaseUuidEntity::getId).collect(Collectors.toList());
            MultiIdentifierLoadAccess<M2mOneWayStudentEntity> access = session
                .byMultipleIds(M2mOneWayStudentEntity.class);
            List<M2mOneWayStudentEntity> studentList = access.multiLoad(ids);
            List<M2mOneWayStudentEntity> srcStudents = ofArrayList(students);
            sort(studentList, comparing(BaseUuidEntity::getId));
            sort(srcStudents, comparing(BaseUuidEntity::getId));
            forEach(studentList, (student, index) -> {
                M2mOneWayStudentEntity srcStudent = srcStudents.get(index);
                assertEquals(student.getStudentName(), srcStudent.getStudentName());
                assertEquals(student.getId(), srcStudent.getId());

                List<M2mOneWayTeacherEntity> studentsSet = ofArrayList(student.getTeacherSet());
                List<M2mOneWayTeacherEntity> srcTeachers = ofArrayList(srcStudent.getTeacherSet());

                studentsSet.sort(Comparator.comparing(BaseUuidEntity::getId));
                srcTeachers.sort(Comparator.comparing(BaseUuidEntity::getId));
                forEach(studentsSet, (teacher, idx) -> {
                    M2mOneWayTeacherEntity srcTeacher = srcTeachers.get(idx);
                    assertEquals(teacher.getTeacherName(), srcTeacher.getTeacherName());
                    assertEquals(teacher.getId(), srcTeacher.getId());
                });
            });
        });
    }
}