package com.moonsky.orm.hibernate.annotation.strategy.table;

import com.moon.core.util.IteratorUtil;
import com.moonsky.orm.hibernate.HibernateBaseTest;
import org.junit.jupiter.api.*;

import java.util.ArrayList;
import java.util.List;

import static com.moon.core.util.RandomStringUtil.nextUpper;

/**
 * @author benshaoye
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class TableIdentityEntityTestTest extends HibernateBaseTest {

    @BeforeEach
    void setUp() {
        initializeFactory(TableIdentityEntity.class, TableIdentitySecondaryEntity.class);
    }

    @AfterEach
    void tearDown() {
        destroy();
    }

    TableIdentityEntity randomEntity() {
        TableIdentityEntity entity = new TableIdentityEntity();
        entity.setCurrentName(nextUpper(4, 8));
        return entity;
    }

    TableIdentitySecondaryEntity randomSecondary() {
        TableIdentitySecondaryEntity entity = new TableIdentitySecondaryEntity();
        entity.setCurrentName(nextUpper(4, 8));
        return entity;
    }

    void insertEntity() {

        TableIdentityEntity entity0 = randomEntity();
        TableIdentityEntity entity1 = randomEntity();
        TableIdentityEntity entity2 = randomEntity();
        TableIdentityEntity entity3 = randomEntity();
        TableIdentityEntity entity4 = randomEntity();
        TableIdentityEntity entity5 = randomEntity();
        TableIdentityEntity entity6 = randomEntity();
        TableIdentityEntity entity7 = randomEntity();

        autoSession(session -> {
            autoTransaction(session, () -> {
                System.out.println("===========================0");
                session.save(entity0);
                System.out.println("---------------------------1");
                session.save(entity1);
                System.out.println(">>>>>>                <<<<<<");
            });
            autoTransaction(session, () -> {
                System.out.println("===========================2");
                session.save(entity2);
                System.out.println("---------------------------3");
                session.save(entity3);
                System.out.println(">>>>>>                <<<<<<");
            });
        }, session -> {
            autoTransaction(session, () -> {
                System.out.println("===========================4");
                session.save(entity4);
                System.out.println("---------------------------5");
                session.save(entity5);
                System.out.println(">>>>>>                <<<<<<");
            });
        });
        autoSession(session -> {
            autoTransaction(session, () -> {
                System.out.println("===========================6");
                session.save(entity6);
                System.out.println("---------------------------7");
                session.save(entity7);
                System.out.println(">>>>>>                <<<<<<");
            });
        });
    }

    void insertSesondary() {

        TableIdentitySecondaryEntity entity0 = randomSecondary();
        TableIdentitySecondaryEntity entity1 = randomSecondary();
        TableIdentitySecondaryEntity entity2 = randomSecondary();
        TableIdentitySecondaryEntity entity3 = randomSecondary();
        TableIdentitySecondaryEntity entity4 = randomSecondary();
        TableIdentitySecondaryEntity entity5 = randomSecondary();
        TableIdentitySecondaryEntity entity6 = randomSecondary();
        TableIdentitySecondaryEntity entity7 = randomSecondary();

        autoSession(session -> {
            autoTransaction(session, () -> {
                System.out.println("===========================0");
                session.save(entity0);
                System.out.println("---------------------------1");
                session.save(entity1);
                System.out.println(">>>>>>                <<<<<<");
            });
            autoTransaction(session, () -> {
                System.out.println("===========================2");
                session.save(entity2);
                System.out.println("---------------------------3");
                session.save(entity3);
                System.out.println(">>>>>>                <<<<<<");
            });
        }, session -> {
            autoTransaction(session, () -> {
                System.out.println("===========================4");
                session.save(entity4);
                System.out.println("---------------------------5");
                session.save(entity5);
                System.out.println(">>>>>>                <<<<<<");
            });
        });
        autoSession(session -> {
            autoTransaction(session, () -> {
                System.out.println("===========================6");
                session.save(entity6);
                System.out.println("---------------------------7");
                session.save(entity7);
                System.out.println(">>>>>>                <<<<<<");
            });
        });
    }

    @Test
    @Order(1)
    @DisplayName("基本插入：save 不执行 insert，commit 时统一执行")
    void testInitTableId() {
        insertEntity();
        insertSesondary();
    }

    @Test
    @Order(0)
    @DisplayName("增长步长阈值切换时更新主键中间表新值")
    void testStepSwitch() {
        List<TableIdentityEntity> list = new ArrayList<>();
        IteratorUtil.forEach(120, idx -> list.add(randomEntity()));

        autoInsert(list);
    }
}