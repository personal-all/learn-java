package com.moonsky.orm.hibernate.annotation.basic;

import com.moon.core.lang.annotation.AnnotationUtil;
import com.moonsky.orm.hibernate.HibernateBaseTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.Entity;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author benshaoye
 */
class MultiIdEntityTestTest extends HibernateBaseTest {

    @BeforeEach
    void setUp() {
        initializeFactory(MultiIdEntity.class);
    }

    @AfterEach
    void tearDown() {
        destroy();
    }

    @Test
    void testTableStructure() {
        String tbName = AnnotationUtil.get(MultiIdEntity.class, Entity.class).name();
        assertExistsTable(tbName);
        assertTableColumnsCount(tbName, 4);
        assertTableColumnTypeLike(tbName, "id", "int");
        assertTableColumnTypeLike(tbName, "serialValue", "varchar(255)");
        assertTableColumnTypeLike(tbName, "address", "varchar(255)");
        assertTableColumnTypeLike(tbName, "accountName", "varchar(255)");
    }
}