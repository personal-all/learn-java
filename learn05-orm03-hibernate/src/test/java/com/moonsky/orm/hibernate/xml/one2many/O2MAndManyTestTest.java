package com.moonsky.orm.hibernate.xml.one2many;

import com.moon.core.io.IOUtil;
import com.moon.core.util.IteratorUtil;
import com.moonsky.orm.hibernate.function.ThrowingConsumer;
import com.moonsky.orm.hibernate.function.ThrowingRunner;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static com.moon.core.util.RandomStringUtil.nextUpper;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author benshaoye
 */
class O2MAndManyTestTest {

    private SessionFactory factory;

    @BeforeEach
    void setUp() {
        factory = new Configuration().addClass(O2MAndMany.class)
            .addClass(O2MAndOne.class)
            .configure()
            .buildSessionFactory();
    }

    @AfterEach
    void tearDown() {
        IOUtil.close(factory);
    }

    void autoSession(ThrowingConsumer<Session>... consumers) {
        for (ThrowingConsumer<? super Session> consumer : consumers) {
            try (Session session = factory.openSession()) {
                consumer.accept(session);
            } catch (Throwable t) {
                throw new IllegalStateException(t);
            }
        }
    }

    void autoTransaction(Session session, ThrowingRunner runner) {
        Transaction tx = session.beginTransaction();
        try {
            runner.run();
            tx.commit();
        } catch (Throwable t) {
            tx.rollback();
            throw new IllegalStateException(t);
        }
    }

    O2MAndMany randomMany() {
        O2MAndMany many = new O2MAndMany();
        many.setCurrentName(nextUpper(3, 8));
        return many;
    }

    O2MAndOne randomOne() {
        O2MAndOne many = new O2MAndOne();
        many.setCurrentName(nextUpper(3, 8));
        return many;
    }

    @Test
    void testInsertRows() {
        List<O2MAndMany> list = new ArrayList<>();
        O2MAndOne one = randomOne();
        autoSession(session -> {
            session.save(one);

            IteratorUtil.forEach(4, index -> {
                O2MAndMany many = randomMany();
                many.setOneId(one.getId());
                list.add(many);
                session.save(many);
            });
        });

        autoSession(session -> {
            String oneId = one.getId();
            O2MAndOne queried = session.get(O2MAndOne.class, oneId);

            assertNotNull(queried);
            assertEquals(oneId, queried.getId());
            assertEquals(one.getCurrentName(), queried.getCurrentName());

            Set<O2MAndMany> oneSet = new TreeSet<>(Comparator.comparing(O2MAndMany::getId));
            Set<O2MAndMany> queriedSet = new TreeSet<>(Comparator.comparing(O2MAndMany::getId));
            oneSet.addAll(list);
            queriedSet.addAll(queried.getSet());

            assertIterableEquals(oneSet, queriedSet);
        });
    }
}