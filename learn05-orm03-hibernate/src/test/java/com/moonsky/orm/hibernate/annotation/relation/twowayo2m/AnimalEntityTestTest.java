package com.moonsky.orm.hibernate.annotation.relation.twowayo2m;

import com.moon.core.lang.annotation.AnnotationUtil;
import com.moon.core.util.IteratorUtil;
import com.moon.core.util.RandomStringUtil;
import com.moon.core.util.SetUtil;
import com.moonsky.orm.hibernate.HibernateBaseTest;
import com.moonsky.orm.hibernate.annotation.BaseUuidEntity;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.persistence.Entity;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author benshaoye
 * @see AnimalEntity
 * @see BuffaloEntity
 * @see TwoWayGradeEntity 见注释
 * @see TwoWayStudentEntity
 */
@DisplayName("测试注解双向多对一“多方”维护关系")
class AnimalEntityTestTest extends HibernateBaseTest {

    @BeforeEach
    void setUp() {
        initializeFactory(AnimalEntity.class, BuffaloEntity.class);
    }

    @AfterEach
    void tearDown() {
        destroy();
    }

    AnimalEntity randomAnimal() {
        AnimalEntity animal = new AnimalEntity();
        animal.setAnimalName(RandomStringUtil.nextUpper(8));
        return animal;
    }

    BuffaloEntity randomBuffalo() {
        BuffaloEntity entity = new BuffaloEntity();
        entity.setBuffaloName(RandomStringUtil.nextLower(6));
        return entity;
    }

    @Test
    void testTableStructure() throws Exception {
        String tableName = AnnotationUtil.get(AnimalEntity.class, Entity.class).name();
        assertExistsTable(tableName);
        assertTableColumnsCount(tableName, 2);
        assertTableColumnTypeLike(tableName, "id", "varchar");
        assertTableColumnTypeLike(tableName, "animalName", "varchar");

        tableName = AnnotationUtil.get(BuffaloEntity.class, Entity.class).name();
        assertExistsTable(tableName);
        assertTableColumnsCount(tableName, 3);
        assertTableColumnTypeLike(tableName, "id", "varchar");
        assertTableColumnTypeLike(tableName, "buffaloName", "varchar");
        assertTableColumnTypeLike(tableName, "animal_id", "varchar");
    }

    @Test
    @DisplayName("测试多方维护关系:先保存一方")
    void testInsetOne() throws Exception {
        List<BuffaloEntity> multiList = createList(6, () -> randomBuffalo());

        AnimalEntity animal = randomAnimal();

        animal.setBuffaloList(multiList);
        IteratorUtil.forEach(multiList, buffalo -> {
            buffalo.setAnimal(animal);
        });

        autoInsert(animal);

        autoInsert(multiList);

        autoSession(session -> {
            AnimalEntity queried = session.get(AnimalEntity.class, animal.getId());
            assertEquals(queried, animal);
            assertEquals(queried.getAnimalName(), animal.getAnimalName());

            Set<BuffaloEntity> queriedSet = SetUtil.ofTreeSet(Comparator.comparing(BaseUuidEntity::getId),
                queried.getBuffaloList());
            Set<BuffaloEntity> multiSet = SetUtil.ofTreeSet(Comparator.comparing(BaseUuidEntity::getId), multiList);

            assertIterableEquals(multiSet, queriedSet);
        });
    }

    @Test
    @DisplayName("测试多方维护关系:先保存多方")
    void testInsetMulti() throws Exception {
        List<BuffaloEntity> multiList = createList(6, () -> randomBuffalo());

        AnimalEntity animal = randomAnimal();

        animal.setBuffaloList(multiList);
        IteratorUtil.forEach(multiList, buffalo -> {
            buffalo.setAnimal(animal);
        });

        assertThrows(Exception.class, () -> {
            autoInsert(multiList);
        });
    }
}