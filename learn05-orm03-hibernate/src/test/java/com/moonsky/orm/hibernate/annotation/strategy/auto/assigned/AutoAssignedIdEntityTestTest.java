package com.moonsky.orm.hibernate.annotation.strategy.auto.assigned;

import com.moonsky.orm.hibernate.HibernateBaseTest;
import org.hibernate.SessionFactory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static com.moon.core.util.RandomStringUtil.nextUpper;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotSame;

/**
 * @author benshaoye
 */
class AutoAssignedIdEntityTestTest extends HibernateBaseTest {

    @BeforeEach
    void setUp() {
        initializeFactory(AutoAssignedIdEntity.class);
    }

    @AfterEach
    void tearDown() {
        destroy();
    }

    @ParameterizedTest
    @ValueSource(strings = {"12", "34"})
    void testInsertAssigned(String id) {
        AutoAssignedIdEntity data = new AutoAssignedIdEntity();

        data.setCurrentName(nextUpper(4, 8));
        data.setId(id);

        autoSession(session -> {
            autoTransaction(session, () -> {
                System.out.println("===========================0");
                session.save(data);
                System.out.println(">>>>>>                <<<<<<");
            });
        }, session -> {
            AutoAssignedIdEntity queried = session.get(AutoAssignedIdEntity.class, id);
            assertNotSame(data, queried);
            assertEquals(data, queried);
        });
    }
}