package com.moonsky.orm.hibernate.annotation.relation.o2o;

import com.moon.core.lang.annotation.AnnotationUtil;
import com.moon.core.lang.ref.FinalAccessor;
import com.moonsky.orm.hibernate.HibernateBaseTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.Entity;

import static com.moon.core.util.RandomStringUtil.nextDigit;
import static com.moon.core.util.RandomStringUtil.nextUpper;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

/**
 * @author benshaoye
 */
class O2oTwoWayIdCardEntityTestTest extends HibernateBaseTest {

    @BeforeEach
    void setUp() {
        initializeFactory(O2oTwoWayIdCardEntity.class, O2oTwoWayStuffEntity.class);
    }

    @AfterEach
    void tearDown() {
        destroy();
    }

    @Test
    void testTableValid() throws Exception {
        String tableName = AnnotationUtil.get(O2oTwoWayStuffEntity.class, Entity.class).name();

        assertExistsTable(tableName);
        assertTableColumnsCount(tableName, 2);
        assertTableColumnTypeLike(tableName, "id", "varchar");
        assertTableColumnTypeLike(tableName, "stuff_name", "varchar");


        tableName = AnnotationUtil.get(O2oTwoWayIdCardEntity.class, Entity.class).name();

        assertExistsTable(tableName);
        assertTableColumnsCount(tableName, 3);
        assertTableColumnTypeIs(tableName, "id_code_val", "varchar(18)");
        assertTableColumnTypeIs(tableName, "stuff_id", "varchar(255)");
    }

    O2oTwoWayIdCardEntity randomIdCard() {
        O2oTwoWayIdCardEntity card = new O2oTwoWayIdCardEntity();
        card.setIdCodeVal(nextDigit(17) + nextUpper(1));
        return card;
    }

    O2oTwoWayStuffEntity randomStuff() {
        O2oTwoWayStuffEntity stuff = new O2oTwoWayStuffEntity();
        stuff.setStuffName(nextUpper(8));
        return stuff;
    }

    @Test
    void testInsertRows() {
        O2oTwoWayStuffEntity stuff = randomStuff();
        O2oTwoWayIdCardEntity card = randomIdCard();
        stuff.setIdCard(card);

        autoInsert(stuff);

        card.setStuff(stuff);
        autoInsert(card);

        FinalAccessor<String> cardIdAccessor = FinalAccessor.of(card.getId());
        FinalAccessor<String> stuffIdAccessor = FinalAccessor.of(stuff.getId());
        autoSession(session -> {
            O2oTwoWayStuffEntity stuffEntity = session.get(O2oTwoWayStuffEntity.class, stuffIdAccessor.get());
            assertEquals(stuff, stuffEntity);
            assertEquals(stuff.getId(), stuffEntity.getId());
            assertEquals(stuff.getStuffName(), stuffEntity.getStuffName());

            O2oTwoWayIdCardEntity cardEntity = stuffEntity.getIdCard();
            assertEquals(card.getId(), cardEntity.getId());
            assertEquals(card.getIdCodeVal(), cardEntity.getIdCodeVal());

            assertSame(stuffEntity, cardEntity.getStuff());
        });

        autoSession(session -> {
            O2oTwoWayIdCardEntity cardEntity = session.get(O2oTwoWayIdCardEntity.class, cardIdAccessor.get());
            assertEquals(card, cardEntity);
            assertEquals(card.getId(), cardEntity.getId());
            assertEquals(card.getIdCodeVal(), cardEntity.getIdCodeVal());

            O2oTwoWayStuffEntity stuffEntity = cardEntity.getStuff();
            assertEquals(stuff.getId(), stuffEntity.getId());
            assertEquals(stuff.getStuffName(), stuffEntity.getStuffName());

            assertSame(cardEntity, stuffEntity.getIdCard());
        });
    }
}