package com.moonsky.orm.hibernate.annotation.basic;

import com.moon.core.lang.annotation.AnnotationUtil;
import com.moonsky.orm.hibernate.HibernateBaseTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.Entity;

/**
 * @author benshaoye
 */
class BasicEntityTestTest extends HibernateBaseTest {

    @BeforeEach
    void setUp() {
        initializeFactory(BasicEntity.class);
    }

    @AfterEach
    void tearDown() {
        destroy();
    }

    @Test
    void testBasicTable() throws Exception {
        String tableName = AnnotationUtil.get(BasicEntity.class, Entity.class).name();
        assertExistsTable(tableName);

        assertTableColumnsCount(tableName, 3);
        assertTableColumnTypeLike(tableName, "id", "int");
        assertTableColumnTypeLike(tableName, "certNo", "varchar");
        assertTableColumnTypeLike(tableName, "current_name", "varchar(64)");
    }
}