package com.moonsky.orm.hibernate.annotation.strategy.table;

import com.moon.core.io.IOUtil;
import com.moon.core.util.IteratorUtil;
import com.moon.core.util.RandomStringUtil;
import com.moonsky.orm.hibernate.HibernateBaseTest;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author benshaoye
 */
class TableIdDefaultEntityTestTest extends HibernateBaseTest {

    @BeforeEach
    void setUp() {
        initializeFactory(TableIdDefaultEntity.class);
    }

    @AfterEach
    void tearDown() {
        destroy();
    }

    TableIdDefaultEntity randomEntoty(){
        TableIdDefaultEntity entity = new TableIdDefaultEntity();
        entity.setCurrentName(RandomStringUtil.nextUpper(3, 10));
        return entity;
    }

    @Test
    void testInsert() {
        List<TableIdDefaultEntity> list = new ArrayList<>();
        IteratorUtil.forEach(120, idx -> list.add(randomEntoty()));

        autoInsert(list);
    }
}