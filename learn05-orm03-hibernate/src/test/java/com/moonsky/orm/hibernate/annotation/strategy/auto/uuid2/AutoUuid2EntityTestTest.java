package com.moonsky.orm.hibernate.annotation.strategy.auto.uuid2;

import com.moon.core.util.IteratorUtil;
import com.moon.core.util.RandomStringUtil;
import com.moonsky.orm.hibernate.HibernateBaseTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author benshaoye
 */
class AutoUuid2EntityTestTest extends HibernateBaseTest {

    @BeforeEach
    void setUp() {
        initializeFactory(AutoUuid2Entity.class);
    }

    @AfterEach
    void tearDown() {
        destroy();
    }

    AutoUuid2Entity randomEntity() {
        AutoUuid2Entity entity = new AutoUuid2Entity();
        entity.setUuid2Name(RandomStringUtil.nextUpper(4, 8));
        return entity;
    }

    @Test
    void testInsertAutoGuid() throws Exception {
        List<AutoUuid2Entity> list = new ArrayList<>();
        IteratorUtil.forEach(25, idx -> list.add(randomEntity()));

        autoInsert(list);
    }
}