package com.moonsky.orm.hibernate.annotation.strategy.identity;

import com.moon.core.io.IOUtil;
import com.moon.core.util.IteratorUtil;
import com.moon.core.util.ListUtil;
import com.moon.core.util.RandomStringUtil;
import com.moonsky.orm.hibernate.HibernateBaseTest;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

/**
 * @author benshaoye
 */
class IdentityEntityTestTest extends HibernateBaseTest {

    @BeforeEach
    void setUp() {
        initializeFactory(IdentityEntity.class);
    }

    @AfterEach
    void tearDown() {
        destroy();
    }

    IdentityEntity randomEntity(){
        IdentityEntity entity = new IdentityEntity();
        entity.setIdentityName(RandomStringUtil.nextUpper(3, 8));
        return entity;
    }

    @Test
    void testName() throws Exception {
        List<IdentityEntity> list = ListUtil.ofArrayList();
        IteratorUtil.forEach(25, idx -> list.add(randomEntity()));

        autoInsert(list);
    }
}