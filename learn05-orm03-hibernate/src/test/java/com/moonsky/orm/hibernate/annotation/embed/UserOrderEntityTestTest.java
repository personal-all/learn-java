package com.moonsky.orm.hibernate.annotation.embed;

import com.moon.core.util.DateUtil;
import com.moonsky.orm.hibernate.HibernateBaseTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

import static com.moon.core.util.RandomStringUtil.nextDigit;
import static com.moon.core.util.RandomStringUtil.nextUpper;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author benshaoye
 */
class UserOrderEntityTestTest extends HibernateBaseTest {

    @BeforeEach
    void setUp() {
        initializeFactory(UserOrderEntity.class);
    }

    @AfterEach
    void tearDown() {
        destroy();
    }

    @Test
    void testUserOrder() throws Exception {
        UserOrderEntity order = new UserOrderEntity();
        order.setOrderNo(nextUpper(2) + DateUtil.format("yyyyMMddHHmmss") + nextDigit(6));
        order.setOwnerId(UUID.randomUUID().toString());

        MetaContent meta = new MetaContent();
        Date date = new Date();
        String currId = nextUpper(4) + nextDigit(8);
        meta.setCreateTime(date);
        meta.setUpdateTime(date);
        meta.setCreateBy(currId);
        meta.setUpdateBy(currId);

        order.setMeta(meta);

        autoInsert(order);

        autoSession(session -> {
            System.out.println(">>> get begin -----------------------------------------");
            UserOrderEntity uo = session.get(UserOrderEntity.class, order.getId());
            System.out.println(">>> get end, get MetaContent begin  -------------------");
            MetaContent content = uo.getMeta();
            System.out.println(">>> get end -------------------------------------------");
            assertTrue(content != null);
            System.out.println(">>> use MetaContent#getClass begin --------------------");
            System.out.println(content.getClass());
            System.out.println(">>> use MetaContent#getClass next ---------------------");
            assertEquals(meta.getCreateBy(), content.getCreateBy());
            assertEquals(meta.getUpdateBy(), content.getUpdateBy());
            /**
             * 注意{@link MetaContent#createTime}没有注解{@link Temporal#value()}={@link TemporalType#TIMESTAMP}
             * 而{@link MetaContent#updateTime}注解了，
             * 被注解的查询出来是{@link Timestamp}格式数据类型，
             * 没有注解的是{@link Date}类型
             */
            assertEquals(meta.getCreateTime(), meta.getCreateTime());
            assertEquals(meta.getUpdateTime().getTime(), meta.getUpdateTime().getTime());
            System.out.println(">>> use MetaContent#getClass end ----------------------");
            assertEquals(order.getOrderNo(), uo.getOrderNo());
            assertEquals(order.getOwnerId(), uo.getOwnerId());
        });
    }
}