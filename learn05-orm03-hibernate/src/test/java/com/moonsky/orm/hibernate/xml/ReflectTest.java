package com.moonsky.orm.hibernate.xml;

import com.moon.core.lang.reflect.FieldUtil;
import com.moon.core.util.ListUtil;
import com.moonsky.orm.hibernate.xml.basic.UserEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * @author benshaoye
 */
@DisplayName("反射泛型类测试")
public class ReflectTest {

    private final List<UserEntity> userList = new ArrayList<>();

    @Test
    @DisplayName("运行时获取正在使用的泛型类型方式")
    void testReflectFieldType() throws Exception {
        Class type = getClass();
        Field field = FieldUtil.getDeclaredField(type, "userList");
        Class fieldType = field.getType();
        System.out.println(fieldType);

        ListUtil.ofArrayList(fieldType.getTypeParameters()).forEach(typeVar -> {
            System.out.println(typeVar.getGenericDeclaration());
            System.out.println(typeVar.getName());
            System.out.println(typeVar);
            System.out.println(typeVar.getClass());
        });

        Type genericType = field.getGenericType();
        System.out.println(genericType);
        System.out.println(genericType.getClass());

        if (genericType instanceof ParameterizedType) {
            ParameterizedType paramType = (ParameterizedType) genericType;
            for (Type type1 : ListUtil.ofArrayList(paramType.getActualTypeArguments())) {
                Assertions.assertSame(UserEntity.class, type1);
            }
        }
    }
}
