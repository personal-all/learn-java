package com.moonsky.orm.hibernate.xml.component;

import com.moon.core.io.IOUtil;
import com.moon.core.util.IteratorUtil;
import com.moon.core.util.ListUtil;
import com.moon.core.util.RandomStringUtil;
import com.moonsky.orm.hibernate.HibernateBaseTest;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.moon.core.util.RandomStringUtil.*;

/**
 * @author benshaoye
 */
class ComponentEnterpriseEntityTestTest extends HibernateBaseTest {

    private SessionFactory factory;

    @BeforeEach
    void setUp() {
        factory = new Configuration().configure().addClass(ComponentEnterpriseEntity.class).buildSessionFactory();
    }

    @AfterEach
    void tearDown() {
        IOUtil.close(getFactory());
    }

    @Override
    protected SessionFactory getFactory() {
        return factory;
    }

    ComponentAddressEntity randomAddress() {
        ComponentAddressEntity address = new ComponentAddressEntity();
        address.setLocation(nextLower(4, 14));
        address.setContactMobile("156" + nextDigit(8));
        address.setContactName(nextUpper(5, 12));
        return address;
    }

    ComponentEnterpriseEntity randomEntity() {
        ComponentEnterpriseEntity entity = new ComponentEnterpriseEntity();
        entity.setEnterpriseName(nextUpper(12, 32));

        entity.setAddress(randomAddress());
        return entity;
    }

    @Test
    void testInsert() {
        List<ComponentEnterpriseEntity> list = ListUtil.ofArrayList();
        IteratorUtil.forEach(115, idx -> list.add(randomEntity()));

        autoInsert(list);
    }
}