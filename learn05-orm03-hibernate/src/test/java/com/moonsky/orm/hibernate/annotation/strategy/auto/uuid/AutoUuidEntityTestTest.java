package com.moonsky.orm.hibernate.annotation.strategy.auto.uuid;

import com.moonsky.orm.hibernate.HibernateBaseTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.moon.core.util.RandomStringUtil.nextUpper;

/**
 * @author benshaoye
 */
class AutoUuidEntityTestTest extends HibernateBaseTest {

    @BeforeEach
    void setUp() {
        initializeFactory(AutoUuidEntity.class);
    }

    @AfterEach
    void tearDown() {
        destroy();
    }

    AutoUuidEntity randomEntity() {
        AutoUuidEntity entity0 = new AutoUuidEntity();
        entity0.setCurrentName(nextUpper(5, 20));
        return entity0;
    }

    @Test
    void testInsetUuidEntity() {
        AutoUuidEntity entity0 = randomEntity();
        AutoUuidEntity entity1 = randomEntity();
        AutoUuidEntity entity2 = randomEntity();
        AutoUuidEntity entity3 = randomEntity();
        AutoUuidEntity entity4 = randomEntity();
        AutoUuidEntity entity5 = randomEntity();
        AutoUuidEntity entity6 = randomEntity();
        AutoUuidEntity entity7 = randomEntity();


        autoSession(session -> {
            autoTransaction(session, () -> {
                System.out.println("===========================0");
                session.save(entity0);
                System.out.println("---------------------------1");
                session.save(entity1);
                System.out.println(">>>>>>                <<<<<<");
            });
            autoTransaction(session, () -> {
                System.out.println("===========================2");
                session.save(entity2);
                System.out.println("---------------------------3");
                session.save(entity3);
                System.out.println(">>>>>>                <<<<<<");
            });
        }, session -> {
            autoTransaction(session,() -> {
                System.out.println("===========================4");
                session.save(entity4);
                System.out.println("---------------------------5");
                session.save(entity5);
                System.out.println(">>>>>>                <<<<<<");
            });
        });
        autoSession(session -> {
            autoTransaction(session, () -> {
                System.out.println("===========================6");
                session.save(entity6);
                System.out.println("---------------------------7");
                session.save(entity7);
                System.out.println(">>>>>>                <<<<<<");
            });
        });
        System.out.println(entity0);
        System.out.println(entity1);
        System.out.println(entity2);
        System.out.println(entity3);
        System.out.println(entity4);
        System.out.println(entity5);
        System.out.println(entity6);
        System.out.println(entity7);
    }
}