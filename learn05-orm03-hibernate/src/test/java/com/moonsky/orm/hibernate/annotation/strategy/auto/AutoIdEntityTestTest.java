package com.moonsky.orm.hibernate.annotation.strategy.auto;

import com.moonsky.orm.hibernate.HibernateBaseTest;
import org.hibernate.Session;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.GenerationType;

import static com.moon.core.util.RandomStringUtil.*;

/**
 * @author benshaoye
 */
class AutoIdEntityTestTest extends HibernateBaseTest {

    @BeforeEach
    void setUp() {
        initializeFactory(AutoIdEntity.class, AutoIdSecondaryEntity.class);
    }

    @AfterEach
    void tearDown() {
        destroy();
    }

    AutoIdEntity randomAutoId() {
        AutoIdEntity auto = new AutoIdEntity();

        auto.setDefaultName(nextUpper(3, 8));
        auto.setEmail(nextLower(4) + nextDigit(5, 8) + "@163.com");
        auto.setMobile("156" + nextDigit(8));
        return auto;
    }

    AutoIdSecondaryEntity randomSecondary() {
        AutoIdSecondaryEntity secondary = new AutoIdSecondaryEntity();

        secondary.setAddress(nextLower(10, 64));
        secondary.setCurrentValue(nextLetter(3, 30));

        return secondary;
    }

    @Test
    void testName() {
        AutoIdEntity auto0 = randomAutoId();
        AutoIdEntity auto1 = randomAutoId();
        AutoIdSecondaryEntity secondary0 = randomSecondary();
        AutoIdSecondaryEntity secondary1 = randomSecondary();
        /**
         * 在使用{@link GenerationType#AUTO}主键策略情况下
         * 同一个事务里，主键序列由 hibernate 维护，每一次{@link Session#save(Object)}
         * 实际上执行的是更新主键序列表的值，在最后提交事务的时候统一执行 insert 语句
         */
        autoSession(session -> {
            autoTransaction(session, () -> {
                System.out.println("=======================0");
                session.save(auto0);
                System.out.println("-----------------------1");
                session.save(secondary0);
                System.out.println("-----------------------2");
                session.save(auto1);
                System.out.println("-----------------------3");
                session.save(secondary1);
                System.out.println("-----------------------4");
            });
        });

        AutoIdEntity auto2 = randomAutoId();
        AutoIdEntity auto3 = randomAutoId();
        AutoIdSecondaryEntity secondary2 = randomSecondary();
        AutoIdSecondaryEntity secondary3 = randomSecondary();
        autoSession(session -> {
            autoTransaction(session, () -> {
                System.out.println("=======================5");
                session.save(auto2);
                System.out.println("-----------------------6");
                session.save(auto3);
                System.out.println("-----------------------7");
                session.save(secondary2);
                System.out.println("-----------------------8");
                session.save(secondary3);
                System.out.println("-----------------------9");
            });
        });
    }
}