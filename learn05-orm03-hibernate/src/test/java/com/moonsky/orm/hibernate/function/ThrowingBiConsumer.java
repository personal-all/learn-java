package com.moonsky.orm.hibernate.function;

/**
 * @author benshaoye
 */
public interface ThrowingBiConsumer<O, S> {

    void accept(O o, S s) throws Throwable;
}
