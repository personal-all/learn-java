package com.moonsky.orm.hibernate.xml.basic;

import com.moon.core.io.IOUtil;
import com.moonsky.orm.hibernate.function.ThrowingConsumer;
import com.moonsky.orm.hibernate.function.ThrowingRunner;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.moon.core.util.RandomStringUtil.nextLower;
import static com.moon.core.util.RandomStringUtil.nextUpper;

/**
 * @author benshaoye
 */
class IncrementIdEntityTestTest {


    private SessionFactory factory;

    @BeforeEach
    void setUp() {
        factory = new Configuration().addClass(IncrementIdEntity.class).configure().buildSessionFactory();
    }

    @AfterEach
    void tearDown() {
        IOUtil.close(factory);
    }

    void autoSession(ThrowingConsumer<? super Session>... consumers) {
        for (ThrowingConsumer<? super Session> consumer : consumers) {
            try (Session session = factory.openSession()) {
                consumer.accept(session);
            } catch (Throwable t) {
                throw new IllegalStateException(t.getMessage(), t);
            }
        }
    }

    void autoTransaction(Session session, ThrowingRunner runner) {
        Transaction tx = session.beginTransaction();
        try {
            runner.run();
            tx.commit();
        } catch (Throwable t) {
            tx.rollback();
            throw new IllegalStateException(t.getMessage(), t);
        }
    }

    IncrementIdEntity autoIncrement() {
        IncrementIdEntity increment = new IncrementIdEntity();
        increment.setKey(nextUpper(4, 10));
        increment.setValue(nextLower(5, 64));
        return increment;
    }

    @Test
    void testInsertIncrement() {
        IncrementIdEntity increment0 = autoIncrement();
        IncrementIdEntity increment1 = autoIncrement();
        IncrementIdEntity increment2 = autoIncrement();
        IncrementIdEntity increment3 = autoIncrement();
        IncrementIdEntity increment4 = autoIncrement();
        IncrementIdEntity increment5 = autoIncrement();

        autoSession(session -> {
            /**
             * 整个过程只执行一次 select max(id) from table
             * 后面的主键序列由 hibernate 管理
             *
             * increment 就是用 hibernate 管理的
             *
             * 同时，这种主键策略的主键类型必须是数字类型
             */
            autoTransaction(session, () -> {
                System.out.println("-------------------------");
                session.save(increment0);
                session.save(increment1);
            });
            autoTransaction(session, () -> {
                System.out.println("-------------------------");
                session.save(increment2);
                session.save(increment3);
            });
        }, session -> {
            autoTransaction(session, () -> {
                session.delete(increment2);
                System.out.println("=========================");
                session.save(increment4);
                session.save(increment5);
            });
        });
    }
}