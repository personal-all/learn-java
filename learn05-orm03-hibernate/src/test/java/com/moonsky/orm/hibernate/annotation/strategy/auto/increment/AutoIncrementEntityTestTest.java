package com.moonsky.orm.hibernate.annotation.strategy.auto.increment;

import com.moon.core.io.IOUtil;
import com.moon.core.util.RandomStringUtil;
import com.moonsky.orm.hibernate.HibernateBaseTest;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.moon.core.util.RandomStringUtil.nextUpper;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author benshaoye
 */
class AutoIncrementEntityTestTest extends HibernateBaseTest {

    @BeforeEach
    void setUp() {
        initializeFactory(AutoIncrementEntity.class);
    }

    @AfterEach
    void tearDown() {
        destroy();
    }

    AutoIncrementEntity randomEntity(){
        AutoIncrementEntity entity = new AutoIncrementEntity();
        entity.setIncrementName(nextUpper(5, 10));
        return entity;
    }

    @Test
    void testInsert() {
        AutoIncrementEntity entity0 = randomEntity();
        AutoIncrementEntity entity1 = randomEntity();
        AutoIncrementEntity entity2 = randomEntity();
        AutoIncrementEntity entity3 = randomEntity();
        AutoIncrementEntity entity4 = randomEntity();
        AutoIncrementEntity entity5 = randomEntity();
        AutoIncrementEntity entity6 = randomEntity();
        AutoIncrementEntity entity7 = randomEntity();


        autoSession(session -> {
            autoTransaction(session, () -> {
                System.out.println("===========================0");
                session.save(entity0);
                System.out.println("---------------------------1");
                session.save(entity1);
                System.out.println(">>>>>>                <<<<<<");
            });
            autoTransaction(session, () -> {
                System.out.println("===========================2");
                session.save(entity2);
                System.out.println("---------------------------3");
                session.save(entity3);
                System.out.println(">>>>>>                <<<<<<");
            });
        }, session -> {
            autoTransaction(session,() -> {
                System.out.println("===========================4");
                session.save(entity4);
                System.out.println("---------------------------5");
                session.save(entity5);
                System.out.println(">>>>>>                <<<<<<");
            });
        });
        autoSession(session -> {
            autoTransaction(session, () -> {
                System.out.println("===========================6");
                session.save(entity6);
                System.out.println("---------------------------7");
                session.save(entity7);
                System.out.println(">>>>>>                <<<<<<");
            });
        });
    }
}