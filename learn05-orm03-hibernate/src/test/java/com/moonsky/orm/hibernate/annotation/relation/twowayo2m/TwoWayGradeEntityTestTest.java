package com.moonsky.orm.hibernate.annotation.relation.twowayo2m;

import com.moon.core.lang.annotation.AnnotationUtil;
import com.moon.core.util.SetUtil;
import com.moonsky.orm.hibernate.HibernateBaseTest;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.persistence.Entity;
import java.util.Comparator;
import java.util.List;

import static com.moon.core.util.RandomStringUtil.nextUpper;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @author benshaoye
 * @see AnimalEntity
 * @see BuffaloEntity
 * @see TwoWayGradeEntity 见注释
 * @see TwoWayStudentEntity
 */
@DisplayName("测试注解双向多对一“一方”维护关系")
class TwoWayGradeEntityTestTest extends HibernateBaseTest {

    @BeforeEach
    void setUp() {
        initializeFactory(TwoWayGradeEntity.class, TwoWayStudentEntity.class);
    }

    @AfterEach
    void tearDown() { destroy(); }

    TwoWayGradeEntity randomGrade() {
        TwoWayGradeEntity grade = new TwoWayGradeEntity();
        grade.setGradeName(nextUpper(5));
        return grade;
    }

    TwoWayStudentEntity randomStudent() {
        TwoWayStudentEntity student = new TwoWayStudentEntity();
        student.setStudentName(nextUpper(8));
        return student;
    }

    @Test
    void testTableStructure() throws Exception {
        String tableName = AnnotationUtil.get(TwoWayGradeEntity.class, Entity.class).name();
        assertExistsTable(tableName);
        assertTableColumnsCount(tableName, 2);
        assertTableColumnTypeLike(tableName, "id", "varchar");
        assertTableColumnTypeLike(tableName, "grade_name", "varchar");

        tableName = AnnotationUtil.get(TwoWayStudentEntity.class, Entity.class).name();
        assertExistsTable(tableName);
        assertTableColumnsCount(tableName, 3);
        assertTableColumnTypeLike(tableName, "id", "varchar");
        assertTableColumnTypeLike(tableName, "student_name", "varchar");
        assertTableColumnTypeLike(tableName, "grade_id", "varchar");
    }

    /**
     * 凡是可由 hibernate 接管的主键策略，
     * 都是在{@link Transaction#commit()}或{@link Session#flush()}的时候，
     * 实际执行 insert 操作
     *
     * @throws Exception
     */
    @Test
    @DisplayName("测试“一方”维护关系:先保存多方")
    void testInsert() throws Exception {
        List<TwoWayStudentEntity> list = createList(8, () -> randomStudent());
        autoInsert(list);

        TwoWayGradeEntity grade = randomGrade();
        grade.setStudentSet(SetUtil.ofTreeSet(Comparator.comparing(TwoWayStudentEntity::getId), list));
        autoInsert(grade);
    }

    @Test
    @DisplayName("测试“一方”维护关系:先保存一方")
    void testInsertByMulti() throws Exception {
        List<TwoWayStudentEntity> list = createList(8, () -> randomStudent());

        TwoWayGradeEntity grade = randomGrade();
        grade.setStudentSet(SetUtil.ofHashSet(list));

        assertThrows(Exception.class, () -> {
            autoInsert(grade);
            autoInsert(list);
        });
    }
}