package com.moonsky.orm.hibernate.function;

/**
 * @author benshaoye
 */
public interface ThrowingMultiConsumer <O, S, T> {

    void accept(O o, S s, T t) throws Throwable;
}
