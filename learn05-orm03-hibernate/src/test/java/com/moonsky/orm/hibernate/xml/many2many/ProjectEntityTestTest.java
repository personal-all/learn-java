package com.moonsky.orm.hibernate.xml.many2many;

import com.moon.core.io.IOUtil;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.DisabledIfSystemProperty;

/**
 * @author benshaoye
 */
class ProjectEntityTestTest {

    private final static String key = "many2many.key.created";
    private final static String cancelVal = "false";

    private SessionFactory factory;

    @BeforeEach
    void setUp() {
        try {
            factory = new Configuration().addClass(ProjectEntity.class)
                .addClass(DeveloperEntity.class)
                .configure()
                .buildSessionFactory();
        } catch (Throwable t) {
            System.setProperty(key, cancelVal);
            throw new IllegalStateException(t);
        }
    }

    @AfterEach
    void tearDown() {
        IOUtil.close(factory);
    }

    @Test
    @DisabledIfSystemProperty(named = key, matches = cancelVal)
    void testInitDatabase() {
        System.out.println("=================");
    }
}