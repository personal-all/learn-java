package com.moonsky.orm.hibernate.function;

/**
 * @author benshaoye
 */
public interface ThrowingRunner {
    void run() throws Throwable;
}
