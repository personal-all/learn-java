package com.moonsky.orm.hibernate.annotation.relation.map;

import com.moonsky.orm.hibernate.HibernateBaseTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author benshaoye
 */
class DefaultMapEntityTestTest extends HibernateBaseTest {

    @BeforeEach
    void setUp() {
        initializeFactory(DefaultMapEntity.class);
    }

    @AfterEach
    void tearDown() {
        destroy();
    }
}