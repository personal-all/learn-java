package com.moonsky.orm.hibernate.annotation.relation.collect;

import com.moon.core.lang.annotation.AnnotationUtil;
import com.moon.core.lang.reflect.FieldUtil;
import com.moon.core.util.Optional;
import com.moonsky.orm.hibernate.HibernateBaseTest;
import org.hibernate.IdentifierLoadAccess;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import java.lang.reflect.Field;
import java.util.Set;

import static com.moon.core.util.IteratorUtil.forEach;
import static com.moon.core.util.RandomStringUtil.nextDigit;
import static com.moon.core.util.RandomStringUtil.nextUpper;
import static com.moon.core.util.RandomUtil.nextInt;
import static com.moon.core.util.SetUtil.ofTreeSet;
import static java.util.Comparator.comparing;
import static java.util.Comparator.naturalOrder;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author benshaoye
 */
class PersonEntityTestTest extends HibernateBaseTest {

    @BeforeEach
    void setUp() {
        initializeFactory(PersonEntity.class);
    }

    @AfterEach
    void tearDown() {
        destroy();
    }

    @Test
    @DisplayName("测试表结构")
    void testTableStructure() {
        Class targetClass = PersonEntity.class;

        String tableName = AnnotationUtil.get(targetClass, Entity.class).name();

        assertExistsTable(tableName);
        assertTableColumnsCount(tableName, 2);
        assertTableColumnTypeLike(tableName, "id", "varchar");
        assertTableColumnTypeLike(tableName, "person_name", "varchar");

        Field genderField = FieldUtil.getDeclaredField(targetClass, "genderSet");
        Field mobileField = FieldUtil.getDeclaredField(targetClass, "mobileSet");
        Field roleField = FieldUtil.getDeclaredField(targetClass, "roleSet");

        JoinTable genderJoinTable = AnnotationUtil.get(genderField, JoinTable.class);
        Column genderColumn = AnnotationUtil.get(genderField, Column.class);
        JoinColumn[] genderJoinCol = genderJoinTable.joinColumns();
        String genderJoinTableName = genderJoinTable.name();
        assertExistsTable(genderJoinTableName);
        assertTableColumnsCount(genderJoinTableName, genderJoinCol.length + 1);
        forEach(genderJoinCol, (col, idx) -> assertTableColumnTypeLike(genderJoinTableName, col.name(), "varchar"));
        assertTableColumnTypeLike(genderJoinTableName, genderColumn.name(), "int");

        JoinTable mobileJoinTable = AnnotationUtil.get(mobileField, JoinTable.class);
        Column mobileColumn = AnnotationUtil.get(mobileField, Column.class);
        JoinColumn[] mobileJoinCol = mobileJoinTable.joinColumns();
        String mobileJoinTableName = mobileJoinTable.name();
        assertExistsTable(mobileJoinTableName);
        assertTableColumnsCount(mobileJoinTableName, mobileJoinCol.length + 1);
        forEach(mobileJoinCol, (col, idx) -> assertTableColumnTypeLike(mobileJoinTableName, col.name(), "varchar"));
        assertTableColumnTypeLike(mobileJoinTableName, mobileColumn.name(), "varchar");

        JoinTable roleJoinTable = AnnotationUtil.get(roleField, JoinTable.class);
        Column roleColumn = AnnotationUtil.get(roleField, Column.class);
        JoinColumn[] roleJoinCol = roleJoinTable.joinColumns();
        String roleJoinTableName = roleJoinTable.name();
        assertExistsTable(roleJoinTableName);
        assertTableColumnsCount(roleJoinTableName, roleJoinCol.length + 1);
        forEach(roleJoinCol, (col, idx) -> assertTableColumnTypeLike(roleJoinTableName, col.name(), "varchar"));
        assertTableColumnTypeLike(roleJoinTableName, roleColumn.name(), "varchar");
    }


    @Test
    @DisplayName("测试数据正确性")
    void testInsertRows() throws Exception {
        PersonEntity person = new PersonEntity();
        person.setPersonName(nextUpper(8));

        Gender[] genders = Gender.values();
        forEach(nextInt(genders.length), idx -> person.addGender(genders[nextInt(genders.length)]));
        RoleEnum[] roles = RoleEnum.values();
        forEach(nextInt(roles.length), idx -> person.addRole(roles[nextInt(roles.length)]));
        forEach(nextInt(13), idx -> person.addMobile("138" + nextDigit(8)));

        Set<Gender> originGenderSet = ofTreeSet(comparing(Gender::name), person.getGenderSet());
        Set<RoleEnum> originRoleSet = ofTreeSet(comparing(RoleEnum::name), person.getRoleSet());
        Set<String> originMobileSet = ofTreeSet(naturalOrder(), person.getMobileSet());

        autoInsert(person);
        final String personId = person.getId();

        outputNextLineOf150('=');
        autoSession(session -> {
            IdentifierLoadAccess<PersonEntity> access = session.byId(PersonEntity.class);
            Optional<PersonEntity> optional = Optional.fromUtil(access.loadOptional(personId));
            assertTrue(optional.isPresent());
            optional.ifPresent(queried -> {
                assertEquals(person.getId(), queried.getId());
                assertEquals(person.getPersonName(), queried.getPersonName());

                outputNextLineOf150('-');
                assertEquals(originGenderSet, ofTreeSet(comparing(Gender::name), queried.getGenderSet()));
                assertEquals(originRoleSet, ofTreeSet(comparing(RoleEnum::name), queried.getRoleSet()));
                assertEquals(originMobileSet, ofTreeSet(naturalOrder(), queried.getMobileSet()));
            });
        });
    }
}