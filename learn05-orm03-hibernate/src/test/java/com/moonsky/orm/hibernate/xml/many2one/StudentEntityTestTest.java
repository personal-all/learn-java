package com.moonsky.orm.hibernate.xml.many2one;

import com.moon.core.io.IOUtil;
import com.moonsky.orm.hibernate.function.ThrowingConsumer;
import com.moonsky.orm.hibernate.function.ThrowingRunner;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.moon.core.util.RandomStringUtil.nextLower;
import static com.moon.core.util.RandomStringUtil.nextUpper;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * @author benshaoye
 */
class StudentEntityTestTest {

    private Configuration config;
    private SessionFactory factory;

    @BeforeEach
    void setUp() {
        Configuration configuration = new Configuration();
        configuration.configure();
        configuration.addClass(StudentEntity.class);
        configuration.addClass(GradeEntity.class);
        SessionFactory sessionFactory = configuration.buildSessionFactory();

        this.config = configuration;
        this.factory = sessionFactory;
    }

    @AfterEach
    void tearDown() {
        IOUtil.close(factory);
        factory = null;
        config = null;
    }

    void autoSession(ThrowingConsumer<? super Session>... consumers) {
        for (ThrowingConsumer<? super Session> consumer : consumers) {
            try (Session session = factory.openSession()) {
                consumer.accept(session);
            } catch (Throwable t) {
                throw new IllegalStateException(t.getMessage(), t);
            }
        }
    }

    void autoTransaction(Session session, ThrowingRunner runner) {
        Transaction tx = session.beginTransaction();
        try {
            runner.run();
        } catch (Throwable t) {
            tx.rollback();
            throw new IllegalStateException(t.getMessage(), t);
        } finally {
            tx.commit();
        }
    }

    StudentEntity randomStudent() {
        StudentEntity student = new StudentEntity();
        student.setStudentName(nextUpper(3, 8));
        return student;
    }

    GradeEntity randomGrade() {
        GradeEntity grade = new GradeEntity();
        grade.setGradeName(nextLower(2, 6));
        return grade;
    }

    @Test
    void testInitializeConfigure() throws Exception {
        GradeEntity grade = randomGrade();
        // 不在事务中执行 insert 也能成功
        autoSession(session -> session.save(grade));

        StudentEntity student = randomStudent();
        student.setGrade(grade);
        autoSession(session -> session.save(student));

        autoSession(session -> {
            StudentEntity queried = session.get(StudentEntity.class, student.getId());
            System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++");
            assertEquals(queried.getId(), student.getId());
            assertEquals(queried.getStudentName(), student.getStudentName());
            assertNotNull(queried.getGrade());
            System.out.println(queried.getGrade().getClass());
            System.out.println("===================================================");
            /**
             * 关联对象默认懒加载
             * 所以在执行上一句 get 的时候，并不会查询{@link GradeEntity}对象
             */
            assertEquals(student, queried);
            System.out.println("---------------------------------------------------");
            System.out.println(queried.getClass());
        });
    }
}