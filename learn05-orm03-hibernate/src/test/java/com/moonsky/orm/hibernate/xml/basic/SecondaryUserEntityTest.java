package com.moonsky.orm.hibernate.xml.basic;

import com.moon.core.io.IOUtil;
import com.moon.core.lang.ref.FinalAccessor;
import com.moon.core.util.IteratorUtil;
import com.moon.core.util.ListUtil;
import com.moon.core.util.MapperUtil;
import com.moonsky.orm.hibernate.function.ThrowingBiConsumer;
import com.moonsky.orm.hibernate.function.ThrowingConsumer;
import com.moonsky.orm.hibernate.function.ThrowingRunner;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.condition.DisabledIfSystemProperty;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.moon.core.util.IteratorUtil.forEach;
import static com.moon.core.util.RandomStringUtil.*;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author benshaoye
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class SecondaryUserEntityTest {

    private final static String propKey = "com.moonsky.orm.hibernate.entity.SecondaryUserEntityTest";
    private final static String cancelMatch = "^[\\d]{1}$";

    private Configuration config;
    private SessionFactory factory;

    @BeforeEach
    void setUp() {
        Configuration configuration = new Configuration();
        configuration.configure();
        configuration.addClass(UserEntity.class);
        SessionFactory sessionFactory = configuration.buildSessionFactory();

        this.config = configuration;
        this.factory = sessionFactory;
    }

    @AfterEach
    void tearDown() {
        IOUtil.close(factory);
        factory = null;
        config = null;
    }

    void autoSession(ThrowingConsumer<? super Session>... consumers) {
        for (ThrowingConsumer<? super Session> consumer : consumers) {
            try (Session session = factory.openSession()) {
                consumer.accept(session);
            } catch (Throwable t) {
                throw new IllegalStateException(t.getMessage(), t);
            }
        }
    }

    void autoTransaction(Session session, ThrowingRunner runner) {
        Transaction tx = session.beginTransaction();
        try {
            runner.run();
        } catch (Throwable t) {
            tx.rollback();
            throw new IllegalStateException(t.getMessage(), t);
        } finally {
            tx.commit();
        }
    }

    void afterInsertRandomRows(int count, ThrowingBiConsumer<? super Session, List<UserEntity>>... consumers) {
        FinalAccessor<List<UserEntity>> accessor = FinalAccessor.of(ListUtil.ofArrayList());
        autoSession(session -> forEach(count, index -> {
            UserEntity user = new UserEntity();
            user.setAddress(nextLetter(5, 20));
            user.setUsername(nextUpper(5, 15));
            user.setPassword(nextLower(5, 15));
            user.setEmail(nextLower(5) + nextDigit(8, 12) + "@163.com");
            user.setMobile("134" + nextDigit(8));

            session.save(user);
            assertNotNull(user.getId());
            accessor.get().add(user);
        }));
        forEach(consumers, consumer -> {
            List<UserEntity> users = accessor.get();
            List<UserEntity> list = MapperUtil.forEachToOther(users, UserEntity.class);
            assertIterableEquals(list, users);
            autoSession(session -> consumer.accept(session, list));
        });
    }

    @Test
    @Order(0)
    @DisplayName("初始数据验证")
    void testRandomInsertRows() throws Exception {
        try {
            afterInsertRandomRows(4, (session, list) -> list.forEach(System.out::println));
            System.setProperty(propKey, "a");
        } catch (Throwable t) {
            System.setProperty(propKey, "1");
            throw new IllegalArgumentException(t.getMessage(), t);
        }
    }

    @Test
    @Disabled
    void testQueryByCriteria() {
        afterInsertRandomRows(12, (session, list) -> {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<UserEntity> query = builder.createQuery(UserEntity.class);
            Query<UserEntity> userQuery = session.createQuery(query);
            List<UserEntity> resultList = userQuery.getResultList();
            resultList.forEach(System.out::println);
        });
    }

    @Test
    @Order(2)
    @DisabledIfSystemProperty(named = propKey, matches = cancelMatch)
    void testMultiIdentifierLoadAccess() {
        afterInsertRandomRows(12, (session, list) -> {
            MultiIdentifierLoadAccess<UserEntity> access = session.byMultipleIds(UserEntity.class);
            System.out.println("========================================================");
            List<UserEntity> queried = access.multiLoad(ListUtil.mapAsList(list, user -> user.getId()));
            System.out.println("========================================================");
            assertIterableEquals(queried, list);
            Collections.reverse(queried);
            assertThrows(Throwable.class, () -> assertIterableEquals(queried, list));
        });
    }

    /**
     * 在同一个会话{@link Session}中，getReference 和 load 始终会重用缓存对象
     * <p>
     * System.out 标记执行顺序，assert 验证数据真伪
     * <p>
     * {@link IdentifierLoadAccess#loadOptional(Serializable)}只是简单包装了一层{@link Optional}
     *
     * @throws Exception
     */
    @Test
    @Order(1)
    @DisabledIfSystemProperty(named = propKey, matches = cancelMatch)
    void testIdentifierAccess() throws Exception {
        afterInsertRandomRows(11, (session, list) -> {
            FinalAccessor<List<UserEntity>> accessor = FinalAccessor.of(ListUtil.ofArrayList());
            IdentifierLoadAccess<UserEntity> access = session.byId(UserEntity.class);
            System.out.println("=====================================");
            IteratorUtil.forEach(list, user -> {
                UserEntity queried = access.getReference(user.getId());
                accessor.get().add(queried);
                assertEquals(user, queried);
            });
            System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            IteratorUtil.forEach(list, (user, idx) -> {
                UserEntity queried = access.load(user.getId());
                assertSame(queried, accessor.get().get(idx));
                assertEquals(user, access.load(user.getId()));
            });
        }, (session, list) -> {
            FinalAccessor<List<UserEntity>> accessor = FinalAccessor.of(ListUtil.ofArrayList());
            IdentifierLoadAccess<UserEntity> access = session.byId(UserEntity.class);
            System.out.println("=====================================");
            IteratorUtil.forEach(list, user -> {
                UserEntity queried = access.load(user.getId());
                accessor.get().add(queried);
                assertEquals(user, queried);
            });
            System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            IteratorUtil.forEach(list, (user, idx) -> {
                UserEntity queried = access.getReference(user.getId());
                assertSame(queried, accessor.get().get(idx));
                assertEquals(user, access.load(user.getId()));
            });
        });
    }

    /**
     * 未在事务中执行 update 只影响当前 session
     */
    @Test
    @Order(4)
    @DisplayName("未在事务中执行【update】")
    @DisabledIfSystemProperty(named = propKey, matches = cancelMatch)
    void testUnTxUpdate() {
        FinalAccessor<UserEntity> originCache = FinalAccessor.of();
        FinalAccessor<UserEntity> copiedCache = FinalAccessor.of();
        afterInsertRandomRows(13, (session, list) -> {
            UserEntity originUser = list.get(0);
            UserEntity copiedUser = MapperUtil.toInstance(originUser, UserEntity.class);
            assertEquals(originUser, copiedUser);

            copiedUser.setUsername(nextDigit(25));
            session.update(copiedUser);

            System.out.println("================================================");
            UserEntity queried = session.get(UserEntity.class, copiedUser.getId());
            assertNotEquals(originUser, queried);
            assertEquals(copiedUser, queried);
            originCache.set(originUser);
            copiedCache.set(copiedUser);
        }, (session, list) -> {
            UserEntity copiedUser = copiedCache.get();
            UserEntity originUser = originCache.get();

            System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++");
            UserEntity queried = session.get(UserEntity.class, copiedUser.getId());

            assertEquals(originUser, queried);
            assertNotEquals(copiedUser, queried);
        });
    }

    /**
     * 未在事务中执行 update
     */
    @Test
    @Order(3)
    @DisplayName("在事务中执行【update】")
    @DisabledIfSystemProperty(named = propKey, matches = cancelMatch)
    void testInTxUpdate() {
        FinalAccessor<UserEntity> originCache = FinalAccessor.of();
        FinalAccessor<UserEntity> copiedCache = FinalAccessor.of();
        afterInsertRandomRows(13, (session, list) -> {
            UserEntity originUser = list.get(0);
            UserEntity copiedUser = MapperUtil.toInstance(originUser, UserEntity.class);
            assertEquals(originUser, copiedUser);

            copiedUser.setUsername(nextDigit(25));

            Transaction tx = session.beginTransaction();
            session.update(copiedUser);
            tx.commit();

            System.out.println("================================================");
            UserEntity queried = session.get(UserEntity.class, copiedUser.getId());
            assertNotEquals(originUser, queried);
            assertEquals(copiedUser, queried);
            originCache.set(originUser);
            copiedCache.set(copiedUser);
        }, (session, list) -> {
            UserEntity copiedUser = copiedCache.get();
            UserEntity originUser = originCache.get();

            System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++");
            UserEntity queried = session.get(UserEntity.class, copiedUser.getId());

            assertNotEquals(originUser, queried);
            assertEquals(copiedUser, queried);
        });
    }

    /**
     * 未在事务中执行 delete，只影响当前事务的 session 数据
     *
     * @throws Exception
     */
    @Test
    @Order(3)
    @DisplayName("未在事务中执行【delete】")
    void testUnTxDelete() throws Exception {
        FinalAccessor<UserEntity> originCache = FinalAccessor.of();
        FinalAccessor<Integer> uniqueIdCache = FinalAccessor.of();
        afterInsertRandomRows(13, (session, list) -> {
            UserEntity originUser = list.get(0);
            final Integer uniqueId = originUser.getId();
            session.delete(originUser);

            assertNull(session.get(UserEntity.class, uniqueId));
            assertThrows(Exception.class, () -> {
                UserEntity user = session.load(UserEntity.class, uniqueId);
                assertNotNull(user);
                System.out.println(user);
            });
            originCache.set(originUser);
            uniqueIdCache.set(uniqueId);
        }, (session, lise) -> {
            final Integer uniqueId = uniqueIdCache.get();
            System.out.println("before get ++++++++++++++++++++++++++++++++++++++++");
            UserEntity user = session.get(UserEntity.class, uniqueId);
            System.out.println("after  get  +++++++++++++++++++++++++++++++++++++++");
            assertNotNull(user);
            assertEquals(originCache.get(), user);
            System.out.println("end    get  +++++++++++++++++++++++++++++++++++++++");
        }, (session, lise) -> {
            final Integer uniqueId = uniqueIdCache.get();
            System.out.println("before load ++++++++++++++++++++++++++++++++++++++++");
            UserEntity user = session.load(UserEntity.class, uniqueId);
            System.out.println("after  load  +++++++++++++++++++++++++++++++++++++++");
            assertNotNull(user);
            assertEquals(originCache.get(), user);
            System.out.println("end    load  +++++++++++++++++++++++++++++++++++++++");
        });
    }

    /**
     * 在事务中执行 delete
     *
     * @throws Exception
     */
    @Test
    @Order(3)
    @DisplayName("在事务中执行【delete】")
    void testInTxDelete() throws Exception {
        FinalAccessor<UserEntity> originCache = FinalAccessor.of();
        FinalAccessor<Integer> uniqueIdCache = FinalAccessor.of();
        afterInsertRandomRows(13, (session, list) -> {
            UserEntity originUser = list.get(0);
            final Integer uniqueId = originUser.getId();

            autoTransaction(session,() -> {
                session.delete(originUser);
            });

            assertNull(session.get(UserEntity.class, uniqueId));
            assertThrows(Exception.class, () -> {
                UserEntity user = session.load(UserEntity.class, uniqueId);
                assertNotNull(user);
                System.out.println(user);
            });
            originCache.set(originUser);
            uniqueIdCache.set(uniqueId);
        }, (session, lise) -> {
            final Integer uniqueId = uniqueIdCache.get();
            System.out.println("before get ++++++++++++++++++++++++++++++++++++++++");
            UserEntity user = session.get(UserEntity.class, uniqueId);
            System.out.println("after  get  +++++++++++++++++++++++++++++++++++++++");
            assertNull(user);
            System.out.println("end    get  +++++++++++++++++++++++++++++++++++++++");
        }, (session, lise) -> {
            final Integer uniqueId = uniqueIdCache.get();
            System.out.println("before load ++++++++++++++++++++++++++++++++++++++++");
            UserEntity user = session.load(UserEntity.class, uniqueId);
            System.out.println("after  load  +++++++++++++++++++++++++++++++++++++++");
            assertNotNull(user);
            assertThrows(Exception.class,() -> {
                assertEquals(originCache.get(), user);
            });
            System.out.println("end    load  +++++++++++++++++++++++++++++++++++++++");
        });
    }
}
