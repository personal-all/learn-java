package com.moonsky.orm.hibernate.annotation.strategy.auto.native0;

import com.moonsky.orm.hibernate.HibernateBaseTest;
import org.hibernate.Session;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.moon.core.util.RandomStringUtil.nextLower;
import static com.moon.core.util.RandomStringUtil.nextUpper;

/**
 * @author benshaoye
 */
class AutoNativeEntityTestTest extends HibernateBaseTest {

    @BeforeEach
    void setUp() {
        initializeFactory(AutoNativeEntity.class);
    }

    @AfterEach
    void tearDown() {
        destroy();
    }

    AutoNativeEntity randomEntity() {

        AutoNativeEntity auto = new AutoNativeEntity();
        auto.setName(nextUpper(5, 18));
        auto.setAddress(nextLower(24, 64));

        return auto;
    }

    @Test
    void testInsertAutoIncrement() {
        /**
         * 每一个{@link Session#save(Object)} 操作均会实时执行 insert 语句
         */
        autoSession(session -> {
            autoTransaction(session, () -> {
                System.out.println("=====================0");
                session.save(randomEntity());
                System.out.println("---------------------1");
                session.save(randomEntity());
                System.out.println("---------------------2");
            });
        });
    }
}