package com.moonsky.orm.hibernate.annotation.relation.o2m;

import com.moon.core.util.IteratorUtil;
import com.moon.core.util.ListUtil;
import com.moon.core.util.RandomStringUtil;
import com.moonsky.orm.hibernate.HibernateBaseTest;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.moon.core.util.SetUtil.ofLinkedHashSet;

/**
 * @author benshaoye
 */
class O2mOnCompanyEntityTestTest extends HibernateBaseTest {

    @BeforeEach
    void setUp() {
        initializeFactory(O2mOnCompanyEntity.class, O2mOnMemberEntity.class);
    }

    @AfterEach
    void tearDown() {
        destroy();
    }

    O2mOnCompanyEntity randomCompany() {
        O2mOnCompanyEntity entity = new O2mOnCompanyEntity();
        entity.setCompanyName(RandomStringUtil.nextUpper(3, 8));
        return entity;
    }

    O2mOnMemberEntity randomMember() {
        O2mOnMemberEntity entity = new O2mOnMemberEntity();
        entity.setMemberName(RandomStringUtil.nextUpper(6));
        return entity;
    }

    /**
     * 条件：主键策略 uuid + MySQL5.6 + 一对多关系
     * 在保存时并不会在{@link Session#save(Object)}中执行 insert 操作，
     * 而是在{@link Session#flush()}中或{@link Transaction#commit()}中执行 insert 操作
     * <p>
     * 同时在保存“一方”时还会维护更新关联“多方”的关联外键值
     *
     * @throws Exception
     */
    @Test
    void testInsert() throws Exception {
        List<O2mOnMemberEntity> memberList = ListUtil.ofArrayList();
        IteratorUtil.forEach(25, idx -> memberList.add(randomMember()));

        autoInsert(memberList);

        O2mOnCompanyEntity entity = randomCompany();
        entity.setMemberSet(ofLinkedHashSet(memberList));
        autoInsert(entity);
    }
}