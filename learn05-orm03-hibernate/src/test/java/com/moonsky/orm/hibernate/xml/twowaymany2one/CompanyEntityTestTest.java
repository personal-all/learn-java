package com.moonsky.orm.hibernate.xml.twowaymany2one;

import com.moon.core.io.IOUtil;
import com.moonsky.orm.hibernate.function.ThrowingConsumer;
import com.moonsky.orm.hibernate.function.ThrowingRunner;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.moon.core.util.RandomStringUtil.nextUpper;

/**
 * @author benshaoye
 */
class CompanyEntityTestTest {

    private SessionFactory factory;

    @BeforeEach
    void setUp() {
        factory = new Configuration().configure()
            .addClass(CompanyEntity.class)
            .addClass(MemberEntity.class)
            .buildSessionFactory();
    }

    @AfterEach
    void tearDown() {
        IOUtil.close(factory);
    }

    void autoSession(ThrowingConsumer<Session>... consumers) {
        for (ThrowingConsumer<? super Session> consumer : consumers) {
            try (Session session = factory.openSession()) {
                consumer.accept(session);
            } catch (Throwable t) {
                throw new IllegalStateException(t);
            }
        }
    }

    void autoTransaction(Session session, ThrowingRunner runner) {
        Transaction tx = session.beginTransaction();
        try {
            runner.run();
            tx.commit();
        } catch (Throwable t) {
            tx.rollback();
            throw new IllegalStateException(t);
        }
    }

    CompanyEntity randomCompany() {
        CompanyEntity company = new CompanyEntity();

        company.setCompanyName(nextUpper(5, 20));

        return company;
    }

    MemberEntity randomMember() {
        MemberEntity member = new MemberEntity();
        member.setMemberName(nextUpper(2, 8));
        return member;
    }

    @Test
    void testInsertCompanyOnly() {
        CompanyEntity company = randomCompany();
        MemberEntity member0 = randomMember();
        MemberEntity member1 = randomMember();
        MemberEntity member2 = randomMember();

        company.ensureMemberSet().add(member0);
        company.ensureMemberSet().add(member1);
        company.ensureMemberSet().add(member2);

        member0.setCompany(company);
        member1.setCompany(company);
        member2.setCompany(company);

        autoSession(session -> {
            autoTransaction(session, () -> {
                session.save(company);
                session.save(member0);
                session.save(member1);
                session.save(member2);
            });
        });
    }
}