package com.moonsky.orm.hibernate.hql;

import com.moon.core.util.CollectUtil;
import com.moon.core.util.IteratorUtil;
import com.moon.core.util.ListUtil;
import com.moonsky.orm.hibernate.HibernateBaseTest;
import org.hibernate.query.Query;
import org.junit.jupiter.api.*;

import java.util.List;

import static com.moon.core.util.IteratorUtil.forEach;
import static com.moon.core.util.ListUtil.ofArrayList;

/**
 * @author benshaoye
 */
class SellerEntityTestTest extends HibernateBaseTest {

    @BeforeEach
    void setUp() {
        initializeFactory(CommodityEntity.class,
            CustomerEntity.class,
            OrderFormEntity.class,
            OrderDetailEntity.class,
            SellerEntity.class);
    }

    @AfterEach
    void tearDown() {
        destroy();
        outputNextLineOf150('=');
    }

    @Nested
    @DisplayName("验证表结构")
    class TableStructure {

        @Test
        void testSeller() throws Exception {
            String tableName = getTableName(SellerEntity.class);
            assertExistsTable(tableName);
            assertTableColumnsCount(tableName, 7);
            assertTableColumnTypeIs(tableName, "id", "varchar(255)");
            assertTableColumnTypeIs(tableName, "seller_name", "varchar(255)");
            assertTableColumnTypeIs(tableName, "telephone", "varchar(255)");
            assertTableColumnTypeIs(tableName, "address", "varchar(255)");
            assertTableColumnTypeIs(tableName, "website", "varchar(255)");
            assertTableColumnTypeLike(tableName, "star", "int");
            assertTableColumnTypeIs(tableName, "business", "varchar(255)");
        }

        @Test
        void testOrderForm() throws Exception {
            String tableName = getTableName(OrderFormEntity.class);
            assertExistsTable(tableName);
            assertTableColumnsCount(tableName, 5);
            assertTableColumnTypeIs(tableName, "id", "varchar(255)");
            assertTableColumnTypeIs(tableName, "customer_id", "varchar(255)");
            assertTableColumnTypeLike(tableName, "trade_date", "date");
            assertTableColumnTypeIs(tableName, "status", "varchar(255)");
            assertTableColumnTypeLike(tableName, "amount", "double");
        }

        @Test
        void testOrderDetail() throws Exception {
            String tableName = getTableName(OrderDetailEntity.class);
            assertExistsTable(tableName);
            assertTableColumnsCount(tableName, 6);
            assertTableColumnTypeIs(tableName, "id", "varchar(255)");
            assertTableColumnTypeIs(tableName, "order_id", "varchar(255)");
            assertTableColumnTypeIs(tableName, "commodity_id", "varchar(255)");
            assertTableColumnTypeIs(tableName, "discount", "varchar(255)");
            assertTableColumnTypeLike(tableName, "act_price", "double");
            assertTableColumnTypeLike(tableName, "amount", "double");
        }

        @Test
        void testCustomer() throws Exception {
            String tableName = getTableName(CustomerEntity.class);
            assertExistsTable(tableName);
            assertTableColumnsCount(tableName, 9);
            assertTableColumnTypeIs(tableName, "id", "varchar(255)");
            assertTableColumnTypeIs(tableName, "customer_name", "varchar(255)");
            assertTableColumnTypeIs(tableName, "telephone", "varchar(255)");
            assertTableColumnTypeIs(tableName, "address", "varchar(255)");
            assertTableColumnTypeIs(tableName, "email", "varchar(255)");
            assertTableColumnTypeIs(tableName, "sex", "varchar(255)");
            assertTableColumnTypeIs(tableName, "description", "varchar(255)");
            assertTableColumnTypeLike(tableName, "age", "int");
            assertTableColumnTypeLike(tableName, "date", "date");
        }

        @Test
        void testCommodity() throws Exception {
            String tableName = getTableName(CommodityEntity.class);
            assertExistsTable(tableName);
            assertTableColumnsCount(tableName, 7);
            assertTableColumnTypeIs(tableName, "id", "varchar(255)");
            assertTableColumnTypeIs(tableName, "commodity_name", "varchar(255)");
            assertTableColumnTypeLike(tableName, "price", "double");
            assertTableColumnTypeIs(tableName, "util", "varchar(255)");
            assertTableColumnTypeIs(tableName, "category", "varchar(255)");
            assertTableColumnTypeIs(tableName, "description", "varchar(255)");
            assertTableColumnTypeIs(tableName, "seller_id", "varchar(255)");
        }
    }

    @Nested
    class DataSelector {

        private String commodityId;

        @BeforeEach
        void setUp() {
            SellerEntity seller0 = new SellerEntity("数码店", "01084752614", "北京市朝阳区东大桥东路34号", "www.3c.com", 4, "这是一家数码店");
            CommodityEntity computer0 = new CommodityEntity("戴尔笔记本001", 4999D, "台", "3C数码", "戴尔个人笔记本电脑001");
            CommodityEntity computer1 = new CommodityEntity("联想电脑A01", 6788D, "台", "3C数码", "联想个人笔记本电脑A01");
            CommodityEntity computer2 = new CommodityEntity("联想电脑A02", 5988D, "台", "3C数码", "联想个人笔记本电脑A02");
            CommodityEntity computer3 = new CommodityEntity("宏基电脑AC101", 9888D, "台", "3C数码", "宏基个人笔记本电脑AC101");
            CommodityEntity computer4 = new CommodityEntity("宏基电脑AC201", 4988D, "台", "3C数码", "宏基个人笔记本电脑AC201");
            CommodityEntity mobile0 = new CommodityEntity("小米8", 2577D, "台", "3C数码", "小米手机8代");
            CommodityEntity mobile1 = new CommodityEntity("小米8透明版", 2577D, "台", "3C数码", "小米手机8代透明版");
            CommodityEntity mobile2 = new CommodityEntity("小米9", 3577D, "台", "3C数码", "小米手机9代");
            CommodityEntity mobile3 = new CommodityEntity("华为P30", 5980D, "台", "3C数码", "小米手机9代炫彩绿版");
            CommodityEntity mobile4 = new CommodityEntity("华为Mate20", 3577D, "台", "3C数码", "华为Mate20 mate 2代");
            CommodityEntity mobile5 = new CommodityEntity("华为Mate30", 3977D, "台", "3C数码", "华为Mate30 mate 3代");
            CommodityEntity mobile6 = new CommodityEntity("iPhone8 4.7", 3599D, "台", "3C数码", "iPhone8 4.7 寸 OLED 屏");
            CommodityEntity mobile7 = new CommodityEntity("iPhone8 Plus 5.5", 4577D, "台", "3C数码", "iPhone8 Plus 5.5 寸");
            CommodityEntity mobile8 = new CommodityEntity("iPhoneX", 7888D, "台", "3C数码", "iPhoneX全面屏");
            CommodityEntity mobile9 = new CommodityEntity("iPhoneXS", 8999D, "台", "3C数码", "iPhoneXS全面屏");
            List<CommodityEntity> commodities0 = ofArrayList(computer0, computer1, computer2, computer3, computer4);
            CollectUtil.addAll(commodities0,
                mobile0,
                mobile1,
                mobile2,
                mobile3,
                mobile4,
                mobile5,
                mobile6,
                mobile7,
                mobile8,
                mobile9);
            forEach(commodities0, comm -> comm.setSeller(seller0));

            SellerEntity seller1 = new SellerEntity("花宠店",
                "0108946614",
                "北京市海淀区海淀南路78号",
                "www.flower.com",
                5,
                "这是一家卖花和卖宠物的店");
            CommodityEntity flower0 = new CommodityEntity("玫瑰999", 2798D, "捧", "鲜花", "情人节礼物");
            CommodityEntity flower1 = new CommodityEntity("百合", 8D, "朵", "鲜花", "家养百合");
            CommodityEntity car0 = new CommodityEntity("英短蓝白", 800D, "只", "猫", "猫");
            CommodityEntity car1 = new CommodityEntity("折耳", 2300D, "只", "猫", "猫");
            CommodityEntity dog0 = new CommodityEntity("金毛", 2000D, "只", "狗", "狗");
            CommodityEntity dog1 = new CommodityEntity("二哈", 3900D, "只", "狗", "狗");
            CommodityEntity dog3 = new CommodityEntity("泰迪", 3788D, "只", "狗", "狗");
            CommodityEntity dog4 = new CommodityEntity("德国牧羊犬", 4888D, "只", "狗", "狗");
            CommodityEntity dog5 = new CommodityEntity("阿拉斯加犬", 5988D, "只", "狗", "狗");
            List<CommodityEntity> commodities1 = ofArrayList(flower0,
                flower1,
                car0,
                car1,
                dog0,
                dog1,
                dog3,
                dog4,
                dog5);
            forEach(commodities1, comm -> comm.setSeller(seller1));

            autoInsert(seller0, seller1);
            autoInsert(commodities0);
            autoInsert(commodities1);

            commodityId = dog5.getId();
        }

        @Test
        void testBaseQuery() throws Exception {
            autoSession(session -> {
                CommodityEntity commodity = session.get(CommodityEntity.class, commodityId);
                System.out.println(commodity);
            });
        }

        @Test
        @Disabled
        void testHqlQuery() throws Exception {
            String hql = "from SellerEntity";
            autoSession(session -> {
                Query query = session.createQuery(hql);
                List<CommodityEntity> queriedEntities = query.list();
                queriedEntities.forEach(System.out::println);
            });
        }
    }
}