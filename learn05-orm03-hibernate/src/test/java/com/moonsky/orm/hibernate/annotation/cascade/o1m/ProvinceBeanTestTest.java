package com.moonsky.orm.hibernate.annotation.cascade.o1m;

import com.moonsky.orm.hibernate.HibernateBaseTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.provider.ValueSource;

/**
 * @author benshaoye
 */
class ProvinceBeanTestTest extends HibernateBaseTest {

    @BeforeEach
    void setUp() {
        initializeFactory(ProvinceBean.class, CityBean.class);
    }

    @AfterEach
    void tearDown() {
        destroy();
    }

    @Nested
    class TableStructure {

        @Test
        void testProvinceBean() throws Exception {
            String tableName = getTableName(ProvinceBean.class);
            assertExistsTable(tableName);
            assertTableColumnsCount(tableName, 3);
            assertTableColumnTypeIs(tableName, "id", "varchar(36)");
            assertTableColumnTypeIs(tableName, "province_name", "varchar(255)");
            assertTableColumnTypeIs(tableName, "province_code", "varchar(255)");
        }

        @Test
        void testCityBean() throws Exception {
            String tableName = getTableName(CityBean.class);
            assertExistsTable(tableName);
            assertTableColumnsCount(tableName, 4);
            assertTableColumnTypeIs(tableName, "id", "varchar(36)");
            assertTableColumnTypeIs(tableName, "city_name", "varchar(255)");
            assertTableColumnTypeIs(tableName, "city_code", "varchar(255)");
            assertTableColumnTypeIs(tableName, "province_id", "varchar(36)");
        }
    }

    @Test
    void testName() throws Exception {

    }
}