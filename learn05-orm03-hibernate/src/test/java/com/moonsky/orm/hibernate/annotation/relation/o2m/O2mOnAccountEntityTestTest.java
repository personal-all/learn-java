package com.moonsky.orm.hibernate.annotation.relation.o2m;

import com.moon.core.util.IteratorUtil;
import com.moon.core.util.ListUtil;
import com.moon.core.util.RandomStringUtil;
import com.moon.core.util.SetUtil;
import com.moonsky.orm.hibernate.HibernateBaseTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Comparator;
import java.util.List;
import java.util.Set;

import static com.moon.core.util.RandomStringUtil.*;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author benshaoye
 */
class O2mOnAccountEntityTestTest extends HibernateBaseTest {

    @BeforeEach
    void setUp() {
        initializeFactory(O2mOnAccountEntity.class, O2mOnAddressEntity.class);
    }

    @AfterEach
    void tearDown() {
        destroy();
    }

    O2mOnAccountEntity randomAccount() {
        O2mOnAccountEntity account = new O2mOnAccountEntity();
        account.setUsername(nextUpper(8));
        account.setPassword(nextLower(12));
        return account;
    }

    O2mOnAddressEntity randomAddress() {
        O2mOnAddressEntity address = new O2mOnAddressEntity();
        address.setLocation(nextLetter(18));
        return address;
    }

    @Test
    void testInsert() throws Exception {
        O2mOnAccountEntity account = randomAccount();
        autoInsert(account);

        List<O2mOnAddressEntity> addressList = createList(23, () -> randomAddress());
        IteratorUtil.forEach(addressList, address -> address.setAccountId(account.getId()));

        autoInsert(addressList);

        autoSession(session -> {
            O2mOnAccountEntity quired = session.get(O2mOnAccountEntity.class, account.getId());
            Set<O2mOnAddressEntity> addressSet = SetUtil.ofTreeSet(Comparator.comparing(O2mOnAddressEntity::getId));
            Set<O2mOnAddressEntity> quiredSet = SetUtil.ofTreeSet(Comparator.comparing(O2mOnAddressEntity::getId));
            addressSet.addAll(addressList);
            System.out.println("====  使用多方集合开始 ==============================================================");
            List<O2mOnAddressEntity> addList = ListUtil.ofArrayList(quired.getAddressSet());
            System.out.println("----  取出一个多方对象 -------------------------------------------");
            O2mOnAddressEntity address = addList.get(0);
            System.out.println("----  使用多返对象的属性 -----------------------------------------");
            String location = address.getLocation();
            System.out.println(location);
            System.out.println("----  比较多方集合 ----------------------------------------------");

            quiredSet.addAll(addList);
            assertIterableEquals(addressSet, quiredSet);
        });
    }
}