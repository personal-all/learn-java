/*
 创建 tb_user 表
 */
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user`
(
    `id`       INT AUTO_INCREMENT COMMENT '主键ID',
    `username` VARCHAR(32) NULL COMMENT '用户名',
    `password` VARCHAR(32) NULL COMMENT '密码',
    `mobile`   VARCHAR(24) NULL COMMENT '手机号',
    `email`    VARCHAR(32) NULL COMMENT '电子邮件',
    `address`  VARCHAR(64) NULL COMMENT '家庭地址',
    PRIMARY KEY (`id`)
) ENGINE InnoDB
  DEFAULT CHARSET `utf8`;;
