package com.moonsky.orm.hibernate.annotation.basic;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * 被 @Entity 注解的实体必须定义主键，
 * 否则报异常：AnnotationException: No identifier specified for entity: (EntityName)
 * <p>
 * 表名默认为实体名，可是通过{@link Entity#name()}手动指定表名：{@link MultiIdEntity}
 *
 * @author benshaoye
 */
@Data
@Entity(name = "tb_ann_basic")
public class BasicEntity {

    /**
     * 默认自增主键 @Id 可注解于多个字段上，
     * <p>
     * 当一个实体存在多个 @Id 时
     * 该实体必须实现 {@link java.io.Serializable}接口，
     * 否则会报错：MappingException: Composite-id class must implement Serializable: (EntityName)
     *
     * @see MultiIdEntity
     */
    @Id
    private Integer id;
    /**
     * 普通字段和实体一样，默认字段名即为列名
     */
    private String certNo;
    /**
     * 普通字段可使用{@link Column}自定义列名以及其他一些属性
     */
    @Column(name = "current_name", length = 64)
    private String currentName;
}
