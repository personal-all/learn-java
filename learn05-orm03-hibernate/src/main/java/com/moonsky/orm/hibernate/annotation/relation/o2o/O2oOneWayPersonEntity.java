package com.moonsky.orm.hibernate.annotation.relation.o2o;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * @author benshaoye
 */
@Data
@Entity(name = "tb_ann_o2o_one_way_person")
public class O2oOneWayPersonEntity {

    @Id
    @GeneratedValue(generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private Integer id;

    private String personName;

    @OneToOne
    @JoinColumn(name = "location_id")
    private O2oOneWayOriginLocationEntity originLocation;

    public O2oOneWayPersonEntity() {}
}
