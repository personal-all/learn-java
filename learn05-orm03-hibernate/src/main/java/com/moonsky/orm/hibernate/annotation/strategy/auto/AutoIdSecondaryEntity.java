package com.moonsky.orm.hibernate.annotation.strategy.auto;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author benshaoye
 */
@Data
@Entity(name = "tb_auto_id_secondary")
public class AutoIdSecondaryEntity {

    @Id
    @GeneratedValue
    private Long id;

    private String currentValue;

    private String address;

    public AutoIdSecondaryEntity() {}
}
