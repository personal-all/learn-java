package com.moonsky.orm.hibernate.xml.twowaymany2one;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author benshaoye
 */
@Data
public class MemberEntity {

    private String id;
    private String memberName;
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private CompanyEntity company;
}
