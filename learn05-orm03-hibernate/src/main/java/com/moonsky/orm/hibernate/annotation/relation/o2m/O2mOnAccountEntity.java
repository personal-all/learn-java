package com.moonsky.orm.hibernate.annotation.relation.o2m;

import com.moonsky.orm.hibernate.annotation.BaseUuidEntity;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.HashSet;
import java.util.Set;

/**
 * @author benshaoye
 * @see O2mOnAddressEntity
 * @see O2mOnAccountEntity
 * @see O2mOnCompanyEntity 见注释
 * @see O2mOnMemberEntity
 */
@Data
@Entity(name = "tb_ann_o2m_with_field_account")
public class O2mOnAccountEntity extends BaseUuidEntity {

    private String username;

    private String password;

    @OneToMany(mappedBy = "accountId")
    private Set<O2mOnAddressEntity> addressSet = new HashSet<>();

    public O2mOnAccountEntity() {
    }
}
