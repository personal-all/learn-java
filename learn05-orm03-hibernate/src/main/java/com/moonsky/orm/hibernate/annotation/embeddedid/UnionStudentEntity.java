package com.moonsky.orm.hibernate.annotation.embeddedid;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author benshaoye
 */
@Data
@Entity(name = "tb_embedded_id_union_student")
public class UnionStudentEntity {

    @Id
    @EmbeddedId
    private UnionPK unionId;

    @Column(name = "student_name")
    private String studentName;

    public UnionStudentEntity() {}
}
