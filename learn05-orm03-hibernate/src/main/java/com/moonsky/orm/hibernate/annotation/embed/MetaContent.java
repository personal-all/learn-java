package com.moonsky.orm.hibernate.annotation.embed;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import java.util.Date;

import static javax.persistence.TemporalType.TIMESTAMP;

/**
 * @author benshaoye
 */
@Data
@Embeddable
public class MetaContent {

    private Date createTime;
    @Temporal(TIMESTAMP)
    private Date updateTime;
    @Column(length = 64)
    private String createBy;
    @Column(length = 64)
    private String updateBy;

    public MetaContent() {
    }
}
