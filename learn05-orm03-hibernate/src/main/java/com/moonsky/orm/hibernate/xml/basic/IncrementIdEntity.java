package com.moonsky.orm.hibernate.xml.basic;

import lombok.Data;

/**
 * @author benshaoye
 */
@Data
public class IncrementIdEntity {

    private Integer id;

    private String key;

    private String value;

    public IncrementIdEntity() {}
}
