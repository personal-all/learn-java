package com.moonsky.orm.hibernate.hql;

import com.moonsky.orm.hibernate.annotation.BaseUuidEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.function.DoubleFunction;

/**
 * 订单项
 *
 * @author benshaoye
 */
@Data
@Entity(name = "tb_ann_hql_order_detail")
public class OrderDetailEntity extends BaseUuidEntity {

    @ManyToOne
    @JoinColumn(name = "order_id")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private OrderFormEntity order;

    @ManyToOne
    @JoinColumn(name = "commodity_id")
    private CommodityEntity commodity;
    /**
     * 折扣
     */
    private String discount;

    @Column(name = "act_price")
    private Double actPrice;

    private Double amount;

    public OrderDetailEntity(String discount, Double actPrice, Double amount) {
        this.setDiscount(discount);
        setActPrice(actPrice);
        setAmount(amount);
    }

    public OrderDetailEntity() {}
}
