package com.moonsky.orm.hibernate.xml.one2many;

import lombok.Data;

import java.util.HashSet;
import java.util.Set;

/**
 * @author benshaoye
 */
@Data
public class AccountEntity {

    private String id;
    private String username;
    private String password;
    private String mobile;
    private String email;
    private Set<AddressEntity> addressSet;

    public Set<AddressEntity> ensureAddressSet(){
        return addressSet == null ? (addressSet = new HashSet<>()) : addressSet;
    }
}
