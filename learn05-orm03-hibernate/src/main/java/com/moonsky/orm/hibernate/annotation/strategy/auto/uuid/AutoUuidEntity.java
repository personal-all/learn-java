package com.moonsky.orm.hibernate.annotation.strategy.auto.uuid;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author benshaoye
 */
@Data
@Entity(name = "tb_auto_uuid")
public class AutoUuidEntity {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    private String id;

    @Column(name = "current_name", length = 48)
    private String currentName;

    public AutoUuidEntity() {}
}
