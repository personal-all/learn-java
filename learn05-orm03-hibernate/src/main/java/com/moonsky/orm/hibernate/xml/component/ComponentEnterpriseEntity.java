package com.moonsky.orm.hibernate.xml.component;

import lombok.Data;

/**
 * @author benshaoye
 */
@Data
public class ComponentEnterpriseEntity {

    private String id;

    private String enterpriseName;

    private ComponentAddressEntity address;

    public ComponentEnterpriseEntity() {}
}
