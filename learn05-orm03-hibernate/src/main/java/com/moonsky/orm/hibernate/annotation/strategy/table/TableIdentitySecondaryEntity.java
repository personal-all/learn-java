package com.moonsky.orm.hibernate.annotation.strategy.table;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * @author benshaoye
 */
@Data
@Entity(name = "tb_table_identity_secondary")
public class TableIdentitySecondaryEntity extends TableIdBaseEntity {

    @Column(name = "current_name", length = 64)
    private String currentName;

    public TableIdentitySecondaryEntity() {}
}
