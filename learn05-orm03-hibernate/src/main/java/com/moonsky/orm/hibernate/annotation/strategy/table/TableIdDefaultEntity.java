package com.moonsky.orm.hibernate.annotation.strategy.table;

import lombok.Data;

import javax.persistence.*;

/**
 * 用默认 table 设置维护组件序列
 * @author benshaoye
 */
@Data
@Entity(name = "tb_table_default")
public class TableIdDefaultEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "defaultTable")
    @TableGenerator(name = "defaultTable")
    private Long id;

    private String currentName;

    public TableIdDefaultEntity() {}
}
