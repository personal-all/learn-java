package com.moonsky.orm.hibernate.annotation.relation.o2o;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author benshaoye
 */
@Data
@Entity(name = "tb_ann_o2o_one_way_origin_location")
public class O2oOneWayOriginLocationEntity {

    @Id
    @GeneratedValue(generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private Integer id;

    private String location;

    public O2oOneWayOriginLocationEntity() {}
}
