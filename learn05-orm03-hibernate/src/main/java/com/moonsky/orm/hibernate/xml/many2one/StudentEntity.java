package com.moonsky.orm.hibernate.xml.many2one;

import lombok.Data;

/**
 * @author benshaoye
 */
@Data
public class StudentEntity {

    private String id;
    private String studentName;
    private GradeEntity grade;
}
