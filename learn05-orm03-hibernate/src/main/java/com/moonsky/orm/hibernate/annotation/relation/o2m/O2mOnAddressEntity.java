package com.moonsky.orm.hibernate.annotation.relation.o2m;

import com.moonsky.orm.hibernate.annotation.BaseUuidEntity;
import lombok.Data;

import javax.persistence.Entity;

/**
 * @author benshaoye
 * @see O2mOnAddressEntity
 * @see O2mOnAccountEntity
 * @see O2mOnCompanyEntity 见注释
 * @see O2mOnMemberEntity
 */
@Data
@Entity(name = "tb_ann_o2m_with_field_address")
public class O2mOnAddressEntity extends BaseUuidEntity {

    private String location;

    private String accountId;

    public O2mOnAddressEntity() {
    }
}
