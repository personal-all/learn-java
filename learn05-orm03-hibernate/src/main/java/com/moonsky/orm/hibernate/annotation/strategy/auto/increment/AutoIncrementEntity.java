package com.moonsky.orm.hibernate.annotation.strategy.auto.increment;

import lombok.Data;
import org.hibernate.Session;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * increment 是用当前数据表主键的最大值加一进行维护；
 * 在第一次插入时会查询最大主键值，之后的自增有 hibernate 维护
 * <p>
 * 同样由 hibernate 维护主键的还有 uuid，
 * 他们的执行方式都是在提交事务的时候才执行真实 insert 语句，
 * 而{@link Session#save(Object)}并不执行真实语句；
 * <p>
 * 此外 assigned 手动指定的方式也是在提交事务的时候才执行真实 insert 语句；
 * <p>
 * native 是与{@link Session#save(Object)}实施执行 insert 语句；
 * <p>
 * 需要注意的是：单独的 AUTO 主键策略，他是由单独的中间表维护主键，同样，
 * 也是在最后提交事务的时候统一执行 insert 语句，但与其他几个不同的是，
 * 每次执行{@link Session#save(Object)}的时候都会查询一次主键中间表
 * 的主键值，并更新下一个值
 *
 * @author benshaoye
 * @see GenerationType#TABLE 是由指定的中间表维护主键，不过里面的值变化
 * 却是由 hibernate，所以在实际执行{@link Session#save(Object)}并不会执行 insert 语句；
 * 另外，{@link TableGenerator#allocationSize()}有一个“步长”的概念，在进行步长切换的时候，
 * 会进行一次中间表更新的下一个值，要求主键类型必须是：数字类型
 */
@Data
@Entity(name = "tb_auto_increment")
public class AutoIncrementEntity {

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private Integer id;

    private String incrementName;

    public AutoIncrementEntity() {}
}
