package com.moonsky.orm.hibernate.xml.many2one;

import lombok.Data;

/**
 * @author benshaoye
 */
@Data
public class GradeEntity {

    private String id;
    private String gradeName;
}
