package com.moonsky.orm.hibernate.annotation.strategy.auto;

import com.moonsky.orm.hibernate.annotation.strategy.auto.native0.AutoNativeEntity;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * @author benshaoye
 */
@Data
@Entity(name = "tb_auto_id")
public class AutoIdEntity {

    /**
     * 在使用 hibernate 版本{5.4.6.Final}，MySQL 版本 {8.x} 下：
     * {@link GenerationType#AUTO}会额外单独维护序列表用于维护主键序列，
     * 同时由于是“增量类型”主键类型必须是数字类型，不能是字符串
     * <p>
     * 如果多张表都使用{@link GenerationType#AUTO}作为主键方式，多张表就使用
     * 同一张表维护主键，另看{@link AutoIdSecondaryEntity}
     * <p>
     * 【注】老版本 hibernate 使用自增主键（MySQL）
     * <p>
     * 当前版本使用自增主键方式看：{@link AutoNativeEntity}
     *
     * @see GenerationType#AUTO
     * @see GenerationType#SEQUENCE 见：{@link SequenceGenerator}
     * @see GenerationType#TABLE 见：{@link TableGenerator}
     * @see GenerationType#IDENTITY
     * @see GeneratedValue#generator() 见：{@link GenericGenerator}、{@link AutoNativeEntity}
     */
    @Id
    @GeneratedValue
    private Integer id;

    private String defaultName;

    private String email;

    private String mobile;

    public AutoIdEntity() {}
}
