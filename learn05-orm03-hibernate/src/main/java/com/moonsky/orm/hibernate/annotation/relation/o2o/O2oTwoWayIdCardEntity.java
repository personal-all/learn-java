package com.moonsky.orm.hibernate.annotation.relation.o2o;

import com.moonsky.orm.hibernate.annotation.BaseUuidEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 * @author benshaoye
 */
@Data
@Entity(name = "tb_ann_o2o_two_way_id_card")
public class O2oTwoWayIdCardEntity extends BaseUuidEntity {

    @Column(name = "id_code_val", length = 18)
    private String idCodeVal;

    @OneToOne
    @JoinColumn(name = "stuff_id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private O2oTwoWayStuffEntity stuff;

    public O2oTwoWayIdCardEntity() {
    }
}
