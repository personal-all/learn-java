package com.moonsky.orm.hibernate.xml.basic;

import lombok.Data;

/**
 * @author benshaoye
 */
@Data
public class UserEntity {
    private Integer id;
    private String username;
    private String password;
    private String mobile;
    private String email;
    private String address;
}
