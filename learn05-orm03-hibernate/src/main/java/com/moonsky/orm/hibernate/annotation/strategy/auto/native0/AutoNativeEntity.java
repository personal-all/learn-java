package com.moonsky.orm.hibernate.annotation.strategy.auto.native0;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author benshaoye
 */
@Data
@Entity(name = "tb_auto_native")
public class AutoNativeEntity {

    /**
     * 主键
     */
    @Id
    @GeneratedValue(
        // 指定一个主键生成器名称
        generator = "nativeId")
    @GenericGenerator(
        // 定义一个主键生成器，并命名
        name = "nativeId",
        /**
         * 生成器的主键策略，此时使用的是和 xml 配置一样的策略:
         * native:
         * increment:
         * assigned:
         * uuid:
         * uuid.hex:
        */
        strategy = "native", parameters = {
        @Parameter(name = "sql-type", value = "integer")
    })
    private Integer id;

    private String name;

    private String address;

    public AutoNativeEntity() {}
}
