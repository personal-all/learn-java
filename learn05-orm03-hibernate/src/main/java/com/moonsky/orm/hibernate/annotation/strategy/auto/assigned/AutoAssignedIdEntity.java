package com.moonsky.orm.hibernate.annotation.strategy.auto.assigned;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author benshaoye
 */
@Data
@Entity(name = "tb_auto_assigned")
public class AutoAssignedIdEntity {

    @Id
    @GeneratedValue(generator = "assigned")
    @GenericGenerator(name = "assigned", strategy = "assigned")
    private String id;

    private String currentName;

    public AutoAssignedIdEntity() {}
}
