package com.moonsky.orm.hibernate.xml.one2many;

import lombok.Data;

import java.util.HashSet;
import java.util.Set;

/**
 * @author benshaoye
 */
@Data
public class O2MAndOne {
    private String id;
    private String currentName;
    private Set<O2MAndMany> set = new HashSet<>();
}
