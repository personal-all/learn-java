package com.moonsky.orm.hibernate.annotation.relation.m2o;

import com.moonsky.orm.hibernate.annotation.BaseUuidEntity;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * 直接通过外键关联查出{@link M2oEnterpriseEntity}
 *
 * @author benshaoye
 */
@Data
@Entity(name = "tb_ann_m2o_employee")
public class M2oEmployeeEntity extends BaseUuidEntity {

    private String employeeName;

    @ManyToOne
    @JoinColumn(name = "enterprise_id")
    private M2oEnterpriseEntity enterprise;

    public M2oEmployeeEntity() {
    }
}
