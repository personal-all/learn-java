package com.moonsky.orm.hibernate.annotation.relation.o2m;

import com.moonsky.orm.hibernate.annotation.BaseUuidEntity;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import java.util.Set;

/**
 * 一、在维护单向一对多关系时，需要有一个外键在“多方”保存“一方”的主键
 * 这个外键可以是“多方”存在的字段，也可以是不存在于实体，而只存在于数据表的列
 * <p>
 * <p>
 * 1. 关联字段存在于“多方”的实体，可以用{@link OneToMany#mappedBy()}指定字段，
 * 此时相当于将关系维护方交给“多方”，如果用 xml 配置，则需要设置 inverse="true",
 * 如：{@link O2mOnAccountEntity}和{@link O2mOnAddressEntity}
 * <p>
 * 2. 如果关联字段不存在于“多方”的实体，只希望这个关联字段存在于数据库的“多方”表中，则需要使用{@link JoinColumn}，
 * 如：{@link O2mOnCompanyEntity}和{@link O2mOnMemberEntity}
 *
 * @author benshaoye
 * @see O2mOnAddressEntity
 * @see O2mOnAccountEntity
 * @see O2mOnCompanyEntity 见注释
 * @see O2mOnMemberEntity
 */
@Data
@Entity(name = "tb_ann_o2m_with_join_column_company")
public class O2mOnCompanyEntity extends BaseUuidEntity {

    private String companyName;

    @OneToMany
    @JoinColumn(name = "company_id")
    private Set<O2mOnMemberEntity> memberSet;

    public O2mOnCompanyEntity() {}
}
