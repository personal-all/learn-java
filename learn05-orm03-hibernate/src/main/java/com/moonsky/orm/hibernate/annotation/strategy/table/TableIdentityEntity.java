package com.moonsky.orm.hibernate.annotation.strategy.table;

import lombok.Data;

import javax.persistence.*;

/**
 * @author benshaoye
 */
@Data
@Entity(name = "tb_table_identity")
public class TableIdentityEntity extends TableIdBaseEntity {

    @Column(name = "current_name", length = 64)
    private String currentName;

    public TableIdentityEntity() {}
}
