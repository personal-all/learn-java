package com.moonsky.orm.hibernate.annotation.relation.twowayo2m;

import com.moonsky.orm.hibernate.annotation.BaseUuidEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 * @author benshaoye
 * @see AnimalEntity
 * @see BuffaloEntity
 * @see TwoWayGradeEntity 见注释
 * @see TwoWayStudentEntity
 */
@Data
@Entity(name = "tb_ann_o2m_two_way_student")
public class TwoWayStudentEntity extends BaseUuidEntity {

    @Column(name = "student_name")
    private String studentName;

    @ManyToOne
    private TwoWayGradeEntity grade;

    public TwoWayStudentEntity() {
    }
}
