package com.moonsky.orm.hibernate.annotation.relation.o2o;

import com.moonsky.orm.hibernate.annotation.BaseUuidEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

/**
 * @author benshaoye
 */
@Data
@Entity(name = "tb_ann_o2o_two_way_stuff")
public class O2oTwoWayStuffEntity extends BaseUuidEntity {

    @Column(name = "stuff_name")
    private String stuffName;

    @OneToOne(mappedBy = "stuff")
    private O2oTwoWayIdCardEntity idCard;

    public O2oTwoWayStuffEntity() {
    }
}
