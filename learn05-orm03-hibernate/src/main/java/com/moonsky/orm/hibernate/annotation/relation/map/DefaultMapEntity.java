package com.moonsky.orm.hibernate.annotation.relation.map;

import com.moonsky.orm.hibernate.annotation.BaseUuidEntity;
import lombok.Data;

import javax.persistence.Entity;
import java.util.Map;

/**
 * @author benshaoye
 */
@Data
@Entity(name = "tb_map_key_default_map")
public class DefaultMapEntity extends BaseUuidEntity {

    private String username;

    private Map<String,String> stringMap;

    public DefaultMapEntity() {
    }
}
