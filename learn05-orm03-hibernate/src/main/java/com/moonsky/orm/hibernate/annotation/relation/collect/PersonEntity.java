package com.moonsky.orm.hibernate.annotation.relation.collect;

import com.moonsky.orm.hibernate.annotation.BaseUuidEntity;
import lombok.Data;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * 当字段是集合时，可以使用：
 * {@link Column} + {@link ElementCollection} + {@link JoinTable}
 * 联合描述这个字段的映射关系，其中：
 * <p>
 * {@link ElementCollection}：表示这是一个需要用集合处理的字段
 * {@link JoinTable}：指定关联表；
 * {@link JoinTable#joinColumns()}：指定关联表，外键列名
 * （不能像 m2m 那样使用{@link JoinTable#inverseJoinColumns()}
 * {@link Column}：指定关联表当前字段的列名
 *
 * @author benshaoye
 */
@Data
@Entity(name = "tb_ann_collect_person")
public class PersonEntity extends BaseUuidEntity {

    @Column(name = "person_name")
    private String personName;
    /**
     * 默认使用枚举索引
     */
    @Column(name = "gender")
    @ElementCollection
    @JoinTable(name = "rel_tb_ann_collect_person_gender", joinColumns = @JoinColumn(name = "person_id"))
    private Set<Gender> genderSet;
    /**
     * 指定枚举使用枚举名
     */
    @Column(name = "role_name")
    @Enumerated(EnumType.STRING)
    @ElementCollection
    @JoinTable(name = "rel_tb_ann_collect_person_role", joinColumns = @JoinColumn(name = "person_id"))
    private Set<RoleEnum> roleSet;

    @Column(name = "mobile")
    @ElementCollection
    @JoinTable(name = "rel_tb_ann_collect_person_mobile", joinColumns = @JoinColumn(name = "person_id"))
    private Set<String> mobileSet;

    public PersonEntity() {
    }

    public Set<String> ensureMobileSet() {
        return mobileSet == null ? (mobileSet = new HashSet<>()) : mobileSet;
    }

    public Set<RoleEnum> ensureRoleSet() {
        return roleSet == null ? (roleSet = new HashSet<>()) : roleSet;
    }

    public Set<Gender> ensureGenderSet() {
        return genderSet == null ? (genderSet = new HashSet<>()) : genderSet;
    }

    public void addMobile(String mobile) {
        ensureMobileSet().add(mobile);
    }

    public void addRole(RoleEnum role) {
        ensureRoleSet().add(role);
    }

    public void addGender(Gender gender) {
        ensureGenderSet().add(gender);
    }
}
