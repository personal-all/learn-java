package com.moonsky.orm.hibernate.annotation.basic;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * 通过{@link Entity#name()}手动指定表名
 *
 * @author benshaoye
 */
@Data
@Entity(name = "tb_ann_multi_id")
public class MultiIdEntity implements Serializable {

    /**
     * 当一个实`体存在多个 @Id 时
     * 该实体必须实现 {@link java.io.Serializable}接口，
     * 否则会报错：MappingException: Composite-id class must implement Serializable: (EntityName)
     */
    @Id
    private Integer id;

    /**
     * 老版本 hibernate 要求字符串主键长度不能过长，
     * 所以要使用{@link Column}限制其长度，
     * 当前版本{5.4.6.Final}没有这个限制
     * (实际上是 MySQL 的限制)
     */
    @Id
    private String serialValue;

    private String address;

    private String accountName;

    public MultiIdEntity() {}
}
