package com.moonsky.orm.hibernate.xml.twowaymany2one;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.HashSet;
import java.util.Set;

/**
 * @author benshaoye
 */
@Data
public class CompanyEntity {

    private String id;
    private String companyName;
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<MemberEntity> memberSet;

    public Set<MemberEntity> ensureMemberSet() {
        return memberSet == null ? (memberSet = new HashSet<>()) : memberSet;
    }
}
