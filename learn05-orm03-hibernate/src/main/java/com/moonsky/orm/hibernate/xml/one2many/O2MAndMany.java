package com.moonsky.orm.hibernate.xml.one2many;

import lombok.Data;

/**
 * @author benshaoye
 */
@Data
public class O2MAndMany {
    private String id;
    private String currentName;
    private String oneId;
}
