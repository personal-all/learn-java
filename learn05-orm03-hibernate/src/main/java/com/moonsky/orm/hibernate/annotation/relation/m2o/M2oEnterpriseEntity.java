package com.moonsky.orm.hibernate.annotation.relation.m2o;

import com.moonsky.orm.hibernate.annotation.BaseUuidEntity;
import lombok.Data;

import javax.persistence.Entity;

/**
 * @author benshaoye
 */
@Data
@Entity(name = "tb_ann_m2o_enterprise")
public class M2oEnterpriseEntity extends BaseUuidEntity {

    private String enterpriseName;

    public M2oEnterpriseEntity() {
    }
}
