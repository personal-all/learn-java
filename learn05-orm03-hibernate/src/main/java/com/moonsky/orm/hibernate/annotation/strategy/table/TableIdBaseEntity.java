package com.moonsky.orm.hibernate.annotation.strategy.table;

import lombok.Data;

import javax.persistence.*;

/**
 * @author benshaoye
 */
@Data
@MappedSuperclass
public class TableIdBaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "table")
    @TableGenerator(name = "table",
                    table = "tb_table_primary_identifier",
                    pkColumnName = "primary_table",
                    valueColumnName = "primary_value")
    private Integer id;

    public TableIdBaseEntity() {}
}
