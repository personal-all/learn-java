package com.moonsky.orm.hibernate.annotation.embeddedid;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * 嵌入式联合主键必须实现接口{@link Serializable}
 *
 * @author benshaoye
 */
@Data
@Embeddable
@AllArgsConstructor
public class UnionPK implements Serializable {

    @Column(name = "cert_no")
    private String certNo;
    @Column(name = "serial_value")
    private String serialValue;

    public UnionPK() {}
}
