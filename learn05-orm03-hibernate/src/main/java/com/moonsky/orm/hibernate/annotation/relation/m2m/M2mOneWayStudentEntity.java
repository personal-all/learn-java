package com.moonsky.orm.hibernate.annotation.relation.m2m;

import com.moonsky.orm.hibernate.annotation.BaseUuidEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

/**
 * @author benshaoye
 */
@Data
@Entity(name = "tb_ann_one_way_m2m_student")
public class M2mOneWayStudentEntity extends BaseUuidEntity {

    @Column(name = "student_name")
    private String studentName;
    /**
     * {@link ManyToMany}：指定多对多关联关系
     * {@link JoinTable}：指定关联关系表，并分别指定关联两张表的外键
     * <p>
     * 如果是单向多对多关系，这样配置注解就可以了；
     * 如果是双向多对多关系，则需要在“对面”类添加集合字段，
     * 并注解{@link ManyToMany#mappedBy()}即可，参考：
     * {@link M2mOneWayTeacherEntity#studentSet}
     * <p>
     * 两个 Exclude 防止循环引用导致 SOF
     */
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToMany
    @JoinTable(name = "rel_tb_one_way_m2m_student_to_teacher",
               joinColumns = @JoinColumn(name = "student_id"),
               inverseJoinColumns = @JoinColumn(name = "teacher_id"))
    private Set<M2mOneWayTeacherEntity> teacherSet;

    public M2mOneWayStudentEntity() {
    }
}
