package com.moonsky.orm.hibernate.annotation.strategy.auto.guid;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author benshaoye
 */
@Data
@Entity(name = "tb_auto_guid")
public class AutoGuidEntity {

    @Id
    @GeneratedValue(generator = "guid")
    @GenericGenerator(name = "guid", strategy = "guid")
    private String id;

    private String guidName;

    public AutoGuidEntity() {}
}
