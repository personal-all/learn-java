package com.moonsky.orm.hibernate.annotation.strategy.identity;

import lombok.Data;

import javax.persistence.*;

/**
 * @author benshaoye
 */
@Data
@Entity(name = "tb_identity_strategy")
public class IdentityEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String identityName;
}
