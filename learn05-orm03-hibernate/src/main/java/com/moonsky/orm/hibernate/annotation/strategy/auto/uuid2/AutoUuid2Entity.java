package com.moonsky.orm.hibernate.annotation.strategy.auto.uuid2;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author benshaoye
 */
@Data
@Entity(name = "tb_auto_uuid_2")
public class AutoUuid2Entity {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    private String id;

    private String uuid2Name;

    public AutoUuid2Entity() {
    }
}
