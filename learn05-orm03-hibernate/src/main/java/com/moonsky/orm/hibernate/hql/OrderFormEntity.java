package com.moonsky.orm.hibernate.hql;

import com.moonsky.orm.hibernate.annotation.BaseUuidEntity;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * 订单
 *
 * @author benshaoye
 */
@Data
@Entity(name = "tb_ann_hql_order_form")
public class OrderFormEntity extends BaseUuidEntity {

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private CustomerEntity customer;

    @Column(name = "trade_date")
    private Date tradeDate;

    private String status;

    private Double amount;

    @OneToMany
    private Set<OrderDetailEntity> orderDetails;

    public OrderFormEntity(Date tradeDate, String status, Double amount) {
        this.tradeDate = tradeDate;
        this.status = status;
        this.amount = amount;
    }

    public OrderFormEntity() {}
}
