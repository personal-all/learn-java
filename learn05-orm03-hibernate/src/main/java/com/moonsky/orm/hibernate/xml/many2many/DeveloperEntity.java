package com.moonsky.orm.hibernate.xml.many2many;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Set;

/**
 * @author benshaoye
 */
@Data
public class DeveloperEntity {

    private String id;
    private String developerName;
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<ProjectEntity> projectSet;
}
