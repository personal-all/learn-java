package com.moonsky.orm.hibernate.annotation.relation.collect;

/**
 * @author benshaoye
 */
public enum RoleEnum {

    OPERATOR,
    ADMIN,
    MANAGER,
    CLIENT,
    SERVICE,
    FATHER,
    SON,
}
