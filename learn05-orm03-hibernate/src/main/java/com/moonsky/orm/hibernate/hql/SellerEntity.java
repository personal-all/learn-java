package com.moonsky.orm.hibernate.hql;

import com.moonsky.orm.hibernate.annotation.BaseUuidEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * 商家
 *
 * @author benshaoye
 */
@Data
@Entity(name = "tb_ann_hql_seller")
public class SellerEntity extends BaseUuidEntity {

    @Column(name = "seller_name")
    private String sellerName;

    private String telephone;

    private String address;

    private String website;

    private Integer star;

    private String business;

    public SellerEntity(
        String sellerName, String telephone, String address, String website, Integer star, String business
    ) {
        this.sellerName = sellerName;
        this.telephone = telephone;
        this.address = address;
        this.website = website;
        this.star = star;
        this.business = business;
    }

    public SellerEntity() {}
}
