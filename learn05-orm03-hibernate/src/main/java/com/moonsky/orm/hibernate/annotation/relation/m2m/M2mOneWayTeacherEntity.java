package com.moonsky.orm.hibernate.annotation.relation.m2m;

import com.moonsky.orm.hibernate.annotation.BaseUuidEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.util.Set;

/**
 * @author benshaoye
 */
@Data
@Entity(name = "tb_ann_one_way_m2m_teacher")
public class M2mOneWayTeacherEntity extends BaseUuidEntity {

    @Column(name = "teacher_name", length = 64)
    private String teacherName;

    @ManyToMany(mappedBy = "teacherSet")
    private Set<M2mOneWayStudentEntity> studentSet;

    public M2mOneWayTeacherEntity() {
    }
}
