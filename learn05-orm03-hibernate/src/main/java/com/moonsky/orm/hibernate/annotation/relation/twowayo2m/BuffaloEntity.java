package com.moonsky.orm.hibernate.annotation.relation.twowayo2m;

import com.moonsky.orm.hibernate.annotation.BaseUuidEntity;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Buffalo：水牛
 *
 * @author benshaoye
 * @see AnimalEntity
 * @see BuffaloEntity
 * @see TwoWayGradeEntity 见注释
 * @see TwoWayStudentEntity
 */
@Data
@Entity(name = "tb_ann_o2m_two_way_buffalo")
public class BuffaloEntity extends BaseUuidEntity {

    private String buffaloName;

    @ManyToOne
    @JoinColumn(name = "animal_id")
    private AnimalEntity animal;

    public BuffaloEntity() {
    }
}
