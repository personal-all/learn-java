package com.moonsky.orm.hibernate.xml.one2many;

import lombok.Data;

/**
 * @author benshaoye
 */
@Data
public class AddressEntity {

    private String id;

    private String location;

    private String contactName;

    private String contactMobile;
}
