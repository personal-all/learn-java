package com.moonsky.orm.hibernate.annotation.embed;

import com.moonsky.orm.hibernate.annotation.BaseUuidEntity;
import lombok.Data;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;

/**
 * 组件类，通过{@link Embeddable}注解标记一个类是一个组件类（{@link MetaContent}）
 * 在另一个类中直接引用这个对象即可（{@link UserOrderEntity#meta}
 * <p>
 * 也可在引用的地方使用注解：{@link Embedded}
 * <p>
 * 这两个注解，其中{@link Embeddable}是必须的，而实际使用中{@link Embedded}不是必须的
 *
 * @author benshaoye
 */
@Data
@Entity(name = "tb_component_user_order")
public class UserOrderEntity extends BaseUuidEntity {

    private String orderNo;

    private String ownerId;

    @Embedded
    private MetaContent meta;

    public UserOrderEntity() {
    }
}
