package com.moonsky.orm.hibernate.annotation.cascade.o1m;

import com.moonsky.orm.hibernate.annotation.cascade.BaseBean;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * @author benshaoye
 */
@Data
@Entity(name = "tb_cascade_o2m_city")
public class CityBean extends BaseBean {

    @Column(name = "city_name")
    private String cityName;

    @Column(name = "city_code")
    private String cityCode;

    @ManyToOne
    @JoinColumn(name = "province_id")
    private ProvinceBean province;

    public CityBean() {
    }
}
