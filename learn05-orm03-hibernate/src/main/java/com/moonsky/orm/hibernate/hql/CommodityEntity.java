package com.moonsky.orm.hibernate.hql;

import com.moonsky.orm.hibernate.annotation.BaseUuidEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * 商品
 *
 * @author benshaoye
 */
@Data
@Entity(name = "tb_ann_hql_commodity")
public class CommodityEntity extends BaseUuidEntity {

    @Column(name = "commodity_name")
    private String commodityName;
    private Double price;
    /**
     * 计量单位
     */
    private String util;
    private String category;
    private String description;
    @ManyToOne
    @JoinColumn(name = "seller_id")
    private SellerEntity seller;

    public CommodityEntity(String commodityName, Double price, String util, String category, String description) {
        this.commodityName = commodityName;
        this.price = price;
        this.util = util;
        this.category = category;
        this.description = description;
    }

    public CommodityEntity() {}
}
