package com.moonsky.orm.hibernate.annotation.relation.twowayo2m;

import com.moonsky.orm.hibernate.annotation.BaseUuidEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import java.util.Set;

/**
 * 双向多对一关系中，只能有“一方”或“多方”维护关联关系，不能由两方同时维护关联关系，否则会报错
 * <p>
 * 1. “一方”维护关系的示例：{@link TwoWayGradeEntity}和{@link TwoWayStudentEntity}
 * 请分别查看{@link TwoWayGradeEntity#studentSet}和{@link TwoWayStudentEntity#grade}的注解
 * <p>
 * 2. “多方”维护关系示例：{@link AnimalEntity#buffaloSet} + {@link BuffaloEntity#animal}
 *
 * @author benshaoye
 * @see AnimalEntity
 * @see BuffaloEntity
 * @see TwoWayGradeEntity 见注释
 * @see TwoWayStudentEntity
 */
@Data
@Entity(name = "tb_ann_o2m_two_way_grade")
public class TwoWayGradeEntity extends BaseUuidEntity {

    @Column(name = "grade_name")
    private String gradeName;

    @OneToMany
    @JoinColumn(name = "grade_id")
    private Set<TwoWayStudentEntity> studentSet;

    public TwoWayGradeEntity() {
    }
}
