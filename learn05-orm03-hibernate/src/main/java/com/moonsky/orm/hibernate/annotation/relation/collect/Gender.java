package com.moonsky.orm.hibernate.annotation.relation.collect;

/**
 * @author benshaoye
 */
public enum Gender {
    MALE,
    FEMALE,
    UNKNOWN
}
