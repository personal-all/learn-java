package com.moonsky.orm.hibernate.xml.many2many;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Set;

/**
 * @author benshaoye
 */
@Data
public class ProjectEntity {

    private String id;

    private String projectName;
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<DeveloperEntity> developerSet;
}
