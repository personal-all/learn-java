package com.moonsky.orm.hibernate.annotation.relation.twowayo2m;

import com.moonsky.orm.hibernate.annotation.BaseUuidEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * @author benshaoye
 * @see AnimalEntity
 * @see BuffaloEntity
 * @see TwoWayGradeEntity 见注释
 * @see TwoWayStudentEntity
 */
@Data
@Entity(name = "tb_ann_o2m_two_way_animal")
public class AnimalEntity extends BaseUuidEntity {

    private String animalName;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "animal")
    private List<BuffaloEntity> buffaloList;

    public AnimalEntity() {
    }
}
