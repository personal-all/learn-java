package com.moonsky.orm.hibernate.xml.component;

import lombok.Data;

/**
 * @author benshaoye
 */
@Data
public class ComponentAddressEntity {

    private String location;

    private String contactName;

    private String contactMobile;

    public ComponentAddressEntity() {}
}
