package com.moonsky.orm.hibernate.annotation.cascade.o1m;

import com.moonsky.orm.hibernate.annotation.cascade.BaseBean;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import java.util.Set;

/**
 * @author benshaoye
 */
@Data
@Entity(name = "tb_cascade_o2m_province")
public class ProvinceBean extends BaseBean {

    @Column(name = "province_name")
    private String provinceName;

    @Column(name = "province_code")
    private String provinceCode;

    @OneToMany(mappedBy = "province")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Set<CityBean> cities;

    public ProvinceBean() {
    }
}
