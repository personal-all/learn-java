package com.moonsky.orm.hibernate.hql;

import com.moonsky.orm.hibernate.annotation.BaseUuidEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.Date;

/**
 * 消费者
 *
 * @author benshaoye
 */
@Data
@Entity(name = "tb_ann_hql_customer")
public class CustomerEntity extends BaseUuidEntity {

    @Column(name = "customer_name")
    private String customerName;

    private String telephone;

    private String address;

    private String email;

    private String sex;

    private String description;

    private Integer age;

    private Date date;

    public CustomerEntity(
        String customerName,
        String telephone,
        String address,
        String email,
        String sex,
        String description,
        Integer age,
        Date date
    ) {
        this.customerName = customerName;
        this.telephone = telephone;
        this.address = address;
        this.email = email;
        this.sex = sex;
        this.description = description;
        this.age = age;
        this.date = date;
    }

    public CustomerEntity() {}
}
