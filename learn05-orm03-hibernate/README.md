#### 主键策略
参考类：org.hibernate.id.factory.internal.DefaultIdentifierGeneratorFactory.DefaultIdentifierGeneratorFactory

#### 项目结构
- 注意要将映射文件放在 resources 目录，而且目录结构要一致
```
|- src
|   |- main
|   |    |- java
|   |    |    |- com
|   |    |    |   |- moon
|   |    |    |   |    |- entity
|   |    |    |   |    |    |- UserEntity.java
|   |    |- resources
|   |    |    |- com
|   |    |    |   |- moon
|   |    |    |   |    |- entity
|   |    |    |   |    |    |- UserEntity.hbm.xml
|   |- test
|   |    |- java
|   |    |- resources
|
```

#### hibernate 数据库方言列表

| 数据库	                     | 方言名                                       | 类指向                          |
|----------------------------|---------------------------------------------|--------------------------------|
| DB2	                     | org.hibernate.dialect.DB2Dialect            | {@link DB2Dialect}             |
| HSQLDB	                 | org.hibernate.dialect.HSQLDialect           | {@link HSQLDialect}            |
| HypersonicSQL              | org.hibernate.dialect.HSQLDialect           | {@link HSQLDialect}            |
| Informix                   | org.hibernate.dialect.InformixDialect       | {@link InformixDialect}        |
|                            | org.hibernate.dialect.Informix10Dialect     | {@link Informix10Dialect}      |
| Ingres                     | org.hibernate.dialect.IngresDialect         | {@link IngresDialect}          |
|                            | org.hibernate.dialect.Ingres9Dialect        | {@link Ingres9Dialect}         |
|                            | org.hibernate.dialect.Ingres10Dialect       | {@link Ingres10Dialect}        |
| Interbase                  | org.hibernate.dialect.InterbaseDialect      | {@link InterbaseDialect}       |
| Microsoft SQL Server 2000  | org.hibernate.dialect.SQLServerDialect      | {@link SQLServerDialect}       |
| Microsoft SQL Server 2005  | org.hibernate.dialect.SQLServer2005Dialect  | {@link SQLServer2005Dialect}   |
| Microsoft SQL Server 2008  | org.hibernate.dialect.SQLServer2008Dialect  | {@link SQLServer2008Dialect}   |
| MySQL                      | org.hibernate.dialect.MySQLDialect          | {@link MySQLDialect}           |
|                            | org.hibernate.dialect.MySQL5Dialect         | {@link MySQL5Dialect}          |
|                            | org.hibernate.dialect.MySQL5InnoDBDialect   | {@link MySQL5InnoDBDialect}    |
|                            | org.hibernate.dialect.MySQL55Dialect        | {@link MySQL55Dialect}         |
|                            | org.hibernate.dialect.MySQL57Dialect        | {@link MySQL57Dialect}         |
|                            | org.hibernate.dialect.MySQL57InnoDBDialect  | {@link MySQL57InnoDBDialect}   |
|                            | org.hibernate.dialect.MySQL8Dialect         | {@link MySQL8Dialect}          |
| Oracle (any version)       | org.hibernate.dialect.OracleDialect         | {@link OracleDialect}          |
| Oracle 8i                  | org.hibernate.dialect.Oracle8iDialect       | {@link Oracle8iDialect}        |
| Oracle 9i                  | org.hibernate.dialect.Oracle9iDialect       | {@link Oracle9iDialect}        |
| Oracle 10g                 | org.hibernate.dialect.Oracle10gDialect      | {@link Oracle10gDialect}       |
| Oracle 11g                 | org.hibernate.dialect.Oracle10gDialect      | {@link Oracle10gDialect}       |
| Oracle 12c                 | org.hibernate.dialect.Oracle12cDialect      | {@link Oracle12cDialect}       |
| PostgreSQL                 | org.hibernate.dialect.PostgreSQL81Dialect   | {@link PostgreSQL81Dialect}    |
|                            | org.hibernate.dialect.PostgreSQL82Dialect   | {@link PostgreSQL82Dialect}    |
|                            | org.hibernate.dialect.PostgreSQL9Dialect    | {@link PostgreSQL9Dialect}     |
|                            | org.hibernate.dialect.PostgreSQL91Dialect   | {@link PostgreSQL91Dialect}    |
|                            | org.hibernate.dialect.PostgreSQL92Dialect   | {@link PostgreSQL92Dialect}    |
|                            | org.hibernate.dialect.PostgreSQL93Dialect   | {@link PostgreSQL93Dialect}    |
|                            | org.hibernate.dialect.PostgreSQL94Dialect   | {@link PostgreSQL94Dialect}    |
|                            | org.hibernate.dialect.PostgreSQL95Dialect   | {@link PostgreSQL95Dialect}    |
|                            | org.hibernate.dialect.PostgreSQL10Dialect   | {@link PostgreSQL10Dialect}    |
| Progress                   | org.hibernate.dialect.ProgressDialect       | {@link ProgressDialect}        |
| SAP DB                     | org.hibernate.dialect.SAPDBDialect          | {@link SAPDBDialect}           |
| Sybase                     | org.hibernate.dialect.SybaseDialect         | {@link SybaseDialect}          |
|                            | org.hibernate.dialect.Sybase11Dialect       | {@link Sybase11Dialect}        |
| Sybase Anywhere            | org.hibernate.dialect.SybaseAnywhereDialect | {@link SybaseAnywhereDialect}  |
