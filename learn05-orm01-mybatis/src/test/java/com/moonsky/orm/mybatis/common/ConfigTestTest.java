package com.moonsky.orm.mybatis.common;

import com.alibaba.druid.pool.DruidDataSource;
import com.moon.core.lang.JoinerUtil;
import com.moon.core.lang.StringUtil;
import com.moon.core.util.Console;
import com.moon.core.util.runner.Runner;
import com.moon.core.util.runner.RunnerUtil;
import com.moonsky.orm.mybatis.common.Config;
import com.moonsky.orm.mybatis.common.bean.Light;
import com.moonsky.orm.mybatis.common.bean.Student;
import com.moonsky.orm.mybatis.common.bean.scan.Admin;
import com.moonsky.orm.mybatis.common.bean.scan.Bag;
import com.moonsky.orm.mybatis.common.bean.scan.User;
import com.moonsky.orm.mybatis.common.interceptor.MoonskyInterceptor;
import com.moonsky.orm.mybatis.common.objectfactory.MoonObjectFactory;
import com.moonsky.orm.mybatis.common.objwrapfactory.MyObjWrapFactory;
import com.moonsky.orm.mybatis.common.reflector.MyReflectorFactory;
import org.apache.ibatis.binding.MapperRegistry;
import org.apache.ibatis.builder.annotation.MapperAnnotationBuilder;
import org.apache.ibatis.builder.xml.XMLConfigBuilder;
import org.apache.ibatis.builder.xml.XMLMapperBuilder;
import org.apache.ibatis.builder.xml.XMLMapperEntityResolver;
import org.apache.ibatis.datasource.DataSourceFactory;
import org.apache.ibatis.mapping.DatabaseIdProvider;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.parsing.PropertyParser;
import org.apache.ibatis.parsing.XNode;
import org.apache.ibatis.parsing.XPathParser;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.reflection.ReflectorFactory;
import org.apache.ibatis.reflection.factory.ObjectFactory;
import org.apache.ibatis.reflection.wrapper.ObjectWrapperFactory;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.type.TypeAliasRegistry;
import org.apache.ibatis.type.TypeHandlerRegistry;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.EntityResolver;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

/**
 * 本类主要测试 mybatis-config.xml 的基本配置，大多数功能性测试暂未测试
 *
 * @author benshaoye
 */
class ConfigTestTest {

    static InputStream getInput(String config) {
        return ConfigTestTest.class.getResourceAsStream(config);
    }


    /**
     * 配置文件的 properties 节点会被解析成一个{@link Properties}
     * 在之后的所有（包括 mapper 文件） attribute 和 文本节点 都可以通过 el 表达式引用这里的值
     * <p>
     * 参考：{@link XNode#parseAttributes(Node)}、
     * {@link XNode#parseBody(Node)} + {@link XNode#getBodyData(Node)}
     * <p>
     * properties 中存在节点但没有值的“值”是空字符串，为空（null）说明不存在这个 name 的 property 节点
     *
     * @param path
     *
     * @see PropertyParser#KEY_ENABLE_DEFAULT_VALUE =org.apache.ibatis.parsing.PropertyParser.enable-default-value
     * -  默认：false {@link PropertyParser#ENABLE_DEFAULT_VALUE}
     * @see PropertyParser#KEY_DEFAULT_VALUE_SEPARATOR =org.apache.ibatis.parsing.PropertyParser.key-default-value-separator
     * -  默认：“:” {@link PropertyParser#DEFAULT_VALUE_SEPARATOR}
     * -  开启这两个属性后，取值可以如下：${key:defaultValue}，{@link PropertyParser.VariableTokenHandler#handleToken(String)}
     * -
     * @see Configuration#setVariables(Properties)
     * @see XPathParser#XPathParser(Reader, boolean, Properties, EntityResolver)
     * @see XMLConfigBuilder#XMLConfigBuilder(Reader, String, Properties)
     * @see XMLConfigBuilder#XMLConfigBuilder(InputStream, String, Properties)
     * -
     * @see XMLConfigBuilder#parseConfiguration(XNode)
     * @see XMLConfigBuilder#propertiesElement(XNode)
     */
    @DisplayName("Property 优先级：resource 覆盖 property 节点")
    @ParameterizedTest
    @ValueSource(strings = "/common/mybatis-config-00-properties.xml")
    void testMyBatisConfig00(String path) throws IOException {
        Properties properties = new Properties();
        properties.load(getInput("/database.properties"));
        InputStream stream = getInput(path);
        XMLConfigBuilder parser = new XMLConfigBuilder(stream);

        SqlSessionFactoryBuilder factoryBuilder = new SqlSessionFactoryBuilder();

        Configuration config = parser.parse();
        Properties props = config.getVariables();
        properties.forEach((name, value) -> {
            assertEquals(value, props.getProperty((String) name));
            String str = String.format("Key: [%s], Value: [%s]", name, value);
            System.out.println(str);
        });
        assertEquals("默认值", props.getProperty("datasource.defaultValue"));
        factoryBuilder.build(config);

        assertFalse(config.isUseColumnLabel());

        Console.out.println("Non value attr of value: [{}], ", props.getProperty("non.value.attr"));
        Console.out
            .println("Not present prop name: [non.value.attr1], value: [{}], ", props.getProperty("non.value.attr1"));
    }

    /**
     * @param path
     *
     * @see Configuration#setVariables(Properties)
     * @see XPathParser#XPathParser(Reader, boolean, Properties, EntityResolver)
     * @see XMLConfigBuilder#XMLConfigBuilder(Reader, String, Properties)
     * @see XMLConfigBuilder#XMLConfigBuilder(InputStream, String, Properties)
     * @see XMLConfigBuilder#parseConfiguration(XNode)
     * @see XMLConfigBuilder#propertiesElement(XNode)
     */
    @DisplayName("Property 优先级：硬编码优先级最高")
    @ParameterizedTest
    @ValueSource(strings = "/common/mybatis-config-01-properties.xml")
    void testMyBatisConfig01(String path) {
        Properties prevDefinition = new Properties();
        prevDefinition.put("datasource.username", "1");
        prevDefinition.put("datasource.password", "1");
        prevDefinition.put("datasource.url", "1");
        prevDefinition.put("datasource.driver-class-name", "1");

        InputStream stream = getInput(path);
        XMLConfigBuilder parser = new XMLConfigBuilder(stream, null, prevDefinition);
        Configuration config = parser.parse();
        assertTrue(config.isUseColumnLabel());

        Properties props = config.getVariables();
        prevDefinition.forEach((name, value) -> {
            String inProps = props.getProperty((String) name);
            String str;
            str = String.format("Key: [%s], Value: [%s]", name, value);
            System.out.println(str);
            System.out.println("=====================================");
            assertEquals(value, inProps);
        });

        SqlSessionFactoryBuilder factoryBuilder = new SqlSessionFactoryBuilder();
        factoryBuilder.build(config);
    }

    /**
     * typeAlias 节点声明的 alias 属性声明的类别名优先级最高
     * 其次是案通过 @Alias 注解声明的类别名
     * 最后注册或扫描到的类默认以类简称为别名；
     * 未在 mybatis 注册的类也可通过类全名得到
     * <p>
     * <p>
     * 初始化 typeAliases，这里定义了 mybatis 内置类别名
     * {@link TypeAliasRegistry#TypeAliasRegistry()}
     * {@link Configuration#Configuration()}
     *
     * @param path
     *
     * @see XMLConfigBuilder#parseConfiguration(XNode)
     * @see XMLConfigBuilder#typeAliasesElement(XNode)
     */
    @ParameterizedTest
    @ValueSource(strings = "/common/mybatis-config-02-type-aliases.xml")
    void testTypeAliases(String path) {
        InputStream stream = getInput(path);
        XMLConfigBuilder parser = new XMLConfigBuilder(stream);
        Configuration config = parser.parse();

        TypeAliasRegistry registry = config.getTypeAliasRegistry();
        Map<String, Class<?>> aliases = new TreeMap<>(registry.getTypeAliases());

        assertSame(Student.class, aliases.get("student0"));
        assertSame(Student.class, registry.resolveAlias("Student0"));

        assertSame(Light.class, aliases.get(Config.LIGHT_NAME.toLowerCase()));
        assertSame(Light.class, registry.resolveAlias(Config.LIGHT_NAME));

        assertSame(Bag.class, aliases.get(Config.BAG_NAME.toLowerCase()));
        assertSame(Bag.class, registry.resolveAlias(Config.BAG_NAME));

        assertSame(Admin.class, aliases.get(Admin.class.getSimpleName().toLowerCase()));
        assertSame(Admin.class, registry.resolveAlias(Admin.class.getSimpleName()));

        assertSame(User.class, aliases.get(User.class.getSimpleName().toLowerCase()));
        assertSame(User.class, registry.resolveAlias(User.class.getSimpleName()));

        aliases.forEach((alias, cls) -> {
            String str = String.format("Name: [%s], class: [%s]", alias, cls);
            System.out.println(str);
        });
    }

    /**
     * 此方法不测试 datasource，即配置文件的 environments 节点
     * 配置文件也关闭了 environments 的配置
     * <p>
     * typeHandlers 虽然在配置文件里声明靠前，但解析却在倒数第二步，即 mappers 节点前，
     * 所以此方法不测试 typeHandlers
     *
     * @throws Exception
     * @see ObjectFactory#setProperties(Properties)
     * @see ObjectWrapperFactory
     * @see ReflectorFactory
     */
    @ParameterizedTest
    @ValueSource(strings = "/common/mybatis-config-03-object-factory.xml")
    void testObjectFactory(String path) throws Exception {
        InputStream stream = getInput(path);
        XMLConfigBuilder parser = new XMLConfigBuilder(stream);
        Configuration config = parser.parse();

        ObjectFactory factory = config.getObjectFactory();
        assertTrue(factory instanceof MoonObjectFactory);

        MoonObjectFactory moonFactory = (MoonObjectFactory) factory;
        Properties props = moonFactory.getProperties();
        assertEquals(2, props.size());
        assertEquals("2", props.getProperty("obj.key2"));
        assertEquals("1", props.getProperty("obj.key1"));

        ObjectWrapperFactory wrapperFactory = config.getObjectWrapperFactory();
        assertTrue(wrapperFactory instanceof MyObjWrapFactory);

        ReflectorFactory reflectorFactory = config.getReflectorFactory();
        assertTrue(reflectorFactory instanceof MyReflectorFactory);
    }

    /**
     * Mybatis 插件
     * <p>
     * 通过 plugins 声明的插件需要实现接口{@link Interceptor}，
     * 每个插件 plugin 可以包含若干名为 property 的子节点，这些将被解析成一个 properties
     * 并调用 {@link Interceptor#setProperties(Properties)}作为插件的初始参数
     *
     * @param path
     *
     * @see XMLConfigBuilder#parseConfiguration(XNode)
     * @see XMLConfigBuilder#pluginElement(XNode)
     */
    @ParameterizedTest
    @ValueSource(strings = "/common/mybatis-config-04-plugins.xml")
    void testPlugins(String path) {
        InputStream stream = getInput(path);
        XMLConfigBuilder parser = new XMLConfigBuilder(stream);
        Configuration config = parser.parse();

        List<Interceptor> interceptorList = config.getInterceptors();
        assertNotNull(interceptorList);
        assertEquals(interceptorList.size(), 1);
        Set<Interceptor> interceptors = new HashSet<>(interceptorList);

        int count = 0;
        MoonskyInterceptor moonskyInterceptor = null;
        for (Interceptor interceptor : interceptors) {
            if (interceptor instanceof MoonskyInterceptor) {
                moonskyInterceptor = (MoonskyInterceptor) interceptor;
                count++;
            }
        }
        assertNotNull(moonskyInterceptor);
        assertEquals(1, count);
        Properties props = moonskyInterceptor.getProperties();
        assertEquals(3, props.size());
        assertEquals("0", props.getProperty("key0"));
        assertEquals("1", props.getProperty("key1"));
        assertEquals("2", props.getProperty("key2"));
    }

    /**
     * 可以使用自定义数据库连接池，需要自定义实现接口{@link DataSourceFactory}
     *
     * @param path
     *
     * @throws Exception
     * @see XMLConfigBuilder#environmentsElement(XNode)
     * @see DataSourceFactory#getDataSource()
     * @see Environment.Builder#dataSource(DataSource)
     * @see Environment#getDataSource()
     */
    @ParameterizedTest
    @ValueSource(strings = "/common/mybatis-config-05-environment.xml")
    void testEnvironment(String path) throws Exception {
        InputStream stream = getInput(path);
        XMLConfigBuilder parser = new XMLConfigBuilder(stream);
        Configuration config = parser.parse();

        DataSource dataSource = config.getEnvironment().getDataSource();
        assertTrue(dataSource instanceof DruidDataSource);
    }

    /**
     * databaseIdProvider 节点解析后会立即执行{@link DatabaseIdProvider#setProperties(Properties)}方法，
     * <p>
     * 如果正确声明了 environments 节点（存在数据源），此时会立即执行{@link DatabaseIdProvider#getDatabaseId(DataSource)}
     * <p>
     * databaseId 只对 mapper 文件中的“语句”节点有效，如：sql、select、insert、update、delete
     * 参考：{@link XMLMapperBuilder#sqlElement(List, String)}、
     * {@link XMLMapperBuilder#buildStatementFromContext(List, String)}
     *
     * @param path
     *
     * @throws Exception
     * @see XMLConfigBuilder#databaseIdProviderElement(XNode)
     */
    @ParameterizedTest
    @ValueSource(strings = "/common/mybatis-config-06-database-id-provider.xml")
    void testDatabaseIdProvider(String path) throws Exception {
        InputStream stream = getInput(path);
        XMLConfigBuilder parser = new XMLConfigBuilder(stream);
        Configuration config = parser.parse();

        Console.out.println(config.getDatabaseId());
    }

    /**
     * 首先还在很前面的时候，就定义了默认枚举处理器，枚举处理器定义在 settings 中
     * {@link Configuration#setDefaultEnumTypeHandler(Class)}
     * {@link XMLConfigBuilder#settingsAsProperties(XNode)}
     * {@link TypeHandlerRegistry#setDefaultEnumTypeHandler(Class)}
     * <p>
     * 首先{@link TypeHandlerRegistry#TypeHandlerRegistry()}注册了一些系统内置的处理器
     *
     * @param path
     *
     * @throws Exception
     * @see XMLConfigBuilder#typeHandlerElement(XNode)
     */
    @ParameterizedTest
    @ValueSource(strings = "/common/mybatis-config-07-type-handlers.xml")
    void testTypeHandlers(String path) throws Exception {
        InputStream stream = getInput(path);
        XMLConfigBuilder parser = new XMLConfigBuilder(stream);
        Configuration config = parser.parse();

        TypeHandlerRegistry registry = config.getTypeHandlerRegistry();
    }

    /**
     * mapper 节点可以包含三种属性，分别包含三种映射方式，
     * 但每个 mapper 节点只能包含其中一个属性
     * - resource:从 classpath 中加载 class 文件
     * - url:加载远程资源
     * - class：加载 mapper 接口，只能是接口，{@link MapperRegistry#addMapper(Class)}一开始就做了是否是接口判断
     * 接口是通过{@link MapperAnnotationBuilder}解析的
     * <p>
     * <p>
     * mapper.xml 文件解析核心类：{@link XPathParser}、{@link XMLMapperBuilder}
     * <p>
     * 1. {@link XPathParser}是一个 XML 文件解析器，将文件解析成一个 java 可读的 XML 文档
     * - 它采用的是 java 内置 xml 解析器，它包括验证（验证文件节点是否合法）和解析
     * <p>
     * - mybatis-config.xml 文件也是 XPathParser 解析的，这里用到的验证规则在包{@link org.apache.ibatis.builder.xml}下
     * 所以即使在 mybatis-config.xml 中不声明 DOCTYPE 头，但使用了不正确的语法，这里仍然会报错，
     * DOCTYPE 中声明的文件只是用于方便开发时的书写，这是 IDE 所支持的
     * 参考：{@link XMLMapperEntityResolver}
     * 参考：{@link Document}
     * <p
     * 2. 上一步解析完 XML 文档后，这里会在{@link XMLMapperBuilder}中解析成 mybatis 规范的“SQL 语句拼接器”
     *
     * @param path
     *
     * @throws Exception
     * @see XMLConfigBuilder#mapperElement(XNode)
     */
    @ParameterizedTest
    @ValueSource(strings = "/common/mybatis-config-08-mappers.xml")
    void testMappers(String path) throws Exception {
        InputStream stream = getInput(path);
        XMLConfigBuilder parser = new XMLConfigBuilder(stream);
        Configuration config = parser.parse();

    }

    Map getMap() {
        Map map = new HashMap();
        map.put("key0", "value");
        map.put("key1", "value");
        map.put("key2", "value");
        return map;
    }

    @Test
    void testForBuildMap() throws Exception {
        Runner runner = RunnerUtil.parse("{key0:'value',key1:'value',key2:'value'}");
        for (int i = 0; i < 100; i++) {
            getMap();
            runner.getClass();
        }
        Map map = runner.run();
        Console.out.println(getMap());
        Console.out.println(map);
        assertEquals(map, getMap());

        int count = 1_000_000_000 / 10000000;
        Console.out.time();
        for (int index = 0; index < count; index++) {
            getMap();
        }
        Console.out.timeEnd();
        Console.out.time();
        for (int i = 0; i < count; i++) {
            runner.run();
        }
        Console.out.timeEnd();
    }

    @ParameterizedTest
    @CsvSource("ds.id.key,value,5")
    void testGeneratePropertyNode(String namePrefix, String valuePrefix, int count) throws Exception {
        int length = String.valueOf(count).length();
        String template = "<property name=\"%s%s\" value=\"%s%s\"/>";
        for (int i = 0; i < count; i++) {
            String value = StringUtil.padStart(i, length, '0');
            String str = String.format(template, namePrefix, value, valuePrefix, value);
            System.out.println(str);
        }
        System.out.println(JoinerUtil.of("=").useForNull("").join(new String[150]));
        Console.out.printStackTrace();
    }
}