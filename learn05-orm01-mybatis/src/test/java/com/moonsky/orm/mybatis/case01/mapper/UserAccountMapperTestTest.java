package com.moonsky.orm.mybatis.case01.mapper;

import org.apache.ibatis.builder.xml.XMLConfigBuilder;
import org.apache.ibatis.parsing.XNode;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author benshaoye
 */
class UserAccountMapperTestTest {

    static void assertInvalidLoadContext(String path) {
        assertThrows(Exception.class, () -> getContext(path));
    }

    static ApplicationContext getContext(String path) {
        return new ClassPathXmlApplicationContext(path);
    }

    @DisplayName("mapper 文件必须定义 namespace")
    @ParameterizedTest
    @ValueSource(strings = "classpath:case01/required-namespace/mybatis-spring.xml")
    void testMapperNamespaceIsRequired(String path) throws Exception {
        assertInvalidLoadContext(path);
    }

    /**
     * {@link SqlSessionFactoryBean#configurationProperties}优先于 properties 的 resource；
     * resource 优先于 property
     *
     * @param path
     *
     * @throws Exception
     */
    @DisplayName("mybatis-config.xml 的属性覆盖优先级")
    @ParameterizedTest
    @ValueSource(strings = "classpath:case01/assert01-properties/mybatis-spring.xml")
    void testAssertProperties(String path) throws Exception {
        ApplicationContext context = getContext(path);
        SqlSessionFactoryBean factoryBean = context.getBean(SqlSessionFactoryBean.class);
        assertFalse(context.containsBean(Configuration.class.getName()));
        Configuration configuration = factoryBean.getObject().getConfiguration();
        Properties properties = configuration.getVariables();

        String value = properties.getProperty("override.by.configurationProperties");
        assertEquals("isSpringConfigValue", value);

        value = properties.getProperty("override.by.resource");
        assertEquals("isResourceValue", value);

        value = properties.getProperty("non.value.attr");
        assertEquals("this.is.an.empty.value", value);

        value = properties.getProperty("mybatis.spring.key");
        assertEquals("ref.mybatis.spring.key", value);

        value = properties.getProperty("mybatis.spring.value");
        assertEquals("ref.mybatis.spring.value", value);

        value = properties.getProperty("ref.mybatis.spring.key");
        assertEquals("ref.mybatis.spring.value", value);
    }

    /**
     * properties 默认值，需要配置两个属性：
     * <p>
     * {@link org.apache.ibatis.parsing.PropertyParser#KEY_ENABLE_DEFAULT_VALUE}（默认 false）
     * org.apache.ibatis.parsing.PropertyParser.enable-default-value=true
     * <p>
     * {@link org.apache.ibatis.parsing.PropertyParser#KEY_DEFAULT_VALUE_SEPARATOR}（默认英文冒号）
     * org.apache.ibatis.parsing.PropertyParser.default-value-separator=:
     *
     * @param path
     *
     * @throws Exception
     * @see XMLConfigBuilder#propertiesElement(XNode)
     */
    @DisplayName("mybatis-config.xml 默认值")
    @ParameterizedTest
    @ValueSource(strings = "classpath:case01/assert02-default-value/mybatis-spring.xml")
    void testDefaultProperty(String path) throws Exception {
        ApplicationContext context = getContext(path);
        SqlSessionFactoryBean factoryBean = context.getBean(SqlSessionFactoryBean.class);
        assertFalse(context.containsBean(Configuration.class.getName()));
        Configuration configuration = factoryBean.getObject().getConfiguration();
        Properties properties = configuration.getVariables();

        // el 表达式取值会从外部参数（properties）中读取

        // el 表达式存在指定值
        String value = properties.getProperty("the.key.present");
        assertEquals("present.value", value);

        // 不存在指定值，使用默认值
        value = properties.getProperty("the.key.absent");
        assertEquals("the.value.absent.use.default.value", value);

        // 不存在指定值，也不存在默认值，使用源完整 el 表达式作为值
        value = properties.getProperty("the.all.absent");
        assertEquals("${the.value.absent}", value);

        // el 表达式取值不会从 resource 或 url 中读取
        value = properties.getProperty("the.resource.preset.key");
        assertEquals("${the.key.present.in.resource}", value);
    }

    @Disabled
    @ParameterizedTest
    @ValueSource(strings = "classpath:case01/assert03-insert/mybatis-spring.xml")
    void testInsertUserAccount(String path) throws Exception {
        ApplicationContext context = getContext(path);
        SqlSessionFactoryBean factoryBean = context.getBean(SqlSessionFactoryBean.class);
        SqlSessionFactory sessionFactory = factoryBean.getObject();

        SqlSession session = sessionFactory.openSession();

        System.out.println(context.containsBean(UserAccountMapper.class.getName()));;
    }
}