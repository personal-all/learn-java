package com.moonsky.orm.mybatis.common;

import org.apache.ibatis.builder.MapperBuilderAssistant;
import org.apache.ibatis.builder.annotation.MapperAnnotationBuilder;
import org.apache.ibatis.builder.xml.XMLMapperBuilder;
import org.apache.ibatis.parsing.XNode;
import org.apache.ibatis.parsing.XPathParser;
import org.apache.ibatis.session.Configuration;
import org.w3c.dom.Document;

import java.util.List;

/**
 * @author benshaoye
 */
public class Mapper {
    /**
     * Annotation Mapper 解析步骤：
     * 1. 不可重复解析和注册：{@link MapperAnnotationBuilder#parse()}
     * 2. 首先查找与接口同目录是否存在同名 xml mapper 文件；
     * 如：接口 com.moon.dao.UserMapper 对应：com/moon/dao/UserMapper.xml；
     * {@link MapperAnnotationBuilder#loadXmlResource()}【这一步不是必须的】
     *
     */

    /**
     * **Mapper.xml 文件解析步骤
     * 1. 首先通过{@link XPathParser}验证并加载 mapper 文件：
     * > 在解析过程中全局变量 properties 始终有效，在 mapper 中任何属性名和文本节点都可以通过
     * “${}”读取里面的值
     * <br>【注】只是有效，此时只是加载了{@link Document}对象，并未解析具体的属性和内容
     *
     * 2. 执行解析：{@link XMLMapperBuilder#parse()}
     * 2.1 验证{@link Configuration}是否已经加载洗过过这个文件，如果加载过将**忽略**这一步，否则，
     * 首先解析这个文件：{@link XMLMapperBuilder#configurationElement(XNode)}，每个 mapper
     * 文件必须要定义 namespace 属性，并且期望它和对应的接口同名
     * > 参考：{@link MapperBuilderAssistant#setCurrentNamespace(String)}；
     * <br>后面会用到：{@link MapperBuilderAssistant#applyCurrentNamespace(String, boolean)}，
     * <br>这个方法的意思是，当前 mapper 文件下的所有子节点（如：parameterType、resultMap、select、sql 等）
     * 所定义的 id 必须以{@link MapperBuilderAssistant#currentNamespace}开头
     * 这一步的主要内容有：
     * 2.1.1 解析 cache-ref 节点：{@link XMLMapperBuilder#cacheRefElement(XNode)}
     * 2.1.2 解析 cache 节点：{@link XMLMapperBuilder#cacheElement(XNode)}
     * > 一个 mapper 文件最多只能有一个`<cache-ref/>`和`<cache/>`节点
     * 2.1.3 解析 parameterType 和 resultMap，可以声明多个 id 不同的 parameterType 和 resultMap：
     * {@link XMLMapperBuilder#parameterMapElement(List<XNode>)}、
     * {@link XMLMapperBuilder#resultMapElements(List<XNode>)}
     * 2.1.4 解析 sql 语句片段：
     * {@link XMLMapperBuilder#sqlElement(List<XNode>)}
     * {@link XMLMapperBuilder#sqlElement(List<XNode>, String)}
     */
}
