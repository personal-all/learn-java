package com.moonsky.orm.mybatis.common.objwrapfactory;

import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.reflection.wrapper.ObjectWrapper;
import org.apache.ibatis.reflection.wrapper.ObjectWrapperFactory;

/**
 * @author benshaoye
 */
public class MyObjWrapFactory implements ObjectWrapperFactory {

    @Override
    public boolean hasWrapperFor(Object object) {
        String str = String.format("调用方法： %s，属于：[%s]。", "hasWrapperFor(Object object)", getClass());
        System.out.println(str);
        return false;
    }

    @Override
    public ObjectWrapper getWrapperFor(MetaObject metaObject, Object object) {
        String str = String.format("调用方法： %s，属于：[%s]。",
            "getWrapperFor(MetaObject metaObject, Object object)",
            getClass());
        System.out.println(str);
        return null;
    }
}
