package com.moonsky.orm.mybatis.case01.mapper;

import com.moonsky.orm.mybatis.case01.bean.UserAccount;

/**
 * @author benshaoye
 */
public interface UserAccountMapper {

    int insertUserAccount(UserAccount account);
}
