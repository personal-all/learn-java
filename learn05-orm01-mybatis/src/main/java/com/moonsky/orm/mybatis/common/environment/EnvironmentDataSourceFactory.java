package com.moonsky.orm.mybatis.common.environment;

import com.alibaba.druid.pool.DruidDataSource;
import com.moon.core.lang.StackTraceUtil;
import com.moon.core.util.Console;
import org.apache.ibatis.datasource.DataSourceFactory;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * 可以使用系统内置的数据库连接池，自定义数据库连接池需要实现接口{@link DataSourceFactory}
 * <p>
 * 见配置文件：classpath:mybatis-config-environment.xml
 *
 * @author benshaoye
 */
public class EnvironmentDataSourceFactory implements DataSourceFactory {

    private Properties properties;

    @Override
    public void setProperties(Properties props) {
        Console.out.println("调用方法：{}",StackTraceUtil.getCallerTrace());
        props.forEach((key, value) -> Console.out.println("Key: [{}], value: [{}]", key, value));
        this.properties = props;
    }

    @Override
    public DataSource getDataSource() {
        Console.out.println("调用方法：{}",StackTraceUtil.getCallerTrace());
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.configFromPropety(this.properties);
        this.properties = null;
        return dataSource;
    }

}
