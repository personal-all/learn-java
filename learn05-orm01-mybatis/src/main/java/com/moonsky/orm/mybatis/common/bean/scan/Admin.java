package com.moonsky.orm.mybatis.common.bean.scan;

/**
 * @author benshaoye
 */
public class Admin {
    private String username;
    private String password;
    private int age;

    public Admin() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
