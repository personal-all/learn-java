package com.moonsky.orm.mybatis.common;

/**
 * @author benshaoye
 */
public class Config {

    public final static String BAG_NAME = "BAG_NAME-A1234567890";
    public final static String LIGHT_NAME = "LIGHT_NAME-B1234567890";
    public final static String STUDENT_NAME = "STUDENT_NAME-C1234567890";
}
