package com.moonsky.orm.mybatis.common.objectfactory;

import org.apache.ibatis.reflection.factory.ObjectFactory;

import java.util.List;
import java.util.Properties;

/**
 * @author benshaoye
 */
public class MoonObjectFactory implements ObjectFactory {

    private Properties properties;

    @Override
    public <T> T create(Class<T> type) {
        String str = String.format("调用方法： %s，属于：[%s]。", "create(Class<T> type)", getClass());
        System.out.println(str);
        return null;
    }

    @Override
    public <T> T create(
        Class<T> type, List<Class<?>> constructorArgTypes, List<Object> constructorArgs
    ) {
        String str = String.format("调用方法： %s，属于：[%s]。",
            "create(Class<T> type, List<Class<?>> types, List<Object> args)",
            getClass());
        System.out.println(str);
        return null;
    }

    @Override
    public <T> boolean isCollection(Class<T> type) {
        String str = String.format("调用方法： %s，属于：[%s]。", "isCollection(Class<T> type)", getClass());
        System.out.println(str);
        return false;
    }

    @Override
    public void setProperties(Properties properties) {
        String str = String.format("调用方法： %s，属于：[%s]。", "(Properties) setter", getClass());
        System.out.println(str);
        this.properties = properties;
    }

    public Properties getProperties() {
        String str = String.format("调用方法： %s，属于：[%s]。", "(Properties) getter", getClass());
        System.out.println(str);
        return properties;
    }
}
