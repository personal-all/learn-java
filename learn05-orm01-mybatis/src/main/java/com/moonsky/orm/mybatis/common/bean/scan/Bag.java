package com.moonsky.orm.mybatis.common.bean.scan;

import com.moonsky.orm.mybatis.common.Config;
import org.apache.ibatis.type.Alias;

/**
 * @author benshaoye
 */
@Alias(Config.BAG_NAME)
public class Bag {
    private String username;
    private int age;

    public Bag() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
