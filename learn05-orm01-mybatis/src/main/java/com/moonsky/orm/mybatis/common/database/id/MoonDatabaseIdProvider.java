package com.moonsky.orm.mybatis.common.database.id;

import com.moon.core.lang.StackTraceUtil;
import com.moon.core.util.Console;
import org.apache.ibatis.mapping.DatabaseIdProvider;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Properties;

/**
 * @author benshaoye
 */
public class MoonDatabaseIdProvider implements DatabaseIdProvider {

    private Properties properties;

    @Override
    public String getDatabaseId(DataSource dataSource) throws SQLException {
        Console.out.println("调用方法：{}",StackTraceUtil.getCallerTrace());
        return dataSource.toString();
    }

    @Override
    public void setProperties(Properties p) {
        Console.out.println("调用方法：{}",StackTraceUtil.getCallerTrace());
        this.properties = p;
    }
}
