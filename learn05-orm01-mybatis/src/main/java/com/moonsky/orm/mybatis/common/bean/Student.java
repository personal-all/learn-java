package com.moonsky.orm.mybatis.common.bean;

import com.moonsky.orm.mybatis.common.Config;
import org.apache.ibatis.type.Alias;

/**
 * @author benshaoye
 */
@Alias(Config.STUDENT_NAME)
public class Student {
    private String username;
    private String password;
    private int age;

    public Student() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
