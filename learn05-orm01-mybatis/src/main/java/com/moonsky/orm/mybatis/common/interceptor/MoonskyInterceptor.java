package com.moonsky.orm.mybatis.common.interceptor;

import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Invocation;

import java.util.Properties;

/**
 * @author benshaoye
 */
public class MoonskyInterceptor implements Interceptor {

    private Properties properties;

    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        String str = String.format("调用方法： %s，属于：[%s]。", "intercept(Invocation invocation)", getClass());
        System.out.println(str);
        return null;
    }

    @Override
    public Object plugin(Object target) {
        String str = String.format("调用方法： %s，属于：[%s]。", "plugin(Object target)", getClass());
        System.out.println(str);
        return null;
    }

    @Override
    public void setProperties(Properties properties) {
        String str = String.format("调用方法： %s，属于：[%s]。", "(Properties) setter", getClass());
        System.out.println(str);
        this.properties = properties;
    }

    public Properties getProperties() {
        String str = String.format("调用方法： %s，属于：[%s]。", "(Properties) getter", getClass());
        System.out.println(str);
        return properties;
    }
}
