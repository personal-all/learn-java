package com.moonsky.orm.mybatis.common.reflector;

import org.apache.ibatis.reflection.Reflector;
import org.apache.ibatis.reflection.ReflectorFactory;

/**
 * @author benshaoye
 */
public class MyReflectorFactory implements ReflectorFactory {

    @Override
    public boolean isClassCacheEnabled() {
        String str = String.format("调用方法： %s，属于：[%s]。", "isClassCacheEnabled()", getClass());
        System.out.println(str);
        return false;
    }

    @Override
    public void setClassCacheEnabled(boolean classCacheEnabled) {
        String str = String.format("调用方法： %s，属于：[%s]。", "setClassCacheEnabled(boolean classCacheEnabled)", getClass());
        System.out.println(str);
    }

    @Override
    public Reflector findForClass(Class<?> type) {
        String str = String.format("调用方法： %s，属于：[%s]。", "findForClass(Class<?> type)", getClass());
        System.out.println(str);
        return null;
    }
}
