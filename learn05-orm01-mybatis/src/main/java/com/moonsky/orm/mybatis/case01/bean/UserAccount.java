package com.moonsky.orm.mybatis.case01.bean;

import lombok.Data;

/**
 * @author benshaoye
 */
@Data
public class UserAccount {

    private String id;

    private String username;

    private String password;

    private String mobile;

    private String email;

    private String address;
}
