DROP TABLE IF EXISTS `tb_user_account`;

CREATE TABLE `tb_user_account`
    (`id`       INT         NOT NULL AUTO_INCREMENT PRIMARY KEY,
     `username` VARCHAR(64) NOT NULL,
     `password` VARCHAR(64) NOT NULL,
     `mobile`   VARCHAR(24) NULL,
     `email`    VARCHAR(32),
     `address`  VARCHAR(128)
    ) ENGINE InnoDB
      CHAR SET `utf8`;