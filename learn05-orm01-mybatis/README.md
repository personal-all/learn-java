## mybatis-config.xml 解析步骤
官方文档：[https://mybatis.org/mybatis-3/zh/index.html](https://mybatis.org/mybatis-3/zh/index.html)
配置文件 mybatis-config.xml 详解：[https://gitee.com/744531854/learn-java/blob/master/learn05-orm01-mybatis/src/test/java/com/moonsky/orm/mybatis/ConfigTestTest.java](https://gitee.com/744531854/learn-java/blob/master/learn05-orm01-mybatis/src/test/java/com/moonsky/orm/mybatis/ConfigTestTest.java)
#### 1. 首先语法必须符合要求，比如 typeAliases 子节点只能为 typeAlias 或 package，
开始看见 "package".equals(nodeName) 能“骗过去”，但是我错了，在加载 XML 文档的时候，
就已经验证了；
> 参考：org.apache.ibatis.builder.xml 包下的 dtd 文件和 xsd 文件；
<br>参考：org.apache.ibatis.parsing.XPathParser
<br>参考：org.apache.ibatis.builder.xml.XMLConfigBuilder

#### 2. 解析根节点 configuration 下的 properties 节点，将里面的所有 property
解析成 java.lang.Properties 返回；
> 2.1 properties 节点上可定义 resource 或 url（只能定义其中一个），它们指向的资源也是
Properties，并且会覆盖 properties 的所有子节点，在构造解析的时候会硬编码传入一个 Properties，
这个 Properties 会覆盖配置文件的信息；
<br>2.2 这样做有个好处就是可用于区分部署环境，如开发环境、测试环境、生产环境的切换；
<br>2.3 这里只能定义常量，不能定义 EL 表达式定义的变量引用，只会被作为普通字符串处理；
<br>参考：org.apache.ibatis.builder.xml.XMLConfigBuilder#propertiesElement(XNode)；

#### 3. 第三步是解析 settings 节点
这个节点的所有子节点也会被解析成一个 Properties 返回，
同时要求 settings 下所有属性全是 Configuration 里面存在的属性，
同时区分大小写，否则将抛出异常
> 参考：org.apache.ibatis.session.Configuration
<br>参考：org.apache.ibatis.builder.xml.XMLConfigBuilder#settingsAsProperties(XNode)

#### 4. 加载自定义 VFS（什么是 VFS？）
> settings 里配置的 vfsImpl 属性：
<br>参考：org.apache.ibatis.io.VFS

#### 5. 选择默认日志实现（可选）
> settings 里配置的 logImpl 属性，可选项有（全类名或 key 皆可）：
<br>SLF4J -> org.apache.ibatis.logging.slf4j.Slf4jImpl
<br>COMMONS_LOGGING -> org.apache.ibatis.logging.commons.JakartaCommonsLoggingImpl
<br>LOG4J -> org.apache.ibatis.logging.log4j.Log4jImpl
<br>LOG4J2 -> org.apache.ibatis.logging.log4j2.Log4j2Impl
<br>JDK_LOGGING -> org.apache.ibatis.logging.jdk14.Jdk14LoggingImpl
<br>STDOUT_LOGGING -> org.apache.ibatis.logging.stdout.StdOutImpl
<br>NO_LOGGING -> org.apache.ibatis.logging.nologging.NoLoggingImpl


#### 6. 解析 typeAliases 节点
> 子节点为 package，默认以类名为简称
<br>子节点为 typeAlias，如定义 alias，则以 alias 为简称；若未定义，取类上是否注解 @Alias，
否则以类名为简称；
<br>typeAlias 节点的 type 必须存在；
<br>typeAlias 定义单个别名的必须在 package 前面；
<br>存储 typeAliases 的类 TypeAliasRegistry 里面存的 alias 实际上都是小写字符串

#### 7. 解析 plugins 节点，实际上是一系列“拦截器”，很多额外的功能可通过 mybatis 插件（拦截器）实现
> 每个 plugin 节点的 interceptor 属性是 org.apache.ibatis.plugin.Interceptor 的实现类，
<br>plugin 的所有子节点将会被解析成 Properties，然后调用 Interceptor#setProperties(Properties)，相当于初始化参数;
<br>interceptor 可以是 typeAliases 中定义的别名;

#### 8. 解析 objectFactory 对象工厂
> 解析步骤和 plugins 相同
<br>attrs：type -> Class
<br>参考：org.apache.ibatis.reflection.factory.ObjectFactory

#### 9. 解析 objectWrapperFactory 对象包装工厂
> attrs：type -> Class
<br>解析步骤和 plugins 基本相同，但这是一个单节点，只能定义一个同名节点
<br>参考：org.apache.ibatis.reflection.wrapper.ObjectWrapperFactory

#### 10. 解析：reflectorFactory 反射工厂，单节点
> attrs：type -> Class
<br>参考：org.apache.ibatis.reflection.ReflectorFactory

#### 11. 加载其他 settings 属性
> 区分大小写，参考：org.apache.ibatis.builder.xml.XMLConfigBuilder#settingsElement()
- autoMappingBehavior：
 1. NONE：
 2. PARTIAL：
 3. FULL：
 > 参考：org.apache.ibatis.session.AutoMappingBehavior
- autoMappingUnknownColumnBehavior：
 1. NONE：
 2. WARNING：
 3. FAILING：
 > 参考：org.apache.ibatis.session.AutoMappingUnknownColumnBehavior
- cacheEnabled：true|false
 1. true：
 2. false：
- proxyFactory：代理类
 > 参考：org.apache.ibatis.executor.loader.ProxyFactory
- lazyLoadingEnabled：懒加载
- aggressiveLazyLoading：触发懒加载中的某一项是，是否同时加载其他所有懒加载项
- multipleResultSetsEnabled：多结果集支持
- useColumnLabel：数据库列名到实体字段名，下划线到驼峰命名的转换
- useGeneratedKeys：
- defaultExecutorType：
 1. SIMPLE：
 2. REUSE：
 3. BATCH：
 > 参考：org.apache.ibatis.session.ExecutorType
- defaultStatementTimeout：执行语句超时，Integer
- defaultFetchSize：
- defaultResultSetType：默认 null
 1. DEFAULT：
 2. FORWARD_ONLY：
 3. SCROLL_INSENSITIVE：
 4. SCROLL_SENSITIVE：
 > 参考：org.apache.ibatis.mapping.ResultSetType
- mapUnderscoreToCamelCase：下划线命名到驼峰命名的转换，Boolean
 1. true
 2. false：（默认，搞笑！！！ Mybatis 这个默认为 false 居然是使用了基本类型 boolean）
- safeRowBoundsEnabled：true|false
- localCacheScope：缓存级别
 1. SESSION：（默认）
 1. STATEMENT：
- jdbcTypeForNull：默认 OTHER
 > 参考：org.apache.ibatis.type.JdbcType
- lazyLoadTriggerMethods：懒加载触发方法
 > 默认：equals,clone,hashCode,toString
- safeResultHandlerEnabled：true|false
 1. true：（默认）
 2. false：
- defaultScriptingLanguage：
- defaultEnumTypeHandler
- callSettersOnNulls
- useActualParamName：
 1. true：（默认）
 2. false：
- returnInstanceForEmptyRow：
 1. true：
 2. false：（默认）
- logPrefix：日志前缀
- configurationFactory：
#### 12. 解析执行环境 environments 节点
> attrs：default -> String // required，这是定义在子节点 environment 的 id 属性
<br>1.可包含多个子节点：environment，但只解析与 environments.default 相等的节点；
<br>2.environment：
<br>2.1 environment.transactionManager:required，解析步骤和 plugins 相同
<br>参考：org.apache.ibatis.transaction.TransactionFactory
<br>2.2 environment.dataSource:required，解析步骤和 plugins 相同
<br>参考：org.apache.ibatis.datasource.DataSourceFactory

#### 13. 解析 databaseIdProvider
> 参考：org.apache.ibatis.mapping.DatabaseIdProvider

#### 14 解析 mappers 节点
这一步很容易忽略，mapper 解析就是在这一步完成的，首先逐个解析 mappers 的子节点，
然后逐个加载对应的接口


