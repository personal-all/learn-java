## JUnit 5
### 官方示例：[https://github.com/junit-team/junit5-samples](https://github.com/junit-team/junit5-samples)
> 演示：[https://www.cnblogs.com/followerofjests/p/10466070.html](https://www.cnblogs.com/followerofjests/p/10466070.html)

#### JUnit5 在 MAVEN 环境中测试：
需要在 pom.xml 文件中添加如下配置：[官方说明](https://github.com/junit-team/junit5-samples/blob/master/junit5-jupiter-starter-maven/pom.xml)
```xml
<plugin>
  <groupId>org.apache.maven.plugins</groupId>
  <artifactId>maven-surefire-plugin</artifactId>
  <!-- JUnit 5 requires Surefire version 2.22.0 or higher -->
  <version>2.22.2</version>
</plugin>
```

添加完后的配置文件结构大致如下：
```xml
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="
          http://maven.apache.org/POM/4.0.0
          http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <parent>
    <artifactId>artifactId</artifactId>
    <groupId>groupId</groupId>
    <version>xx.xx.xx</version>
  </parent>
  
  <modelVersion>4.0.0</modelVersion>
  
  <artifactId>learn03-test02-junit5</artifactId>

  <dependencies>
    <!-- junit5 依赖包 -->
    <dependency>
      <groupId>org.junit.jupiter</groupId>
      <artifactId>junit-jupiter</artifactId>
      <version>5.5.2</version>
      <scope>test</scope>
    </dependency>
  </dependencies>
  
  <build>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>3.8.1</version>
        <configuration>
          <source>8</source>
          <target>8</target>
        </configuration>
      </plugin>
      
      <!-- 以下是要添加的配置，上面的其他配置根据情况添加 -->
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <!-- JUnit 5 requires Surefire version 2.22.0 or higher -->
        <version>2.22.2</version>
      </plugin>
    </plugins>
  </build>
</project>
```