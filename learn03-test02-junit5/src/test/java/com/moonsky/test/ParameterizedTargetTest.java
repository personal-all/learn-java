package com.moonsky.test;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import static java.lang.String.format;

/**
 * JUnit 5 参数化测试
 *
 * @author benshaoye
 * @see ParameterizedTargetTest JUnit 5 参数化测试演示
 * @see NestedTargetTest JUnit 5 嵌套类测试演示
 * @see FirstTargetTestTest JUnit 5 基本测试演示
 */
@DisplayName("JUnit 5 参数化测试")
public class ParameterizedTargetTest {

    /**
     * 基本
     *
     * @param value
     *
     * @throws Exception
     * @see ParameterizedTest 代替{@link Test}，表示当前方法是参数化测试方法
     * @see ValueSource 数据源，支持 8 大基本数据类型数组和 String[]、Class[]
     */
    @DisplayName("第一")
    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3})
    void testFirst(int value) throws Exception {
        System.out.println(format("【%s】参数：%d", "第一", value));
    }

    @DisplayName("第二")
    @ParameterizedTest
    @ValueSource(strings = {"one", "two", "third", "four"})
    void testSecond(String value) throws Exception {
        System.out.println(format("【%s】参数：%s", "第二", value));
    }

    /**
     * 多三数测试
     *
     * @param key
     * @param value
     *
     * @see CsvSource 默认分隔符是英文逗号
     */
    @DisplayName("第四")
    @ParameterizedTest
    @CsvSource({"1,One", "2,Two", "3,Three"})
    void testFour(int key, String value) {
        System.out.printf("Key: %d, Value: %s", key, value);
    }

    /**
     * @param key
     * @param value
     *
     * @see CsvSource 自定义分隔符
     */
    @DisplayName("第四")
    @ParameterizedTest
    @CsvSource(value = {"100|One", "200|Two", "300|Three"}, delimiter = '|')
    void testFive(int key, String value) {
        System.out.printf("Key: %d, Value: %s", key, value);
    }
}
