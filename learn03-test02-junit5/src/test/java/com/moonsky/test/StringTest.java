package com.moonsky.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.regex.Pattern;

/**
 * @author benshaoye
 */
@DisplayName("字符串相关测试")
public class StringTest {

    @ParameterizedTest
    @DisplayName("正则表达式分组替换")
    @CsvSource({"123456789,123******789", "777888999,777******999"})
    void testReplacedAll(String replace, String expected) throws Exception {
        String replaced = replace.replaceAll("^([\\d]{3})[\\d]{3}([\\dxX]{3})$", "$1******$2");
        Assertions.assertEquals(expected, replaced);
    }

    @ParameterizedTest
    @DisplayName("身份证号去敏")
    @CsvSource({
        "235407195106112745,235******745", "210203197503102721,210******721",
        "23540719510611274X,235******74X", "21020319750310272x,210******72x",
    })
    void testCertNo(String replace, String expected) throws Exception {
        String replaced = replace.replaceAll("^([\\d]{3})[\\d]{12}([\\dxX]{3})$", "$1******$2");
        Assertions.assertEquals(expected, replaced);
    }
}
