package com.moonsky.test;

import org.junit.jupiter.api.*;

import static java.lang.String.format;

/**
 * 嵌套类测试演示
 * <p>
 * 随着测试的增多，前套类的作用多用于按功能分组
 *
 * @author benshaoye
 * @see ParameterizedTargetTest JUnit 5 参数化测试演示
 * @see NestedTargetTest JUnit 5 嵌套类测试演示
 * @see FirstTargetTestTest JUnit 5 基本测试演示
 */
@DisplayName("嵌套类测试")
public class NestedTargetTest {

    @BeforeAll
    static void beforeAll() {
        System.out.println(format("本类 【%s】 测试开始\n", FirstTargetTestTest.class));
    }

    @AfterAll
    static void afterAll() {
        System.out.println(format("本类 【%s】 测试结束，清除数据", FirstTargetTestTest.class));
    }

    @BeforeEach
    void setUp() {
        System.out.println("方法测试【开始】……");
    }

    @AfterEach
    void tearDown() {
        System.out.println("方法测试【结束】……\n");
    }

    @Nested
    @DisplayName("第一个内部类")
    class NestedOneTest {

        @Test
        void testFirst() throws Exception {
            System.out.println("第一个嵌套类第一个方法");
        }

        @Test
        void testSecond() throws Exception {
            System.out.println("第一个嵌套类第二个方法");
        }
    }

    /**
     * 内部类可以有自己的生命周期注解
     */
    @Nested
    @DisplayName("第二个内部类")
    class NestedTwoTest {

        @BeforeEach
        void setUp() {
            System.out.println("内部类方法生命周期【开始】……");
        }

        @AfterEach
        void tearDown() {
            System.out.println("内部类方法生命周期【结束】……");
        }

        @Test
        void testFirst() throws Exception {
            System.out.println("第二个嵌套类第一个方法");
        }

        @Test
        void testSecond() throws Exception {
            System.out.println("第二个嵌套类第二个方法");
        }
    }

    /**
     * 不能是静态内部类，静态内部类不会被执行
     */
    @Nested
    @DisplayName("静态内部类")
    static class NestedStaticTest {

        @Test
        void testFirst() throws Exception {
            System.out.println("第一个嵌套类第一个方法");
        }

        @Test
        void testSecond() throws Exception {
            System.out.println("第一个嵌套类第二个方法");
        }
    }
}
