package com.moonsky.test;

import org.junit.jupiter.api.*;

/**
 * @author benshaoye
 */
@DisplayName("测试顺序")
public class OrderTargetTest {

    static int orderedValueTest = 0;

    /**
     * 有序测试：
     * | 注解类              | 含义      |
     * |--------------------|----------|
     * | Alphanumeric       | 字母排序   |
     * | OrderAnnotation    | 数字顺序   |
     * | Random             | 随机      |
     * <p>
     * 1. 在测试类上注解{@link TestMethodOrder}
     * 2. 在测试方法上使用注解{@link Order}标记执行测试顺序
     * 3. 未标记顺序的最后执行
     */
    @Nested
    @DisplayName("有序")
    @TestMethodOrder(MethodOrderer.OrderAnnotation.class)
    class OrderedTarget {


        @Test
        @Order(1)
        void testOrderedOne() throws Exception {
            Assertions.assertEquals(0, orderedValueTest);
            System.out.println(1);
            orderedValueTest = 1;
        }

        @Test
        @Order(3)
        void testOrderedThird() throws Exception {
            Assertions.assertEquals(2, orderedValueTest);
            System.out.println(3);
            orderedValueTest = 3;
        }

        @Test
        @Order(2)
        void testOrderedTwo() throws Exception {
            Assertions.assertEquals(1, orderedValueTest);
            System.out.println(2);
            orderedValueTest = 2;
        }

        @Test
        void testUnordered() throws Exception {
            System.out.println("testUnordered");
            Assertions.assertEquals(3, orderedValueTest);
        }
    }

    @Nested
    @DisplayName("无序")
    @TestMethodOrder(MethodOrderer.OrderAnnotation.class)
    class UnorderedTarget {

        @Test
        void testOrderedOne() throws Exception {
            System.out.println(1);
        }

        @Test
        void testOrderedThird() throws Exception {
            System.out.println(3);
        }

        @Test
        void testOrderedTow() throws Exception {
            System.out.println(2);
        }
    }
}
