package com.moonsky.test;

import org.junit.jupiter.api.*;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import static java.lang.String.format;

/**
 * 基本测试类
 *
 * @author benshaoye
 * @see ParameterizedTargetTest JUnit 5 参数化测试演示
 * @see NestedTargetTest JUnit 5 嵌套类测试演示
 * @see FirstTargetTestTest JUnit 5 基本测试演示
 */
@DisplayName("我的测试类：FirstTargetTestTest")
class FirstTargetTestTest {

    /**
     * @see BeforeAll 这个注解只能修饰静态方法
     */
    @BeforeAll
    static void beforeAll() {
        System.out.println(format("本类 【%s】 测试开始\n", FirstTargetTestTest.class));
    }

    @AfterAll
    static void afterAll() {
        System.out.println(format("本类 【%s】 测试结束，清除数据", FirstTargetTestTest.class));

        Set<String> set = new HashSet<>();
        set.add("第一个测试方法");
        set.add("第三个测试方法");
        set.add("第四个测试方法");
        set.add("第五个测试方法");
        set.add("第二个测试方法");
        System.out.println("=============================");
        set.forEach(System.out::println);
        System.out.println("=============================");
        new TreeSet<>(set).forEach(System.out::println);
        set = new HashSet<>();
        set.add("testFirstTest");
        set.add("testThirdTest");
        set.add("testFourthTest");
        set.add("testFifthTest");
        set.add("testSecondTest");
        System.out.println("=============================");
        set.forEach(System.out::println);
        System.out.println("=============================");
        new TreeSet<>(set).forEach(System.out::println);
    }

    @BeforeEach
    void setUp() {
        System.out.println("方法测试【开始】……");
    }

    @AfterEach
    void tearDown() {
        System.out.println("方法测试【结束】……\n");
    }

    @Test
    void testFirstTest() throws Exception {
        System.out.println(format("第一个测试方法：【%s】", "testFirstTest"));
    }

    /**
     * @throws Exception
     * @see DisplayName 指定测试报告名称，默认是方法名{@link #testFirstTest()}
     */
    @Test
    @DisplayName("第二个测试方法")
    void testSecondTest() throws Exception {
        System.out.println(format("第二个测试方法：【%s】", "testFirstTest"));
    }

    /**
     * @throws Exception
     * @see RepeatedTest 重复执行测试 n 次，{@link RepeatedTest#name()}指定名称
     * 重复执行独立于默认执行，即：value = 2，实际会执行三次
     */
    @Test
    @RepeatedTest(value = 2, name = "重复执行测试")
    @DisplayName("第三个测试方法")
    void testThirdTest() throws Exception {
        System.out.println(format("第三个测试方法：【%s】", "testFirstTest"));
    }

    /**
     * @throws Exception
     * @see RepeatedTest 重复执行测试 n 次，{@link RepeatedTest#name()}有三个占位符，
     * {displayName}：测试报告名称
     * {currentRepetition}：当前第几次重复执行
     * {totalRepetitions}：总共重复次数
     */
    @Test
    @RepeatedTest(value = 2, name = "{displayName}：第 {currentRepetition}/{totalRepetitions} 次重复测试")
    @DisplayName("第四个测试方法")
    void testFourthTest() throws Exception {
        System.out.println(format("第四个测试方法：【%s】", "testFirstTest"));
    }

    /**
     * @throws Exception
     * @see Disabled 被修饰的类不会执行测试
     */
    @Test
    @Disabled
    @DisplayName("@Disabled 禁用测试")
    void testOneHundredTest() throws Exception {
        System.out.println(format("第一百个测试方法：【%s】", "testOneHundredTest"));
    }
}