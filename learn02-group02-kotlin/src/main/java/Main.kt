/**
 *
 * @author benshaoye
 */
fun main() {
    println("name")
    println(sum(1, 2))
}

fun sum(a: Int, b: Int): Int {
    return a + b
}

fun sum0(a: Int, b: Int) = a + b