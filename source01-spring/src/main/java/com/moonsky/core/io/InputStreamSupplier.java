package com.moonsky.core.io;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author benshaoye
 */
public interface InputStreamSupplier {

    /**
     * 得到{@link InputStream}
     *
     * @return
     */
    InputStream getInputStream() throws IOException;
}
