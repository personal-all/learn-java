package com.moonsky.core.io;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

/**
 * @author benshaoye
 */
public interface Resource extends InputStreamSupplier {

    @Override
    InputStream getInputStream() throws IOException;

    boolean exists();

    default boolean isReadable() { return exists(); }

    default boolean isOpen() { return false; }

    default boolean isFile() { return false; }

    URL getUrl() throws IOException;

    URI getUri() throws IOException;

    File getFile() throws IOException;

    default ReadableByteChannel readableChannel() throws IOException {
        return Channels.newChannel(getInputStream());
    }

    long contentLength() throws IOException;

    long lastModified() throws IOException;

    Resource createRelative(String relativePath) throws IOException;

    String getFilename();

    String getDescription();
}
