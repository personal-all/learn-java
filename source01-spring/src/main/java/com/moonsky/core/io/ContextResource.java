package com.moonsky.core.io;

/**
 * @author benshaoye
 */
public interface ContextResource extends Resource {

    String getPathWithinContext();
}
