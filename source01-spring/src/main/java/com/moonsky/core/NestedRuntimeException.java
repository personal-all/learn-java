package com.moonsky.core;

/**
 * @author benshaoye
 */
public abstract class NestedRuntimeException extends RuntimeException {

    private final static long serialVersionUID = 1L;

    static {
        NestedExceptionUtils.class.getName();
    }

    public NestedRuntimeException() { }

    public NestedRuntimeException(String message) { super(message); }

    public NestedRuntimeException(String message, Throwable cause) { super(message, cause); }

    public NestedRuntimeException(Throwable cause) { super(cause); }

    public NestedRuntimeException(
        String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    @Override
    public String getMessage() {
        return NestedExceptionUtils.buildMessage(super.getMessage(), getCause());
    }

    public Throwable getRootCause() {
        return NestedExceptionUtils.getRootCause(this);
    }

    public Throwable getMostSpecificCause() {
        return NestedExceptionUtils.getMostSpecificCause(this);
    }

    public boolean contains(Class<?> exType) {
        if (exType == null) {
            return false;
        }
        if (exType.isInstance(this)) {
            return true;
        }
        Throwable cause = getCause();
        if (cause == this) {
            return false;
        }
        if (cause instanceof NestedRuntimeException) {
            return ((NestedRuntimeException) cause).contains(exType);
        } else {
            while (cause != null) {
                if (exType.isInstance(cause)) {
                    return true;
                }
                if (cause.getCause() == cause) {
                    break;
                }
                cause = cause.getCause();
            }
            return false;
        }
    }
}
