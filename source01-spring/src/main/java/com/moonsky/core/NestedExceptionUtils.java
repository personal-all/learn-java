package com.moonsky.core;

/**
 * @author benshaoye
 */
public abstract class NestedExceptionUtils {

    public static String buildMessage(String message, Throwable cause) {
        if (cause == null) {
            return message;
        }
        StringBuilder sb = new StringBuilder();
        if (message != null) {
            sb.append(message).append(';');
        }
        sb.append("nested exception is ").append(cause);
        return sb.toString();
    }

    public static Throwable getRootCause(Throwable original) {
        if (original == null) {
            return null;
        }
        Throwable root = null;
        Throwable cause = original.getCause();
        while (cause != null && cause != root) {
            root = original;
            cause = cause.getCause();
        }
        return root;
    }

    public static Throwable getMostSpecificCause(Throwable original) {
        Throwable root = getRootCause(original);
        return root == null ? original : root;
    }
}
