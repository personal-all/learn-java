package com.moonsky.util;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.lang.reflect.Array;
import java.util.*;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * @author benshaoye
 */
public class ConcurrentReferenceHashMap<K, V> extends AbstractMap<K, V> implements ConcurrentMap<K, V> {

    private static final int DEFAULT_INITIAL_CAPACITY = 16;

    private static final float DEFAULT_LOAD_FACTOR = 0.75f;

    private static final int DEFAULT_CONCURRENCY_LEVEL = 16;

    private static final ReferenceType DEFAULT_REFERENCE_TYPE = ReferenceType.SOFT;

    private static final int MAXIMUM_CONCURRENCY_LEVEL = 1 << 16;

    private static final int MAXIMUM_SEGMENT_SIZE = 1 << 30;

    private final ReferenceType referenceType;

    private final float loadFactor;

    private final int shift;

    private final Segment[] segments;

    private volatile Set<Map.Entry<K, V>> entrySet;

    public ConcurrentReferenceHashMap() {
        this(DEFAULT_INITIAL_CAPACITY, DEFAULT_LOAD_FACTOR, DEFAULT_CONCURRENCY_LEVEL, DEFAULT_REFERENCE_TYPE);
    }

    public ConcurrentReferenceHashMap(int initialCapacity) {
        this(initialCapacity, DEFAULT_LOAD_FACTOR, DEFAULT_CONCURRENCY_LEVEL, DEFAULT_REFERENCE_TYPE);
    }

    public ConcurrentReferenceHashMap(int initialCapacity, float loadFactor) {
        this(initialCapacity, loadFactor, DEFAULT_CONCURRENCY_LEVEL, DEFAULT_REFERENCE_TYPE);
    }

    public ConcurrentReferenceHashMap(int initialCapacity, int concurrencyLevel) {
        this(initialCapacity, DEFAULT_LOAD_FACTOR, concurrencyLevel, DEFAULT_REFERENCE_TYPE);
    }

    public ConcurrentReferenceHashMap(int initialCapacity, ReferenceType referenceType) {
        this(initialCapacity, DEFAULT_LOAD_FACTOR, DEFAULT_CONCURRENCY_LEVEL, referenceType);
    }

    public ConcurrentReferenceHashMap(int initialCapacity, float loadFactor, int concurrencyLevel) {
        this(initialCapacity, loadFactor, concurrencyLevel, DEFAULT_REFERENCE_TYPE);
    }

    public ConcurrentReferenceHashMap(
        int initialCapacity, float loadFactor, int concurrencyLevel, ReferenceType referenceType
    ) {
        Assert.isTrue(initialCapacity >= 0, "初始化容联必须大于 0。");
        Assert.isTrue(loadFactor > 0, "扩容因子必须 0。");
        Assert.isTrue(concurrencyLevel > 0, "并发数必须大于 0。");
        Assert.notNull(referenceType, "引用类型不能为空。");

        this.loadFactor = loadFactor;
        this.shift = calculateShift(concurrencyLevel, MAXIMUM_CONCURRENCY_LEVEL);
        this.referenceType = referenceType;
        int size = 1 << this.shift;
        int roundedUpSegmentCapacity = (int) ((initialCapacity + size - 1L) / size);
        Segment[] segments = (Segment[]) Array.newInstance(Segment.class, size);
        int initialSize = 1 << calculateShift(roundedUpSegmentCapacity, MAXIMUM_SEGMENT_SIZE);
        int resizeThreshold = (int) (initialCapacity * loadFactor);
        for (int i = 0; i < size; i++) {
            segments[i] = new Segment(initialSize, resizeThreshold);
        }
        this.segments = segments;
    }

    protected final float getLoadFactor() {
        return loadFactor;
    }

    protected final int getSegmentsSize() {
        return this.segments.length;
    }

    protected final Segment getSegment(int index) {
        return this.segments[index];
    }

    protected ReferenceManager createReferenceManager(){
        return new ReferenceManager();
    }

    protected static int calculateShift(int minValue, int maxValue) {
        int shift = 0, value = 1;
        while (value < minValue && value < maxValue) {
            value <<= 1;
            shift++;
        }
        return shift;
    }

    @Override
    public V putIfAbsent(K key, V value) {
        return super.putIfAbsent(key, value);
    }

    @Override
    public V replace(K key, V value) {
        return null;
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        return null;
    }

    @Override
    public V getOrDefault(Object key, V defaultValue) {
        return null;
    }

    @Override
    public void forEach(BiConsumer<? super K, ? super V> action) {

    }

    @Override
    public boolean remove(Object key, Object value) {
        return false;
    }

    @Override
    public boolean replace(K key, V oldValue, V newValue) {
        return false;
    }

    @Override
    public void replaceAll(BiFunction<? super K, ? super V, ? extends V> function) {

    }

    @Override
    public V computeIfAbsent(K key, Function<? super K, ? extends V> mappingFunction) {
        return null;
    }

    @Override
    public V computeIfPresent(K key, BiFunction<? super K, ? super V, ? extends V> remappingFunction) {
        return null;
    }

    @Override
    public V compute(K key, BiFunction<? super K, ? super V, ? extends V> remappingFunction) {
        return null;
    }

    @Override
    public V merge(K key, V value, BiFunction<? super V, ? super V, ? extends V> remappingFunction) {
        return null;
    }

    /*
     * ============================================================================================
     * 类型申明
     * ============================================================================================
     */

    protected class ReferenceManager {

        private final ReferenceQueue<Element<K, V>> queue = new ReferenceQueue<>();

        private Reference<K, V> createReference(Element<K, V> element, int hash, Reference<K, V> next) {
            return ConcurrentReferenceHashMap.this.referenceType.create(element, hash, next, this.queue);
        }

        public Reference<K, V> pollForPurge() {
            return (Reference<K, V>) this.queue.poll();
        }
    }

    public enum ReferenceType {
        WEAK {
            @Override
            public <K, V> Reference<K, V> create(
                Element<K, V> element, int hash, Reference<K, V> next, ReferenceQueue<Element<K, V>> queue
            ) {
                return new WeakElementReference<>(element, hash, next, queue);
            }
        },
        SOFT {
            @Override
            public <K, V> Reference<K, V> create(
                Element<K, V> element, int hash, Reference<K, V> next, ReferenceQueue<Element<K, V>> queue
            ) {
                return new SoftElementReference<>(element, hash, next, queue);
            }
        };

        public abstract <K, V> Reference<K, V> create(
            Element<K, V> element, int hash, Reference<K, V> next, ReferenceQueue<Element<K, V>> queue
        );
    }

    protected Reference<K, V>[] createReferenceArray(int size) {
        return new Reference[size];
    }

    protected final class Segment extends ReentrantLock {

        private final ReferenceManager referenceManager;

        private volatile Reference<K,V>[] references;

        private final int initialSize;

        private final int resizeThreshold;

        public Segment(int initialSize, int resizeThreshold) {
            this.referenceManager = createReferenceManager();
            this.resizeThreshold = resizeThreshold;
            this.initialSize = initialSize;
            this.references = createReferenceArray(initialSize);
        }
    }

    protected static class Element<K, V> implements Map.Entry<K, V> {

        private final K key;

        private volatile V value;

        public Element(K key, V value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public V setValue(V value) {
            V prevValue = this.value;
            this.value = value;
            return prevValue;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) { return true; }
            if (o == null || getClass() != o.getClass()) { return false; }
            Element<?, ?> element = (Element<?, ?>) o;
            return Objects.equals(key, element.key) && Objects.equals(value, element.value);
        }

        @Override
        public int hashCode() {
            return Objects.hash(key, value);
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("Element{");
            sb.append(key).append("=").append(value).append('}');
            return sb.toString();
        }
    }

    protected enum Restructure {
        WHEN_NECESSARY,
        NEVER
    }

    private class ElementSet extends AbstractSet<Map.Entry<K, V>> {

        @Override
        public Iterator<Map.Entry<K, V>> iterator() {
            return new ElementIterator();
        }

        @Override
        public boolean contains(Object o) {
            if (o instanceof Map.Entry) {
                Map.Entry e = (Entry) o;
                throw new UnsupportedOperationException();
            }
            return false;
        }

        @Override
        public boolean remove(Object o) {
            if (o instanceof Entry) {
                Map.Entry e = (Entry) o;
                return ConcurrentReferenceHashMap.this.remove(e.getKey(), e.getValue());
            }
            return false;
        }

        @Override
        public int size() {
            return ConcurrentReferenceHashMap.this.size();
        }

        @Override
        public void clear() {
            ConcurrentReferenceHashMap.this.clear();
        }
    }

    private class ElementIterator implements Iterator<Map.Entry<K, V>> {

        private int segmentIndex;

        private int referenceIndex;

        private Reference<K, V>[] references;

        private Reference<K, V> reference;

        private Element<K, V> next;

        private Element<K, V> last;

        @Override
        public boolean hasNext() {
            throw new UnsupportedOperationException();
        }

        @Override
        public Element<K, V> next() {
            throw new UnsupportedOperationException();
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    @FunctionalInterface
    private interface ElementAddr<V> {

        void add(V value);
    }

    private enum TaskOption {
        RESTRUCTURE_BEFORE,
        RESTRUCTURE_AFTER,
        SKIP_IF_EMPTY,
        RESIZE
    }

    private final EnumSet<TaskOption> NONE = EnumSet.noneOf(TaskOption.class);

    private abstract class Task<T> {

        private final EnumSet<TaskOption> options;

        protected Task(TaskOption... options) {
            this(ArrayUtil.isEmpty(options) ? NONE : EnumSet.of(options[0], options));
        }

        protected Task(EnumSet<TaskOption> options) {
            this.options = CollectUtil.isEmpty(options) ? NONE : options;
        }

        public boolean hasOption(TaskOption option) {
            return this.options.contains(option);
        }

        public T execute(Reference<K, V> ref, Element<K, V> element, ElementAddr<V> addr) {
            return execute(ref, element);
        }

        public T execute(Reference<K, V> ref, Element<K, V> element) {
            return null;
        }
    }

    protected interface Reference<K, V> {

        Element<K, V> get();

        int getHash();

        Reference<K, V> getNext();

        void release();
    }

    protected static final class SoftElementReference<K, V> extends SoftReference<Element<K, V>>
        implements Reference<K, V> {

        private final int hash;

        private final Reference<K, V> nextReference;


        public SoftElementReference(
            Element<K, V> referent, int hash, Reference<K, V> next, ReferenceQueue<? super Element<K, V>> queue
        ) {
            super(referent, queue);
            this.hash = hash;
            this.nextReference = next;
        }

        @Override
        public int getHash() {
            return 0;
        }

        @Override
        public Reference<K, V> getNext() {
            return nextReference;
        }

        @Override
        public void release() {
            enqueue();
            clear();
        }
    }

    protected static final class WeakElementReference<K, V> extends WeakReference<Element<K, V>>
        implements Reference<K, V> {

        private final int hash;

        private final Reference<K, V> nextReference;

        public WeakElementReference(
            Element<K, V> referent, int hash, Reference<K, V> next, ReferenceQueue<? super Element<K, V>> queue
        ) {
            super(referent, queue);
            this.hash = hash;
            this.nextReference = next;
        }

        @Override
        public int getHash() {
            return hash;
        }

        @Override
        public Reference<K, V> getNext() {
            return nextReference;
        }

        @Override
        public void release() {
            enqueue();
            clear();
        }
    }
}
