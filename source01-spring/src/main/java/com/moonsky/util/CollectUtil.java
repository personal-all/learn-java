package com.moonsky.util;

import java.util.Collection;

/**
 * @author benshaoye
 */
public abstract class CollectUtil {

    public static <T> boolean isEmpty(Collection<? extends T> collect) {
        return collect == null || collect.isEmpty();
    }
}
