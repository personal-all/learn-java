package com.moonsky.util;

/**
 * @author benshaoye
 */
public abstract class StringUtils {

    private final static String FOLDER_SEPARATOR = "/";
    private final static String WINDOWS_FOLDER_SEPARATOR = "\\";
    private final static String TOP_PATH = "..";
    private final static String CURRENT_PATH = ".";
    private final static char EXTENSION_SEPATATOR = '.';

    public static boolean isEmpty(Object str) {
        return str == null || "".equals(str);
    }

    public static boolean hasLength(CharSequence str) {
        return str != null && str.length() > 0;
    }

    public static boolean hasLength(String str) {
        return str != null && !str.isEmpty();
    }

    public static boolean hasText(CharSequence str) {
        return hasLength(str) && containsText(str);
    }

    public static boolean hasText(String str) {
        return hasLength(str) && containsText(str);
    }

    private static boolean containsText(CharSequence str) {
        int strLen = str.length();
        for (int i = 0; i < strLen; i++) {
            if (!Character.isWhitespace(str.charAt(i))) {
                return true;
            }
        }
        return false;
    }

    public static boolean containsWhitespace(CharSequence str) {
        if (!hasLength(str)) {
            return false;
        }
        int strLen = str.length();
        for (int i = 0; i < strLen; i++) {
            if (Character.isWhitespace(str.charAt(i))) {
                return true;
            }
        }
        return false;
    }

    public static String trimWhitespace(String str) {
        if (!hasLength(str)) {
            return str;
        }
        int beginIndex = 0;
        int endIndex = str.length() - 1;
        while (beginIndex < endIndex && Character.isWhitespace(str.charAt(beginIndex))) {
            beginIndex++;
        }
        while (endIndex > beginIndex && Character.isWhitespace(str.charAt(endIndex))) {
            endIndex--;
        }
        return str.substring(beginIndex, endIndex + 1);
    }

    public static String trimAllWhitespace(String str) {
        if (!(hasLength(str))) {
            return str;
        }
        int strLen = str.length();
        StringBuilder sb = new StringBuilder(strLen);
        for (int i = 0; i < strLen; i++) {
            char ch = str.charAt(i);
            if (!Character.isWhitespace(ch)) {
                sb.append(ch);
            }
        }
        return sb.toString();
    }

    public static String trimLeadingWhitespace(String str) {
        if (!(hasLength(str))) {
            return str;
        }
        int index = 0;
        int len = str.length();
        while (index < len && Character.isWhitespace(str.charAt(index))) {
            index++;
        }
        return index < len ? str.substring(index) : "";
    }

    public static String trimTrailingWhitespace(String str) {
        if (str == null) {
            return null;
        }
        int index = str.length() - 1;
        while (index > 0 && Character.isWhitespace(str.charAt(index))) {
            index--;
        }
        return index > 0 ? str.substring(0, index + 1) : "";
    }
}
