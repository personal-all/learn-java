package com.moonsky.util;

/**
 * @author benshaoye
 */
public abstract class Assert {

    public static void isTrue(boolean value) {
        isTrue(value, "false");
    }

    public static void isTrue(boolean value, String message) {
        if (!value) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void isFalse(boolean value) {
        isTrue(value, "true");
    }

    public static void isFalse(boolean value, String message) {
        if (value) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void isNull(Object value) {
        isNull(value, "Required a null value.");
    }

    public static void isNull(Object value, String message) {
        if (value != null) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void notNull(Object value) {
        isNull(value, "Required a not null value.");
    }

    public static void notNull(Object value, String message) {
        if (value == null) {
            throw new NullPointerException(message);
        }
    }
}
