package com.moonsky.util;

/**
 * @author benshaoye
 */
public abstract class ArrayUtil {

    public static <T> boolean isEmpty(T... items) {
        return items == null || items.length == 0;
    }
}
