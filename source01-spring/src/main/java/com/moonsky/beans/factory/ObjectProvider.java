package com.moonsky.beans.factory;

import com.moonsky.beans.BeansException;

import java.util.Iterator;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * @author benshaoye
 */
public interface ObjectProvider<T> extends ObjectFactory<T>, Iterable<T> {

    T getObject(Object... args) throws BeansException;

    T getIfAvailable() throws BeansException;

    default T getIfAvailable(Supplier<T> defaultSupplier) {
        T value = getIfAvailable();
        return value == null ? defaultSupplier.get() : value;
    }

    default void ifAvailable(Consumer<T> consumer) {
        T value = getIfAvailable();
        if (value != null) {
            consumer.accept(value);
        }
    }

    T getIfUnique() throws BeansException;

    default T getIfUnique(Supplier<T> defaultSupplier) throws BeansException {
        T value = getIfUnique();
        return value == null ? defaultSupplier.get() : value;
    }

    default void ifUnique(Consumer<T> consumer) {
        T value = getIfUnique();
        if (value != null) {
            consumer.accept(value);
        }
    }

    default Stream<T> stream() {
        throw new UnsupportedOperationException();
    }

    @Override
    default Iterator<T> iterator() {
        return stream().iterator();
    }

    default Stream<T> orderedStream() {
        throw new UnsupportedOperationException();
    }
}
