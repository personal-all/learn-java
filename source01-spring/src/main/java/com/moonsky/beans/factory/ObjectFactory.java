package com.moonsky.beans.factory;

/**
 * @author benshaoye
 */
@FunctionalInterface
public interface ObjectFactory<T> {

    T getObject();
}
