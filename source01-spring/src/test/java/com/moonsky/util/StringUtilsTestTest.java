package com.moonsky.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author benshaoye
 */
@DisplayName("String 工具类")
class StringUtilsTestTest {

    @Nested
    @DisplayName("测试去除空格")
    static class TrimText {

        @ParameterizedTest
        @DisplayName("去除首尾空格")
        @CsvSource({" abc ,abc", "   11 11 ,11 11"})
        void testTrimWhitespace(String text, String expect) throws Exception {
            assertEquals(expect, StringUtils.trimWhitespace(text));
        }

        @ParameterizedTest
        @DisplayName("去除全部空格")
        @CsvSource({" abc ,abc", "   11 11 ,1111"})
        void testTrimAllWhitespace(String text, String expect) throws Exception {
            assertEquals(expect, StringUtils.trimAllWhitespace(text));
        }

        @ParameterizedTest
        @DisplayName("去除头部空格")
        @CsvSource({" abc ,abc ", "   11 11 ,11 11 "})
        void testTrimLeadingWhitespace(String text, String expect) throws Exception {
            assertEquals(expect, StringUtils.trimLeadingWhitespace(text));
        }

        @ParameterizedTest
        @DisplayName("去除尾部空格")
        @CsvSource({" abc , abc", "   11 11 ,   11 11"})
        void testTrimTrailingWhitespace(String text, String expect) throws Exception {
            assertEquals(expect, StringUtils.trimTrailingWhitespace(text));
        }
    }

}