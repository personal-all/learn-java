# learn-java

### 介绍
这是一个 Java 学习项目

### 模块介绍

##### (1). 工具模块
主要用来临时给其他模块做一些即时的功能，避免频繁写同样的代码
###### 01. [learn01-base01-util](https://gitee.com/744531854/learn-java/blob/master/learn01-base01-util)
这是基本工具类模块，里面有一些自己写的常用 java 工具类。
这些工具类一部分是参考其他源码写的；一部分是工作中遇到的实际需求，总结后写的；
还有一部分是在这基础之上结合自己的想法完成的。部分示例：
1. [RunnerUtil](https://gitee.com/744531854/learn-java/blob/master/learn01-base01-util/src/main/java/com/moon/common/core/util/runner/RunnerUtil.java)：
java 字符串表达式解析器，运行字符串形式的表达式，其语法大多与 java 语法相同；
2. [ValidatorUtil](https://gitee.com/744531854/learn-java/blob/master/learn01-base01-util/src/main/java/com/moon/common/core/util/validator/ValidatorUtil.java)：
对象多条件验证器；
3. [IteratorUtil](https://gitee.com/744531854/learn-java/blob/master/learn01-base01-util/src/main/java/com/moon/common/core/util/IteratorUtil.java)：
通用迭代器，可以迭代“任意”对象，并且不会发生 NPE；
4. [MapperUtil](https://gitee.com/744531854/learn-java/blob/master/learn01-base01-util/src/main/java/com/moon/common/core/util/MapperUtil.java)：
映射器，提供 Bean to Map、Map to Bean、Bean to other bean class、Bean to other bean 之间的映射；
5. [JSONUtil](https://gitee.com/744531854/learn-java/blob/master/learn01-base01-util/src/main/java/com/moon/common/core/util/JSONUtil.java)：
一个简单的 json 实现，其实当实现完 RunnerUtil 后，这个 JSONUtil。。。太简单了
6. [PropertiesUtil](https://gitee.com/744531854/learn-java/blob/master/learn01-base01-util/src/main/java/com/moon/common/core/util/PropertiesUtil.java)：
一个 properties 文件封装，它提供了缓存、刷新、分组（PropertiesGroup）等
7. [TypeUtil](https://gitee.com/744531854/learn-java/blob/master/learn01-base01-util/src/main/java/com/moon/common/core/util/TypeUtil.java)：
类型转换器，提供各种数据数据类型之间的转换
8. [ResultSetUtil](https://gitee.com/744531854/learn-java/blob/master/learn01-base01-util/src/main/java/com/moon/common/core/sql/ResultSetUtil.java)：
有时候使用原生 sql 编程或 jdbcTemplate 的时候，用这个将 ResultSet 转换成 JavaBean，避免手写冗长的代码，只需要保证查询的标签名和字段名一致即可
9. [EncryptUtil](https://gitee.com/744531854/learn-java/blob/master/learn01-base01-util/src/main/java/com/moon/common/core/security/EncryptUtil.java)：
密码加密工具类
10. [BeanInfoUtil](https://gitee.com/744531854/learn-java/blob/master/learn01-base01-util/src/main/java/com/moon/common/core/beans/BeanInfoUtil.java)：
Java 实体信息工具类，可用于获取 getter、setter 方法、设置值等
11. and so on...

##### (2). 第三方独立库
###### 01. [learn02-group01-lombok](https://gitee.com/744531854/learn-java/blob/master/learn02-group01-lombok)
nothing
###### 02. [learn02-group02-kotlin](https://gitee.com/744531854/learn-java/blob/master/learn02-group02-kotlin)
nothing
###### 03. [learn02-group03-apt](https://gitee.com/744531854/learn-java/blob/master/learn02-group03-apt)
nothing

###### 04. [learn03-log01-logback](https://gitee.com/744531854/learn-java/blob/master/learn03-log01-logback)
nothing
###### 05. [learn03-test02-junit5](https://gitee.com/744531854/learn-java/blob/master/learn03-test02-junit5)
这是 JUnit5 的使用方法介绍，开始使用的是 JUnit4，后来遇到一些特殊的需求（忘了什么原因...），
就专门去查阅了一些 JUnit5 的使用介绍，这里的区别主要有：
1. 运行器声明方式：@RunWith -> @ExtendWith
2. 参数化测试：@ParameterizedTest + @ValueSource|@CsvSource
3. 自定义测试方法名：@DisplayName
4. 分组测试：@Nested
5. 重复测试：@RepeatedTest
6. 禁用测试：@Ignore -> @Disabled

##### (3). Spring 框架 
###### 01. [learn04-spring02-ioc](https://gitee.com/744531854/learn-java/blob/master/learn04-spring02-ioc)
###### 02. [learn04-spring03-beans](https://gitee.com/744531854/learn-java/blob/master/learn04-spring03-beans)
1. `XML`文件中如何定义一个`bean`
2. `bean`的生命周期（共11个，有些不需要定义 id 的 bean 就是在声明周期里完成了他必须的动作）
3. `bean`绑定参数的方式
###### 03. [learn04-spring04-aop](https://gitee.com/744531854/learn-java/blob/master/learn04-spring04-aop)
aop 基础
###### 04. [learn04-spring05-aspectj](https://gitee.com/744531854/learn-java/blob/master/learn04-spring05-aspectj)
aspectj 使用介绍
###### 05. [learn04-spring06-jdbc-template](https://gitee.com/744531854/learn-java/blob/master/learn04-spring06-jdbc-template)
###### 06. [learn04-spring07-transaction](https://gitee.com/744531854/learn-java/blob/master/learn04-spring07-transaction)

##### (4). ORM 框架
###### 01. [learn05-orm01-mybatis](https://gitee.com/744531854/learn-java/tree/master/learn05-orm01-mybatis/learn05-orm01-mybatis)
针对 mybatis-config.xml 以及 mapper 文件的测试和源码分析。比如不同属性不同值对执行过程和结果的影响
###### 02. [learn05-orm02-mybatis-plus](https://gitee.com/744531854/learn-java/blob/master/learn05-orm02-mybatis-plus)
###### 03. [learn05-orm03-hibernate](https://gitee.com/744531854/learn-java/blob/master/learn05-orm03-hibernate)

###### (5). MVC 框架
###### 01. [learn06-mvc01-simple](https://gitee.com/744531854/learn-java/blob/master/learn06-mvc01-simple)
这是一个最简单的 spring mvc 的写法，包括`Controller`两种声明方式以及两种返回`ModelAndView`的方式。
###### 02. [learn06-mvc02-databind](https://gitee.com/744531854/learn-java/blob/master/learn06-mvc02-databind)
这是 spring mvc 数据绑定说明，包括了各种数据类型的绑定：
1. 基本数据类型绑定：不能绑定`null`值；
2. 包装类型数据绑定：可以使用`null`值；
3. 数组；
4. List、Map 绑定需放在单独声明的`java bean`对象中；
5. Set 绑定除了需要放在单独的`java bean`中外，还要手动预先填充对象；
6. 同一个请求方法，不同对象同名字段的处理：`@InitBinder`注解在入参为`org.springframework.web.bind.WebDataBinder`的方法上；
7. 自定义 Spring MVC 类型转换器
###### 03. [learn06-mvc03-databind-json](https://gitee.com/744531854/learn-java/blob/master/learn06-mvc03-databind-json)
###### 04. [learn06-mvc04-interceptor](https://gitee.com/744531854/learn-java/blob/master/learn06-mvc04-interceptor)
Spring MVC 拦截器使用方法

##### (6). 独立项目
###### 02. [project01-person-management](https://gitee.com/744531854/learn-java/blob/master/project01-person-management)

##### (7). 源码
###### 01. [source01-spring](https://gitee.com/744531854/learn-java/blob/master/source01-spring)
nothing
### 软件架构
软件架构说明


### 安装教程

1. xxxx
2. xxxx
3. xxxx

### 使用说明

1. xxxx
2. xxxx
3. xxxx

### 使用参考：

1. learn01-base01-util：这是一个简单工具类，用来被其他木块依赖
2. mybatis-plus：

文档：[https://gitee.com/744531854/learn-java/blob/master/learn05-orm02-mybatis-plus](https://gitee.com/744531854/learn-spring/blob/master/learn05-orm02-mybatis-plus)


### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)