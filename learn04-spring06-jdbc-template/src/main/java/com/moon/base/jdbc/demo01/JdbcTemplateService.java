package com.moon.base.jdbc.demo01;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * @author benshaoye
 */
public class JdbcTemplateService {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void insertValues() {
        String sql0 = "INSERT INTO t_user (name, sex, birthday) VALUES ('长啥0', '男', '2010-04-08')";
        String sql1 = "INSERT INTO t_user (name, sex, birthday) VALUES ('长啥1', '男', '2010-04-08')";
        String sql2 = "INSERT INTO t_user (name, sex, birthday) VALUES ('长啥2', '男', '2010-04-08')";
        String sql3 = "INSERT INTO t_user (name, sex, birthday) VALUES ('长啥3', '男', '2010-04-08')";
        jdbcTemplate.update(sql0);
        jdbcTemplate.update(sql1);
        jdbcTemplate.update(sql2);
        jdbcTemplate.update(sql3);
    }
}
