package com.moon.base.jdbc.demo01;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

/**
 * @author benshaoye
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/jdbc.demo01.application.xml")
public class JdbcTemplateServiceTestTest {

    @Autowired
    private JdbcTemplateService templateService;

    @Autowired
    private JdbcTemplate jdbcTemplate;


    @Before
    public void setUp() throws Exception {
    }

    public static <T> T getOne() {
        return null;
    }

    @Test
    public void testExecute() {
        String drop = "DROP TABLE IF EXISTS t_user_default;";
        jdbcTemplate.execute(drop);
        String ddl = "CREATE TABLE t_user_default (id int, name varchar(38));";
        jdbcTemplate.execute(ddl);

        jdbcTemplate.queryForList("SHOW TABLES").forEach(value -> {
            System.out.println(value);
        });

        jdbcTemplate.execute(drop);
    }

    @Test
    public void testInsert() {
        templateService.insertValues();

        jdbcTemplate.queryForList("SELECT * FROM t_user").forEach(row -> {
            System.out.println(row);
        });

        jdbcTemplate.update("delete from t_user");
    }
}