package com.moonsky.mvc.validation;

import com.moonsky.mvc.validation.annotation.StringIn;
import lombok.Data;

/**
 * @author benshaoye
 */
@Data
public class UserDTO {

    @StringIn(values = "male,female")
    private String sex;

    public UserDTO() {}
}
