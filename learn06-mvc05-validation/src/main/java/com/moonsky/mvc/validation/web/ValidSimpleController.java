package com.moonsky.mvc.validation.web;

import com.moon.core.util.Console;
import com.moonsky.mvc.validation.annotation.StringIn;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author benshaoye
 */
@Validated
@RestController
@RequestMapping("validate/simple")
public class ValidSimpleController {

    public ValidSimpleController() {}

    @RequestMapping("sex")
    public Object validStringSex(
        Map<String, Object> map, @StringIn(values = "male,female", nullable = false) String sex
    ) {
        Console.out.println("Sex: {}", sex);
        map.put("sex", sex);
        return "index";
    }
}
