package com.moonsky.mvc.validation.annotation.validator;

import com.moonsky.mvc.validation.annotation.ShortAnyIn;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author benshaoye
 */
public class ShortAnyInValidator extends CollectValidator implements ConstraintValidator<ShortAnyIn, Object> {

    public ShortAnyInValidator() { super(hashSet(), false); }

    @Override
    public void initialize(ShortAnyIn annotation) {
        initialArgs(annotation.nullable(), annotation.values(), Short::valueOf);
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) { return doValidateValue(value); }

    @Override
    protected Class getTargetClass() { return ShortAnyIn.class; }

    @Override
    protected boolean afterPreTransformTest(String value) { return NUMERIC.test(value); }
}
