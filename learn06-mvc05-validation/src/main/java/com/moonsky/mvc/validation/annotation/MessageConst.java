package com.moonsky.mvc.validation.annotation;

/**
 * @author benshaoye
 */
final class MessageConst {

    final static String AT_ALL = "所有项必须是数值 [{values}] 之一";

    final static String AT_ANY = "至少一项是数值 [{values}] 之一";
}
