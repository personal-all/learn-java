
#### `Spring`事务
```xml
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-tx</artifactId>
      <version>5.2.0.RELEASE</version>
    </dependency>
```

###### 说明
1. @Transactional 只能应用到 public 方法才有效。
2. 在默认配置中，Spring FrameWork 的事务框架代码只会将出现 runtime, unchecked 异常的事务标记为回滚，
也就是说事务中抛出的异常是 RuntimeException 或其子类，这样事务才会回滚（默认情况下 Error 也会导致事务回滚）。
但是，在默认配置的情况下，所有的 checked 异常都不会引起事务回滚。

###### 事务传播机制：org.springframework.transaction.annotation.Propagation
> org.springframework.transaction.TransactionDefinition

| 事务传播机制        | 事务机制特性                                                                    |
|-------------------|-------------------------------------------------------------------------------|
| REQUIRED (默认值)  | 如果存在一个事务，则支持当前事务。如果没有事务则开启一个新的事务。(默认值，适合绝大多数场景)  |
| SUPPORTS          | 如果存在一个事务，支持当前事务。如果没有事务，则非事务的执行。                           |
| MANDATORY         | 如果已经存在一个事务，支持当前事务。如果没有一个活动的事务，则抛出异常。                   |
| REQUIRES_NEW      | 总是开启一个新的事务。如果一个事务已经存在，则将这个存在的事务挂起。                      |
| NOT_SUPPORTED     | 总是非事务地执行，并挂起任何存在的事务。                                             |
| NEVER             | 总是非事务地执行，如果存在一个活动事务，则抛出异常。                                   |
| NESTED            | 如果一个活动的事务存在，则运行在一个嵌套的事务中。如果没有活动事务，则按REQUIRED属性执行。  |

###### 事务隔离级别：org.springframework.transaction.annotation.Isolation
> org.springframework.transaction.TransactionDefinition

| 事务隔离级别        | 事务隔离特性                                                                    |
|-------------------|-------------------------------------------------------------------------------|
| DEFAULT           | 使用后端数据库默认的隔离级别。（默认值，也是绝大多数场景）。                             |
| READ_UNCOMMITTED  | 最低的隔离级别，允许读取尚未提交的数据变更，可能会导致脏读、幻读或不可重复读。              |
| READ_COMMITTED    | 允许读取并发事务已经提交的数据，可以阻止脏读，但是幻读或不可重复读仍有可能发生。            |
| REPEATABLE_READ   | 对同一字段的多次读取结果都是一致的，除非数据是被本身事务自己所修改，可以阻止脏读和不可重复读，但幻读仍有可能发生。|
| SERIALIZABLE      | 最高的隔离级别，完全服从ACID的隔离级别。所有的事务依次逐个执行，这样事务之间就完全不可能产生干扰，也就是说，该级别可以防止脏读、不可重复读以及幻读。但是这将严重影响程序的性能。通常情况下也不会用到该级别。|

###### `@Transactional`其他属性
1. value 和 transactionManager：事务管理器，PlatformTransactionManager 接口；
2. timeout：事务超时时间，单位是<strong>秒</strong>。是指一个事务所允许执行的最长时间，
如果超过该时间限制但事务还没有完成，则自动回滚事务。默认值是当前数据库默认事务过期时间。
3. readOnly：事务是否是只读的，默认是 false。对于只读查询，可以指定事务类型为 readonly，即只读事务。
由于只读事务不存在数据的修改， 因此数据库将会为只读事务提供一些优化手段
4. rollbackFor：设置需要进行回滚的异常类数组，当方法中抛出指定异常数组中的异常时，则事务回滚。
5. rollbackForClassName：设置需要进行回滚的异常类名称数组，当方法中抛出指定异常名称数组中的异常时，则进行事务回滚。
6. noRollbackFor：设置不需要进行回滚的异常类数组，当方法中抛出指定异常数组中的异常时，不进行事务回滚。
7. noRollbackForClassName：设置不需要进行回滚的异常类名称数组，当方法中抛出指定异常名称数组中的异常时，不进行事务回滚。