package com.moonsky.group.lombok;

/**
 * @author benshaoye
 */
abstract class AbstractData {

    protected final static String address = "北京朝阳区";
    protected final static String sex = "男";
    protected final static int age = 23;
    protected final static String name = "老萨";
}
