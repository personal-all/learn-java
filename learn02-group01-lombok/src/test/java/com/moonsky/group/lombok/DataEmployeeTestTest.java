package com.moonsky.group.lombok;

import lombok.Data;
import lombok.NonNull;
import org.junit.Test;

/**
 * @author benshaoye
 */
public class DataEmployeeTestTest extends AbstractData {

    @Test
    public void testName() {
        DataEmployee employee = new DataEmployee();
        employee.setAddress(address);
        employee.setAge(age);
        employee.setName(name);
        employee.setSex(sex);
        System.out.println(employee);

        WitherUser user = new WitherUser();
        user.setName(name);
        user = user.withName(name);
        System.out.println(user);
    }

    /*
     * @NonNull:
     * 1. 注解于字段上，会再构造方法和 setter 中验证非 null；
     * 2. 注解于方法参数上，会在入参处验证参数非 null；
     *
     */

    @Data
    public static class Many {

        @NonNull
        private String name;
    }

    @Test
    public void testNonNullOfField() throws Exception {
        try {
            Many many = new Many(null);
            throw new Exception();
        } catch (NullPointerException e) {
        }
        Many many = new Many("");
        try {
            many.setName(null);
            throw new Exception();
        } catch (NullPointerException e) {
        }
    }

    void nonNullOfParam(@NonNull String name) {
        System.out.println(String.format("Name: %s", name));
    }

    @Test
    public void testNonNullOfParam() throws Exception {
        System.out.println(String.format("Name: %s", null));
        try {
            nonNullOfParam(null);
            throw new Exception();
        } catch (NullPointerException e) {
        }
    }

    @NonNull
    void nonNullOfMethod(String name) {
        System.out.println(String.format("Name: %s", name));
    }

    @Test
    public void testNonNullOfMethod() throws Exception {
        System.out.println(String.format("Name: %s", null));
        try {
            nonNullOfMethod(null);
            throw new Exception();
        } catch (NullPointerException e) {
        }
    }
}