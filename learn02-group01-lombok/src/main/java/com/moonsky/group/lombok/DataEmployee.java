package com.moonsky.group.lombok;

import lombok.Data;
import lombok.Singular;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author benshaoye
 */
@Data
@Accessors(chain = true)
public class DataEmployee {

    private String name;

    private String sex;

    private String address;

    private int age;

    @Singular
    private List<String> roles;

    public void setSex(String sex) {
        if (age < 0) {
            this.sex = sex;
        }
    }
}
