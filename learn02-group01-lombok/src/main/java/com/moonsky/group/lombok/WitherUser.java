package com.moonsky.group.lombok;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Wither;

/**
 * @author benshaoye
 */
@Data
@Wither
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class WitherUser {

    private String name;

    private String sex;

    private String address;

    private int age;
}
