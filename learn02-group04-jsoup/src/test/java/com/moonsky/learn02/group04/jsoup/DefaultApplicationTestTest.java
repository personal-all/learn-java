package com.moonsky.learn02.group04.jsoup;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.Test;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author benshaoye
 */
class DefaultApplicationTestTest {

    public static class DefaultSerializable implements Serializable {

        private final static long serialVerionUID = 10028L;
        private String name;
        private int age;
        private String sex;

        public DefaultSerializable(String name, int age, String sex) {
            this.name = name;
            this.age = age;
            this.sex = sex;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) { return true; }
            if (o == null || getClass() != o.getClass()) { return false; }
            DefaultSerializable that = (DefaultSerializable) o;
            return age == that.age && Objects.equals(name, that.name) && Objects.equals(sex, that.sex);
        }

        @Override
        public int hashCode() {
            return Objects.hash(name, age, sex);
        }
    }

    @Test
    void testDefaultJsoup() throws Exception {
        Connection connection = Jsoup.connect("https://www.taobao.com");
        Document document = connection.get();
        String html = document.toString();
        System.out.println(html);

        Element body = document.body();
    }
}