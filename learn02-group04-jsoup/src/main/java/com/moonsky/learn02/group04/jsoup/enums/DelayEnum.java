package com.moonsky.learn02.group04.jsoup.enums;

/**
 * @author benshaoye
 */
public enum  DelayEnum {

    DELAY, FIXED
}
