package com.moonsky.learn02.group04.jsoup.function;

/**
 * @author benshaoye
 */
public interface ThrowingRunner {

    void run() throws Throwable;
}
