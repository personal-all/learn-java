package com.moonsky.learn02.group04.jsoup.model;

import com.moonsky.learn02.group04.jsoup.enums.DelayEnum;
import com.moonsky.learn02.group04.jsoup.function.ThrowingRunner;

import java.util.Queue;
import java.util.function.Supplier;

/**
 * @author benshaoye
 */
public class RequestModel {

    private final static long DEFAULT_TIME_INTERVAL = 2 * 60 * 1000;

    private final long timeInterval;

    private final Supplier<ThrowingRunner> supplier;

    private DelayEnum delayType;

    private boolean termination;

    private long currentTime;

    public RequestModel(Supplier<ThrowingRunner> supplier) {
        this(supplier, DEFAULT_TIME_INTERVAL);
    }

    public RequestModel(Supplier<ThrowingRunner> supplier, long timeInterval) {
        this.timeInterval = timeInterval;
        this.supplier = supplier;
    }

    private RequestModel(Supplier<ThrowingRunner> supplier, long timeInterval, DelayEnum delayEnum, long currentTime) {
        this(supplier, timeInterval);
        this.delayType = delayEnum;
        this.currentTime = currentTime;
    }

    public void begin() {

    }

    public void next(Queue queue) {
        next(queue, currentTime);
    }

    public void next(Queue queue, long now) {
        if (!termination) {
            queue.offer(getNext(now));
        }
    }

    private RequestModel getNext(long now) {
        return new RequestModel(supplier, timeInterval, delayType, now + timeInterval);
    }
}
