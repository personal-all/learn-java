package com.moonsky.mvc02.databind.web;

import com.moonsky.mvc02.databind.vo.BaseMap;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author benshaoye
 */
@Controller
public class MapController {

    @RequestMapping("map")
    public String map(ModelMap map, BaseMap container) {
        map.put("container", container);
        return "viewer-map";
    }
}
