package com.moonsky.mvc02.databind.entity;

import java.util.Map;

/**
 * @author benshaoye
 */
public class Book {

    private String name;

    private int price;

    private int count;

    private Author author;

    private Map<String, Integer> map;

    public Book(String name, int price, int count, Author author) {
        this.name = name;
        this.price = price;
        this.count = count;
        this.author = author;
    }

    public Book() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Map<String, Integer> getMap() {
        return map;
    }

    public void setMap(Map<String, Integer> map) {
        this.map = map;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }
}
