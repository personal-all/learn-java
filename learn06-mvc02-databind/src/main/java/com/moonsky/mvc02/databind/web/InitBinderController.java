package com.moonsky.mvc02.databind.web;

import com.moonsky.mvc02.databind.entity.Author;
import com.moonsky.mvc02.databind.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author benshaoye
 */
@Controller
@RequestMapping("init")
public class InitBinderController {

    @InitBinder("user")
    public void initUserBinder(WebDataBinder binder) {
        binder.setFieldDefaultPrefix("user.");
    }

    @InitBinder("author")
    public void initAuthorBinder(WebDataBinder binder) {
        binder.setFieldDefaultPrefix("author.");
    }

    @RequestMapping("same-prop")
    public String sameProperty(ModelMap modelMap, User user, Author author) {
        modelMap.addAttribute("author", author);
        modelMap.addAttribute("user", user);
        return "viewer-init-prop";
    }

    @RequestMapping("prop")
    public String property(ModelMap modelMap, User user) {
        modelMap.addAttribute("user", user);
        return "viewer-init-prop";
    }
}
