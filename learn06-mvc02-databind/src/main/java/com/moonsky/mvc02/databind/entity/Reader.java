package com.moonsky.mvc02.databind.entity;

import java.util.List;
import java.util.Set;

/**
 * @author benshaoye
 */
public class Reader {
    private String name;

    private String area;

    private String edu;

    private Book[] readed;

    private Set<Book> willRead;

    private List<Book> willBuy;

    public Reader() {
    }

    public Reader(
        String name, String area, String edu, Book[] readed, Set<Book> willRead, List<Book> willBuy
    ) {
        this.name = name;
        this.area = area;
        this.edu = edu;
        this.readed = readed;
        this.willRead = willRead;
        this.willBuy = willBuy;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getEdu() {
        return edu;
    }

    public void setEdu(String edu) {
        this.edu = edu;
    }

    public Book[] getReaded() {
        return readed;
    }

    public void setReaded(Book[] readed) {
        this.readed = readed;
    }

    public Set<Book> getWillRead() {
        return willRead;
    }

    public void setWillRead(Set<Book> willRead) {
        this.willRead = willRead;
    }

    public List<Book> getWillBuy() {
        return willBuy;
    }

    public void setWillBuy(List<Book> willBuy) {
        this.willBuy = willBuy;
    }
}
