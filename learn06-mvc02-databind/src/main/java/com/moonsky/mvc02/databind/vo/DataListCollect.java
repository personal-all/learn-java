package com.moonsky.mvc02.databind.vo;

import com.moonsky.mvc02.databind.entity.Author;
import com.moonsky.mvc02.databind.entity.User;

import java.util.List;

/**
 * @author benshaoye
 */
public class DataListCollect {
    private List<User> users;
    private List<Author> authors;

    public DataListCollect() {
    }

    public DataListCollect(List<Author> authors) {
        this.authors = authors;
    }

    public DataListCollect(List<User> users, List<Author> authors) {
        this.users = users;
        this.authors = authors;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DataCollect{");
        sb.append("users=").append(users);
        sb.append(", authors=").append(authors);
        sb.append('}');
        return sb.toString();
    }
}
