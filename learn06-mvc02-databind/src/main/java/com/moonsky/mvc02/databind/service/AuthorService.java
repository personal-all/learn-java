package com.moonsky.mvc02.databind.service;

import com.moonsky.mvc02.databind.entity.Author;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author benshaoye
 */
@Service
public class AuthorService {

    private final List<Author> authors = new ArrayList<>();

    public void add(Author author) {
        authors.add(author);
    }

    public List<Author> findAll() {
        return new ArrayList<>(authors);
    }
}
