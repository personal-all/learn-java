package com.moonsky.mvc02.databind.vo;

import com.moonsky.mvc02.databind.entity.Author;
import com.moonsky.mvc02.databind.entity.Contact;
import com.moonsky.mvc02.databind.entity.User;

import java.util.HashSet;
import java.util.Set;

/**
 * @author benshaoye
 */
public class DataSetCollect {
    private Set<Contact> users;
    private Set<Author> authors;

    public DataSetCollect() {
        users = new HashSet<>();
        users.add(new Contact());
        users.add(new Contact());
        users.add(new Contact());
        users.add(new Contact());
        users.add(new Contact());
        users.add(new Contact());

        authors = new HashSet<>();
        authors.add(new Author());
        authors.add(new Author());
        authors.add(new Author());
        authors.add(new Author());
        authors.add(new Author());
        authors.add(new Author());
        authors.add(new Author());
        authors.add(new Author());
        authors.add(new Author());
        authors.add(new Author());
        authors.add(new Author());
        authors.add(new Author());
        authors.add(new Author());
        authors.add(new Author());
        authors.add(new Author());
        authors.add(new Author());
    }

    public DataSetCollect(Set<Author> authors) {
        this.authors = authors;
    }

    public DataSetCollect(Set<Contact> users, Set<Author> authors) {
        this.users = users;
        this.authors = authors;
    }

    public Set<Contact> getUsers() {
        return users;
    }

    public void setUsers(Set<Contact> users) {
        this.users = users;
    }

    public Set<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(Set<Author> authors) {
        this.authors = authors;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DataCollect{");
        sb.append("users=").append(users);
        sb.append(", authors=").append(authors);
        sb.append('}');
        return sb.toString();
    }
}
