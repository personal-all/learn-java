package com.moonsky.mvc02.databind.web;

import com.moonsky.mvc02.databind.vo.BaseCollect;
import com.moonsky.mvc02.databind.vo.DataListCollect;
import com.moonsky.mvc02.databind.vo.DataSetCollect;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author benshaoye
 */
@Controller
public class CollectController {

    /**
     * 集合不能直接使用集合接收，应该用一个包装类将集合包装后以字段形式出现
     *
     * @param map
     * @param collect
     *
     * @return
     */
    @RequestMapping("collect")
    public String collect(ModelMap map, BaseCollect collect) {
        map.put("collect", collect);
        return "viewer-collect";
    }

    @RequestMapping("data/collect")
    public String collectList(ModelMap map, DataListCollect collect) {
        map.put("collect", collect);
        return "viewer-data-collect";
    }

    @RequestMapping("set/collect")
    public String collectSet(ModelMap map, DataSetCollect collect) {
        map.put("collect", collect);
        return "viewer-data-collect";
    }
}
