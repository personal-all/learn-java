package com.moonsky.mvc02.databind.web;

import com.moonsky.mvc02.databind.entity.Author;
import com.moonsky.mvc02.databind.entity.Book;
import com.moonsky.mvc02.databind.service.AuthorService;
import com.moonsky.mvc02.databind.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

/**
 * @author benshaoye
 */
@Controller
public class DatabindController {

    @Autowired
    private AuthorService authorService;
    @Autowired
    private BookService bookService;

    /**
     * 页面导航
     *
     * @param to
     *
     * @return
     */
    @RequestMapping({"/view/{to}", "/page/{to}"})
    public String view(@PathVariable("to") String to) { return to; }

    @RequestMapping(value = "/add/author")
    public String addAuthor(Map<String, Object> model, Author author) {
        authorService.add(author);
        model.put("authors", authorService.findAll());
        return "author-show";
    }

    @RequestMapping(value = "/add/book")
    public String addBook(Map<String, Object> model, Book book) {
        bookService.add(book);
        model.put("books", bookService.findAll());
        return "book-show";
    }
}
