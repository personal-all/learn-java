package com.moonsky.mvc02.databind.service;

import com.moonsky.mvc02.databind.entity.Reader;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author benshaoye
 */
@Service
public class DatabindService {

    public static List<Reader> database = new ArrayList<>();

    public void save(Reader reader) {
        if (reader != null) {
            database.add(reader);
        }
    }

    public void save(Collection<Reader> readers) {
        if (readers != null) {
            database.addAll(readers);
        }
    }

    public List<Reader> findAll() {
        return new ArrayList<>(database);
    }
}
