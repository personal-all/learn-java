package com.moonsky.mvc02.databind.util;

import org.springframework.core.convert.converter.Converter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author benshaoye
 */
public class String2DateConverter implements Converter<String, Date> {

    @Override
    public Date convert(String source) {
        try {
            return source == null ? null : new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(source);
        } catch (ParseException e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        }
    }
}
