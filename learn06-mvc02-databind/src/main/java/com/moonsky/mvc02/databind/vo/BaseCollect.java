package com.moonsky.mvc02.databind.vo;

import java.util.List;
import java.util.Set;

/**
 * @author benshaoye
 */
public class BaseCollect {

    private List<String> stringList;

    private List<Integer> integerList;

    private List<Long> longList;

    private List<Double> doubleList;

    private Set<String> stringSet;

    private Set<Integer> integerSet;

    private Set<Long> longSet;

    private Set<Double> doubleSet;

    private List<Double> doubleQueue;

    private List list;

    public BaseCollect() {
    }

    public List<String> getStringList() {
        return stringList;
    }

    public void setStringList(List<String> stringList) {
        this.stringList = stringList;
    }

    public List<Integer> getIntegerList() {
        return integerList;
    }

    public void setIntegerList(List<Integer> integerList) {
        this.integerList = integerList;
    }

    public List<Long> getLongList() {
        return longList;
    }

    public void setLongList(List<Long> longList) {
        this.longList = longList;
    }

    public List<Double> getDoubleList() {
        return doubleList;
    }

    public void setDoubleList(List<Double> doubleList) {
        this.doubleList = doubleList;
    }

    public Set<String> getStringSet() {
        return stringSet;
    }

    public void setStringSet(Set<String> stringSet) {
        this.stringSet = stringSet;
    }

    public Set<Integer> getIntegerSet() {
        return integerSet;
    }

    public void setIntegerSet(Set<Integer> integerSet) {
        this.integerSet = integerSet;
    }

    public Set<Long> getLongSet() {
        return longSet;
    }

    public void setLongSet(Set<Long> longSet) {
        this.longSet = longSet;
    }

    public Set<Double> getDoubleSet() {
        return doubleSet;
    }

    public void setDoubleSet(Set<Double> doubleSet) {
        this.doubleSet = doubleSet;
    }

    public List<Double> getDoubleQueue() {
        return doubleQueue;
    }

    public void setDoubleQueue(List<Double> doubleQueue) {
        this.doubleQueue = doubleQueue;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BaseCollect{");
        sb.append("stringList=").append(stringList);
        sb.append(", integerList=").append(integerList);
        sb.append(", longList=").append(longList);
        sb.append(", doubleList=").append(doubleList);
        sb.append(", stringSet=").append(stringSet);
        sb.append(", integerSet=").append(integerSet);
        sb.append(", longSet=").append(longSet);
        sb.append(", doubleSet=").append(doubleSet);
        sb.append(", doubleQueue=").append(doubleQueue);
        sb.append(", list=").append(list);
        sb.append('}');
        return sb.toString();
    }
}
