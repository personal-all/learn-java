package com.moonsky.mvc02.databind.util;

import org.springframework.core.convert.converter.Converter;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author benshaoye
 */
public class Date2StringConverter implements Converter<Date, String> {

    @Override
    public String convert(Date source) {
        return source == null ? null : new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(source);
    }
}
