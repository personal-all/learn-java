package com.moonsky.mvc02.databind.vo;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author benshaoye
 */
public class BaseMap {

    private Map<String, Double> stringDoubleMap;
    private Map<String, Integer> stringIntMap;
    private Map<String, Long> stringLongMap;
    private Map<Long, Double> longDoubleMap;

    private Map<Double, Date> doubleDateMap;
    private Map<Double, Date[]> doubleDateArrMap;

    private Map<Long, Timestamp> longTimestampMap;
    private Map<Long, List<Timestamp>> longTimestampListMap;

    public Map<Long, Timestamp> getLongTimestampMap() {
        return longTimestampMap;
    }

    public void setLongTimestampMap(Map<Long, Timestamp> longTimestampMap) {
        this.longTimestampMap = longTimestampMap;
    }

    public Map<Long, List<Timestamp>> getLongTimestampListMap() {
        return longTimestampListMap;
    }

    public void setLongTimestampListMap(Map<Long, List<Timestamp>> longTimestampListMap) {
        this.longTimestampListMap = longTimestampListMap;
    }

    public Map<Double, Date[]> getDoubleDateArrMap() {
        return doubleDateArrMap;
    }

    public void setDoubleDateArrMap(Map<Double, Date[]> doubleDateArrMap) {
        this.doubleDateArrMap = doubleDateArrMap;
    }

    public Map<Double, Date> getDoubleDateMap() {
        return doubleDateMap;
    }

    public void setDoubleDateMap(Map<Double, Date> doubleDateMap) {
        this.doubleDateMap = doubleDateMap;
    }

    public Map<String, Double> getStringDoubleMap() {
        return stringDoubleMap;
    }

    public void setStringDoubleMap(Map<String, Double> stringDoubleMap) {
        this.stringDoubleMap = stringDoubleMap;
    }

    public Map<String, Integer> getStringIntMap() {
        return stringIntMap;
    }

    public void setStringIntMap(Map<String, Integer> stringIntMap) {
        this.stringIntMap = stringIntMap;
    }

    public Map<String, Long> getStringLongMap() {
        return stringLongMap;
    }

    public void setStringLongMap(Map<String, Long> stringLongMap) {
        this.stringLongMap = stringLongMap;
    }

    public Map<Long, Double> getLongDoubleMap() {
        return longDoubleMap;
    }

    public void setLongDoubleMap(Map<Long, Double> longDoubleMap) {
        this.longDoubleMap = longDoubleMap;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("MapContainer{");
        sb.append("stringDoubleMap=").append(stringDoubleMap);
        sb.append(", stringIntMap=").append(stringIntMap);
        sb.append(", stringLongMap=").append(stringLongMap);
        sb.append(", longDoubleMap=").append(longDoubleMap);
        sb.append(", doubleDateMap=").append(doubleDateMap);
        sb.append(", doubleDateArrMap=").append(doubleDateArrMap);
        sb.append(", longTimestampMap=").append(longTimestampMap);
        sb.append(", longTimestampListMap=").append(longTimestampListMap);
        sb.append('}');
        return sb.toString();
    }
}
