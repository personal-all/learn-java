package com.moonsky.mvc02.databind.web;

import com.moonsky.mvc02.databind.util.Date2StringConverter;
import com.moonsky.mvc02.databind.util.String2DateConverter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;

/**
 * @author benshaoye
 */
@Controller
public class PrimitiveController {

    /*
     * 1、重定向和转发有一个重要的不同：
     * 当使用转发时，JSP容器将使用一个内部的方法来调用目标页面，新的页面继续处理同一个请求，而浏览器将不会知道这个过程。
     * 与之相反，重定向方式的含义是第一个页面通知浏览器发送一个新的页面请求。
     * 因为，当你使用重定向时，浏览器中所显示的URL会变成新页面的URL, 而当使用转发时，该URL会保持不变。
     * 重定向的速度比转发慢，因为浏览器还得发出一个新的请求。
     * 同时，由于重定向方式产生了一个新的请求，所以经过一次重 定向后，request内的对象将无法使用。
     *
     * 2、怎么选择是重定向还是转发呢？通常情况下转发更快，而且能保持request内的对象，所以他是第一选择。
     * 但是由于在转发之后，浏览器中URL仍然指向开始页面，此时如果重载当前页面，开始页面将会被重新调用。
     * 如果你不想看到这样的情况，则选择转发。
     *
     * 转发和重定向的区别：不要仅仅为了把变量传到下一个页面而使用session作用域，
     * 那会无故增大变量的作用域，转发也许可以帮助你解决这个问题。
     *
     * 重定向：以前的request中存放的变量全部失效，并进入一个新的request作用域。
     * 转发：以前的request中存放的变量不会失效，就像把两个页面拼到了一起。
     */

    @RequestMapping("intValue")
    public String intValue(ModelMap model, int intValue) {
        model.put("intValue", intValue);
        return "viewer-value";
    }

    @RequestMapping("longValue")
    public String longValue(ModelMap model, long longValue) {
        model.put("longValue", longValue);
        return "viewer-value";
    }

    @RequestMapping("doubleValue")
    public String doubleValue(ModelMap model, double doubleValue) {
        model.put("doubleValue", doubleValue);
        return "viewer-value";
    }

    /**
     * boolean 类型会将 null 值转换成默认值 false
     * 但{@link Boolean} 类型不会做这个转换，仍然为 null，
     * 因为 Boolean 是可以接收 null 值的
     * <p>
     * 但 int、long、double 等不会将 null 转换成对应默认值 0
     *
     * @param model
     * @param booleanValue
     *
     * @return
     *
     * @see #booleanWrapperValue(ModelMap, Boolean)
     */
    @RequestMapping("booleanValue")
    public String booleanValue(ModelMap model, boolean booleanValue) {
        model.put("booleanValue", booleanValue);
        return "viewer-value";
    }

    @RequestMapping("intWrapperValue")
    public String intWrapperValue(ModelMap model, Integer intWrapperValue) {
        model.put("intWrapperValue", intWrapperValue);
        return "viewer-value";
    }

    @RequestMapping("longWrapperValue")
    public String longWrapperValue(ModelMap model, Long longWrapperValue) {
        model.put("longWrapperValue", longWrapperValue);
        return "viewer-value";
    }

    @RequestMapping("doubleWrapperValue")
    public String doubleWrapperValue(ModelMap model, Double doubleWrapperValue) {
        model.put("doubleWrapperValue", doubleWrapperValue);
        return "viewer-value";
    }

    /**
     * Boolean 是可以接收 null 值的
     *
     * @param model
     * @param booleanWrapperValue
     *
     * @return
     *
     * @see #booleanValue(ModelMap, boolean)
     */
    @RequestMapping("booleanWrapperValue")
    public String booleanWrapperValue(ModelMap model, Boolean booleanWrapperValue) {
        model.put("booleanWrapperValue", booleanWrapperValue);
        return "viewer-value";
    }

    @RequestMapping("stringWrapperValue")
    public String stringWrapperValue(ModelMap model, String stringWrapperValue) {
        model.put("stringWrapperValue", stringWrapperValue);
        return "viewer-value";
    }

    /**
     * 这里用到自定义类型转换器，并参考 spring-mvc.xml 中注册方式
     *
     * @param model
     * @param dateValue
     *
     * @return
     *
     * @see Date2StringConverter
     * @see String2DateConverter
     */
    @RequestMapping("dateValue")
    public String dateValue(ModelMap model, Date dateValue) {
        model.put("dateValue", dateValue);
        return "viewer-value";
    }
}
