package com.moonsky.mvc02.databind.vo;

import com.moonsky.mvc02.databind.entity.Author;
import com.moonsky.mvc02.databind.entity.User;

/**
 * @author benshaoye
 */
public class Wrapper {
    private User user;
    private Author author;

    public Wrapper() {
    }

    public Wrapper(User user, Author author) {
        this.user = user;
        this.author = author;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }
}
