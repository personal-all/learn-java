package com.moonsky.mvc02.databind.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Arrays;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * @author benshaoye
 */
@Controller
public class ArrayController {

    @RequestMapping(value = "array/ints", method = POST)
    public String ints(ModelMap map, int[] values) {
        map.put("values", values);
        map.put("intValues", Arrays.toString(values));
        return "viewer-array";
    }

    @RequestMapping(value = "array/longs", method = POST)
    public String longs(ModelMap map, long[] values) {
        map.put("values", values);
        map.put("longValues", Arrays.toString(values));
        return "viewer-array";
    }

    @RequestMapping(value = "array/doubles", method = POST)
    public String doubles(ModelMap map, double[] values) {
        map.put("values", values);
        map.put("doubleValues", Arrays.toString(values));
        return "viewer-array";
    }

    @RequestMapping(value = "array/strings", method = POST)
    public String strings(ModelMap map, String[] values) {
        map.put("values", values);
        map.put("stringValues", Arrays.toString(values));
        return "viewer-array";
    }
}
