package com.moonsky.mvc02.databind.service;

import com.moonsky.mvc02.databind.entity.Book;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author benshaoye
 */
@Service
public class BookService {

    private final List<Book> books = new ArrayList<>();

    public void add(Book author) {
        books.add(author);
    }

    public List<Book> findAll() {
        return new ArrayList<>(books);
    }

}
