<%--
  Created by IntelliJ IDEA.
  User: ZhangDongMin
  Date: 2019/9/30
  Time: 11:49
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Title</title>
  <style>
    .inline-width-100 {
      display: inline-block;
      width: 100px;
    }

    h5 {
      padding-top: 25px;
      border-top: 2px solid #3d79b6;
    }

    .display-flex {
      display: flex;
    }

    .flex-1 {
      flex: 1;
    }
  </style>
</head>
<body>
<div><a href="/index.jsp">返回</a></div>
<div>
  <h5>数据</h5>
  <div>${user}</div>
  <div>${author}</div>
</div>
<div class="display-flex">
  <div class="flex-1">
    <form action="/init/same-prop" method="post">
      <h5>没有命名空间至 /init/same-prop</h5>
      <div>
        <span class="inline-width-100">&nbsp;</span>
      </div>
      <div>
        <span class="inline-width-100">name</span>
        <input type="text" name="name">
      </div>
      <div>
        <span class="inline-width-100">age</span>
        <input type="text" name="age">
      </div>
      <div>
        <span class="inline-width-100">sex</span>
        <input type="text" name="sex">
      </div>
      <div>
        <span class="inline-width-100">operator</span>
        <button type="submit">提交</button>
      </div>
    </form>
  </div>
  <div class="flex-1">
    <form action="/init/same-prop" method="post">
      <h5>有命名空间至 /init/same-prop</h5>
      <div>
        <span class="inline-width-100">user.name</span>
        <input type="text" name="user.name">
      </div>
      <div>
        <span class="inline-width-100">author.name</span>
        <input type="text" name="author.name">
      </div>
      <div>
        <span class="inline-width-100">age</span>
        <input type="text" name="age">
      </div>
      <div>
        <span class="inline-width-100">sex</span>
        <input type="text" name="sex">
      </div>
      <div>
        <span class="inline-width-100">operator</span>
        <button type="submit">提交</button>
      </div>
    </form>
  </div>
  <div class="flex-1">
    <form action="/init/prop" method="post">
      <h5>有命名空间至 /init/prop</h5>
      <div>
        <span class="inline-width-100">name</span>
        <input type="text" name="name">
      </div>
      <div>
        <span class="inline-width-100">age</span>
        <input type="text" name="age">
      </div>
      <div>
        <span class="inline-width-100">sex</span>
        <input type="text" name="sex">
      </div>
      <div>
        <span class="inline-width-100">operator</span>
        <button type="submit">提交</button>
      </div>
    </form>
  </div>
</div>
</body>
</html>
