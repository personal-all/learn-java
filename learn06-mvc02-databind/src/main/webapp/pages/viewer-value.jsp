<%--
  Created by IntelliJ IDEA.
  User: ZhangDongMin
  Date: 2019/9/17
  Time: 4:49
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Title</title>
  <style>
    div, span {
      margin: 20px;
    }

    input {
      padding: 5px 12px;
      border: 1px solid #123;
    }
  </style>
</head>
<body>
<div><a href="/index.jsp">返回</a></div>
<div style="display: flex;">
  <div style="flex: 1">
    <div>
      <h3>Content</h3>
      <div>
        int value：${intValue}
      </div>
      <div>
        long value：${longValue}
      </div>
      <div>
        double value：${doubleValue}
      </div>
      <div>
        boolean value：${booleanValue}
      </div>
    </div>
  </div>
  <div style="flex: 1">
    <div>
      <h3>Content</h3>
      <div>
        int WrapperValue：${intWrapperValue}
      </div>
      <div>
        long WrapperValue：${longWrapperValue}
      </div>
      <div>
        double WrapperValue：${doubleWrapperValue}
      </div>
      <div>
        string WrapperValue：${booleanWrapperValue}
      </div>
    </div>
  </div>
</div>
<div style="display: flex;">
  <div style="flex: 1;">
    <form action="/intValue">
  <span>
    <input placeholder="intValue" autofocus type="number" name="intValue">
  </span>
    </form>
    <form action="/longValue">
  <span>
    <input placeholder="longValue" autofocus type="number" name="longValue">
  </span>
    </form>
    <form action="/doubleValue">
  <span>
    <input placeholder="doubleValue" step="0.001" autofocus type="number" name="doubleValue">
  </span>
    </form>
    <form action="/booleanValue">
  <span>
    <input placeholder="booleanValue" autofocus type="text" name="booleanValue">
  </span>
    </form>
  </div>
  <div style="flex: 1;">
    <form action="/intWrapperValue">
  <span>
    <input placeholder="intWrapperValue" autofocus type="number" name="intWrapperValue">
  </span>
    </form>
    <form action="/longWrapperValue">
  <span>
    <input placeholder="longWrapperValue" autofocus type="number" name="longWrapperValue">
  </span>
    </form>
    <form action="/doubleWrapperValue">
  <span>
    <input placeholder="doubleWrapperValue" step="0.001" autofocus type="number" name="doubleWrapperValue">
  </span>
    </form>
    <form action="/booleanWrapperValue">
  <span>
    <input placeholder="booleanWrapperValue" autofocus type="text" name="booleanWrapperValue">
  </span>
    </form>
  </div>
</div>
<div>
  <form action="/stringWrapperValue">
  <span>
    <input placeholder="stringWrapperValue" autofocus type="text" name="stringWrapperValue">
  </span>
    <span>
      string WrapperValue：${stringWrapperValue}
    </span>
  </form>
</div>
<div>
  <form action="/dateValue">
  <span>
    <input placeholder="dateValue" autofocus type="text" name="dateValue">
  </span>
    <span>
      Date value：${dateValue}
    </span>
  </form>
</div>
</body>
</html>
