<%--
  Created by IntelliJ IDEA.
  User: ZhangDongMin
  Date: 2019/9/15
  Time: 22:12
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Title</title>
  <script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js"></script>
  <script>
    const $ = jQuery;

    $(function () {
      const counter = (function () {
        let index = 1;
        return () => index++
      })();
      $('.add').on('click', function (e) {
        e.preventDefault();
        const index = counter();
        const $form = $(this).closest('form');
        $('<div><input name="values" type="number"></div>').appendTo($form).find('input').val(index);
      });
    })
  </script>
</head>
<body>
<div><a href="/index.jsp">返回</a></div>
<div>
  <div>int 数组</div>
  <div>${intsValues}</div>
  <form action="/array/ints" method="post">
    <div>
      <button class="add">增加</button>
      <input type="submit" value="提交"></div>
    <div><input name="values" type="number" value="0"></div>
  </form>
</div>
<div>
  <div>long 数组</div>
  <div>${longsValues}</div>
  <form  action="/array/longs" method="post">
    <div>
      <button class="add">增加</button>
      <input type="submit" value="提交"></div>
    <div><input name="values" type="number" value="0"></div>
  </form>
</div>
<div>
  <div>double 数组</div>
  <div>${doublesValues}</div>
  <form  action="/array/doubles" method="post">
    <div>
      <button class="add">增加</button>
      <input type="submit" value="提交"></div>
    <div><input name="values" type="number" value="0"></div>
  </form>
</div>
<div>
  <div>string 数组</div>
  <div>${stringsValues}</div>
  <form  action="/array/strings" method="post">
    <div>
      <button class="add">增加</button>
      <input type="submit" value="提交"></div>
    <div><input name="values" type="number" value="0"></div>
  </form>
</div>

</body>
</html>
