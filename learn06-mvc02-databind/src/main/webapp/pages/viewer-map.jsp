<%--
  Created by IntelliJ IDEA.
  User: ZhangDongMin
  Date: 2019/9/15
  Time: 22:12
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Title</title>
  <style>
    .btn {
      padding: 0 12px;
      border: none;
      background: seagreen;
      color: #fff;
      height: 32px;
      line-height: 32px;
      font-size: 14px;
      outline: none;
      border-radius: 3px;
      cursor: pointer;
      transition-duration: .3s;
    }

    .btn.display-block {
      width: 100%;
    }

    .btn:hover {
      background: #2b8251;
    }

    .btn:active {
      background: #349c62;
    }

    .display-flex {
      display: flex;
    }

    .display-block {
      display: block;
    }

    .flex-1, .input-container {
      flex: 1;
    }

    .btn[type=submit] {
      margin: 20px 0;
    }
  </style>
</head>
<body>
${container}
<form action="/map" method="post">
  <div class="block-form" data-namespace="stringDoubleMap">
    <h4></h4>
    <div class="display-flex">
      <div>
        <button class="btn add">添加</button>
      </div>
      <div class="input-container">
        <input type="number" name="stringDoubleMap['first']" value="20">
        <input type="number" name="stringDoubleMap['second']" value="30">
      </div>
    </div>
  </div>
  <div class="block-form" data-namespace="stringIntMap">
    <h4></h4>
    <div class="display-flex">
      <div>
        <button class="btn add">添加</button>
      </div>
      <div class="input-container">
        <input type="number" name="stringIntMap['first']" value="20">
        <input type="number" name="stringIntMap['second']" value="30">
      </div>
    </div>
  </div>
  <div class="block-form" data-namespace="stringLongMap">
    <h4></h4>
    <div class="display-flex">
      <div>
        <button class="btn add">添加</button>
      </div>
      <div class="input-container">
        <input type="number" name="stringLongMap['first']" value="20">
        <input type="number" name="stringLongMap['second']" value="30">
      </div>
    </div>
  </div>
  <div class="block-form" data-namespace="longDoubleMap">
    <h4></h4>
    <div class="display-flex">
      <div>
        <button class="btn add">添加</button>
      </div>
      <div class="input-container">
        <input type="number" name="longDoubleMap[30]" value="300">
        <input type="number" name="longDoubleMap[40]" value="400">
      </div>
    </div>
  </div>
  <div class="block-form" data-namespace="doubleDateMap">
    <h4></h4>
    <div class="display-flex">
      <div>
        <button class="btn add">添加</button>
      </div>
      <div class="input-container">
        <input type="text" name="doubleDateMap[30]" value="2019-06-08 15:16:17">
        <input type="text" name="doubleDateMap[40]" value="2019-06-08 15:16:18">
      </div>
    </div>
  </div>
  <div class="block-form" data-namespace="doubleDateArrMap">
    <h4></h4>
    <div class="display-flex">
      <div>
        <button class="btn add">添加</button>
      </div>
      <div class="input-container">
        <input type="text" name="doubleDateArrMap[30]" value="2019-06-08 15:16:17">
        <input type="text" name="doubleDateArrMap[30]" value="2019-06-09 15:16:17">
        <input type="text" name="doubleDateArrMap[40]" value="2019-06-08 15:16:18">
        <input type="text" name="doubleDateArrMap[40]" value="2019-06-09 15:16:18">
      </div>
    </div>
  </div>
  <div class="block-form" data-namespace="longTimestampMap">
    <h4></h4>
    <div class="display-flex">
      <div>
        <button class="btn add">添加</button>
      </div>
      <div class="input-container">
        <input type="text" name="longTimestampMap[30]" value="2019-06-08 15:16:17">
        <input type="text" name="longTimestampMap[40]" value="2019-06-08 15:16:18">
      </div>
    </div>
  </div>
  <div class="block-form" data-namespace="longTimestampListMap">
    <h4></h4>
    <div class="display-flex">
      <div>
        <button class="btn add">添加</button>
      </div>
      <div class="input-container">
        <input type="text" name="longTimestampListMap[30]" value="2019-06-08 15:16:17">
        <input type="text" name="longTimestampListMap[30]" value="2019-06-09 15:16:17">
        <input type="text" name="longTimestampListMap[40]" value="2019-06-08 15:16:18">
        <input type="text" name="longTimestampListMap[40]" value="2019-06-09 15:16:18">
      </div>
    </div>
  </div>
  <button class="display-block btn" type="submit">提交</button>
</form>
<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js"></script>
<script>
  $(function () {
    $('.add').hide().on('click', function (e) {
      e.preventDefault();
    }).closest('.block-form').each(function () {
      $(this).find('h4').text(this.dataset.namespace + ' 演示');
    })
  })
</script>
</body>
</html>
