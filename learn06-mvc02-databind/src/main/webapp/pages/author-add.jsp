<%--
  Created by IntelliJ IDEA.
  User: ZhangDongMin
  Date: 2019/9/15
  Time: 22:11
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<head>
  <title>Title</title>
  <style>
    div {
      margin: 20px;
    }
  </style>
</head>
<body>
<div><a href="/index.jsp">返回</a></div>
<%-- 注意 post 提交，否则会乱码 --%>
<form action="/add/author" method="post">
  <div>
    <input type="text" name="name" placeholder="名字">
  </div>
  <div>
    <input type="text" name="sex" placeholder="性别">
  </div>
  <div>
    <input type="text" name="age" placeholder="年龄">
  </div>
  <div>
    <input type="submit" value="提交">
  </div>
</form>
</body>
</html>
