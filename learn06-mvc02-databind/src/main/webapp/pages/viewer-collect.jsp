<%--
  Created by IntelliJ IDEA.
  User: ZhangDongMin
  Date: 2019/9/15
  Time: 22:12
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Title</title>
<%--  <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>--%>
  <script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js"></script>
<%--  <script src="/bootstrap/js/bootstrap.min.js"></script>--%>
  <style>
    .form-data h4 {

    }
  </style>
  <script>
    const $ = jQuery;

    $(function () {
      $('.form-data').find('div').each(function () {
        $(this).find('h4').text(this.dataset.namespace)
      })
      $('.add').on('click', function (e) {
        e.preventDefault();
        const $add = $(this);
        const $namespace = $add.closest('div');
        const namespace = $namespace[0].dataset.namespace;
        $('<input type="text">').appendTo($namespace).attr('name', namespace);
      });
    });
  </script>
</head>
<body>
<div><a href="/index.jsp">返回</a></div>
<div>
  <form action="/collect" class="form-data" method="post">
    <input type="submit" value="提交">
    <div data-namespace="stringList">
      <h4>stringList</h4>
      <div>${collect.stringList}</div>
      <button class="btn add">添加</button>
    </div>
    <div data-namespace="integerList">
      <h4>stringList</h4>
      <div>${collect.integerList}</div>
      <button class="add">添加</button>
    </div>
    <div data-namespace="longList">
      <h4>stringList</h4>
      <div>${collect.longList}</div>
      <button class="add">添加</button>
    </div>
    <div data-namespace="doubleList">
      <h4>stringList</h4>
      <div>${collect.doubleList}</div>
      <button class="add">添加</button>
    </div>
    <div data-namespace="stringSet">
      <h4>stringList</h4>
      <div>${collect.stringSet}</div>
      <button class="add">添加</button>
    </div>
    <div data-namespace="longSet">
      <h4>stringList</h4>
      <div>${collect.longSet}</div>
      <button class="add">添加</button>
    </div>
    <div data-namespace="doubleSet">
      <h4>stringList</h4>
      <div>${collect.doubleSet}</div>
      <button class="add">添加</button>
    </div>
    <div data-namespace="doubleQueue">
      <h4>stringList</h4>
      <div>${collect.doubleQueue}</div>
      <button class="add">添加</button>
    </div>
    <div data-namespace="list">
      <h4>stringList</h4>
      <div>${collect.list}</div>
      <button class="add">添加</button>
    </div>
  </form>
</div>
<div style="height: 300px;">
  ${collect}
</div>
</body>
</html>
