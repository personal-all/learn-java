<%--
  Created by IntelliJ IDEA.
  User: ZhangDongMin
  Date: 2019/9/16
  Time: 22:17
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Title</title>
  <style>
    span {
      padding: 20px;
    }
  </style>
</head>
<body>
<div>
  <h4>基本类型数据绑定</h4>
  <span><a href="/view/author-add">/author-add</a></span>
  <span><a href="/view/viewer-value">/viewer-value</a></span>
  <span><a href="/view/viewer-array">/viewer-array</a></span>
  <span><a href="/view/viewer-collect">/viewer-collect</a></span>
  <span><a href="/view/viewer-map">/viewer-map</a></span>
</div>
<div>
  <h4>不同对象相同属性的绑定</h4>
  <div>
    <a href="/init/prop?user.name=user&author.name=author&age=24&sex=1">
      【/prop】：/init/prop?user.name=user&author.name=author&age=24&sex=1
    </a>
  </div>
  <div>
    <a href="/init/same-prop?name=benshaoye&age=24&sex=1">
      【/same-prop】：/init/same-prop?name=benshaoye&age=24&sex=1
    </a>
  </div>
  <div>
    <a href="/init/same-prop?user.name=user&author.name=author&age=24&sex=1">
      【/same-prop】：/init/same-prop?user.name=user&author.name=author&age=24&sex=1
    </a>
  </div>
</div>
</body>
</html>
