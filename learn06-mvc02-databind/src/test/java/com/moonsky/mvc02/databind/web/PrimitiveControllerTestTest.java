package com.moonsky.mvc02.databind.web;

import org.hamcrest.CoreMatchers;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static java.lang.String.format;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * @author benshaoye
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring-mvc.xml")
@WebAppConfiguration
public class PrimitiveControllerTestTest {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    private Matcher isNull = CoreMatchers.is((Object) null);

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
    }
    @Before
    public void first(){
        System.out.println("1");
    }
    @Before
    public void second(){
        System.out.println("2");
    }

    ResultActions getActions(String uri, String param, Object value) throws Exception {
        return mockMvc.perform(get(uri).param(param, value == null ? null : value.toString()));
    }

    @Test
    public void testIntValue() throws Exception {
        String target = "intValue";
        ResultActions actions = getActions(format("/%s", target), target, 0);
        actions.andExpect(status().isOk());
        actions.andExpect(model().attribute(target, 0));
        actions.andExpect(handler().handlerType(PrimitiveController.class));
        actions.andExpect(handler().methodName(target));
        actions.andExpect(view().name("viewer-value"));
        actions.andExpect(request().attribute(target, 0));
    }

    @Test
    public void testLongValue() throws Exception {
        String target = "longValue";
        ResultActions actions = getActions(format("/%s", target), target, 0L);
        actions.andExpect(status().isOk());
        actions.andExpect(model().attribute(target, 0L));
        actions.andExpect(handler().handlerType(PrimitiveController.class));
        actions.andExpect(handler().methodName(target));
        actions.andExpect(view().name("viewer-value"));
        actions.andExpect(request().attribute(target, 0L));
    }

    @Test
    public void testDoubleValue() throws Exception {
        String target = "doubleValue";
        ResultActions actions = getActions(format("/%s", target), target, 0D);
        actions.andExpect(status().isOk());
        actions.andExpect(model().attribute(target, 0D));
        actions.andExpect(handler().handlerType(PrimitiveController.class));
        actions.andExpect(handler().methodName(target));
        actions.andExpect(view().name("viewer-value"));
        actions.andExpect(request().attribute(target, 0D));
    }

    @Test
    public void testBooleanValue() throws Exception {
        String target = "booleanValue";
        ResultActions actions = getActions(format("/%s", target), target, true);
        actions.andExpect(status().isOk());
        actions.andExpect(model().attribute(target, true));
        actions.andExpect(handler().handlerType(PrimitiveController.class));
        actions.andExpect(handler().methodName(target));
        actions.andExpect(view().name("viewer-value"));
        actions.andExpect(request().attribute(target, true));

        actions = getActions(format("/%s", target), target, false);
        actions.andExpect(status().isOk());
        actions.andExpect(model().attribute(target, false));
        actions.andExpect(handler().handlerType(PrimitiveController.class));
        actions.andExpect(handler().methodName(target));
        actions.andExpect(view().name("viewer-value"));
        actions.andExpect(request().attribute(target, false));

        actions = getActions(format("/%s", target), target, null);
        actions.andExpect(status().isOk());
        actions.andExpect(model().attribute(target, false));
        actions.andExpect(handler().handlerType(PrimitiveController.class));
        actions.andExpect(handler().methodName(target));
        actions.andExpect(view().name("viewer-value"));
        actions.andExpect(request().attribute(target, false));
    }

    @Test
    public void testIntWrapperValue() throws Exception {
        String target = "intWrapperValue";
        ResultActions actions = getActions(format("/%s", target), target, 0);
        actions.andExpect(status().isOk());
        actions.andExpect(model().attribute(target, 0));
        actions.andExpect(handler().handlerType(PrimitiveController.class));
        actions.andExpect(handler().methodName(target));
        actions.andExpect(view().name("viewer-value"));
        actions.andExpect(request().attribute(target, 0));

        actions = getActions(format("/%s", target), target, null);
        actions.andExpect(status().isOk());
        actions.andExpect(model().attribute(target, isNull));
        actions.andExpect(handler().handlerType(PrimitiveController.class));
        actions.andExpect(handler().methodName(target));
        actions.andExpect(view().name("viewer-value"));
        actions.andExpect(request().attribute(target, isNull));
    }

    @Test
    public void testLongWrapperValue() throws Exception {
        String target = "longWrapperValue";
        ResultActions actions = getActions(format("/%s", target), target, 0L);
        actions.andExpect(status().isOk());
        actions.andExpect(model().attribute(target, 0L));
        actions.andExpect(handler().handlerType(PrimitiveController.class));
        actions.andExpect(handler().methodName(target));
        actions.andExpect(view().name("viewer-value"));
        actions.andExpect(request().attribute(target, 0L));

        actions = getActions(format("/%s", target), target, null);
        actions.andExpect(status().isOk());
        actions.andExpect(model().attribute(target, isNull));
        actions.andExpect(handler().handlerType(PrimitiveController.class));
        actions.andExpect(handler().methodName(target));
        actions.andExpect(view().name("viewer-value"));
        actions.andExpect(request().attribute(target, isNull));
    }

    @Test
    public void testDoubleWrapperValue() throws Exception {
        String target = "doubleWrapperValue";
        ResultActions actions = getActions(format("/%s", target), target, 0D);
        actions.andExpect(status().isOk());
        actions.andExpect(model().attribute(target, 0D));
        actions.andExpect(handler().handlerType(PrimitiveController.class));
        actions.andExpect(handler().methodName(target));
        actions.andExpect(view().name("viewer-value"));
        actions.andExpect(request().attribute(target, 0D));

        actions = getActions(format("/%s", target), target, null);
        actions.andExpect(status().isOk());
        actions.andExpect(model().attribute(target, isNull));
        actions.andExpect(handler().handlerType(PrimitiveController.class));
        actions.andExpect(handler().methodName(target));
        actions.andExpect(view().name("viewer-value"));
        actions.andExpect(request().attribute(target, isNull));
    }

    @Test
    public void testBooleanWrapperValue() throws Exception {
        String target = "booleanWrapperValue";
        ResultActions actions = getActions(format("/%s", target), target, true);
        actions.andExpect(status().isOk());
        actions.andExpect(model().attribute(target, true));
        actions.andExpect(handler().handlerType(PrimitiveController.class));
        actions.andExpect(handler().methodName(target));
        actions.andExpect(view().name("viewer-value"));
        actions.andExpect(request().attribute(target, true));

        actions = getActions(format("/%s", target), target, false);
        actions.andExpect(status().isOk());
        actions.andExpect(model().attribute(target, false));
        actions.andExpect(handler().handlerType(PrimitiveController.class));
        actions.andExpect(handler().methodName(target));
        actions.andExpect(view().name("viewer-value"));
        actions.andExpect(request().attribute(target, false));

        actions = getActions(format("/%s", target), target, null);
        actions.andExpect(status().isOk());
        actions.andExpect(model().attribute(target, isNull));
        actions.andExpect(handler().handlerType(PrimitiveController.class));
        actions.andExpect(handler().methodName(target));
        actions.andExpect(view().name("viewer-value"));
        actions.andExpect(request().attribute(target, isNull));
    }

    @Test
    public void testStringWrapperValue() throws Exception {
        String target = "stringWrapperValue";
        ResultActions actions = getActions(format("/%s", target), target, target);
        actions.andExpect(status().isOk());
        actions.andExpect(model().attribute(target, target));
        actions.andExpect(handler().handlerType(PrimitiveController.class));
        actions.andExpect(handler().methodName(target));
        actions.andExpect(view().name("viewer-value"));
        actions.andExpect(request().attribute(target, target));

        actions = getActions(format("/%s", target), target, null);
        actions.andExpect(status().isOk());
        actions.andExpect(model().attribute(target, isNull));
        actions.andExpect(handler().handlerType(PrimitiveController.class));
        actions.andExpect(handler().methodName(target));
        actions.andExpect(view().name("viewer-value"));
        actions.andExpect(request().attribute(target, isNull));
    }

    @Test
    public void testDateValue() throws Exception {
        String pattern = "yyyy-MM-dd HH:mm:ss";
        DateFormat formatter = new SimpleDateFormat(pattern);
        String dateStr = formatter.format(new Date());
        String target = "dateValue";
        ResultActions actions = getActions(format("/%s", target), target, dateStr);

        actions.andExpect(status().isOk());
        actions.andExpect(model().size(1));
        actions.andExpect(model().attribute(target, CoreMatchers.is(formatter.parse(dateStr))));
        actions.andExpect(view().name("viewer-value"));
    }
}