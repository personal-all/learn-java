package com.moonsky.mvc02.databind.web;

import org.junit.Test;
import org.junit.runner.Description;
import org.junit.runner.Result;
import org.junit.runner.RunWith;
import org.junit.runner.Runner;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;
import org.junit.runner.notification.RunNotifier;
import org.springframework.test.context.TestExecutionListeners;

/**
 * @author benshaoye
 */
@RunWith(SimpleTest.CustomRunner.class)
@TestExecutionListeners
public class SimpleTest {

    public static class CustomRunner extends Runner {

        private final Class target;

        public CustomRunner(Class target) {this.target = target;}


        @Override
        public Description getDescription() {
            return Description.createSuiteDescription(target);
        }

        @Override
        public void run(RunNotifier notifier) {
            notifier.addFirstListener(new CustomListener());
            System.out.println("================>> " + getClass());
        }
    }

    public static class CustomListener extends RunListener {

        public CustomListener() {
            super();
        }

        @Override
        public void testRunStarted(Description description) throws Exception {
            System.out.println(String.format("执行方法：%s", "testRunStarted"));
        }

        @Override
        public void testRunFinished(Result result) throws Exception {
            System.out.println(String.format("执行方法：%s", "testRunFinished"));
        }

        @Override
        public void testStarted(Description description) throws Exception {
            System.out.println(String.format("执行方法：%s", "testStarted"));
        }

        @Override
        public void testFinished(Description description) throws Exception {
            System.out.println(String.format("执行方法：%s", "testFinished"));
        }

        @Override
        public void testFailure(Failure failure) throws Exception {
            System.out.println(String.format("执行方法：%s", "testFailure"));
        }

        @Override
        public void testAssumptionFailure(Failure failure) {
            System.out.println(String.format("执行方法：%s", "testAssumptionFailure"));
        }

        @Override
        public void testIgnored(Description description) throws Exception {
            System.out.println(String.format("执行方法：%s", "testIgnored"));
        }
    }

    @Test
    public void testCustomRunner() throws Exception {
        System.out.println("====================== 这里根本不输出");
    }
}
