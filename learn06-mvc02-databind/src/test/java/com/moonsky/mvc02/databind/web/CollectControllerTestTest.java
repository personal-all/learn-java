package com.moonsky.mvc02.databind.web;

import com.moonsky.mvc02.databind.entity.Author;
import com.moonsky.mvc02.databind.entity.User;
import com.moonsky.mvc02.databind.vo.BaseCollect;
import com.moonsky.mvc02.databind.vo.DataListCollect;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.ModelMap;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;

import static java.lang.String.format;
import static java.lang.System.out;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author benshaoye
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring-mvc.xml")
@WebAppConfiguration
public class CollectControllerTestTest {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    /**
     * @throws Exception
     * @see BaseCollect
     */
    @Test
    public void testCollect() throws Exception {
        String[] values = {"111", "222", "333", "444", "555"};

        MockHttpServletRequestBuilder request = get("/collect");
        request.param("stringList", values).param("integerList", values);
        request.param("longList", values).param("doubleList", values);

        request.param("stringSet", values).param("integerSet", values);
        request.param("longSet", values).param("doubleSet", values);

        request.param("doubleQueue", values).param("list", values);

        ResultActions actions = mockMvc.perform(request);

        actions.andExpect(status().isOk());
        actions.andExpect(model().hasNoErrors());

        MvcResult result = actions.andReturn();
        ModelAndView mv = result.getModelAndView();
        ModelMap map = mv.getModelMap();

        BaseCollect bean = (BaseCollect) map.get("collect");
        assertCollect(bean.getDoubleSet(), values, v -> Double.parseDouble(v));
        assertCollect(bean.getLongSet(), values, v -> Long.parseLong(v));
        assertCollect(bean.getIntegerSet(), values, v -> Integer.parseInt(v));
        assertCollect(bean.getDoubleQueue(), values, v -> Double.parseDouble(v));

        assertCollect(bean.getDoubleList(), values, v -> Double.parseDouble(v));
        assertCollect(bean.getLongList(), values, v -> Long.parseLong(v));
        assertCollect(bean.getIntegerList(), values, v -> Integer.parseInt(v));
        assertCollect(bean.getStringList(), values, v -> (v));
        assertCollect(bean.getStringSet(), values, v -> (v));
        assertCollect(bean.getList(), values, v -> (v));


        map.forEach((key, value) -> {
            out.println();
            out.println(format("Key type: [%s] for key: {%s};", key.getClass(), key));
        });

        printlnCollect(bean.getDoubleList(), bean.getLongList());
        printlnCollect(bean.getIntegerList(), bean.getStringList());

        printlnCollect(bean.getDoubleSet(), bean.getLongSet());
        printlnCollect(bean.getIntegerSet(), bean.getStringSet());

        printlnCollect(bean.getDoubleQueue(), bean.getList());
    }

    public <T> void assertCollect(Collection<T> collect, String[] values, Function<String, T> converter) {
        if (collect == null) {
            return;
        }
        int index = 0;
        for (T value : collect) {
            Assert.assertEquals(converter.apply(values[index++]), value);
        }
    }

    public void printlnCollect(Collection... collections) {
        for (Collection collection : collections) {
            printlnCollect(collection);
        }
    }

    public <T> void printlnCollect(Collection<T> collect) {
        out.println(format("Collection type: [%s]", collect.getClass()));
        collect.forEach(value -> {
            out.println(format("Value type: [%s] for value: {%s};", value.getClass(), value));
        });
    }

    private void buildRequestParam(MockHttpServletRequestBuilder request) {
        request.param("users[0].name", "张三").param("users[0].sex", "1").param("users[0].age", "35");
        request.param("users[1].name", "李四").param("users[1].sex", "2").param("users[1].age", "24");
        request.param("users[3].name", "王五").param("users[3].sex", "0").param("users[3].age", "30");

        request.param("authors[0].name", "作者01").param("authors[0].sex", "0").param("authors[0].age", "30");
        request.param("authors[1].name", "作者02").param("authors[1].sex", "4").param("authors[1].age", "56");
        request.param("authors[10].name", "作者03").param("authors[10].sex", "5").param("authors[10].age", "67");
    }

    private void assertAuthorList(List<Author> authorList) {
        Assert.assertEquals(11, authorList.size());
        Author empty = new Author();

        Assert.assertEquals("作者01", authorList.get(0).getName());
        Assert.assertEquals(0, authorList.get(0).getSex());
        Assert.assertEquals(30, authorList.get(0).getAge());

        Assert.assertEquals("作者02", authorList.get(1).getName());
        Assert.assertEquals(4, authorList.get(1).getSex());
        Assert.assertEquals(56, authorList.get(1).getAge());

        Assert.assertEquals(empty, authorList.get(2));
        Assert.assertEquals(empty, authorList.get(3));
        Assert.assertEquals(empty, authorList.get(4));
        Assert.assertEquals(empty, authorList.get(5));
        Assert.assertEquals(empty, authorList.get(6));
        Assert.assertEquals(empty, authorList.get(7));
        Assert.assertEquals(empty, authorList.get(8));
        Assert.assertEquals(empty, authorList.get(9));

        Assert.assertEquals("作者03", authorList.get(10).getName());
        Assert.assertEquals(5, authorList.get(10).getSex());
        Assert.assertEquals(67, authorList.get(10).getAge());
    }

    private void assertUserList(List<User> userList) {
        Assert.assertEquals(4, userList.size());
        User empty = new User();

        Assert.assertEquals("张三", userList.get(0).getName());
        Assert.assertEquals(1, userList.get(0).getSex());
        Assert.assertEquals(35, userList.get(0).getAge());

        Assert.assertEquals("李四", userList.get(1).getName());
        Assert.assertEquals(2, userList.get(1).getSex());
        Assert.assertEquals(24, userList.get(1).getAge());

        Assert.assertEquals(empty, userList.get(2));

        Assert.assertEquals("王五", userList.get(3).getName());
        Assert.assertEquals(0, userList.get(3).getSex());
        Assert.assertEquals(30, userList.get(3).getAge());
    }

    @Test
    public void testCollectList() throws Exception {
        MockHttpServletRequestBuilder request = get("/data/collect");

        buildRequestParam(request);

        ResultActions actions = mockMvc.perform(request);

        actions.andExpect(status().isOk());
        actions.andExpect(model().attribute("collect", instanceOf(DataListCollect.class)));

        MvcResult result = actions.andReturn();

        ModelAndView mv = result.getModelAndView();

        DataListCollect collect = (DataListCollect) mv.getModelMap().get("collect");

        List<Author> authorList = collect.getAuthors();
        List<User> userList = collect.getUsers();

        assertAuthorList(authorList);
        assertUserList(userList);
    }

    private void buildSetRequestParam(MockHttpServletRequestBuilder request) {
        request.param("users[0].name", "张三").param("users[0].sex", "1").param("users[0].age", "35");
        request.param("users[1].name", "李四").param("users[1].sex", "2").param("users[1].age", "24");
        request.param("users[2].name", "王五").param("users[2].sex", "0").param("users[2].age", "30");

        request.param("authors[0].name", "作者01").param("authors[0].sex", "0").param("authors[0].age", "30");
    }

    @Test
    public void testCollectSet() throws Exception {
        MockHttpServletRequestBuilder request = get("/set/collect");

        buildSetRequestParam(request);

        ResultActions actions = mockMvc.perform(request);
        actions.andExpect(status().isOk());
    }
}