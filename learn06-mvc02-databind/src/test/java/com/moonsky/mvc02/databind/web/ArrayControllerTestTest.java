package com.moonsky.mvc02.databind.web;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runner.Runner;
import org.junit.runner.notification.RunNotifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.ModelMap;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * JUnit 运行步骤：
 * <p>
 * 1、通过{@link RunWith}改变其默认执行类，要求执行类实现接口{@link Runner}
 * - {@link Runner}中有三个方法，通过方法名可知，最终执行方法肯定是{@link Runner#run(RunNotifier)}
 * <p>
 * 2、执行构造函数，所有执行均需要一个实例来执行，这是面向对象编程应该有的基本思维，
 * - 所以我们找到{@link SpringJUnit4ClassRunner}
 * - 的构造器{@link SpringJUnit4ClassRunner#SpringJUnit4ClassRunner(Class)}，
 * - 参数 class 就是要测试的目标类但是我们看{@link Runner}的源码可知，其中并没有需要 class 的地方，
 * - 也就是说，我们自定义的测试类，和我们实际要测试的类并没有什么关系，但是其中{@link Runner#getDescription()}
 * - 必须有个非空返回值。{@link SimpleTest.CustomRunner}
 * <p>
 * 3、{@link Runner#run(RunNotifier)}的{@link RunNotifier}是个什么东西呢？通过查看他的源码，仅看方法名像是
 * - 一个与生命周期相关的描述对象
 *
 * @author benshaoye
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring-mvc.xml")
@WebAppConfiguration
public class ArrayControllerTestTest {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    @Test
    public void testInts() throws Exception {
        String[] values = {"1", "2", "3", "4", "5", "6"};
        MockHttpServletRequestBuilder request = post("/array/ints");
        request.param("values", values);

        ResultActions actions = mockMvc.perform(request);

        actions.andExpect(status().isOk());
        actions.andExpect(handler().handlerType(ArrayController.class));
        actions.andExpect(handler().methodName("ints"));

        actions.andExpect(model().attribute("values", instanceOf(int[].class)));
        actions.andExpect(model().attribute("intValues", instanceOf(String.class)));
        actions.andExpect(model().size(2));
        actions.andExpect(view().name("viewer-array"));

        MvcResult result = actions.andReturn();
        ModelAndView mv = result.getModelAndView();
        ModelMap map = mv.getModelMap();
        int[] mvValues = (int[]) map.get("values");
        int[] expect = {1, 2, 3, 4, 5, 6};
        Assert.assertArrayEquals(mvValues, expect);
        String intValues = (String) map.get("intValues");
        Assert.assertEquals(Arrays.toString(expect), intValues);
    }

    @Test
    public void testLongs() throws Exception {
        String[] values = {"1", "2", "3", "4", "5", "6"};
        MockHttpServletRequestBuilder request = post("/array/longs");
        request.param("values", values);

        ResultActions actions = mockMvc.perform(request);

        actions.andExpect(status().isOk());
        actions.andExpect(handler().handlerType(ArrayController.class));
        actions.andExpect(handler().methodName("longs"));

        actions.andExpect(model().attribute("values", instanceOf(long[].class)));
        actions.andExpect(model().attribute("longValues", instanceOf(String.class)));
        actions.andExpect(model().size(2));
        actions.andExpect(view().name("viewer-array"));

        MvcResult result = actions.andReturn();
        ModelAndView mv = result.getModelAndView();
        ModelMap map = mv.getModelMap();
        long[] mvValues = (long[]) map.get("values");
        long[] expect = {1, 2, 3, 4, 5, 6};
        Assert.assertArrayEquals(mvValues, expect);
        String intValues = (String) map.get("longValues");
        Assert.assertEquals(Arrays.toString(expect), intValues);
    }

    @Test
    public void testDoubles() throws Exception {
        String[] values = {"1", "2", "3", "4", "5", "6"};
        MockHttpServletRequestBuilder request = post("/array/doubles");
        request.param("values", values);

        ResultActions actions = mockMvc.perform(request);

        actions.andExpect(status().isOk());
        actions.andExpect(handler().handlerType(ArrayController.class));
        actions.andExpect(handler().methodName("doubles"));

        actions.andExpect(model().attribute("values", instanceOf(double[].class)));
        actions.andExpect(model().attribute("doubleValues", instanceOf(String.class)));
        actions.andExpect(model().size(2));
        actions.andExpect(view().name("viewer-array"));

        MvcResult result = actions.andReturn();
        ModelAndView mv = result.getModelAndView();
        ModelMap map = mv.getModelMap();
        double[] mvValues = (double[]) map.get("values");
        double[] expect = {1, 2, 3, 4, 5, 6};
        String intValues = (String) map.get("doubleValues");
        Assert.assertEquals(Arrays.toString(expect), intValues);
    }

    @Test
    public void testStrings() throws Exception {
        String[] values = {"1", "2", "3", "4", "5", "6"};
        MockHttpServletRequestBuilder request = post("/array/strings");
        request.param("values", values);

        ResultActions actions = mockMvc.perform(request);

        actions.andExpect(status().isOk());
        actions.andExpect(handler().handlerType(ArrayController.class));
        actions.andExpect(handler().methodName("strings"));

        actions.andExpect(model().attribute("values", instanceOf(String[].class)));
        actions.andExpect(model().attribute("stringValues", instanceOf(String.class)));
        actions.andExpect(model().size(2));
        actions.andExpect(view().name("viewer-array"));

        MvcResult result = actions.andReturn();
        ModelAndView mv = result.getModelAndView();
        ModelMap map = mv.getModelMap();
        String[] mvValues = (String[]) map.get("values");
        String[] expect = {"1", "2", "3", "4", "5", "6"};
        Assert.assertArrayEquals(mvValues, expect);
        String intValues = (String) map.get("stringValues");
        Assert.assertEquals(Arrays.toString(expect), intValues);
    }

    @Test
    @Ignore
    public void testArrayIntsWithJson() throws Exception {}
}