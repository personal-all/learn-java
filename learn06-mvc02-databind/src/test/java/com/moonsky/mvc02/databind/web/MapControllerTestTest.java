package com.moonsky.mvc02.databind.web;

import com.moonsky.mvc02.databind.vo.BaseMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.ModelMap;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * @author benshaoye
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring-mvc.xml")
@WebAppConfiguration
public class MapControllerTestTest {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    @Test
    public void testStringDoubleMap() throws Exception {
        MockHttpServletRequestBuilder request = get("/map");

        request.param("stringDoubleMap['first']", "20");
        request.param("stringDoubleMap['second']", "30");

        ResultActions actions = mockMvc.perform(request);

        actions.andExpect(status().isOk());
        actions.andExpect(view().name("viewer-map"));

        actions.andExpect(model().attribute("container", instanceOf(BaseMap.class)));

        MvcResult result = actions.andReturn();
        ModelAndView mv = result.getModelAndView();
        ModelMap modelMap = mv.getModelMap();
        BaseMap container = (BaseMap) modelMap.get("container");

        Map<String, Double> stringDoubleMap = container.getStringDoubleMap();
        assertNotNull(stringDoubleMap);
        assertEquals(2, stringDoubleMap.size());
        assertEquals(Double.valueOf(20), stringDoubleMap.get("first"));
        assertEquals(Double.valueOf(30), stringDoubleMap.get("second"));
    }

    @Test
    public void testStringIntMap() throws Exception {
        MockHttpServletRequestBuilder request = get("/map");
        request.param("stringIntMap['first']", "20");
        request.param("stringIntMap['second']", "30");

        ResultActions actions = mockMvc.perform(request);

        actions.andExpect(status().isOk());
        actions.andExpect(view().name("viewer-map"));

        actions.andExpect(model().attribute("container", instanceOf(BaseMap.class)));

        MvcResult result = actions.andReturn();
        ModelAndView mv = result.getModelAndView();
        ModelMap modelMap = mv.getModelMap();
        BaseMap container = (BaseMap) modelMap.get("container");

        Map<String, Integer> map = container.getStringIntMap();
        assertNotNull(map);
        assertEquals(2, map.size());
        assertEquals(Integer.valueOf(20), map.get("first"));
        assertEquals(Integer.valueOf(30), map.get("second"));
    }

    @Test
    public void testStringLongMap() throws Exception {
        MockHttpServletRequestBuilder request = get("/map");

        request.param("stringLongMap['first']", "20");
        request.param("stringLongMap['second']", "30");

        ResultActions actions = mockMvc.perform(request);

        actions.andExpect(status().isOk());
        actions.andExpect(view().name("viewer-map"));

        actions.andExpect(model().attribute("container", instanceOf(BaseMap.class)));

        MvcResult result = actions.andReturn();
        ModelAndView mv = result.getModelAndView();
        ModelMap modelMap = mv.getModelMap();
        BaseMap container = (BaseMap) modelMap.get("container");

        Map<String, Long> map = container.getStringLongMap();
        assertNotNull(map);
        assertEquals(2, map.size());
        assertEquals(Long.valueOf(20), map.get("first"));
        assertEquals(Long.valueOf(30), map.get("second"));
    }

    @Test
    public void testLongDoubleMap() throws Exception {
        MockHttpServletRequestBuilder request = get("/map");

        request.param("longDoubleMap[30]", "300");
        request.param("longDoubleMap[40]", "400");

        ResultActions actions = mockMvc.perform(request);
        actions.andExpect(status().isOk());
        actions.andExpect(view().name("viewer-map"));
        actions.andExpect(model().attribute("container", instanceOf(BaseMap.class)));

        MvcResult result = actions.andReturn();
        ModelAndView mv = result.getModelAndView();
        ModelMap modelMap = mv.getModelMap();
        BaseMap container = (BaseMap) modelMap.get("container");

        Map<Long, Double> longDoubleMap = container.getLongDoubleMap();
        assertNotNull(longDoubleMap);
        assertEquals(2, longDoubleMap.size());
        assertEquals(Double.valueOf(300), longDoubleMap.get(Long.valueOf(30)));
        assertEquals(Double.valueOf(400), longDoubleMap.get(Long.valueOf(40)));
    }

    @Test
    public void testDoubleDateMap() throws Exception {
        MockHttpServletRequestBuilder request = get("/map");

        Date now = new Date();
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:dd");
        String dateStr = formatter.format(now);
        request.param("doubleDateMap[30]", dateStr);
        request.param("doubleDateMap[40]", dateStr);

        request.param("doubleDateArrMap[30]", dateStr, dateStr, dateStr);
        request.param("doubleDateArrMap[40]", dateStr, dateStr, dateStr, dateStr);

        ResultActions actions = mockMvc.perform(request);
        actions.andExpect(status().isOk());
        actions.andExpect(view().name("viewer-map"));
        actions.andExpect(model().attribute("container", instanceOf(BaseMap.class)));

        MvcResult result = actions.andReturn();
        ModelAndView mv = result.getModelAndView();
        ModelMap modelMap = mv.getModelMap();
        BaseMap container = (BaseMap) modelMap.get("container");

        Map<Double, Date> doubleDateMap = container.getDoubleDateMap();
        assertNotNull(doubleDateMap);
        assertEquals(2, doubleDateMap.size());

        Date date = doubleDateMap.get(Double.valueOf(30));
        assertEquals(formatter.format(date), dateStr);

        Map<Double, Date[]> doubleDateArrMap = container.getDoubleDateArrMap();
        assertNotNull(doubleDateArrMap);
        assertEquals(2, doubleDateArrMap.size());

        assertTrue((doubleDateArrMap.get(Double.valueOf(30)) instanceof Date[]));
        Date[] dateArr = doubleDateArrMap.get(Double.valueOf(30));
        assertTrue(dateArr.length == 3);

        for (int i = 0; i < 3; i++) {
            assertEquals(formatter.format(dateArr[i]), dateStr);
        }

        assertTrue((doubleDateArrMap.get(Double.valueOf(40)) instanceof Date[]));
        dateArr = doubleDateArrMap.get(Double.valueOf(40));
        assertTrue(dateArr.length == 4);

        for (int i = 0; i < 4; i++) {
            assertEquals(formatter.format(dateArr[i]), dateStr);
        }
    }

    @Test
    public void testLongTimestampMap() throws Exception {
        MockHttpServletRequestBuilder request = get("/map");

        Date now = new Date();
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:dd");
        String dateStr = formatter.format(now);

        request.param("longTimestampMap[30]", dateStr);
        request.param("longTimestampMap[40]", dateStr);

        request.param("longTimestampListMap[30]", dateStr, dateStr, dateStr);
        request.param("longTimestampListMap[40]", dateStr, dateStr, dateStr, dateStr);

        ResultActions actions = mockMvc.perform(request);
        actions.andExpect(status().isOk());
        actions.andExpect(view().name("viewer-map"));
        actions.andExpect(model().attribute("container", instanceOf(BaseMap.class)));

        MvcResult result = actions.andReturn();
        ModelAndView mv = result.getModelAndView();
        ModelMap modelMap = mv.getModelMap();
        BaseMap container = (BaseMap) modelMap.get("container");

        Map<Long, Timestamp> doubleDateMap = container.getLongTimestampMap();
        assertNotNull(doubleDateMap);
        assertEquals(2, doubleDateMap.size());

        Timestamp date = doubleDateMap.get(Long.valueOf(30));
        assertEquals(formatter.format(date), dateStr);

        date = doubleDateMap.get(Long.valueOf(40));
        assertEquals(formatter.format(date), dateStr);

        Map<Long, List<Timestamp>> longTimestampListMap = container.getLongTimestampListMap();
        assertNotNull(longTimestampListMap);
        assertEquals(2, longTimestampListMap.size());

        assertTrue((longTimestampListMap.get(Long.valueOf(30)) instanceof List));
        List<Timestamp> dateArr = longTimestampListMap.get(Long.valueOf(30));
        assertTrue(dateArr.size() == 3);
        for (int i = 0; i < 3; i++) {
            assertEquals(formatter.format(dateArr.get(i)), dateStr);
        }

        assertTrue((longTimestampListMap.get(Long.valueOf(40)) instanceof List));
        dateArr = longTimestampListMap.get(Long.valueOf(40));
        assertTrue(dateArr.size() == 4);
        for (int i = 0; i < 4; i++) {
            assertEquals(formatter.format(dateArr.get(i)), dateStr);
        }
    }
}