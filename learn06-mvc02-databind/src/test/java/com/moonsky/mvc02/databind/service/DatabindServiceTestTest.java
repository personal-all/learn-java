package com.moonsky.mvc02.databind.service;

import com.moonsky.mvc02.databind.entity.Author;
import com.moonsky.mvc02.databind.entity.Book;
import com.moonsky.mvc02.databind.entity.Reader;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.*;

/**
 * @author benshaoye
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring-mvc.xml")
public class DatabindServiceTestTest {

    @Autowired
    private DatabindService databindService;

    private Book aBook(String authorName, int sex, int age, String bookName, int price, int count) {
        Author author = new Author(authorName, sex, age);
        Book book = new Book(bookName, price, count, author);
        return book;
    }

    @Test
    public void testSave() {
        Book[] readed = {
            aBook("三毛", 0, 38, "春天的故事", 23, 100), aBook("四毛", 1, 28, "Java 变成", 56, 89),
        };


        Book[] willBuyArr = {
            aBook("五毛", 0, 34, "守望者", 56, 87), aBook("六毛", 1, 25, "007大战秦始皇", 1, 2000),
        };
        List<Book> willBuy = new ArrayList<>(Arrays.asList(willBuyArr));


        Book[] willReadArr = {
            aBook("七毛", 0, 34, "守望者（续集）", 56, 87), aBook("八毛", 1, 25, "007大战秦始皇（二）", 1, 2000),
        };
        Set<Book> willRead = new HashSet<>(Arrays.asList(willReadArr));

        Reader reader = new Reader("春天", "北京", "本科", readed, willRead, willBuy);

        databindService.save(reader);

        List<Reader> allReader = databindService.findAll();

        Assert.assertTrue(allReader.size() == 1);

        // 模拟数据库的情况如此，真实数据库应该用相等判断
        Assert.assertTrue(allReader.get(0) == reader);

        Reader presentReader = allReader.get(0);
        Assert.assertThat(presentReader, CoreMatchers.instanceOf(Reader.class));
        Assert.assertThat(presentReader.getName(), CoreMatchers.is("春天"));
    }

    @Test
    public void testSave1() {
    }

    @Test
    public void testFindAll() {
    }
}