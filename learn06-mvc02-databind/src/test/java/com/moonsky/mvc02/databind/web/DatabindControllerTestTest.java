package com.moonsky.mvc02.databind.web;

import com.moonsky.mvc02.databind.entity.Author;
import com.moonsky.mvc02.databind.entity.Book;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * @author benshaoye
 */
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring-mvc.xml")
public class DatabindControllerTestTest {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    @Test
    public void testAddAuthor() throws Exception {
        MockHttpServletRequestBuilder builder = get("/add/author");
        builder.param("name", "张三").param("sex", "0").param("age", "36");
        ResultActions actions = mockMvc.perform(builder);

        actions.andExpect(status().isOk());

        actions.andExpect(handler().handlerType(DatabindController.class));
        actions.andExpect(handler().methodName("addAuthor"));
        actions.andExpect(model().attributeDoesNotExist("name", "sex", "age"));
        actions.andExpect(model().attributeExists("authors"));
        actions.andExpect(model().attribute("authors", instanceOf(List.class)));
        actions.andExpect(view().name("author-show"));

        ModelAndView mv = actions.andReturn().getModelAndView();
        List<Author> authors = (List<Author>) mv.getModelMap().get("authors");

        assertThat(authors, CoreMatchers.notNullValue());
        assertThat(authors.size(), CoreMatchers.is(1));

        Author author = authors.get(0);
        assertThat(author.getSex(), CoreMatchers.is(0));
        assertThat(author.getAge(), CoreMatchers.is(36));
        assertThat(author.getName(), CoreMatchers.is("张三"));
    }

    @Test
    public void testView() throws Exception {
        MockHttpServletRequestBuilder builder = get("/view/addSet");
        ResultActions actions = mockMvc.perform(builder);
        actions.andExpect(MockMvcResultMatchers.status().isOk());
        actions.andExpect(MockMvcResultMatchers.model().size(0));
        actions.andExpect(MockMvcResultMatchers.view().name("addSet"));

        builder = get("/page/addSet");
        actions = mockMvc.perform(builder);
        actions.andExpect(MockMvcResultMatchers.status().isOk());
        actions.andExpect(MockMvcResultMatchers.model().size(0));
        actions.andExpect(MockMvcResultMatchers.view().name("addSet"));
    }

    @Test
    public void testAddBook() throws Exception {
        MockHttpServletRequestBuilder builder = get("/add/book");
        builder.param("name", "语文");
        builder.param("price", "23");
        builder.param("count", "34");
        builder.param("author.name", "中国教育部");
        builder.param("author.sex", "3");
        builder.param("author.age", "70");
        builder.param("map.date", "2019");

        ResultActions actions = mockMvc.perform(builder);

        actions.andExpect(MockMvcResultMatchers.status().isOk());

        actions.andExpect(model().attribute("book", CoreMatchers.instanceOf(Book.class)));
        actions.andExpect(model().attribute("books", CoreMatchers.instanceOf(List.class)));

        ModelAndView mv = actions.andReturn().getModelAndView();
        Book book = (Book) mv.getModelMap().get("book");

        assertThat(book, CoreMatchers.notNullValue());

        assertThat(book.getName(), CoreMatchers.is("语文"));
        assertThat(book.getPrice(), CoreMatchers.is(23));
        assertThat(book.getCount(), CoreMatchers.is(34));

        Author author = book.getAuthor();
        assertThat(author, CoreMatchers.notNullValue());
        assertThat(author.getName(), CoreMatchers.is("中国教育部"));
        assertThat(author.getSex(), CoreMatchers.is(3));
        assertThat(author.getAge(), CoreMatchers.is(70));

        Map<String, Integer> map = book.getMap();
        assertThat(map, CoreMatchers.notNullValue());
    }
}