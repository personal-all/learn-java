package com.moonsky.mvc02.databind.web;

import com.moonsky.mvc02.databind.entity.Author;
import com.moonsky.mvc02.databind.entity.User;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * @author benshaoye
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring-mvc.xml")
@WebAppConfiguration
public class InitBinderControllerTestTest {

    @Autowired
    private WebApplicationContext context;
    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    @Test
    public void testSameProperty() throws Exception {
        testSamePropertyBase();
        testSamePropertyNamespace();
    }

    public void testSamePropertyBase() throws Exception {
        String name = "张三";
        Integer age = 24;
        Integer sex = 1;
        MockHttpServletRequestBuilder builder = get("/init/same-prop");
        builder.param("name", name);
        builder.param("age", age.toString());
        builder.param("sex", sex.toString());
        ResultActions actions = mockMvc.perform(builder);

        User targetUser = new User(name, sex, age);
        Author targetAuthor = new Author(name, sex, age);

        actions.andExpect(status().isOk());
        actions.andExpect(model().attribute("user", CoreMatchers.is(targetUser)));
        actions.andExpect(model().attribute("author", CoreMatchers.is(targetAuthor)));

        actions.andExpect(view().name("viewer-init-prop"));
    }

    public void testSamePropertyNamespace() throws Exception {
        String userName = "张三";
        String authorName = "李四";
        Integer age = 24;
        Integer sex = 1;
        MockHttpServletRequestBuilder builder = get("/init/same-prop");
        builder.param("user.name", userName);
        builder.param("author.name", authorName);
        builder.param("age", age.toString());
        builder.param("sex", sex.toString());
        ResultActions actions = mockMvc.perform(builder);

        User targetUser = new User(userName, sex, age);
        Author targetAuthor = new Author(authorName, sex, age);

        actions.andExpect(status().isOk());
        actions.andExpect(model().attribute("user", CoreMatchers.is(targetUser)));
        actions.andExpect(model().attribute("author", CoreMatchers.is(targetAuthor)));

        actions.andExpect(view().name("viewer-init-prop"));
    }

    @Test
    public void testProperty() throws Exception {
    }
}