#### 基础示例

1. 创建一个 Event 数据类；
> 自定义一个 Event（如：RequestEvent、订单等），
2. 实现 Event 工厂接口（EventFactory），创建&lt;空的&gt;数据对象；（如：RequestEventFactory）
3. 创建事件监听类，实现 Event 处理器接口（EventHandler），用于处理数据对象
4. 实例化 Disruptor 实例，配置相应参数;
5. 生产数据

#### Disruptor 核心原理

#### 深入 RingBuffer 数据结构

#### Disruptor 核心：RingBuffer、Disruptor

#### Disruptor 核心：Sequence、Sequence Barrier

#### Disruptor 核心：WaitStrategy 等待策略及选择

#### Disruptor 核心：Event、EventProcessor 核心线程

#### Disruptor 核心：EventHandler 事件（消费者）处理器

#### Disruptor 核心：WorkProcessor 核心工作器