package com.moonsky.multi.disruptor.quickstart;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Date;

/**
 * @author benshaoye
 */
class RunnerTestTest {

    @ParameterizedTest
    @ValueSource(ints = {100,200,500,1000,2000,5000,10000})
    void testDefaultExecuteTask(int count) throws Exception {
        long start = new Date().getTime();
        Runner.executorTask(count);
        long last = new Date().getTime();
        System.out.println(String.format("Duration: %d ms", (last - start)));
    }
}