package com.moonsky.multi.disruptor.quickstart;

import com.lmax.disruptor.BlockingWaitStrategy;
import com.lmax.disruptor.EventFactory;
import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.WaitStrategy;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import static java.lang.Runtime.getRuntime;
import static java.util.concurrent.Executors.newFixedThreadPool;

/**
 * @author benshaoye
 */
public class Runner {

    public static void executorTask(int count) {

        RequestEventFactory factory = new RequestEventFactory();
        int ringBufferSize = 1024 * 1024;
        ExecutorService service = newFixedThreadPool(getRuntime().availableProcessors());

        /*
         * 创建 disruptor 对象
         */

        /**
         * 参考由线程工厂创建的构造器
         *
         * @see Disruptor#Disruptor(EventFactory, int, ThreadFactory)
         * @see Disruptor#Disruptor(EventFactory, int, ThreadFactory, ProducerType, WaitStrategy) 基本
         *
         * @see Disruptor#Disruptor(RingBuffer, Executor)
         * @see RingBuffer#create(ProducerType, EventFactory, int, WaitStrategy)
         * @see RingBuffer#createMultiProducer(EventFactory, int)
         * @see RingBuffer#createMultiProducer(EventFactory, int, WaitStrategy)
         * @see RingBuffer#createSingleProducer(EventFactory, int)
         * @see RingBuffer#createSingleProducer(EventFactory, int, WaitStrategy)
         *
         * @see Executors#defaultThreadFactory()
         * @see Executors#privilegedThreadFactory()
         */
        Disruptor<RequestEvent> disruptor = new Disruptor<RequestEvent>(
            // 1. 事件工厂
            factory,
            // 2. 容器大小
            ringBufferSize,
            // 3. 线程池
            service,
            // 4. 单生产者 / 多生产者模型
            ProducerType.SINGLE,
            // 等待策略
            new BlockingWaitStrategy());

        /*
         * 添加消费者监听器，即事件处理器
         */

        disruptor.handleEventsWith(new RequestEventHandler());

        /*
         * 启动
         */

        disruptor.start();

        /*
         * 生产数据
         */

        // 核心数据容器
        RingBuffer<RequestEvent> buffer = disruptor.getRingBuffer();

        RequestEventProducer producer = new RequestEventProducer(buffer);
        for (int i = 0; i < count; i++) {
            /**
             * Disruptor 的核心就是如何生产数据填入数据到 RingBuffer 中
             * @see Disruptor#Disruptor(RingBuffer, Executor)
             */
            producer.createEvent(i);
        }

        disruptor.shutdown();
        service.shutdown();
    }
}
