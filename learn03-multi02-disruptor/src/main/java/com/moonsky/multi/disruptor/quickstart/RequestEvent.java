package com.moonsky.multi.disruptor.quickstart;

import java.util.Objects;

/**
 * @author benshaoye
 */
public class RequestEvent {

    private String value;

    public RequestEvent() {}

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }
        RequestEvent that = (RequestEvent) o;
        return Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return value == null ? 0 : value.hashCode();
    }

    @Override
    public String toString() {
        return value;
    }
}
