package com.moonsky.multi.disruptor.quickstart;

import com.lmax.disruptor.RingBuffer;

/**
 * @author benshaoye
 */
public class RequestEventProducer {

    private RingBuffer<RequestEvent> buffer;

    public RequestEventProducer(RingBuffer<RequestEvent> buffer) {
        this.buffer = buffer;
    }

    /**
     * 步骤：
     * 1. 生产消息的时候，先从 ringBuffer 获取一个可用序号
     *
     * @param value
     */
    public void createEvent(long value) {


        long sequence = buffer.next();
        // 2. 根据序号找到具体 RequestEvent 对象，此时是一个空对象（属性为空）
        // 即刚由 EventFactory 生产出来的对象
        RequestEvent event = buffer.get(sequence);

        // 3. 填充数据
        event.setValue(String.valueOf(value));

        // 4. 提交发布数据，只有发布的数据才能被消费
        buffer.publish(sequence);
    }

    private void defaultCreate(long value) {
        long sequence = buffer.next();
        RequestEvent event = buffer.get(sequence);
        event.setValue(String.valueOf(value));
        buffer.publish(sequence);
    }

    /**
     * 官方推荐，使用 try 代码块，并把{@link RingBuffer#publish(long)}放进 finally 中
     *
     * @param value
     */
    private void useTryCreate(long value) {
        long sequence = buffer.next();
        try {
            RequestEvent event = buffer.get(sequence);
            event.setValue(String.valueOf(value));
        } finally {
            buffer.publish(sequence);
        }
    }
}
