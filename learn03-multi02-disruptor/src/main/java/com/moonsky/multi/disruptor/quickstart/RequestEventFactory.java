package com.moonsky.multi.disruptor.quickstart;

import com.lmax.disruptor.EventFactory;

/**
 * @author benshaoye
 */
public class RequestEventFactory implements EventFactory<RequestEvent> {

    public RequestEventFactory() {}

    @Override
    public RequestEvent newInstance() {
        // 返回一个空的消息数据对象
        return new RequestEvent();
    }
}
