package com.moonsky.multi.disruptor.quickstart;

import com.lmax.disruptor.EventHandler;

/**
 * @author benshaoye
 */
public class RequestEventHandler implements EventHandler<RequestEvent> {

    public RequestEventHandler() {}

    @Override
    public void onEvent(RequestEvent event, long sequence, boolean endOfBatch) throws Exception {
        // 处理消费逻辑
        System.out.println("消费数据：" + event);
    }
}
