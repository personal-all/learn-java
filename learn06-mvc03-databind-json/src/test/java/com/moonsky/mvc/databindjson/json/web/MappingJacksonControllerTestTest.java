package com.moonsky.mvc.databindjson.json.web;

import com.alibaba.fastjson.JSON;
import com.moonsky.mvc.databindjson.json.vo.ResumeVO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author benshaoye
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration("classpath:spring-mvc-bind-json.xml")
@WebAppConfiguration
class MappingJacksonControllerTestTest {

    @Autowired
    private WebApplicationContext context;
    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    @Test
    void testRequestFirst() throws Exception {

        ResumeVO resume = new ResumeVO();
        resume.setAge(24);
        resume.setName("benshaoye");


        MockHttpServletRequestBuilder builder = get("/mapping/jackson/first");
        builder.contentType("text/html;charset=UTF-8");

        builder.param("resume", JSON.toJSONString(resume));

        ResultActions actions = mockMvc.perform(builder);

        actions.andExpect(status().isOk());
        actions.andDo(MockMvcResultHandlers.print());
    }
}