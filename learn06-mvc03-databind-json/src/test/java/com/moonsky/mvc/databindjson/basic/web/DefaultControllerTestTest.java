package com.moonsky.mvc.databindjson.basic.web;

import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author benshaoye
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration("classpath:spring-mvc.xml")
@WebAppConfiguration
class DefaultControllerTestTest {

    @Autowired
    private WebApplicationContext context;
    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    @Test
    void testIndex() throws Exception {
        ResultActions actions = mockMvc.perform(post("/default/index"));

        actions.andExpect(status().isOk());
        actions.andExpect(jsonPath("$.username", CoreMatchers.is("张三")));
        actions.andExpect(jsonPath("$.password", CoreMatchers.is("少爷")));
    }

    @Test
    void testDatabind() throws Exception {
        String username = "天下无贼", password = "123456";
        int age = 20, sex = 20;
        MockHttpServletRequestBuilder request = post("/default/databind");
        request.param("username", username).param("password", password);
        request.param("age", String.valueOf(age)).param("sex", String.valueOf(sex));
        ResultActions actions = mockMvc.perform(request);

        actions.andExpect(status().isOk());
        actions.andExpect(jsonPath("$.username", CoreMatchers.is(username)));
        actions.andExpect(jsonPath("$.password", CoreMatchers.is(password)));
    }

    @Test
    void testOther() throws Exception {
        String username = "111111", password = "123456";
        int age = 20, sex = 20;
        MockHttpServletRequestBuilder request = post("/default/other/" + username);
        request.param("username", username).param("password", password);
        request.param("age", String.valueOf(age)).param("sex", String.valueOf(sex));

        request.header("password", password);

        ResultActions actions = mockMvc.perform(request);

        actions.andExpect(status().isOk());
        actions.andExpect(jsonPath("$.username", CoreMatchers.is(username)));
        actions.andExpect(jsonPath("$.password", CoreMatchers.is(password)));
        actions.andDo(MockMvcResultHandlers.print());
    }
}