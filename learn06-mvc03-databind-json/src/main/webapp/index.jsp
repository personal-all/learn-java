<%--
  Created by IntelliJ IDEA.
  User: ZhangDongMin
  Date: 2019/9/29
  Time: 11:32
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Title</title>
  <style>
    .inline-label {
      display: inline-block;
      width: 120px;
    }
  </style>
</head>
<body>
<div>
  <span class="inline-label"></span>
  <a href="/default/index">/default/index</a>
</div>
<div>
  <span class="inline-label"></span>
  <a href="/default/databind?username=benshaoye&password=benshaoye&age=24&sex=2">
    /default/databind?username=benshaoye&password=benshaoye&age=24&sex=2
  </a>
</div>
<div>
  <span class="inline-label"></span>
  <a href="/default/json-bind?username=benshaoye&password=benshaoye&age=24&sex=2">
    /default/json-bind?username=benshaoye&password=benshaoye&age=24&sex=2
  </a>
</div>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script>
  setTimeout(function () {
    const instance = axios.create({
      headers: {
        ContentType: 'application/json;charset=UTF-8',
      }
    });

    instance.get('/mapping/jackson/first', {
      data: {
        resume: {
          name: 'zhangsan',
          age: 25
        }
      }
    }).then(res => {
      console.log(res);
    });
  });
</script>
</body>
</html>
