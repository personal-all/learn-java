package com.moonsky.mvc.databindjson.basic.web;

import com.moonsky.mvc.databindjson.basic.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author benshaoye
 */
@Controller
@RequestMapping("default")
public class DefaultController {

    /**
     * /default/index
     *
     * @return
     */
    @RequestMapping("index")
    @ResponseBody
    public Object index() {
        User user = new User();
        user.setAge(25);
        user.setSex(1);
        user.setUsername("张三");
        user.setPassword("少爷");
        return user;
    }

    /**
     * /default/databind?username=benshaoye&password=benshaoye&age=24&sex=2
     *
     * @param user
     *
     * @return
     */
    @RequestMapping("databind")
    @ResponseBody
    public Object databind(User user) {
        return user;
    }

    @RequestMapping("other/{username}")
    @ResponseBody
    public Object other(
        @PathVariable("username") String username,
        @RequestHeader("password") String password) {
        User user = new User();
        user.setAge(25);
        user.setSex(1);
        user.setUsername(username);
        user.setPassword(password);
        return user;
    }
}
