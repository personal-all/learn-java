package com.moonsky.mvc.databindjson.basic.base;

/**
 * @author benshaoye
 */
public interface DataConverter<F, T> {

    T from(F f);

    F to();
}
