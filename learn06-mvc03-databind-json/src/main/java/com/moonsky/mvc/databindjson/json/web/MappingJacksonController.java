package com.moonsky.mvc.databindjson.json.web;

import com.alibaba.fastjson.JSON;
import com.moonsky.mvc.databindjson.json.vo.ResumeVO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author benshaoye
 */
@Controller
@RequestMapping("mapping/jackson")
public class MappingJacksonController {

    public MappingJacksonController() {
    }

    @ResponseBody
    @RequestMapping("first")
    public Object first(@RequestBody ResumeVO resume) {

        return JSON.parse(JSON.toJSONString(resume));
    }
}
