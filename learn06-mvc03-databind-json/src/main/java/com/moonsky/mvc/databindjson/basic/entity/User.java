package com.moonsky.mvc.databindjson.basic.entity;

/**
 * @author benshaoye
 */
public class User {

    private String username;
    private String password;
    private int age;
    private int sex;

    public User() {
    }

    public User(String username, String password, int age, int sex) {
        this.username = username;
        this.password = password;
        this.age = age;
        this.sex = sex;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }
}
