package com.moonsky.mvc.databindjson.json.vo;

/**
 * @author benshaoye
 */
public class ResumeVO {

    private String name;

    private Integer age;

    public ResumeVO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
