package com.moonsky.auth.shiro;

import com.moon.core.util.Console;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.UnauthorizedException;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.realm.SimpleAccountRealm;
import org.apache.shiro.subject.Subject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static com.moon.core.lang.StackTraceUtil.getCallerTrace;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author benshaoye
 */
@DisplayName("角色认证")
public class ShiroRoleAuthTest extends BaseShiroTest {

    private final static String ADMIN = "admin";
    private final static String CUSTOMER = "customer";

    void initializeAccountAndRoles() {

        SimpleAccountRealm realm = new SimpleAccountRealm();
        // 用户名 + 密码 + 【角色列表】
        realm.addAccount(USERNAME, PASSWORD, ADMIN, CUSTOMER);

        /**
         *  1. 构建{@link SecurityManager}环境
         */
        DefaultSecurityManager defaultManager = new DefaultSecurityManager();
        defaultManager.setRealm(realm);

        // 绑定 securityManager
        SecurityUtils.setSecurityManager(defaultManager);

        Console.out.println(getCallerTrace());
    }

    @BeforeEach
    void setUp() {
        initializeAccountAndRoles();
    }

    @Disabled
    @DisplayName("角色授权通过")
    @ParameterizedTest
    @CsvSource({ADMIN, CUSTOMER})
    void testRole01Valid(String role) throws Exception {

        Subject subject = SecurityUtils.getSubject();

        UsernamePasswordToken token = new UsernamePasswordToken(USERNAME, PASSWORD);

        subject.login(token);

        assertTrue(subject.isAuthenticated());
        subject.checkRole(role);
    }

    @DisplayName("角色授权不通过")
    @ParameterizedTest
    @CsvSource({ADMIN + "1", CUSTOMER + "0"})
    void testRole02Invalid(String role) throws Exception {

        Subject subject = SecurityUtils.getSubject();

        UsernamePasswordToken token = new UsernamePasswordToken(USERNAME, PASSWORD, role);

        subject.login(token);
        assertTrue(subject.isAuthenticated());

        assertThrows(UnauthorizedException.class, () -> {
            try {
                subject.checkRole(role);
            } catch (Exception e) {
                Console.out.println(e);
                throw e;
            }
        });
    }

}
