package com.moonsky.auth.shiro;

import com.moon.core.util.Console;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.CredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.realm.SimpleAccountRealm;
import org.apache.shiro.subject.Subject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static com.moon.core.lang.StackTraceUtil.getCallerTrace;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author benshaoye
 */
@DisplayName("基本账号密码测试")
class ShiroAccountAuthTest extends BaseShiroTest {

    void initializeAccount() {
        /**
         *  1. 构建{@link SecurityManager}环境
         */
        DefaultSecurityManager defaultManager = new DefaultSecurityManager();
        SimpleAccountRealm realm = new SimpleAccountRealm();
        // 用户名 + 密码 + 【角色列表】
        realm.addAccount(USERNAME, PASSWORD);

        defaultManager.setRealm(realm);

        // 绑定 securityManager
        SecurityUtils.setSecurityManager(defaultManager);

        Console.out.println(getCallerTrace());
    }

    @BeforeEach
    void setUp() {
        initializeAccount();
    }

    /**
     * 正确数据测试
     *
     * @throws Exception
     */
    @DisplayName("正确数据")
    @Test
    void testAuthValid() throws Exception {
        /**
         * 2. 得到默认主体
         */
        Subject subject = SecurityUtils.getSubject();
        /**
         * 主体提交认证请求
         */
        UsernamePasswordToken token = new UsernamePasswordToken(USERNAME, PASSWORD);
        subject.login(token);

        // 测试
        assertTrue(subject.isAuthenticated());
    }

    /**
     * 账号错误
     *
     * @throws Exception
     */
    @DisplayName("账号错误")
    @Test
    void testAuthInvalidWithUsername() throws Exception {

        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(USERNAME + "1", PASSWORD);
        assertThrows(UnknownAccountException.class, () -> {
            try {
                subject.login(token);
            } catch (Exception e) {
                Console.out.println(e);
                throw e;
            }
        });
        assertFalse(subject.isAuthenticated());
    }

    /**
     * 密码错误
     *
     * @throws Exception
     */
    @DisplayName("密码错误")
    @Test
    void testAuthInvalidWithPassword() throws Exception {

        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(USERNAME, PASSWORD + "1");
        assertThrows(CredentialsException.class, () -> {
            try {
                subject.login(token);
            } catch (Exception e) {
                Console.out.println(e);
                throw e;
            }
        });

        assertFalse(subject.isAuthenticated());
    }
}