package com.moonsky.auth.shiro;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.realm.text.IniRealm;
import org.apache.shiro.subject.Subject;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @author benshaoye
 */
@DisplayName("IniRealm 测试")
public class ShiroIniRealmTest {

    @Disabled
    @Test
    @DisplayName("权限验证")
    void testName() throws Exception {
        IniRealm realm = new IniRealm("classpath:user.ini");

        DefaultSecurityManager defaultManager = new DefaultSecurityManager();
        defaultManager.setRealm(realm);

        SecurityUtils.setSecurityManager(defaultManager);

        Subject subject = SecurityUtils.getSubject();

        UsernamePasswordToken token = new UsernamePasswordToken("mark", "123456");
        subject.login(token);
        subject.checkRole("admin");
        subject.checkRole("customer");
        subject.checkPermission("user:update");
        subject.checkPermission("user:delete");
        assertThrows(Exception.class, () -> subject.checkPermission("user:create"));
    }
}
