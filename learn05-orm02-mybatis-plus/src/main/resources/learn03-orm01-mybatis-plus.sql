/**
  这是学习本节课所需要的数据库信息

  命名和书写规范：
  1、所有 MySQL SQL 语句关键字大写；
  2、所有定义的表名、列名等全小写；
  3、单词间用下滑线分割；
 */

-- 1、创建数据库
-- CREATE SCHEMA learn_orm_mybatis_plus DEFAULT CHARACTER SET utf8;

-- 2、创建数据表
DROP TABLE IF EXISTS tb_only_use_for_test_user;
CREATE TABLE `tb_only_use_for_test_user`
    (`id`          INT(8)       NOT NULL AUTO_INCREMENT,
     `name`        VARCHAR(64)  NULL,
     `cert_no`     VARCHAR(32)  NULL,
     `birthday`    DATETIME     NULL,
     `sex`         VARCHAR(10)  NULL,
     `age`         INT(10)      NULL,
     `address`     VARCHAR(255) NULL,
     `create_time` DATETIME     NULL,
     PRIMARY KEY (`id`)
    ) ENGINE = INNODB
      CHARSET = utf8;

DROP TABLE IF EXISTS tb_only_use_for_test_company;
CREATE TABLE `tb_only_use_for_test_company`
    (`id`             INT(8)      NOT NULL AUTO_INCREMENT,
     `company_name`   VARCHAR(64) NULL,
     `address`        VARCHAR(64) NULL,
     `create_date`    DATETIME    NULL DEFAULT now(),
     `contact_name`   VARCHAR(32) NULL,
     `contact_mobile` VARCHAR(18) NULL,
     PRIMARY KEY (`id`)
    ) ENGINE = INNODB
      CHARSET = utf8;

DROP TABLE IF EXISTS tb_only_use_for_test_logic_delete;
CREATE TABLE `tb_only_use_for_test_logic_delete`
    (`id`             INT(8)      NOT NULL AUTO_INCREMENT,
     `city_name`      VARCHAR(64) NULL,
     `create_time`    DATETIME    NULL DEFAULT now(),
     `contact_name`   VARCHAR(32) NULL,
     `contact_mobile` VARCHAR(18) NULL,
     `logic_deleted`  VARCHAR(32) NULL,
     PRIMARY KEY (`id`)
    ) ENGINE = INNODB
      CHARSET = utf8;

DROP TABLE IF EXISTS tb_only_use_for_test_dict;
CREATE TABLE `tb_only_use_for_test_dict`
    (`id`        INT(8)      NOT NULL AUTO_INCREMENT,
     `dict_name` VARCHAR(64) NULL,
     `version`   INT(8) NULL,
     PRIMARY KEY (`id`)
    ) ENGINE = INNODB
      CHARSET = utf8;