package com.moonsky.mybatis.plus.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.moonsky.mybatis.plus.entity.UserEntity;
import com.moonsky.mybatis.plus.mapper.UserMapper;
import com.moonsky.mybatis.plus.service.UserService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

/**
 * @author benshaoye
 */
@Service
@Primary
public class UserServiceImpl extends ServiceImpl<UserMapper, UserEntity> implements UserService {}
