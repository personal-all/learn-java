package com.moonsky.mybatis.plus.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author benshaoye
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("tb_only_use_for_test_dict")
public class DictEntity {

    private String id;

    private String dictName;
    /**
     * 乐观锁
     * <p>
     * 官方文档：https://mp.baomidou.com/guide/optimistic-locker-plugin.html
     */
    @Version
    private Integer version;
}
