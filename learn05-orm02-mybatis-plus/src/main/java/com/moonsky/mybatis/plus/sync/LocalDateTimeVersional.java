package com.moonsky.mybatis.plus.sync;

import java.time.LocalDateTime;

/**
 * @author benshaoye
 */
public class LocalDateTimeVersional implements Versional<LocalDateTime> {

    @Override
    public LocalDateTime initializeValue() {
        return LocalDateTime.now();
    }

    @Override
    public LocalDateTime next(LocalDateTime previousVersion) {
        return LocalDateTime.now();
    }
}
