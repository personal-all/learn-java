package com.moonsky.mybatis.plus.sync;

import java.time.LocalTime;

/**
 * @author benshaoye
 */
public class LocalTimeVersional implements Versional<LocalTime> {

    @Override
    public LocalTime initializeValue() { return LocalTime.now(); }

    @Override
    public LocalTime next(LocalTime previousVersion) {
        return LocalTime.now();
    }
}
