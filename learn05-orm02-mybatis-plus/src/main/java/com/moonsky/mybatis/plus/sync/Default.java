package com.moonsky.mybatis.plus.sync;

import java.io.Serializable;

/**
 * @author benshaoye
 */
final class Default implements Versional {

    @Override
    public Serializable initializeValue() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Serializable next(Serializable previousVersion) {
        throw new UnsupportedOperationException();
    }
}
