package com.moonsky.mybatis.plus.sync;

/**
 * @author benshaoye
 */
public class LongVersional implements Versional<Long> {

    @Override
    public Long initializeValue() { return 0L; }

    @Override
    public Long next(Long previousVersion) {
        return previousVersion + 1;
    }
}
