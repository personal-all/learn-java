package com.moonsky.mybatis.plus.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.moonsky.mybatis.plus.entity.UserEntity;

import java.util.List;
import java.util.Map;

/**
 * @author benshaoye
 */
public interface UserMapper extends BaseMapper<UserEntity> {
}
