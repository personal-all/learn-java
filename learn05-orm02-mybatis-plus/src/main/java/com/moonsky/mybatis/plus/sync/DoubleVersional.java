package com.moonsky.mybatis.plus.sync;

/**
 * @author benshaoye
 */
public class DoubleVersional implements Versional<Double> {

    @Override
    public Double initializeValue() { return 0D; }

    @Override
    public Double next(Double previousVersion) {
        return previousVersion + 1;
    }
}
