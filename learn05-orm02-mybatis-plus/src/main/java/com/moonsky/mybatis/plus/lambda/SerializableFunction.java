package com.moonsky.mybatis.plus.lambda;

import java.io.Serializable;
import java.util.function.Function;

/**
 * @author benshaoye
 */
public interface SerializableFunction<T, R> extends Function<T, R>, Serializable {}
