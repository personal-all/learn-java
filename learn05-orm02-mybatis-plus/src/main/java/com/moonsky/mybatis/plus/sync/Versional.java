package com.moonsky.mybatis.plus.sync;

import java.io.Serializable;

/**
 * @author benshaoye
 */
public interface Versional<T extends Serializable> {

    /**
     * 初始值
     * <p>
     * 在 insert 语句中，当乐观锁版本为 null 时自动调用
     *
     * @return
     */
    T initializeValue();

    /**
     * 下一版本号
     *
     * @param previousVersion
     *
     * @return
     */
    T next(T previousVersion);
}
