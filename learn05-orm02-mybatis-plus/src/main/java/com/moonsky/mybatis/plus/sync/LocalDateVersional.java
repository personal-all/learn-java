package com.moonsky.mybatis.plus.sync;

import java.time.LocalDate;

/**
 * @author benshaoye
 */
public class LocalDateVersional implements Versional<LocalDate> {

    @Override
    public LocalDate initializeValue() { return LocalDate.now(); }

    @Override
    public LocalDate next(LocalDate previousVersion) {
        return LocalDate.now();
    }
}
