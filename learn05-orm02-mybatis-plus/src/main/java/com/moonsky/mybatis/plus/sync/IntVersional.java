package com.moonsky.mybatis.plus.sync;

/**
 * @author benshaoye
 */
public class IntVersional implements Versional<Integer> {

    @Override
    public Integer initializeValue() { return 0; }

    @Override
    public Integer next(Integer previousVersion) {
        return previousVersion + 1;
    }
}
