package com.moonsky.mybatis.plus.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import static java.time.format.DateTimeFormatter.ofPattern;

/**
 * @author benshaoye
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("tb_only_use_for_test_logic_delete")
public class LogicDeleteEntity {

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private transient final DateTimeFormatter formatter = ofPattern("yyyy-MM-dd HH:mm:ss");
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private transient final DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private String id;
    private String cityName;
    private String contactName;
    @TableField(fill = FieldFill.INSERT)
    private String contactMobile;
    /**
     * 逻辑删除初始值
     * <p>
     * 按道理，不应该这样设置，但是框架好像不支持，待确认中
     */
    @TableLogic
    private String logicDeleted = "normal";
    private Date createTime;

    @ToString.Include(name = "createTimeText")
    public String getCreateTimeText() {
        return createTime == null ? null : format.format(createTime);
    }
}
