package com.moonsky.mybatis.plus.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author benshaoye
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("tb_only_use_for_test_user")
public class UserEntity {

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private transient final DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @TableId(type = IdType.AUTO)
    private String id;
    private String name;
    @TableField(condition = SqlCondition.LIKE)
    private String certNo;
    private Date birthday;
    private String sex;
    /**
     * 自定义如小于、大于、大于等于、小于等于
     * - @TableField(condition = "%s&lt;#{%s}")
     */
    private Integer age;
    private String address;
    private Date createTime;

    public UserEntity(
        String name, String certNo, Date birthday, String sex, int age, String address, Date createTime
    ) {
        this.name = name;
        this.certNo = certNo;
        this.birthday = birthday;
        this.sex = sex;
        this.age = age;
        this.address = address;
        this.createTime = createTime;
    }

    @ToString.Include(name = "birthdayText")
    public String getBirthdayText() {
        return birthday == null ? null : formatter.format(birthday);
    }

    @ToString.Include(name = "createTimeText")
    public String getCreateTimeText() {
        return createTime == null ? null : formatter.format(createTime);
    }
}
