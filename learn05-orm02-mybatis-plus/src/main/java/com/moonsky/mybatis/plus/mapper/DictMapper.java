package com.moonsky.mybatis.plus.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.moonsky.mybatis.plus.entity.DictEntity;

/**
 * @author benshaoye
 */
public interface DictMapper extends BaseMapper<DictEntity> {}
