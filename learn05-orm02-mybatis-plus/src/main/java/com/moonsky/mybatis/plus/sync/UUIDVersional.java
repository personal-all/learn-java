package com.moonsky.mybatis.plus.sync;

import java.util.UUID;

/**
 * @author benshaoye
 */
public class UUIDVersional implements Versional<String> {

    @Override
    public String initializeValue() {
        return UUID.randomUUID().toString();
    }

    @Override
    public String next(String previousVersion) {
        return UUID.randomUUID().toString();
    }
}
