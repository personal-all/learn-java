package com.moonsky.mybatis.plus.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.moonsky.mybatis.plus.entity.CompanyEntity;

/**
 * @author benshaoye
 */
public interface CompanyMapper extends BaseMapper<CompanyEntity> {}
