package com.moonsky.mybatis.plus.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author benshaoye
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("tb_only_use_for_test_company")
public class CompanyEntity extends Model<CompanyEntity> {

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private transient final DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @TableId(type = IdType.AUTO)
    private String id;
    private String companyName;
    private String address;
    private Date createDate;
    private String contactName;
    private String contactMobile;

    @ToString.Include(name = "createDateText")
    public String getCreateDateText() {
        return createDate == null ? null : formatter.format(createDate);
    }
}
