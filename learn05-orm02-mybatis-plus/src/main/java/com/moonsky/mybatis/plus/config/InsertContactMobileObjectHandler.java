package com.moonsky.mybatis.plus.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import static org.springframework.util.StringUtils.isEmpty;

/**
 * @author benshaoye
 */
@Component
public class InsertContactMobileObjectHandler implements MetaObjectHandler {

    final static String fieldName = "contactMobile";

    @Override
    public void insertFill(MetaObject meta) {
        System.out.println(">>> Sign in insertFill");
        if (meta.hasSetter(fieldName) && isEmpty(meta.getValue(fieldName))) {
            System.out.println(">>> Execution insertFill");
            setInsertFieldValByName(fieldName, "13485796248", meta);
            // meta.setValue(fieldName, "13485796248");
        }
    }

    @Override
    public void updateFill(MetaObject metaObject) {
    }
}
