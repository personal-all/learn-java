package com.moonsky.mybatis.plus.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.moonsky.mybatis.plus.entity.LogicDeleteEntity;

import java.util.List;

/**
 * @author benshaoye
 */
public interface LogicDeleteMapper extends BaseMapper<LogicDeleteEntity> {

    default List<LogicDeleteEntity> selectAll() { return selectList(null); }
}
