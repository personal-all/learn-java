package com.moonsky.mybatis.plus.sync;

import java.math.BigDecimal;

/**
 * @author benshaoye
 */
public class BigDecimalVersional implements Versional<BigDecimal> {

    @Override
    public BigDecimal initializeValue() { return BigDecimal.ZERO; }

    @Override
    public BigDecimal next(BigDecimal previousVersion) {
        return previousVersion.add(BigDecimal.ONE);
    }
}
