package com.moonsky.mybatis.plus.sync;

import java.util.Date;

/**
 * @author benshaoye
 */
public class DateVersional implements Versional<Date> {

    @Override
    public Date initializeValue() { return new Date(); }

    @Override
    public Date next(Date previousVersion) {
        return new Date();
    }
}
