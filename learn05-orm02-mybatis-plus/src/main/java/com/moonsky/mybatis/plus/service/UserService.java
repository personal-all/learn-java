package com.moonsky.mybatis.plus.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.moonsky.mybatis.plus.entity.UserEntity;

/**
 * @author benshaoye
 */
public interface UserService extends IService<UserEntity> {}
