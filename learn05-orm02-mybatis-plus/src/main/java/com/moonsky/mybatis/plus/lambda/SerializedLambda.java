package com.moonsky.mybatis.plus.lambda;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 欺骗 jvm，完全复制自{@link java.lang.invoke.SerializedLambda}
 * 类名必须一致、序列号UID必须一致、字段类型必须一致
 * 方法、构造函数无要求
 *
 * @author benshaoye
 * @see java.lang.invoke.SerializedLambda
 */
@SuppressWarnings("unused")
public class SerializedLambda implements Serializable {

    private static final long serialVersionUID = 8025925345765570181L;

    private Class<?> capturingClass;
    private String functionalInterfaceClass;
    private String functionalInterfaceMethodName;
    private String functionalInterfaceMethodSignature;
    private String implClass;
    private String implMethodName;
    private String implMethodSignature;
    private int implMethodKind;
    private String instantiatedMethodType;
    private Object[] capturedArgs;

    private SerializedLambda() { }

    public static byte[] serialize(Object object) throws IOException {
        if (object == null) {
            return null;
        }
        ByteArrayOutputStream stream = new ByteArrayOutputStream(1024);
        ObjectOutputStream oos = new ObjectOutputStream(stream);
        oos.writeObject(object);
        oos.flush();
        return stream.toByteArray();
    }

    static class InnerObjInStream extends ObjectInputStream {

        public InnerObjInStream(byte[] in) throws IOException {
            super(new ByteArrayInputStream(in));
        }

        @Override
        protected Class<?> resolveClass(ObjectStreamClass desc) throws IOException, ClassNotFoundException {
            Class<?> clazz = super.resolveClass(desc);
            return clazz == java.lang.invoke.SerializedLambda.class ? SerializedLambda.class : clazz;
        }
    }

    public static SerializedLambda load(SerializableFunction<?, ?> lambda) {
        if (!lambda.getClass().isSynthetic()) {
            throw new IllegalArgumentException("只能是 lambda 表达式");
        }
        try (ObjectInputStream objIn = new InnerObjInStream(serialize(lambda))) {
            return (SerializedLambda) objIn.readObject();
        } catch (ClassNotFoundException | IOException e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        }
    }

    /**
     * 获取接口 class
     *
     * @return 返回 class 名称
     */
    public String getFunctionalInterfaceClassName() {
        return toClassName(functionalInterfaceClass);
    }

    /**
     * 获取实现的 class
     *
     * @return 实现类
     */
    public Class<?> getImplClass() {
        return forName(getImplClassName());
    }

    /**
     * 获取 class 的名称
     *
     * @return 类名
     */
    public String getImplClassName() { return toClassName(implClass); }

    /**
     * 获取实现者的方法名称
     *
     * @return 方法名称
     */
    public String getImplMethodName() {
        return implMethodName;
    }

    /**
     * 正常化类名称，将类名称中的 / 替换为 .
     *
     * @param name 名称
     *
     * @return 正常的类名
     */
    private String toClassName(String name) { return name.replace('/', '.'); }

    private static final Pattern INSTANTIATED_METHOD_TYPE = Pattern.compile(
        "\\(L(?<instantiatedMethodType>[\\S&&[^;)]]+);\\)L[\\S]+;");

    public Class getInstantiatedMethodType() {
        Matcher matcher = INSTANTIATED_METHOD_TYPE.matcher(instantiatedMethodType);
        if (matcher.find()) {
            return forName(toClassName(matcher.group("instantiatedMethodType")));
        }
        throw new IllegalArgumentException("无法从 " + instantiatedMethodType + " 解析调用实例。。。");
    }

    public static Class<?> forName(String name) {
        try {
            return Class.forName(name);
        } catch (ClassNotFoundException e) {
            throw new IllegalArgumentException("找不到指定类：" + name, e);
        }
    }

    /**
     * @return 字符串形式
     */
    @Override
    public String toString() {
        return String.format("%s -> %s::%s",
            getFunctionalInterfaceClassName(),
            getImplClass().getSimpleName(),
            implMethodName);
    }

}
