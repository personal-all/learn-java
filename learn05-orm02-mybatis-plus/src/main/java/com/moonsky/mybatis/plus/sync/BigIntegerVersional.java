package com.moonsky.mybatis.plus.sync;

import java.math.BigInteger;

/**
 * @author benshaoye
 */
public class BigIntegerVersional implements Versional<BigInteger> {

    @Override
    public BigInteger initializeValue() { return BigInteger.ZERO; }

    @Override
    public BigInteger next(BigInteger previousVersion) {
        return previousVersion.add(BigInteger.ONE);
    }
}
