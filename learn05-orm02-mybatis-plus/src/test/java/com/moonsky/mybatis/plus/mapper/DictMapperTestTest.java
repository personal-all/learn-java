package com.moonsky.mybatis.plus.mapper;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.moonsky.mybatis.plus.Data;
import com.moonsky.mybatis.plus.entity.DictEntity;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author benshaoye
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration("classpath:mybatis-plus-config.xml")
@WebAppConfiguration
class DictMapperTestTest {

    @Autowired
    private DictMapper dictMapper;

    @BeforeEach
    void setUp() {
        DictEntity dict = new DictEntity();
        dict.setDictName("职位名称");
        dict.setVersion(1);
        dictMapper.insert(dict);
    }

    @AfterEach
    void tearDown() {
        dictMapper.selectList(null).forEach(user -> dictMapper.deleteById(user.getId()));
    }


    @Test
    void testSelectList() throws Exception {
        List<DictEntity> dicts = dictMapper.selectList(null);

        Data.require(dicts, 1);

        DictEntity dict = dictMapper.selectOne(Wrappers.<DictEntity>lambdaQuery().eq(DictEntity::getId, "1"));
        int beforeVersion = dict.getVersion();
        dict.setDictName("职位级别");
        dictMapper.updateById(dict);
        dict = dictMapper.selectOne(Wrappers.<DictEntity>lambdaQuery().eq(DictEntity::getId, "1"));
        int afterVersion = dict.getVersion();
        assertEquals(beforeVersion + 1, afterVersion);
    }

    @Test
    void testUnVersionInsertThenUpdate() throws Exception {
        String name = "学历";
        DictEntity dict = new DictEntity();
        dict.setDictName(name);
        dictMapper.insert(dict);

        DictEntity dbDict = dictMapper.selectOne(Wrappers.<DictEntity>lambdaQuery().eq(DictEntity::getDictName, name));

        System.out.println(dict);
        System.out.println(dbDict);
        name = "最低学历";
        dbDict.setDictName(name);
        dbDict.setVersion(0);
        assertNotEquals(1, dictMapper.updateById(dbDict));

        DictEntity secondDict = dictMapper.selectOne(Wrappers.<DictEntity>lambdaQuery().eq(DictEntity::getDictName,
            name));
        assertNull(secondDict);
    }
}
