package com.moonsky.mybatis.plus.mapper;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.moonsky.mybatis.plus.Data;
import com.moonsky.mybatis.plus.entity.LogicDeleteEntity;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * @author benshaoye
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration("classpath:mybatis-plus-config.xml")
@WebAppConfiguration
class LogicDeleteMapperTestTest {

    @Autowired
    private LogicDeleteMapper deleteMapper;

    @BeforeEach
    void setUp() {
        LogicDeleteEntity delete = new LogicDeleteEntity();
        delete.setCityName("北京");
        delete.setContactMobile("13224343523");
        delete.setContactName("谷嘉诚");
        delete.setCreateTime(new Date());
        deleteMapper.insert(delete);

        delete = new LogicDeleteEntity();
        delete.setCityName("天津");
        // delete.setContactMobile("13848759214");
        delete.setContactName("李荣旭");
        delete.setCreateTime(new Date());
        deleteMapper.insert(delete);
    }

    @AfterEach
    void tearDown() {
        final String append = "OR logic_deleted IS NULL";
        LambdaQueryWrapper<LogicDeleteEntity> select = Wrappers.lambdaQuery();
        select.last(append);
        deleteMapper.selectList(select).forEach(delete -> {
            LambdaUpdateWrapper<LogicDeleteEntity> update = Wrappers.lambdaUpdate();
            update.eq(LogicDeleteEntity::getId, delete.getId()).last(append);
            deleteMapper.delete(update);
        });
    }

    @Test
    void testSelectAll() throws Exception {
        List<LogicDeleteEntity> entities = deleteMapper.selectAll();
        assertNotNull(entities);
        Data.require(entities, 2);

        LogicDeleteEntity delete = entities.get(0);
        deleteMapper.deleteById(delete.getId());

        entities = deleteMapper.selectAll();
        assertNotNull(entities);
        Data.require(entities, 1);
    }
}