package com.moonsky.mybatis.plus.mapper;

import com.moonsky.mybatis.plus.entity.CompanyEntity;
import org.joda.time.DateTime;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * @author benshaoye
 */
@DisplayName("ActiveRecord")
@ExtendWith(SpringExtension.class)
@ContextConfiguration("classpath:mybatis-plus-config.xml")
@WebAppConfiguration
class CompanyMapperTestTest {

    @BeforeEach
    void setUp() {
        CompanyEntity company = new CompanyEntity();
        company.setCompanyName("南方国际租赁有限公司");
        company.setAddress("你猜我在哪儿");
        company.setContactName("熊作伟");
        company.setContactMobile("13556482015");

        company.insert();

        company = new CompanyEntity();
        company.setCompanyName("横琴黑水磐石基金管理有限责任公司");
        company.setAddress("你再猜我在哪儿");
        company.setContactName("杨丽");
        company.setContactMobile("1851374868");
        company.setCreateDate(DateTime.now().minusYears(1).toDate());

        company.insert();
    }

    @AfterEach
    void tearDown() {
        CompanyEntity company = new CompanyEntity();
        company.selectAll().forEach(CompanyEntity::deleteById);
    }

    @Test
    void testInsert() throws Exception {
        CompanyEntity company = new CompanyEntity();
        List<CompanyEntity> companies = company.selectAll();
        assertNotNull(companies);
        assertEquals(2, companies.size());
        companies.forEach(System.out::println);
    }
}