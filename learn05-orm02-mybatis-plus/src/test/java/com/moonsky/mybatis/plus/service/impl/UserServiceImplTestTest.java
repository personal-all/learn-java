package com.moonsky.mybatis.plus.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.moonsky.mybatis.plus.Data;
import com.moonsky.mybatis.plus.entity.UserEntity;
import com.moonsky.mybatis.plus.mapper.UserMapper;
import com.moonsky.mybatis.plus.service.UserService;
import org.joda.time.DateTime;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author benshaoye
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration("classpath:mybatis-plus-config.xml")
@WebAppConfiguration
class UserServiceImplTestTest {

    @Autowired
    private WebApplicationContext context;
    @Autowired
    private UserMapper mapper;
    @Autowired
    private UserService userService;

    private Date createTime;
    private Date createTimeNext;

    private List<UserEntity> setUpInsertData;

    private LambdaQueryWrapper<UserEntity> lambdaWrapper;
    private QueryWrapper<UserEntity> userWrapper;
    private List<UserEntity> userList;

    @BeforeEach
    void setUp() {
        this.createTime = new Date();
        setUpInsertData = Data.createList(createTime);
        setUpInsertData.forEach(mapper::insert);
        createTimeNext = new DateTime(createTime.getTime()).plusSeconds(5).toDate();
        lambdaWrapper = Wrappers.lambdaQuery();
        userWrapper = Wrappers.query();
    }

    @AfterEach
    void tearDown() {
        mapper.selectList(null).forEach(user -> mapper.deleteById(user.getId()));
    }

    @Test
    @DisplayName("getOne")
    void testGetOnt() throws Exception {
        lambdaWrapper.like(UserEntity::getCertNo, "197");
        assertThrows(Exception.class, () -> {
            userService.getOne(lambdaWrapper);
        });

        UserEntity user = userService.getOne(lambdaWrapper, false);
        assertNotNull(user);
    }

    @Test
    @DisplayName("lambdaQuery")
    void testLambdaQuery() throws Exception {
        userList = userService.lambdaQuery().list();
        assertNotNull(userList);
        assertEquals(4, userList.size());
        userList.forEach(System.out::println);

        userList = userService.lambdaQuery()
            .select(UserEntity::getAge, UserEntity::getAddress, UserEntity::getCertNo)
            .like(UserEntity::getAddress, "凉山")
            .list();
        assertNotNull(userList);
        assertEquals(2, userList.size());
        userList.forEach(System.out::println);
    }
}