package com.moonsky.mybatis.plus.lambda;

import com.moonsky.mybatis.plus.entity.UserEntity;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertSame;

/**
 * @author benshaoye
 */
class SerializeFunctionTestTest {

    void doParse(List list) {
        SerializableFunction<UserEntity, Object> serializeFunction = UserEntity::getAge;
        SerializedLambda lambda = SerializedLambda.load(serializeFunction);

        System.out.println(lambda);
        System.out.println(lambda.getImplClass());
        System.out.println(lambda.getInstantiatedMethodType());
        System.out.println(lambda.getImplMethodName());

        list.add(serializeFunction);
    }

    @Test
    void testParseFieldFromLambda() throws Exception {
        List list = new ArrayList();
        final int max = 10;
        for (int i = 0; i < max; i++) {
            doParse(list);
            System.out.println("=====================================================");
        }

        for (int i = 0; i < max; i++) {
            Object outer = list.get(i);
            for (int idx = i + 1; idx < max; idx++) {
                Object inner = list.get(idx);
                assertSame(outer, inner);
                assertSame(outer.getClass(), inner.getClass());
            }
        }
    }

    void doParse0(List list, Object obj) {
        list.add((Supplier) () -> obj);
    }

    /**
     * 包含变量的 lambda 每次返回不同对象，不包含变量的返回同一对象
     *
     * @throws Exception
     */
    @Test
    void testVarLambda() throws Exception {
        List list = new ArrayList();
        final int max = 10;
        for (int i = 0; i < max; i++) {
            doParse0(list, max);
        }

        for (int i = 0; i < max; i++) {
            Object outer = list.get(i);
            for (int idx = i + 1; idx < max; idx++) {
                Object inner = list.get(idx);
                assertNotSame(outer, inner);
                assertSame(outer.getClass(), inner.getClass());
            }
        }
    }
}