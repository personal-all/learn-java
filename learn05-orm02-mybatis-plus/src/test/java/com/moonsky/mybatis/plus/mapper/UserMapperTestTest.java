package com.moonsky.mybatis.plus.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.interfaces.Compare;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moonsky.mybatis.plus.Data;
import com.moonsky.mybatis.plus.entity.UserEntity;
import org.joda.time.DateTime;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author benshaoye
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration("classpath:mybatis-plus-config.xml")
@WebAppConfiguration
class UserMapperTestTest {

    @Autowired
    private UserMapper mapper;

    private Date createTime;
    private Date createTimeNext;

    private List<UserEntity> setUpInsertData;

    private LambdaQueryWrapper<UserEntity> lambdaWrapper;
    private QueryWrapper<UserEntity> userWrapper;
    private List<UserEntity> userList;

    @BeforeEach
    void setUp() {
        this.createTime = new Date();
        setUpInsertData = Data.createList(createTime);
        setUpInsertData.forEach(mapper::insert);
        createTimeNext = new DateTime(createTime.getTime()).plusSeconds(5).toDate();
        lambdaWrapper = Wrappers.lambdaQuery();
        userWrapper = Wrappers.query();
    }

    @AfterEach
    void tearDown() {
        mapper.selectList(null).forEach(user -> mapper.deleteById(user.getId()));
    }

    @Test
    @DisplayName("selectWithGroupBy")
    void testGroupBy() {
        userWrapper.select("sum(age) ageSum, sex").groupBy("sex");
        List<Object> objects = mapper.selectObjs(userWrapper);
        Data.require(objects, 2);
        List<Map<String, Object>> maps = mapper.selectMaps(userWrapper);
        Data.require(maps, 2);
    }

    @Test
    @DisplayName("selectPage")
    void testSelectPage() {
        int age = Data.defaultAge();

        Page<UserEntity> page = new Page<>();
        page.setCurrent(1);
        page.setSize(3);
        lambdaWrapper.select(UserEntity::getId, UserEntity::getAddress).eq(UserEntity::getAge, age);

        IPage<UserEntity> paged = mapper.selectPage(page, lambdaWrapper);
        System.out.println(page);
        System.out.println(paged);
        assertSame(page, paged);

        userList = page.getRecords();
        Data.require(userList, 1);

        page = new Page<>(1, 2);
        paged = mapper.selectPage(page, Wrappers.query());
        System.out.println(page);
        System.out.println(paged);
        assertSame(page, paged);

        userList = page.getRecords();
        Data.require(userList, 2);
    }

    /**
     * {@link Compare#allEq(Map, boolean)}如果第二个参数为 false，将忽略 null 值查询条件
     */
    @Test
    @DisplayName("selectWithWrappedAllEq")
    void testSelectAllEq() {
        int age = Data.defaultAge();

        Map<String, Object> params = new HashMap<>();
        params.put("sex", "男");
        params.put("age", age);
        userWrapper.allEq(params);

        userList = mapper.selectList(userWrapper);
        Data.require(userList, 1);

        params.put("age", null);

        userWrapper.allEq(params, false);
        userList = mapper.selectList(userWrapper);
        Data.require(userList, 1);
    }

    @Test
    @DisplayName("updateById")
    void testUpdateById() throws Exception {
        userList = mapper.selectList(null);
        assertNotNull(userList);
        assertEquals(4, userList.size());

        UserEntity user = userList.get(0);
        String id = user.getId();

        UserEntity update = new UserEntity();
        update.setId(id);
        update.setAge(59);
        mapper.updateById(update);

        UserEntity updated = mapper.selectById(id);
        assertEquals(id, updated.getId());
        assertEquals(updated.getAge(), update.getAge());
        assertEquals(updated.getCreateTime(), user.getCreateTime());
        assertNotEquals(updated.getAge(), user.getAge());
    }

    /**
     * {@link BaseMapper#selectOne(Wrapper)}要求查询结果集最多只能有一条数据
     *
     * @throws Exception
     */
    @Test
    @DisplayName("selectOne")
    void testSelectOne() throws Exception {
        int age = Data.defaultAge();

        lambdaWrapper.select(UserEntity::getId, UserEntity::getAddress, UserEntity::getCertNo)
            .lt(UserEntity::getAge, age);

        // 这里返回一个集合，但 selectOne 最多只能返回一条数据，故应该抛出异常
        assertThrows(Exception.class, () -> {
            UserEntity user = mapper.selectOne(lambdaWrapper);
            System.out.println(user);
        });

        lambdaWrapper = Wrappers.lambdaQuery();
        lambdaWrapper.select(UserEntity::getId, UserEntity::getAddress, UserEntity::getCertNo)
            .eq(UserEntity::getAge, age - 1);
        // 这里应该能正确返回一条 null 的数据
        assertDoesNotThrow(() -> {
            UserEntity user = mapper.selectOne(lambdaWrapper);
            assertNull(user);
        });

        lambdaWrapper = Wrappers.lambdaQuery();
        lambdaWrapper.select(UserEntity::getId, UserEntity::getAddress, UserEntity::getCertNo)
            .eq(UserEntity::getAge, age);
        assertDoesNotThrow(() -> {
            UserEntity user = mapper.selectOne(lambdaWrapper);
            assertNotNull(user);
        });
    }

    @Test
    @DisplayName("selectCount")
    void testSelectCount() throws Exception {
        int age = Data.defaultAge();

        lambdaWrapper.eq(UserEntity::getAge, age);
        assertEquals(1, mapper.selectCount(lambdaWrapper));

        assertEquals(4, mapper.selectCount(null));
    }

    /**
     * {@link BaseMapper#selectMaps(Wrapper)}这个方法的返回值避免了当查询列少于实体字段数时，
     * 非查询字段的值都为 null，null 太多了看着糟心
     *
     * @throws Exception
     */
    @Test
    @Timeout(3)
    @DisplayName("selectMaps")
    void testSelectMaps() throws Exception {
        int age = Data.defaultAge();

        lambdaWrapper.select(UserEntity::getId, UserEntity::getAddress, UserEntity::getCertNo)
            .eq(UserEntity::getAge, age);

        List<Map<String, Object>> maps = mapper.selectMaps(lambdaWrapper);
        Data.require(maps, 1);
        userList = mapper.selectList(lambdaWrapper);
        Data.require(userList, 1);

        /**
         * {@link QueryWrapper#select(String...)} 这里是数据库对应的列名
         * 而不是实体对应的字段名
         */
        userWrapper.select("name", "cert_no", "sex", "address");
        maps = mapper.selectMaps(userWrapper);
        Data.require(maps, 4);
        maps.forEach(map -> map.values().forEach(value -> {
            assertTrue(value instanceof String);
        }));
    }

    @Test
    @DisplayName("selectByEntity")
    void testSelectMapByEntity() {
        List<UserEntity> list = Data.createList();
        UserEntity user = new UserEntity();
        user.setSex(list.get(0).getSex());
        user.setCertNo("197");

        userWrapper.setEntity(user);

        userWrapper.select(field -> field.getPropertyType() == String.class);

        mapper.selectMaps(userWrapper).forEach(map -> {
            map.forEach((key, value) -> {
                if ("id".equals(key)) {
                    assertTrue(value instanceof Integer);
                } else {
                    assertTrue(value instanceof String);
                }
            });
            System.out.println(map);
        });
    }

    /**
     * {@link BaseMapper#selectObjs(Wrapper)}方法只会将第一列数据返回
     *
     * @throws Exception
     */
    @Test
    @DisplayName("selectObjs")
    void testSelectByObjs() throws Exception {
        int age = Data.defaultAge();

        lambdaWrapper.select(UserEntity::getId, UserEntity::getAddress).eq(UserEntity::getAge, age);

        List<Object> list = mapper.selectObjs(lambdaWrapper);
        Data.require(list, 1);
        list.forEach(item -> assertTrue(item instanceof Integer));
    }

    /**
     * 要求 Map 的 key 为列名，value 为值，查询的时候采用 '=' 而不是 like
     *
     * @throws Exception
     */
    @Test
    @DisplayName("selectByMap")
    void testSelectByMap() throws Exception {
        int age = Data.defaultAge();
        List<UserEntity> userList;

        Map<String, Object> columnMap = new HashMap<>();
        columnMap.put("age", age);
        userList = mapper.selectByMap(columnMap);
        Data.require(userList, 1);
    }

    @Test
    @DisplayName("selectList")
    void testSelectList() {
        assertNotNull(mapper);

        List<UserEntity> userList = mapper.selectList(null);

        assertEquals(4, userList.size());

        userList.forEach(System.out::println);
    }

    @Test
    @DisplayName("insert & selectList")
    void testInsert() {
        UserEntity user = Data.create("明大哥", "152224198908049211", "北京雍和宫");
        mapper.insert(user);

        List<UserEntity> userList = mapper.selectList(null);
        assertEquals(5, userList.size());
        userList.forEach(System.out::println);
    }

    @Test
    @DisplayName("selectList & Wrapper")
    void testQueryMapper() {
        int age = Data.defaultAge() - 1;

        QueryWrapper<UserEntity> userWrapper;
        List<UserEntity> userList;
        Data.out();

        userWrapper = Wrappers.<UserEntity>query().le("age", age);
        userWrapper.le("create_time", createTimeNext);
        userList = mapper.selectList(userWrapper);
        assertEquals(2, userList.size());
        userList.forEach(System.out::println);

        Data.out();

        userWrapper = Wrappers.<UserEntity>query().le(false, "age", age);
        userWrapper.le("create_time", createTimeNext);
        userList = mapper.selectList(userWrapper);
        assertEquals(4, userList.size());
        userList.forEach(System.out::println);
    }

    @Test
    @DisplayName("selectList & Lambda Wrapper")
    void testLambdaQueryMapper() throws Exception {
        int age = Data.defaultAge() - 1;

        LambdaQueryWrapper<UserEntity> userWrapper;
        List<UserEntity> userList;

        userWrapper = Wrappers.lambdaQuery();
        userWrapper.ge(UserEntity::getAge, age);
        userList = mapper.selectList(userWrapper);
        assertEquals(2, userList.size());
        userList.forEach(System.out::println);

        Data.out();

        userWrapper = Wrappers.lambdaQuery();
        userWrapper.ge(UserEntity::getAge, age).likeRight(UserEntity::getAddress, "凉山");
        userList = mapper.selectList(userWrapper);
        assertEquals(0, userList.size());
        userList.forEach(System.out::println);

        Data.out();

        userWrapper = Wrappers.lambdaQuery();
        userWrapper.ge(UserEntity::getAge, age).likeLeft(UserEntity::getAddress, "凉山");
        userList = mapper.selectList(userWrapper);
        assertEquals(1, userList.size());
        userList.forEach(System.out::println);
    }
}