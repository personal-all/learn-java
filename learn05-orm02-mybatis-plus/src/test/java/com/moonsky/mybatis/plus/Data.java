package com.moonsky.mybatis.plus;

import com.moonsky.mybatis.plus.entity.UserEntity;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static java.lang.Integer.valueOf;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author benshaoye
 */
public class Data {
    public static <T> void require(List<T> list, int size) {
        assertEquals(size, list.size());
        out(list);
    }

    public  static <T> void out(List<T> list) {
        list.forEach(System.out::println);
        out();
    }

    public final static String line;

     static {
        char[] chars = new char[350];
        Arrays.fill(chars, '=');
        line = ">> " + new String(chars) + " <<\n";
    }

    public  static void out() {
        System.out.println(line);
    }

    void sleep() {
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public  final static Date birthday(String certNo) {
        String day = certNo.substring(6, 14);
        int year = valueOf(day.substring(0, 4));
        int month = valueOf(day.substring(4, 6));
        int date = valueOf(day.substring(6, 8));
        DateTime dateTime = new DateTime(year, month, date, 0, 0);
        return dateTime.toDate();
    }

    public  final static String sex(String certNo) {
        Character ch = certNo.charAt(16);
        return Integer.valueOf(ch.toString()) % 2 == 0 ? "女" : "男";
    }

    public  final static int age(String certNo) {
        return DateTime.now().getYear() - valueOf(certNo.substring(6, 10));
    }

    public  final static UserEntity create(String name, String certNo, String address) {
        return create(name, certNo, address, new Date());
    }

    public  final static UserEntity create(String name, String certNo, String address, Date createTime) {
        return new UserEntity(name, certNo, birthday(certNo), sex(certNo), age(certNo), address, createTime);
    }

    public  final static List<UserEntity> createList() {
        return createList(new Date());
    }

    public  final static List<UserEntity> createList(Date createTime) {
        List<UserEntity> users = new ArrayList<>();
        users.add(create("张诗琪", "235407195106112745", "天津和平门", createTime));
        users.add(create("顾雅欣", "210203197503102721", "天津和平门", createTime));
        users.add(create("叶敏艳", "511502199103223189", "四川凉山", createTime));
        users.add(create("狄莉珍", "230822197201256034", "四川凉山", createTime));
        return users;
    }

    public  final static int defaultAge() {
        String certNo = "230822197201256034";
        int age = age(certNo);
        System.out.println(String.format(">> 测试身份证号：%s， 年龄：%d", certNo, age));
        out();
        return age;
    }
}
