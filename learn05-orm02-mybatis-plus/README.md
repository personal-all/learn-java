## MyBatis Plus 使用说明：
配置文档：[https://gitee.com/744531854/learn-java/blob/master/learn05-orm02-mybatis-plus/src/main/resources/mybatis-plus-config.xml](https://gitee.com/744531854/learn-java/blob/master/learn05-orm02-mybatis-plus/src/main/resources/mybatis-plus-config.xml)
### 安装：
在 Maven 中引入依赖
```xml
<dependency>
  <groupId>com.baomidou</groupId>
  <artifactId>mybatis-plus</artifactId>
  <!-- 需要注意各版本之间的差异 -->
  <version>最新版本</version>
</dependency>
```
##### 一些各版本之间的差异
1. v3.2.0 之后不内置支持性能监控，SQL 打印分析需要依赖第三方包
> p6spy 配置文件模板：[https://gitee.com/744531854/learn-java/blob/master/learn05-orm02-mybatis-plus/src/main/resources/spy.properties](https://gitee.com/744531854/learn-java/blob/master/learn05-orm02-mybatis-plus/src/main/resources/spy.properties)
<br>MyBatis-Plus 官方：[https://mp.baomidou.com/guide/p6spy.html](https://mp.baomidou.com/guide/p6spy.html)
```xml
<dependency>
  <groupId>p6spy</groupId>
  <artifactId>p6spy</artifactId>
  <version>3.8.6</version>
</dependency>
```

### 注意点：
1. 在 Spring 中，乐观锁要以插件形式插件 OptimisticLockerInterceptor 要注册到 MybatisSqlSessionFactoryBean 的
plugins 中；
> 文档：[https://mp.baomidou.com/guide/optimistic-locker-plugin.html](https://mp.baomidou.com/guide/optimistic-locker-plugin.html)
<br>乐观锁支持的数据类型有：int、Integer、long、Long、Date、Timestamp、LocalDateTime
<br>乐观锁的初始值仍然需要手动设置

2. 物理分页插件也要注册到 MybatisSqlSessionFactoryBean 的 plugins 中；
3. 逻辑删除功能，在 insert 的时候，逻辑字段需要手动设置默认值；
4. mybatisx 插件
> 打开 IDEA：File -> Settings -> Plugins -> Browse Repositories
<br>
搜索并安装：mybatisx