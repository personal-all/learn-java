package com.moonsky.mvc02.simple.annotation;

import org.hamcrest.CoreMatchers;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * @author benshaoye
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring-mvc.xml")
// web 项目用 mock 测试的话，需要加这个注解，否则无法注入 WebApplicationContext
@WebAppConfiguration
public class AnnotationControllerTestTest {

    @Autowired
    private WebApplicationContext context;

    @Test
    public void testMv() throws Exception {
        MockMvc mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        ResultActions actions = mockMvc.perform(get("/mv"));

        actions.andExpect(MockMvcResultMatchers.status().isOk());

        actions.andExpect(model().attribute("name", "Jerry"));

        actions.andExpect(model().attributeDoesNotExist("age", "sex"));
        actions.andExpect(model().attribute("name", CoreMatchers.is("Jerry")));
        actions.andExpect(model().size(1));
        actions.andExpect(model().hasNoErrors());

        actions.andExpect(view().name("show"));
    }

    @Test
    public void testModel() throws Exception {
        MockMvc mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        ResultActions actions = mockMvc.perform(get("/model"));

        actions.andExpect(MockMvcResultMatchers.status().isOk());

        actions.andExpect(model().attribute("name", "Cat"));

        actions.andExpect(model().attributeDoesNotExist("age", "sex"));
        actions.andExpect(model().attribute("name", CoreMatchers.is("Cat")));
        actions.andExpect(model().size(1));
        actions.andExpect(model().hasNoErrors());

        actions.andExpect(view().name("show"));
    }

    @Test
    public void testModelMap() throws Exception {
        MockMvc mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        ResultActions actions = mockMvc.perform(get("/modelMap"));

        actions.andExpect(MockMvcResultMatchers.status().isOk());

        actions.andExpect(model().attribute("name", "Gou zi"));

        actions.andExpect(model().attributeDoesNotExist("age", "sex"));
        actions.andExpect(model().attribute("name", CoreMatchers.is("Gou zi")));
        actions.andExpect(model().size(1));
        actions.andExpect(model().hasNoErrors());

        actions.andExpect(view().name("show"));
    }

    @Test
    public void testMap() throws Exception {
        MockMvc mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        ResultActions actions = mockMvc.perform(get("/map"));

        actions.andExpect(MockMvcResultMatchers.status().isOk());

        actions.andExpect(model().attribute("name", "Er Gou zi"));

        actions.andExpect(model().attributeDoesNotExist("age", "sex"));
        actions.andExpect(model().attribute("name", CoreMatchers.is("Er Gou zi")));
        actions.andExpect(model().size(1));
        actions.andExpect(model().hasNoErrors());

        actions.andExpect(view().name("show"));
    }

    /**
     * @throws Exception
     * @see AnnotationController#noneMapping()
     */
    @Test
    @Ignore
    public void testNoneMapping() throws Exception {
        MockMvc mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        ResultActions actions = mockMvc.perform(get("noneMapping"));
        actions.andExpect(status().isOk());
        actions.andExpect(model().attribute("name", CoreMatchers.is("Jerry")));
    }

    @Test
    public void testNoneMappingParamName() throws Exception {
        MockMvc mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        ResultActions actions = mockMvc.perform(get("noneMappingParamName").param("name", "xiaoli"));
        actions.andExpect(status().isOk());
        actions.andExpect(model().attribute("inputName", CoreMatchers.is("xiaoli")));
        actions.andExpect(model().attribute("name", CoreMatchers.is("Jerry")));
    }
}