package com.moonsky.mvc02.simple.annotation;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

/**
 * @author benshaoye
 */
@Controller
public class AnnotationController {

    /*
     * 当 RequestMapping 不配置 value 时，默认就是方法名
     *
     * 但一个 Controller 中只能有一个为声明 mapping 的 RequestMapping
     */

    // @RequestMapping
    public ModelAndView noneMapping() {
        ModelAndView mv = new ModelAndView();
        mv.addObject("name", "Jerry");
        return mv;
    }

    @RequestMapping
    public ModelAndView noneMappingParamName(String name) {
        ModelAndView mv = new ModelAndView();
        mv.addObject("inputName", name);
        mv.addObject("name", "Jerry");
        return mv;
    }

    @RequestMapping("/mv")
    public ModelAndView mv() {
        ModelAndView mv = new ModelAndView();

        mv.addObject("name", "Jerry");

        mv.setViewName("show");

        return mv;
    }

    @RequestMapping("/model")
    public String model(Model model) {

        model.addAttribute("name", "Cat");

        return "show";
    }

    @RequestMapping("/modelMap")
    public String modelMap(ModelMap model) {

        model.addAttribute("name", "Gou zi");

        return "show";
    }

    @RequestMapping("/map")
    public String map(Map map) {

        map.put("name", "Er Gou zi");

        return "show";
    }
}
