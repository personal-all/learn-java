package com.moonsky.mvc02.simple.annotation;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

/**
 * @author benshaoye
 */
@Controller
public class FormController {

    @RequestMapping("submit")
    public String submit(Map map, String name, String price) {

        System.out.println(String.format("Name: %s, Price: %s", name, price));

        map.put("name", name);
        map.put("price", price);

        return "success";
    }
}
