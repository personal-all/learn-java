package com.moonsky.mvc02.simple;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author benshaoye
 */
public class DefaultController implements Controller {

    @Override
    public ModelAndView handleRequest(
        HttpServletRequest request, HttpServletResponse response
    ) {
        ModelAndView mv = new ModelAndView();
        mv.addObject("name", "Tom");
        mv.setViewName("show");
        return mv;
    }
}
