<%--
  Created by IntelliJ IDEA.
  User: ZhangDongMin
  Date: 2019/9/15
  Time: 11:53
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Title</title>
  <style>
    .links > a {
      margin: 10px 30px;
    }
  </style>
</head>
<body>
<div class="links">
  <p>
    你可以访问以下连接查看相应视图：
  </p>
  <a href="/default">/default</a>
  <a href="/mv">/mv</a>
  <a href="/model">/model</a>
  <a href="/modelMap">/modelMap</a>
  <a href="/map">/map</a>
  <a href="/index.jsp">/index.jsp</a>
  <a href="/add.jsp">/add.jsp</a>
</div>
</body>
</html>
