## 这是一个 spring-mvc 最简单的演示模块
只包含了一个最简单的表单数据提交，所以叫 simple 嘛！！！
### 一、配置 Controller 的方式有两种
1. 实现接口 org.springframework.web.servlet.mvc.Controller 方式：

这种方式配置的 Controller 还需要使用`org.springframework.web.servlet.handler.SimpleUrlHandlerMapping`声明映射关系；

例：[DefaultController](https://gitee.com/744531854/learn-java/blob/master/learn06-mvc01-simple/src/main/java/com/moonsky/mvc02/simple/DefaultController.java)

> 这种方式实现复杂，实际中很少使用（用起来也烦）

2. 使用注解：
在配置文件中配置 `<mvc:annotation-driven/>` 自动扫描，这样我们只需要在`Controller`中使用注解声明映射关系即可；

@org.springframework.stereotype.Controller

@org.springframework.web.bind.annotation.RequestMapping

例：[AnnotationController](https://gitee.com/744531854/learn-java/blob/master/learn06-mvc01-simple/src/main/java/com/moonsky/mvc02/simple/annotation/AnnotationController.java)

### 二、如何构造返回 ModelAndView？
1. 直接将处理方法返回值声明为 ModelAndView
2. 返回值为 String；
此时入参需要声明为 java.util.Map 或 org.springframework.ui.Model，这个对象就是 Model 对象，可在页面使用
> org.springframework.ui.ModelMap 是 java.util.Map 的一个实现类 

##### 参考：[spring-mvc.xml](https://gitee.com/744531854/learn-java/blob/master/learn06-mvc01-simple/src/main/resources/spring-mvc.xml)
