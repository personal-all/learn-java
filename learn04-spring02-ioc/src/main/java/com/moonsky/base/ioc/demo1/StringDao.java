package com.moonsky.base.ioc.demo1;

/**
 * @author benshaoye
 */
public class StringDao implements BaseDao<String> {

    @Override
    public String repeatAndBack(String s) {
        String msg = s + s;
        System.out.println(msg);
        return msg;
    }
}
