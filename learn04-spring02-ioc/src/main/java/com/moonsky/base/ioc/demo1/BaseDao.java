package com.moonsky.base.ioc.demo1;

/**
 * @author benshaoye
 */
public interface BaseDao<T> {

    String repeatAndBack(T t);
}
