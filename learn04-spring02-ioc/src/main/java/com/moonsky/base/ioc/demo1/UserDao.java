package com.moonsky.base.ioc.demo1;

/**
 * @author benshaoye
 */
public interface UserDao {
    void save();
    void find();
    void update();
    void delete();
}
