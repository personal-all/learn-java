package com.moonsky.base.ioc.demo1;

/**
 * @author benshaoye
 */
public interface StudentDao {
    void sayHello();
}
