package com.moonsky.base.ioc.demo1;

/**
 * @author benshaoye
 */
public class StudentDaoImpl implements StudentDao {

    private String studentName;

    @Override
    public void sayHello() {
        System.out.println("Hello, " + studentName);
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }
}
