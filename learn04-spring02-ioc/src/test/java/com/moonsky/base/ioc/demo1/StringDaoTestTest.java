package com.moonsky.base.ioc.demo1;

import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Method;

/**
 * @author benshaoye
 */
public class StringDaoTestTest {

    @Test
    public void testSave() throws Exception {
        String value = "1234567";
        StringDao dao = new StringDao();
        Assert.assertEquals(value + value, dao.repeatAndBack(value));

        Class type = StringDao.class;
        Method strMethod = type.getMethod("repeatAndBack", String.class);
        Method objMethod = type.getMethod("repeatAndBack", Object.class);

        value = "111111";
        Assert.assertTrue(objMethod.isBridge());
        Assert.assertEquals(value + value, objMethod.invoke(dao, value));
        Assert.assertFalse(strMethod.isBridge());
        Assert.assertEquals(value + value, strMethod.invoke(dao, value));
    }
}