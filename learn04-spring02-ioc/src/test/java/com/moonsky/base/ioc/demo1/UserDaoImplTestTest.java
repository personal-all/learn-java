package com.moonsky.base.ioc.demo1;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author benshaoye
 */
public class UserDaoImplTestTest {

    @Test
    public void testTraditional4User() {
        UserDao userDao = new UserDaoImpl();

        userDao.save();
        userDao.find();
        userDao.update();
        userDao.delete();
    }

    @Test
    public void testSpring4User() {
        String path = "ioc.demo1.applicationContext.xml";
        ApplicationContext context = new ClassPathXmlApplicationContext(path);
        UserDao userDao = (UserDao) context.getBean("userDao");

        userDao.save();
        userDao.find();
        userDao.update();
        userDao.delete();
    }

    @Test
    public void testTraditional4Student() {
        StudentDao studentDao = new StudentDaoImpl();

        ((StudentDaoImpl) studentDao).setStudentName("张三");

        studentDao.sayHello();
    }

    @Test
    public void testSpring4Student() {
        String path = "ioc.demo1.applicationContext.xml";
        ApplicationContext context = new ClassPathXmlApplicationContext(path);
        StudentDao studentDao = (StudentDao) context.getBean("studentDao");

        studentDao.sayHello();
    }
}